package com.esys.impressivedemo;

import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.digitalindiapayment.BuildConfig;
import com.digitalindiapayment.R;
import com.digitalindiapayment.application.MyApplication;
import com.digitalindiapayment.deviceConnect.DeviceContract;
import com.digitalindiapayment.deviceConnect.DevicePresenter;
import com.digitalindiapayment.morphoScanConnect.RDFingureScanActivity;
import com.digitalindiapayment.morphoScanConnect.scannerdevice.BluetoothScannerDevice;
import com.digitalindiapayment.scannerConnect.evolute.EvoluteScanner;
import com.digitalindiapayment.scannerConnect.hemen.HemenScanner;
import com.digitalindiapayment.util.Util;
import com.esys.impressivedemo.bluetooth.BluetoothComm;
import com.esys.impressivedemo.bluetooth.BluetoothPair;
import com.impress.api.Printer;
import com.morpho.sample.FingerprintData;
import com.morpho.sample.KeyFile;

import org.greenrobot.eventbus.EventBus;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;


public class BluetoothDevicelist extends Activity implements OnClickListener, DeviceContract.View {

    /**
     * Bluetooth communication connection object
     */
    public BluetoothComm mBTcomm = null;


    boolean activate;
    /**
     * CONST: device type bluetooth 2.1
     */
    public static final int DEVICE_TYPE_BREDR = 0x01;
    /**
     * CONST: device type bluetooth 4.0 ble
     */
    public static final int DEVICE_TYPE_BLE = 0x02;
    /**
     * CONST: device type bluetooth double mode
     */
    public static final int DEVICE_TYPE_DUMO = 0x03;
    public final static String EXTRA_DEVICE_TYPE = "android.bluetooth.device.extra.DEVICE_TYPE";
    public static BluetoothAdapter mBT = BluetoothAdapter.getDefaultAdapter();
    /**
     * SimpleAdapter object (list display container objects)
     */
    Context context = this;
    Button btn_Scan, btn_Exit;
    /**
     * Discovery is Finished
     */
    private boolean _discoveryFinished;
    /**
     * bluetooth List View
     */
    private ListView mlvList = null;
    /**
     * Storage the found bluetooth devices
     * format:<MAC, <Key,Val>>;Key=[RSSI/NAME/COD(class od device)/BOND/UUID]
     */
    private Hashtable<String, Hashtable<String, String>> mhtFDS = null;
    /**
     * (List of storage arrays for display) dynamic array of objects / ** ListView's
     */
    private ArrayList<HashMap<String, Object>> malListItem = null;
    private SimpleAdapter msaListItemAdapter = null;
    private boolean mbBonded = false;

    private MyApplication mGP = null;


    private DeviceContract.UserAction devicePresenter;

    private ProgressDialog progressDialog;

    private String terminalIdOfDevice;

    private IntentFilter discoveryFilter;
    private IntentFilter foundFilter;

    private Boolean isfoundReceiverRegister = false;
    private Boolean isfinshedReceiverRegister = false;
    private Map<String, Class> scannerSelection;
    private final String TAG = "BlutoothDeviceList";

    BluetoothScannerDevice bluetoothScannerDevice;
    /**
     * Scan for Bluetooth devices. (broadcast listener)
     */
    private BroadcastReceiver _foundReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            /* bluetooth device profiles*/
            Hashtable<String, String> htDeviceInfo = new Hashtable<String, String>();





			/* get the search results */
            BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
            /* create found device profiles to htDeviceInfo*/
            Bundle b = intent.getExtras();
            htDeviceInfo.put("RSSI", String.valueOf(b.get(BluetoothDevice.EXTRA_RSSI)));
            if (null == device.getName())
                htDeviceInfo.put("NAME", "Null");
            else
                htDeviceInfo.put("NAME", device.getName());

            htDeviceInfo.put("COD", String.valueOf(b.get(BluetoothDevice.EXTRA_CLASS)));
            if (device.getBondState() == BluetoothDevice.BOND_BONDED)
                htDeviceInfo.put("BOND", getString(R.string.actDiscovery_bond_bonded));
            else
                htDeviceInfo.put("BOND", getString(R.string.actDiscovery_bond_nothing));

            String sDeviceType = String.valueOf(b.get(EXTRA_DEVICE_TYPE));
            if (!sDeviceType.equals("null"))
                htDeviceInfo.put("DEVICE_TYPE", sDeviceType);
            else
                htDeviceInfo.put("DEVICE_TYPE", "-1");

			/*adding scan to the device profiles*/
            mhtFDS.put(device.getAddress(), htDeviceInfo);

			/*Refresh show list*/
            showDevices();
        }
    };
    /**
     * Bluetooth scanning is finished processing.(broadcast listener)
     */
    private BroadcastReceiver _finshedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(getString(R.string.app_name), ">>Bluetooth scanning is finished");
            _discoveryFinished = true; //set scan is finished
            //unregisterReceiver(_foundReceiver);
            //unregisterReceiver(_finshedReceiver);
            if (isfoundReceiverRegister) {
                unregisterReceiver(_foundReceiver);
                isfoundReceiverRegister = false;
            }
            if (isfinshedReceiverRegister) {
                unregisterReceiver(_finshedReceiver);
                isfinshedReceiverRegister = false;
            }


            if (null != mhtFDS && mhtFDS.size() > 0) {
                Toast.makeText(BluetoothDevicelist.this,
                        getString(R.string.actDiscovery_msg_select_device),
                        Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(BluetoothDevicelist.this,
                        getString(R.string.actDiscovery_msg_not_find_device),
                        Toast.LENGTH_LONG).show();
            }
        }
    };
    //static Setup impressSetUp = null;//SplashScreen.impressSetUp;
    private BroadcastReceiver _mPairingRequest = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            BluetoothDevice device = null;
            if (intent.getAction().equals(BluetoothDevice.ACTION_BOND_STATE_CHANGED)) {
                device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                if (device.getBondState() == BluetoothDevice.BOND_BONDED)
                    mbBonded = true;
                else
                    mbBonded = false;
            }
        }
    };

    ImpressiveclassEnum myEnum;

    /**
     * start run
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_btdiscovery);

        btn_Scan = (Button) findViewById(R.id.btn_Scan);
        btn_Scan.setOnClickListener(this);

        mGP = (MyApplication) getApplicationContext();


        myEnum = (ImpressiveclassEnum) getIntent().getSerializableExtra("ENUM");

        this.mlvList = (ListView) this.findViewById(R.id.actDiscovery_lv);
        this.mlvList.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                devicePresenter = new DevicePresenter(BluetoothDevicelist.this, getApplicationContext());
                String sMAC = ((TextView) arg1.findViewById(R.id.device_item_ble_mac)).getText().toString();
                bluetoothScannerDevice = new BluetoothScannerDevice();
                bluetoothScannerDevice.setName(mhtFDS.get(sMAC).get("NAME"));
                bluetoothScannerDevice.setMac(sMAC);
                MyApplication.connectedDevice = bluetoothScannerDevice;
                Intent result = new Intent();
                result.putExtra("MAC", sMAC);
                result.putExtra("RSSI", mhtFDS.get(sMAC).get("RSSI"));
                result.putExtra("NAME", mhtFDS.get(sMAC).get("NAME"));
                result.putExtra("COD", mhtFDS.get(sMAC).get("COD"));
                result.putExtra("BOND", mhtFDS.get(sMAC).get("BOND"));
                result.putExtra("DEVICE_TYPE", toDeviceTypeString(mhtFDS.get(sMAC).get("DEVICE_TYPE")));
                //setResult(Activity.RESULT_OK, result);
                //finish();

                //when using evolute with rd in scan mode device should be connected to the evolute rd service not dipl app

                if (Util.PREFERENCES.getDeviceName(getApplicationContext()).equals(getString(R.string.device_evolute_name))) {
                    Util.PREFERENCES.setEvoluteBluetoothName(context, mhtFDS.get(sMAC).get("NAME"));
                    Util.PREFERENCES.setEvoluteBluetoothMac(context, sMAC);
                    if (myEnum == ImpressiveclassEnum.PRINT) {
                        Intent intent = new Intent();
                        intent.putExtra("MAC",sMAC);
                        intent.putExtra("BluetoothName",mhtFDS.get(sMAC).get("NAME"));
                        setResult(Activity.RESULT_OK,intent);
                        finish();
                    } else {
                        try {
                            Intent act = new Intent("in.gov.uidai.rdservice.fp.INFO");
                            PackageManager packageManager = getPackageManager();
                            List activities = packageManager.queryIntentActivities(act, PackageManager.MATCH_DEFAULT_ONLY);
                            final boolean isIntentSafe = activities.size() > 0;
                            Log.d(TAG, "Boolean check for activities = " + isIntentSafe);
                            if (!isIntentSafe) {
                                Toast.makeText(getApplicationContext(), "No RD Service Available", Toast.LENGTH_SHORT).show();
                            }
                            Log.d(TAG, "No of activities = " + activities.size());
                            startActivityForResult(act, 103);
                        } catch (Exception e) {
                            Log.e(TAG, "Error while connecting to RDService");
                            e.printStackTrace();
                        }
                    }


                } else if (mhtFDS.get(sMAC).get("BOND").equals(getString(R.string.actDiscovery_bond_nothing))) {
                    new PairTask().execute(sMAC);
                } else {
                    BluetoothDevice mBDevice = mBT.getRemoteDevice(sMAC);
                    new ConnSocketTask().execute(mBDevice.getAddress());
                }


            }
        });

        //new scanDeviceTask().execute("");
        btn_Scan.performClick();

        devicePresenter = new DevicePresenter(this, getApplicationContext());

        scannerSelection = new HashMap<>();
        scannerSelection.put(getResources().getString(R.string.device_hemen_name), HemenScanner.class);
        if (Util.PREFERENCES.isRDEnabled(context)) {
            scannerSelection.put(getString(R.string.device_evolute_name), RDFingureScanActivity.class);
        } else {
            scannerSelection.put(getResources().getString(R.string.device_evolute_name), EvoluteScanner.class);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!isfoundReceiverRegister) {
            registerReceiver(_finshedReceiver, discoveryFilter);
            isfoundReceiverRegister = true;
        }
        if (!isfinshedReceiverRegister) {
            registerReceiver(_foundReceiver, foundFilter);
            isfinshedReceiverRegister = true;
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (isfoundReceiverRegister) {
            unregisterReceiver(_foundReceiver);
            isfoundReceiverRegister = false;
        }
        if (isfinshedReceiverRegister) {
            unregisterReceiver(_finshedReceiver);
            isfinshedReceiverRegister = false;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();


        try {
            if (mBT.isDiscovering())
                mBT.cancelDiscovery();

            try {
                // BluetoothComm.mosOut = null;
                // BluetoothComm.misIn = null;
            } catch (NullPointerException e) {
            }
            System.gc();

            //mGP.closeConn();

        } catch (Exception E) {

        }
    }

    @Override
    public void onBackPressed() {

        System.gc();
        finish();
    }

    private void startSearch() {
        _discoveryFinished = false;


        if (null == mhtFDS)
            this.mhtFDS = new Hashtable<String, Hashtable<String, String>>();
        else
            this.mhtFDS.clear();

		/* Register Receiver*/
        discoveryFilter = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);

        foundFilter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        //registerReceiver(_finshedReceiver, discoveryFilter);
        //registerReceiver(_foundReceiver, foundFilter);

        if (!isfoundReceiverRegister) {
            registerReceiver(_finshedReceiver, discoveryFilter);
            isfoundReceiverRegister = true;
        }
        if (!isfinshedReceiverRegister) {
            registerReceiver(_foundReceiver, foundFilter);
            isfinshedReceiverRegister = true;
        }


        mBT.startDiscovery();//start scan

        this.showDevices(); //the first scan clear show list
    }

    private String toDeviceTypeString(String sDeviceTypeId) {
        Pattern pt = Pattern.compile("^[-\\+]?[\\d]+$");
        if (pt.matcher(sDeviceTypeId).matches()) {
            switch (Integer.valueOf(sDeviceTypeId)) {
                case DEVICE_TYPE_BREDR:
                    return getString(R.string.device_type_bredr);
                case DEVICE_TYPE_BLE:
                    return getString(R.string.device_type_ble);
                case DEVICE_TYPE_DUMO:
                    return getString(R.string.device_type_dumo);
                default:
                    return getString(R.string.device_type_bredr);
            }
        } else
            return sDeviceTypeId;
    }

    /* Show devices list */
    protected void showDevices() {
        if (null == this.malListItem)
            this.malListItem = new ArrayList<HashMap<String, Object>>();

        if (null == this.msaListItemAdapter) {
            //Generate adapter Item and dynamic arrays corresponding element
            this.msaListItemAdapter = new SimpleAdapter(this, malListItem,//Data Sources
                    R.layout.list_view_item_devices,//ListItem's XML implementation
                    //Child corresponding dynamic arrays and ImageItem
                    new String[]{"NAME", "MAC", "COD", "RSSI", "DEVICE_TYPE", "BOND"},
                    //A ImageView ImageItem XML file inside, two TextView ID
                    new int[]{R.id.device_item_ble_name,
                            R.id.device_item_ble_mac,
                            R.id.device_item_ble_cod,
                            R.id.device_item_ble_rssi,
                            R.id.device_item_ble_device_type,
                            R.id.device_item_ble_bond
                    }
            );

            this.mlvList.setAdapter(this.msaListItemAdapter);
        }


        this.malListItem.clear();//Clear history entries
        Enumeration<String> e = this.mhtFDS.keys();

        while (e.hasMoreElements()) {
            HashMap<String, Object> map = new HashMap<String, Object>();
            String sKey = e.nextElement();
            map.put("MAC", sKey);
            map.put("NAME", this.mhtFDS.get(sKey).get("NAME"));
            map.put("RSSI", this.mhtFDS.get(sKey).get("RSSI"));
            map.put("COD", this.mhtFDS.get(sKey).get("COD"));
            map.put("BOND", this.mhtFDS.get(sKey).get("BOND"));
            map.put("DEVICE_TYPE", toDeviceTypeString(this.mhtFDS.get(sKey).get("DEVICE_TYPE")));
            this.malListItem.add(map);
        }
        this.msaListItemAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_Scan:
                new scanDeviceTask().execute("");
                //return true;
                break;

            default:
                break;
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101) {
            if (resultCode == RESULT_OK) {
                finish();
            }
        } else if (requestCode == 102) {
            finish();
            if (resultCode == RESULT_OK) {


                FingerprintData fingerprintData = new FingerprintData();
                fingerprintData.fp = data.getStringExtra("DATA");
                fingerprintData.fpv = data.getStringExtra("DATA");
                EventBus.getDefault().post(fingerprintData);


            }
        } else if (requestCode == 103) {
            if (resultCode == Activity.RESULT_OK) {
                //get device infomation from rd swrvice here
                String DeviceInfoData = data.getStringExtra("DEVICE_INFO");
                Log.e(TAG, "Device Info data:" + DeviceInfoData);
                String rdServiceInfo = data.getStringExtra("RD_SERVICE_INFO");
                Log.e(TAG, "RD service Info data:" + DeviceInfoData);
                //pass device infomation string to getSerialNumber() to retrive device terminal id
                getSerialNumber(DeviceInfoData);
            }
        }
    }

    @Override
    public void showDeviceValidity(Boolean isDeviceValid, String message) {

        if (BuildConfig.DEBUG) {
            isDeviceValid = true;
        }
        if (message != null) {
            Util.showLongToast(getApplicationContext(), message);
        }
        if (progressDialog != null) {
            progressDialog.dismiss();
        }

        if (isDeviceValid) {

            //Toast.makeText(getApplicationContext(), getString(R.string.actMain_msg_device_connect_succes), Toast.LENGTH_SHORT).show();

            Intent all_intent = null;
            if (myEnum == ImpressiveclassEnum.PRINT) {
                //all_intent = new Intent(getApplicationContext(), PrintScreen.class);
                //all_intent = new Intent(getApplicationContext(), Act_Printer.class);
                //all_intent.putExtra("DATA", getIntent().getParcelableExtra("DATA"));
                //startActivityForResult(all_intent, 101);
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_OK, returnIntent);
                finish();

            } else if (myEnum == ImpressiveclassEnum.SCAN) {
                String scannerName = Util.PREFERENCES.getDeviceName(getApplicationContext());
                all_intent = new Intent(getApplicationContext(), scannerSelection.get(scannerName));
                //all_intent = new Intent(getApplicationContext(), Act_FPS.class);
                all_intent.putExtra(Util.KEYS.DEVICE, bluetoothScannerDevice);
                startActivityForResult(all_intent, 102);
            }
        } else {
            ((MyApplication) getApplicationContext()).closeConn();

            //finish();


        }
    }

    @Override
    public void showLoader() {
        progressDialog = new ProgressDialog(BluetoothDevicelist.this);
        progressDialog.setMessage(getString(R.string.actMain_msg_device_connecting));
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    private class scanDeviceTask extends AsyncTask<String, String, Integer> {
        /**
         * Constants: Bluetooth is not turned on
         */
        private static final int RET_BLUETOOTH_NOT_START = 0x0001;
        /**
         * Constant: the device search complete
         */
        private static final int RET_SCAN_DEVICE_FINISHED = 0x0002;
        /**
         * Wait a Bluetooth device starts the maximum time (in S)
         */
        private static final int miWATI_TIME = 10;
        /**
         * Every thread sleep time (in ms)
         */
        private static final int miSLEEP_TIME = 150;
        /**
         * Process waits prompt box
         */
        private ProgressDialog mpd = null;


        @Override
        public void onPreExecute() {

            this.mpd = new ProgressDialog(BluetoothDevicelist.this);
            this.mpd.setMessage(getString(R.string.actDiscovery_msg_scaning_device));
            this.mpd.setCancelable(true);
            this.mpd.setCanceledOnTouchOutside(true);
            this.mpd.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    _discoveryFinished = true;
                }
            });
            this.mpd.show();

            startSearch();
        }

        @Override
        protected Integer doInBackground(String... params) {
            if (!mBT.isEnabled())
                return RET_BLUETOOTH_NOT_START;

            int iWait = miWATI_TIME * 1000;
            //Wait miSLEEP_TIME seconds, start the Bluetooth device before you start scanning
            while (iWait > 0) {
                if (_discoveryFinished)
                    return RET_SCAN_DEVICE_FINISHED;
                else
                    iWait -= miSLEEP_TIME;
                SystemClock.sleep(miSLEEP_TIME);
                ;
            }
            return RET_SCAN_DEVICE_FINISHED;
        }

        @Override
        public void onProgressUpdate(String... progress) {
        }

        @Override
        public void onPostExecute(Integer result) {
            if (this.mpd.isShowing())
                this.mpd.dismiss();

            if (mBT.isDiscovering())
                mBT.cancelDiscovery();

            if (RET_SCAN_DEVICE_FINISHED == result) {

            } else if (RET_BLUETOOTH_NOT_START == result) {
                Toast.makeText(BluetoothDevicelist.this, getString(R.string.actDiscovery_msg_bluetooth_not_start),
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class PairTask extends AsyncTask<String, String, Integer> {
        /**
         * Constants: the pairing is successful
         */
        static private final int RET_BOND_OK = 0x00;
        /**
         * Constants: Pairing failed
         */
        static private final int RET_BOND_FAIL = 0x01;
        /**
         * Constants: Pairing waiting time (15 seconds)
         */
        static private final int iTIMEOUT = 1000 * 15;
        public BluetoothDevice mBDevice = null;
        /**
         * Process waits prompt box
         */
        private ProgressDialog mpd = null;

        /**
         * Thread start initialization
         */
        @Override
        public void onPreExecute() {

            //Toast.makeText(getApplicationContext(),getString(R.string.actMain_msg_bluetooth_Bonding),Toast.LENGTH_SHORT).show();
            registerReceiver(_mPairingRequest, new IntentFilter(BluetoothPair.PAIRING_REQUEST));
            registerReceiver(_mPairingRequest, new IntentFilter(BluetoothDevice.ACTION_BOND_STATE_CHANGED));

            mpd = new ProgressDialog(BluetoothDevicelist.this);
            mpd.setMessage(getString(R.string.actMain_msg_bluetooth_Bonding));
            mpd.setCancelable(false);
            mpd.setCanceledOnTouchOutside(false);
            mpd.show();
        }

        @Override
        protected Integer doInBackground(String... arg0) {
            final int iStepTime = 150;
            int iWait = iTIMEOUT;
            try {
                mBDevice = mBT.getRemoteDevice(arg0[0]);//arg0[0] is MAC address
                BluetoothPair.createBond(mBDevice);
                mbBonded = false;
            } catch (Exception e1) {
                Log.d(getString(R.string.app_name), "create Bond failed!");
                e1.printStackTrace();
                return RET_BOND_FAIL;
            }
            while (!mbBonded && iWait > 0) {
                SystemClock.sleep(iStepTime);
                iWait -= iStepTime;
            }
            if (iWait > 0) {
                //RET_BOND_OK
                Log.e("Application", "create Bond failed! RET_BOND_OK ");
            } else {
                //RET_BOND_FAIL
                Log.e("Application", "create Bond failed! RET_BOND_FAIL ");
            }
            return (int) ((iWait > 0) ? RET_BOND_OK : RET_BOND_FAIL);
        }


        @Override
        public void onPostExecute(Integer result) {
            unregisterReceiver(_mPairingRequest);
            mpd.dismiss();
            if (RET_BOND_OK == result) {
                Toast.makeText(getApplicationContext(), getString(R.string.actMain_msg_bluetooth_Bond_Success), Toast.LENGTH_SHORT).show();

                new ConnSocketTask().execute(mBDevice.getAddress());

            } else {
                Toast.makeText(getApplicationContext(), getString(R.string.actMain_msg_bluetooth_Bond_fail), Toast.LENGTH_LONG).show();
                try {
                    BluetoothPair.removeBond(mBDevice);
                } catch (Exception e) {
                    Log.d(getString(R.string.app_name), "removeBond failed!");
                    e.printStackTrace();
                }

                //new ConnSocketTask().execute(mBDevice.getAddress());
            }
        }
    }

    private class ConnSocketTask extends AsyncTask<String, String, Integer> {
        /**
         * Constants: connection fails
         */
        private static final int CONN_FAIL = 0x01;
        /**
         * Constant: the connection is established
         */
        private static final int CONN_SUCCESS = 0x02;
        /**
         * Process waits prompt box
         */
        private ProgressDialog mpd = null;

        /**
         * Thread start initialization
         */
        @Override
        public void onPreExecute() {
            mpd = new ProgressDialog(BluetoothDevicelist.this);
            mpd.setMessage(getString(R.string.actMain_msg_device_connecting));
            mpd.setCancelable(false);
            mpd.setCanceledOnTouchOutside(false);
            mpd.show();
        }

        @Override
        protected Integer doInBackground(String... arg0) {
            if (mGP.createConn(arg0[0])) {

                Printer printer = null;
                if (Util.PREFERENCES.getDeviceName(getApplicationContext()).equals(getString(R.string.device_evolute_name))) {
                    terminalIdOfDevice = KeyFile.getDevise(context);

                } else {
                    terminalIdOfDevice = "DIP061700000001";
                }


                if (Util.PREFERENCES.getDeviceName(getApplicationContext()).equals(getString(R.string.device_evolute_name))) {
                    try {
                        printer = new Printer(ImpressSetup.onCreate(getApplicationContext()), BluetoothComm.mosOut, BluetoothComm.misIn);
                        terminalIdOfDevice = printer.sGetSerialNumber();
                        if (terminalIdOfDevice != null) {
                            if (terminalIdOfDevice.contains("FIMP") && terminalIdOfDevice.indexOf("FIMP") == 0) {
                                terminalIdOfDevice = terminalIdOfDevice.replace("FIMP", "F");
                            }
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


                return CONN_SUCCESS;
            } else {
                return CONN_FAIL;
            }


        }

        /**
         * After blocking cleanup task execution
         */
        @Override
        public void onPostExecute(Integer result) {

            mpd.dismiss();
            if (CONN_SUCCESS == result) {
                if (terminalIdOfDevice == null) {
                    terminalIdOfDevice = "";
                }
                // For Fingure print scan it only check terminal
                if (myEnum == ImpressiveclassEnum.SCAN) {
                    KeyFile.setDevise(getApplicationContext(), terminalIdOfDevice);
                    devicePresenter.checkDeviceTerminalId(terminalIdOfDevice);
                } else {
                    // For printing it will bypass the checkTerminalid Api
                    showDeviceValidity(true, null);
                }

            } else {
                Toast.makeText(getApplicationContext(), getString(R.string.actMain_msg_device_connect_fail), Toast.LENGTH_SHORT).show();
            }

        }
    }

    public void getSerialNumber(String deviceInfo) {
        // get device terminal id from device info using xml parsing and validate terminalid from server using service
        String name;
        String serialNumber = null;
        try {
            InputStream is = new ByteArrayInputStream(deviceInfo.getBytes());
            DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
            domFactory.setIgnoringComments(true);
            DocumentBuilder builder = domFactory.newDocumentBuilder();
            Document doc = builder.parse(is);
            NodeList additionalInfo = doc.getElementsByTagName("Info");
            if (additionalInfo.getLength() > 0) {
                //info tag contains atleast one info tag then retrive name and value from DeviceSerial from info tag
                Element info = (Element) additionalInfo.item(0);
                name = info.getAttribute("name");
                if (name != null && name.equalsIgnoreCase("DeviceSerial")) {
                    serialNumber = info.getAttribute("value");
                    Log.e(TAG, serialNumber);
                    if (serialNumber != null && !serialNumber.isEmpty()) {
                        if (serialNumber.contains("FIMP") && serialNumber.indexOf("FIMP") == 0) {
                            terminalIdOfDevice = serialNumber.replace("FIMP", "F");
                            KeyFile.setDevise(getApplicationContext(), terminalIdOfDevice);
                            if (devicePresenter != null) {
                                Util.isBluetoothConnected = true;
                                devicePresenter.checkDeviceTerminalId(terminalIdOfDevice);
                                Log.e("DeviceTerminalID","Evolute RD serial no = "+terminalIdOfDevice);

                            }
                        }
                    } else {
                        Toast.makeText(context, "Invalid Terminal Id", Toast.LENGTH_SHORT).show();
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();

        }

    }


}

