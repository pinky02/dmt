package com.esys.impressivedemo;

import android.content.Context;

import com.digitalindiapayment.R;
import com.impress.api.Setup;

/**
 * Created by rajeshpandya on 25/04/17.
 */

public class ImpressSetup {


    private static Setup impressSetUp = null;
    private static boolean activate;

    private ImpressSetup() {

    }




    public static Setup onCreate(Context context) {
        if (impressSetUp == null) {
            try {
                impressSetUp = new Setup();
                activate = impressSetUp.blActivateLibrary(context, R.raw.licence_full);
            } catch (Exception e) {
                impressSetUp = null;
                activate = false;
            }
        }
        return impressSetUp;
    }

    public static boolean isActivate() {
        return activate;
    }


    public static void nullify(){
        impressSetUp = null;
    }

}
