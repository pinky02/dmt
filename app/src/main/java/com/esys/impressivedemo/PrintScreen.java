package com.esys.impressivedemo;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.digitalindiapayment.R;
import com.digitalindiapayment.application.MyApplication;
import com.digitalindiapayment.pojo.TransactionDetailsBean;
import com.esys.impressivedemo.bluetooth.BluetoothComm;
import com.impress.api.Printer;

/**
 * Created by rajeshpandya on 26/04/17.
 */

public class PrintScreen extends Activity {

    public static final int DEVICE_NOTCONNECTED = -100;
    public Printer ptr;
    int Activity_RESULT = Activity.RESULT_CANCELED;
    private ProgressDialog dlgPg;

    private byte bFontstyleSmall = 0x03;
    private byte bFontstyleBig = 0x02;
    private byte bFontstyleSmallLine = 0x07;
    private Button btn_print;
    private TransactionDetailsBean bean;
    private TextView headerScanPrintText ;
    private ImageView imgFingerprint;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scan_print_receipt_activity);

        btn_print = (Button) findViewById(R.id.btn_print);
        dlgPg = new ProgressDialog(PrintScreen.this);
        dlgPg.setMessage("Please Wait...");
        dlgPg.setCancelable(false);

        headerScanPrintText =(TextView)findViewById(R.id.headerScanPrintText);
        imgFingerprint = (ImageView)findViewById(R.id.imgFingerprint);
        headerScanPrintText.setText(getString(R.string.print_receipt));
        //
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        imgFingerprint.setBackground(getDrawable(R.drawable.ic_printer));}else {
            imgFingerprint.setBackground(getResources().getDrawable(R.drawable.ic_printer));
        }
        imgFingerprint.setVisibility(View.GONE);

        bean = getIntent().getParcelableExtra("DATA");




        try {
            ptr = new Printer(ImpressSetup.onCreate(getApplicationContext()), BluetoothComm.mosOut, BluetoothComm.misIn);
        } catch (Exception e) {
            e.printStackTrace();
        }

        btn_print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new EnterTextTask().execute(0);

            }
        });


    }

    public void showResult(int iRetVal) {
        if (iRetVal == DEVICE_NOTCONNECTED) {
            Toast.makeText(getApplicationContext(), "Device not connected", Toast.LENGTH_LONG).show();
            ((MyApplication) getApplicationContext()).closeConn();

        } else if (iRetVal == Printer.PR_SUCCESS) {
            Toast.makeText(getApplicationContext(), "Print Success", Toast.LENGTH_LONG).show();
            Activity_RESULT = RESULT_OK;

        } else if (iRetVal == Printer.PR_PLATEN_OPEN) {
            Toast.makeText(getApplicationContext(), "Printer platen is open", Toast.LENGTH_LONG).show();
        } else if (iRetVal == Printer.PR_PAPER_OUT) {
            Toast.makeText(getApplicationContext(), "Printer paper is out", Toast.LENGTH_LONG).show();
        } else if (iRetVal == Printer.PR_IMPROPER_VOLTAGE) {
            Toast.makeText(getApplicationContext(), "Printer improper voltage", Toast.LENGTH_LONG).show();
        } else if (iRetVal == Printer.PR_FAIL) {
            Toast.makeText(getApplicationContext(), "Printer failed", Toast.LENGTH_LONG).show();
        } else if (iRetVal == Printer.PR_PARAM_ERROR) {
            Toast.makeText(getApplicationContext(), "Printer param error", Toast.LENGTH_LONG).show();
        } else if (iRetVal == Printer.PR_NO_RESPONSE) {
            Toast.makeText(getApplicationContext(), "No response from impress device", Toast.LENGTH_LONG).show();
        } else if (iRetVal == Printer.PR_DEMO_VERSION) {
            Toast.makeText(getApplicationContext(), "Library is in demo version", Toast.LENGTH_LONG).show();
        } else if (iRetVal == Printer.PR_INVALID_DEVICE_ID) {
            Toast.makeText(getApplicationContext(), "Connected  device is not license authenticated", Toast.LENGTH_LONG).show();
        } else if (iRetVal == Printer.PR_ILLEGAL_LIBRARY) {
            Toast.makeText(getApplicationContext(), "Library not valid", Toast.LENGTH_LONG).show();

        }
        onBackPressed();

    }

    @Override
    public void onBackPressed() {

        Intent returnIntent = new Intent();
        setResult(Activity_RESULT, returnIntent);
        finish();
    }

    /*   This method shows the EnterTextAsyc  AsynTask operation */
    public class EnterTextTask extends AsyncTask<Integer, Integer, Integer> {
        /* displays the progress dialog until background task is completed*/

        private int iRetVal;

        @Override
        protected void onPreExecute() {
            dlgPg.show();
            super.onPreExecute();
        }

        /* Task of EnterTextAsyc performing in the background*/
        @Override
        protected Integer doInBackground(Integer... params) {
            try {



                String line0 =  "        Digital India Payments ltd        ";
                String line1 =  "  Transaction Receipt  ";
                String line2 =  "Customer Copy - " + bean.serviceMod;
                String line3 =  "Date & Time    : " +  bean.timestamp;
                String line4 =  "Terminal ID    : " + bean.terminalId;
                String line5 =  "Agent ID       : " + bean.agentId;
                String line6 =  "BC Name        : " + bean.BCName;
                String line7 =  "mATN req ID    : " + bean.mATNreqID;
                String line8 =  "Txn ID         : " + bean.transactionId;
                String line9 =  "UIDAI Auth Code: " + bean.aadhaarNo;
                String line10 = "Txn Status     : " + bean.remark;
                String line11 = "Txn Amt        : " + bean.amount;
                String line12 = "A/c Bal        : " + bean.balance;
                String line13 = "   Note: Pls do not pay any charges/fee   " +
                                "               for this txn               ";
                String newLine = "\n";

                ptr.iFlushBuf();


                ptr.iPrinterAddData(bFontstyleBig, newLine);

                ptr.iPrinterAddData(bFontstyleSmallLine, line0);
                iRetVal = ptr.iStartPrinting(1);
                ptr.iFlushBuf();

                iRetVal =   ptr.iBmpPrint(PrintScreen.this,R.raw.logo);

                ptr.iFlushBuf();


                ptr.iPrinterAddData(bFontstyleBig, line1);

                //ptr.iPrinterAddData(bFontstyleSmall, newLine);
                ptr.iPrinterAddData(bFontstyleSmall, line2);
                //ptr.iPrinterAddData(bFontstyleSmall, newLine);
                ptr.iPrinterAddData(bFontstyleSmall, line3);
                //ptr.iPrinterAddData(bFontstyleSmall, newLine);
                ptr.iPrinterAddData(bFontstyleSmall, line4);
                //ptr.iPrinterAddData(bFontstyleSmall, newLine);
                ptr.iPrinterAddData(bFontstyleSmall, line5);
                //ptr.iPrinterAddData(bFontstyleSmall, newLine);
                ptr.iPrinterAddData(bFontstyleSmall, line6);
                //ptr.iPrinterAddData(bFontstyleSmall, newLine);
                ptr.iPrinterAddData(bFontstyleSmall, line7);
                //ptr.iPrinterAddData(bFontstyleSmall, newLine);
                ptr.iPrinterAddData(bFontstyleSmall, line8);
                //ptr.iPrinterAddData(bFontstyleSmall, newLine);
                ptr.iPrinterAddData(bFontstyleSmall, line9);
                //ptr.iPrinterAddData(bFontstyleSmall, newLine);
                ptr.iPrinterAddData(bFontstyleSmall, line10);
                //ptr.iPrinterAddData(bFontstyleSmall, newLine);
                ptr.iPrinterAddData(bFontstyleSmall, line11);
                //ptr.iPrinterAddData(bFontstyleSmall, newLine);
                ptr.iPrinterAddData(bFontstyleSmall, line12);
                ptr.iPrinterAddData(bFontstyleSmall, line13);
                ptr.iPrinterAddData(bFontstyleSmall, newLine +  newLine + newLine );
                ptr.iPrinterAddData(bFontstyleSmall, newLine +newLine);


                iRetVal = ptr.iStartPrinting(1);






            } catch (Exception e) {
                iRetVal = DEVICE_NOTCONNECTED;
                return iRetVal;
            }
            return iRetVal;
        }

        /* This displays the status messages of EnterTextAsyc in the dialog box */
        @Override
        protected void onPostExecute(Integer result) {
            dlgPg.dismiss();
            showResult(iRetVal);
            super.onPostExecute(result);
        }
    }

}
