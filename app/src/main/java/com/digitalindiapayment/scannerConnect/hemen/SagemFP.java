package com.digitalindiapayment.scannerConnect.hemen;

public class SagemFP {
	static int inc = 0;	
public static byte[] captureISO() {
		byte Enroll_Buff[] = new byte[80];

		int Count = 0;
		Enroll_Buff[Count++] = 0x02; // STX
		Enroll_Buff[Count++] = 0x61; // DATA PACKET
		Enroll_Buff[Count++] = (byte) (0x00 + inc); // Counter

		Enroll_Buff[Count++] = 0x21; // Enroll
		Enroll_Buff[Count++] = 0x0C; // Length of the packet LSB
		Enroll_Buff[Count++] = 0x00; // Length of the packet MSB

		Enroll_Buff[Count++] = 0x00; // DB Identifier
		Enroll_Buff[Count++] = 0x00; // TIme out LSB
		Enroll_Buff[Count++] = 0x00; // Time out MSB

		Enroll_Buff[Count++] = 0x00; // Acquition Quality Threshold
		Enroll_Buff[Count++] = 0x01; // Enrollment Type (Default: 00-PKC,01-ISO

		Enroll_Buff[Count++] = 0x01; // Number of Fingers
		Enroll_Buff[Count++] = 0x00; // Save Record
		Enroll_Buff[Count++] = (byte)0xff; // Export Minitue Size

		// For ISO
		Enroll_Buff[Count++] = 0x38; // ISO P
		Enroll_Buff[Count++] = 0x01; //
		Enroll_Buff[Count++] = 0x00; //
		Enroll_Buff[Count++] = 0x6E; // ISO related

		Enroll_Buff[Count++] = 0x00;
		Enroll_Buff[Count++] = 0x00; //
		Enroll_Buff[Count++] = 0x1B;
		Enroll_Buff[Count++] = 0x03;

		byte[] out = new byte[Count];
		System.arraycopy(Enroll_Buff, 0, out, 0, Count);
		Util.setCRCSagem(out);

		return out;
	}

	public static byte[] verifyISOTemplate(byte[] template) {

		byte Enroll_Buff[] = new byte[1350];

		byte CRC[] = new byte[2];
		int LengthTotal = 11;

		Enroll_Buff[0] = 0x02; // Packet ID
		Enroll_Buff[1] = 0x61;
		Enroll_Buff[2] = (byte) (0x00 + inc); // RC
		Enroll_Buff[3] = 0x20; // data identifier - Verify
		byte[] l1 = Util.writeShort(template.length + 8 + 8); // Added 8 extra
																// for ISO_PK
																// Details
		Enroll_Buff[4] = (byte) l1[0];// (byte)(template.length+8); // ILV
										// 1+2+template
		Enroll_Buff[5] = (byte) l1[1]; // Length
		Enroll_Buff[6] = 0x00; // Time Out
		Enroll_Buff[7] = 0x00; // Time Out
		Enroll_Buff[8] = 0x05; // Matching Threshold
		Enroll_Buff[9] = 0x00; // Matching Threshold
		Enroll_Buff[10] = 0x00; // Aquisition Quality Threshold
		// ISO_PK DETAILS
		byte[] l3 = Util.writeShort(template.length + 8);
		Enroll_Buff[11] = 0x3F; // ID_ISO_PK
		Enroll_Buff[12] = l3[0]; // L1+L2
		Enroll_Buff[13] = l3[1]; // L1+L2
		// ISO_PK PARAM
		Enroll_Buff[14] = 0x40;
		Enroll_Buff[15] = 0x02;
		Enroll_Buff[16] = 0x00;
		Enroll_Buff[17] = (byte)0xff;
		Enroll_Buff[18] = 0x01;

		// ISO_PK_DATA_ISO_FMR
		Enroll_Buff[19] = 0x6E; // iso-6E
		byte[] l2 = Util.writeShort(template.length);
		Enroll_Buff[20] = l2[0];
		Enroll_Buff[21] = l2[1];

		System.arraycopy(template, 0, Enroll_Buff, 14 + 8, template.length); // Template
																				// data
		LengthTotal = 14 + template.length + 8;
		CRC[0] = 0x00;
		CRC[1] = 0x00;

		Enroll_Buff[LengthTotal] = CRC[0];
		Enroll_Buff[LengthTotal + 1] = CRC[1];

		Enroll_Buff[LengthTotal + 2] = 0x1b;
		Enroll_Buff[LengthTotal + 3] = 0x03;
		byte[] out = new byte[LengthTotal + 3 + 1];
		System.arraycopy(Enroll_Buff, 0, out, 0, out.length);
		Util.setCRCSagem(out);

		return out;
	}
}
