package com.digitalindiapayment.scannerConnect.hemen;//package com.olive.access;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


public class SerialDevice {
	public InputStream in;
	public OutputStream out;
	public static final byte STX=0x7E;
	public static final byte ETX=0x04;
	 
	public SerialDevice(InputStream in,OutputStream out){
		this.in=in;
		this.out=out;
	}
	public void send(byte[] data)throws IOException{
		//byte rd = 0x00;
		/*SimpleByteBuffer buf=new SimpleByteBuffer();
		 
		
			buf.append(STX);
			buf.append(data);
			buf.append(ETX);
		 	buf.append(calLRC(data));
         * */
//		 	System.out.println("==>"+HexString.bufferToHex(buf.toByteArray()));
//		    out.write(buf.toByteArray());
			out.flush();
		 
	}
	public void sendPlain(byte[] data) throws IOException{
		System.out.println("==>"+HexString.bufferToHex(data));
		out.write(data);
		out.flush();
	}
	public byte[] receive(int noBytes) throws IOException{
		
		byte[] read=new byte[noBytes];
		in.read(read);
		//System.out.println("<=="+HexString.bufferToHex(read));
		return read; 
	}
	public byte[] receive() throws IOException{
		byte[] read=new byte[in.available()];
		int size=in.read(read);
		if(size>0)
			System.out.println("<=="+HexString.bufferToHex(read) );
			
		return read; 
	}
public byte[] receiveUntillRead(byte end) throws IOException{

        
		byte[] res = null;  
		try {
			SimpleByteBuffer buffer = new SimpleByteBuffer();
			int count = 0;
			while (true) {
				int rd = in.read();// read byte
				buffer.append((byte)rd);
				//System.out.println(HexString.bufferToHex(buffer.toByteArray()));
				count++;
				if (rd == end) {
					buffer.append((byte)rd);
					break;
				}
			}
			res = buffer.toByteArray();
			if (res.length == 0)
				return null;
		} catch (Exception e) {
			e.printStackTrace();
		}//finally{
		return  res;
        //}
}
//	}

	public void setMode(byte mode) throws IOException{
		send(new byte[]{mode});
		//no response coming
		//receive(1); // verify response and throw error
	}
	
    public void setBaudRate(byte mode) throws IOException{
		send(new byte[]{mode});
		receive(); // verify response and throw error
	}
	private byte calLRC(byte[]command)
	{
		//byte [] crcData = new byte[command.length-2];
		//System.arraycopy(command, 1, crcData, 0, crcData.length);
		byte lrc=0x00;
		for(int i=0;i<command.length;i++){
			lrc=(byte)(lrc^command[i]);
		}
		lrc=(byte)(lrc^ETX); //including ETX
		//byte[] temp =new byte[1];
		//temp[0] = lrc ;
		//System.out.println("LRC:"+Integer.toHexString(lrc));
		return lrc;
	}
	
}
