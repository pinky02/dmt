package com.digitalindiapayment.scannerConnect.hemen;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;

import com.digitalindiapayment.R;
import com.esys.impressivedemo.bluetooth.BluetoothComm;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class HemenScanner extends AppCompatActivity {
    /////////////////// For print variables ////////////////////
    int[] packetBuffer = new int[5000];
    String ComData=null;
    int noBytes = 0,ct = 0,nofByts=0,k=0,totDataLen;
/////////////////// For print variables ////////////////////

    byte[] fpDatabytes = null;
    byte[] MyDatabytes = null;
    byte ENROLL_ID= 0x21;
    byte ILV_OK=0x00;
    byte ILVSTS_OK=0x00;
    byte ILVSTS_HIT=0x01;
    byte ILVSTS_NO_HIT=0x02;
    InputStream inputstream;
    OutputStream outputstream;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hemen_scanner); 
        inputstream = BluetoothComm.misIn;
        outputstream = BluetoothComm.mosOut;

        new EnterTextTask().execute(0);


    }


    public byte[] Enroll() {
        byte[] cmd = SagemFP.captureISO();

        try {

            outputstream.write(cmd);
            outputstream.flush();

            byte[] tmp = receiveUntill((byte) 0x1b);
            return tmp;
        } catch (Exception e) {
            Log.d("Hemen","exception"+e.getMessage());
            //return null;
        }
        return null;
    }

    public byte[] receiveUntill(byte end) throws IOException {

        int i_1b = 0, offset = 0,iNew = 0;
        byte[] res = null;
        byte[] fpEnroll = new byte[1000];
        byte[] iMystore = null;
        byte[] template = null;
        try {
            SimpleByteBuffer buffer = new SimpleByteBuffer();
            while (true) {
                int rd = inputstream.read();// read byte
                if (rd == (byte) 0x1B) {
                    iNew = rd;
                    rd = inputstream.read();
                    if(rd == (byte)0x03)
                    {
                        buffer.append((byte)iNew);
                        buffer.append((byte) rd);
                        break;
                    }
                    else if(rd == (byte)0x12)
                    {
                        rd = (byte)0x11;
                        buffer.append((byte) rd);

                    }
                    else if(rd == (byte)0x1b)
                    {
                        buffer.append((byte) rd);

                    }
                    else if(rd == (byte)0x14)
                    {
                        rd = (byte)0x13;
                        buffer.append((byte) rd);
                    }
                }
                else
                    buffer.append((byte)rd);
            }
            String resDataStr = HexString.bufferToHex(buffer.toByteArray());
            iMystore = HexString.hexToBuffer(resDataStr);

            if(iMystore[0] == (byte)0xff)
                offset = 4;
            else
                offset = 3;
            byte ack[] = {0x02, 0x62, iMystore[offset+2]};


            outputstream.write(ack);
            outputstream.flush();
            int fmrStartIndex = resDataStr.indexOf("464D52");
            if(fmrStartIndex==-1)
            {
                Log.d("Hemen","Enrollment is not proper, Please do it again");
                return null;
            }
            String fpDataStr = resDataStr.substring(fmrStartIndex);
            fpDataStr = fpDataStr.substring(0, fpDataStr.length() - 8);
            fpDatabytes = HexString.hexToBuffer(fpDataStr);
            fpEnroll = HexString.hexToBuffer(fpDataStr);

            // Log.d("Hemen","resp:"+Integer.toHexString(iMystore[0] &0xff)+Integer.toHexString(iMystore[1] &0xff)+Integer.toHexString(iMystore[2] &0xff)+Integer.toHexString(iMystore[3] &0xff)+Integer.toHexString(iMystore[4] &0xff)+Integer.toHexString(iMystore[5] &0xff));
            if (iMystore[offset + 0] == 0x02 && iMystore[offset + 1] == (byte) 0xe1 && iMystore[offset + 2] >= 0x00) {
                if ((byte) iMystore[offset + 3] == ENROLL_ID) {
                    if ((byte) iMystore[offset + 6] == ILV_OK) {
                        Log.d("Hemen","\n SUCCESS ENROLLMENT");
                    }
                    else{
                        switch(iMystore[offset+6])
                        {
                            case -1:
                                Log.d("Hemen","An error occured during enroll");
                                break;
                            case -2:
                                Log.d("Hemen","Input parameters not valid");
                                break;

                            case -6:
                                Log.d("Hemen","Timeout has expired !!!");
                                break;

                            case 34:
                                Log.d("Hemen","False finger detected !!!");
                                //ResultDlg.m_cs_Msg2.Format("False finger detected !!!");
                                break;

                            case 35:
                                Log.d("Hemen","Finger too moist !!!");
                                break;

                            default:
                                Log.d("Hemen","ERROR in enroll");
                        }
                        Log.d("Hemen","ERROR in enroll");
                    }
                }
            }

            res = buffer.toByteArray();
            if (res.length == 0) {
                return null;
            }
        } catch (Exception e) {
        }

        return fpEnroll;

    }

    public void fp_mode()
    {
        byte[] fp_mode = new byte[6];

        fp_mode[0] = 0x7e;
        fp_mode[1] = (byte) 0xc1;
        fp_mode[2] = 0x04;
        fp_mode[3] = (byte) 0xc5;
        try {


            outputstream.write(fp_mode);
            outputstream.flush();
            //ReadPr();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        try {
            Thread.sleep(3000);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }

    }
    public void pr_mode()
    {
        byte[] pr_mode = new byte[6];
        pr_mode[0] = 0x7e;
        pr_mode[1] = (byte) 0xc2;
        pr_mode[2] = 0x04;
        pr_mode[3] = (byte) 0xc6;
        try {

            outputstream.write(pr_mode);
            outputstream.flush();
            // ReadPr();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    private class EnterTextTask extends AsyncTask<Integer, Integer, Integer> {
        /* displays the progress dialog until background task is completed*/
        private int iRetVal;
        private String fingurePrint;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        /* Task of EnterTextAsyc performing in the background*/
        @Override
        protected Integer doInBackground(Integer... params) {
            fp_mode();
            fingurePrint =  Base64.encodeToString(Enroll(), Base64.DEFAULT) ;
            pr_mode();
            return iRetVal;
        }

        /* This displays the status messages of EnterTextAsyc in the dialog box */
        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            Intent returnIntent = new Intent();
            returnIntent.putExtra("DATA",fingurePrint);
            setResult(Activity.RESULT_OK, returnIntent);
            finish();

        }
    }


}
