package com.digitalindiapayment.scannerConnect.mantra;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.digitalindiapayment.R;
import com.digitalindiapayment.pojo.morphoXmlBeans.DeviceInfo;
import com.digitalindiapayment.pojo.morphoXmlBeans.Opts;
import com.digitalindiapayment.pojo.morphoXmlBeans.PidData;
import com.digitalindiapayment.pojo.morphoXmlBeans.PidOptions;
import com.digitalindiapayment.util.Util;
import com.morpho.sample.FingerprintData;

import org.greenrobot.eventbus.EventBus;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.StringWriter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MantraScanner extends AppCompatActivity {
    private static final int TYPE_GET_INFO = 1;
    private static final int TYPE_CAPTURE = 2;

    private static final String TAG = "MAntraScanner";
    @BindView(R.id.deviceStatus)
    TextView deviceStatus;
    @BindView(R.id.btn_connect)
    Button btnConnect;
    @BindView(R.id.btn_scan)
    Button btnPrint;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mantra_scanner);
        ButterKnife.bind(this);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case TYPE_GET_INFO:
                if (resultCode == Activity.RESULT_OK) {
                    try {
                        if (data != null) {
                            String result = data.getStringExtra("DEVICE_INFO");
                            String rdService = data.getStringExtra("RD_SERVICE_INFO");
                            String display = "";
                            if (rdService != null) {
                                display = rdService + "\n\n";
                            }
                            if (result != null) {
                                Serializer serializer = new Persister();
                                DeviceInfo info = serializer.read(DeviceInfo.class, result);

                                if(!info.dpId.trim().equals("")) {
                                    display += "Device Code: " + info.dc + "\n\n"
                                           // + "Serial No: " + info. + "\n\n"
                                            + "dpId: " + info.dpId + "\n\n"
                                            + "MC: " + info.mc + "\n\n"
                                            + "MI: " + info.mi + "\n\n"
                                            + "rdsId: " + info.rdsId + "\n\n"
                                            + "rdsVer: " + info.rdsVer;

                                    deviceStatus.setText("Device Id: " + info.dpId);
                                    //setOutputText(display);
                                }else{
                                    deviceStatus.setText("Device not Ready ");
                                }
                            }
                        }
                    } catch (Exception e) {
                        Log.e("Error", "Error while deserialze device info", e);
                    }
                }
                break;
            case TYPE_CAPTURE:
                if (resultCode == Activity.RESULT_OK) {
                    try {
                        if (data != null) {

                            //Log.d(TAG,);
                            String result = data.getStringExtra("PID_DATA");
                            Log.d(TAG,result);
                            if (result != null) {

                                Log.d(TAG, result);

                                //Serializer serializer = new Persister();
                                //PidData info = serializer.read(PidData.class, result);


                                Serializer serializer = new Persister();
                                PidData info = serializer.read(PidData.class, result);

                                Log.d(TAG, "Fielsd : " + info._Resp.errCode);

                                if (info._Resp.errCode.equals("0")) {
                                    //Log.d(TAG, info);
                                    FingerprintData fingerprintData = new FingerprintData();
                                    //fingerprintData.fp = info.Data;
                                    // fingerprintData.fpv = fpvBase64Output.get(0);
                                    //fingerprintData.fpv = info.Data;
                                    EventBus.getDefault().post(fingerprintData);
                                    ////Intent returnIntent = new Intent();
                                    ////returnIntent.putExtra("DATA",fingureprint);
                                    //setResult(Activity.RESULT_OK, returnIntent);
                                    finish();


                                } else {
                                    deviceStatus.setText(info._Resp.errInfo);
                                    Util.showShortToast(getApplicationContext(), info._Resp.errInfo);
                                }
                            }
                        }
                    } catch (Exception e) {
                        Log.e("Error", "Error while deserialize pid data", e);
                    }
                }
                break;
        }
    }

    private String getPIDOptions() {
        try {

            ///////////
            int fingerCount = 1;
            // FMR:0 FIR:1
            int fingerType = 1;
            // For XML : 0 Protobuf:1
            int fingerFormat = 0;
            String pidVer = "2.0";
            String timeOut = "10000";
            String posh = "UNKNOWN";


            Opts opts = new Opts();
            opts.fCount = String.valueOf(fingerCount);
            opts.fType = String.valueOf(fingerType);
            opts.iCount = "0";
            opts.iType = "0";
            opts.pCount = "0";
            opts.pType = "0";
            opts.format = String.valueOf(fingerFormat);
            opts.pidVer = pidVer;
            opts.timeout = timeOut;
//            opts.otp = "";
            opts.posh = posh;
            String env = "P";

            opts.env = env;

            PidOptions pidOptions = new PidOptions();
            pidOptions.ver = pidVer;
            pidOptions.Opts = opts;

            Serializer serializer = new Persister();
            StringWriter writer = new StringWriter();
            serializer.write(pidOptions, writer);
            return writer.toString();
        } catch (Exception e) {
            Log.e("Error", e.toString());
        }
        return null;
    }


    public void onButtonClick(View view) {
        Log.d(TAG, "Intent pass Data : " + getPIDOptions());

    }

    @OnClick({R.id.btn_connect, R.id.btn_scan})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_connect:
                Intent intent1 = new Intent();
                intent1.setAction("in.gov.uidai.rdservice.fp.INFO");
                //intent1.putExtra("PID_OPTIONS", getPIDOptions());
                startActivityForResult(intent1, TYPE_GET_INFO);
                break;
            case R.id.btn_scan:
                Intent intent2 = new Intent();
                intent2.setAction("in.gov.uidai.rdservice.fp.CAPTURE");
                intent2.putExtra("PID_OPTIONS", getPIDOptions());
                startActivityForResult(intent2, TYPE_CAPTURE);
                break;
        }
    }
}
