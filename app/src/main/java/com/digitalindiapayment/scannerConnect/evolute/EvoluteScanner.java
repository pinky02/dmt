package com.digitalindiapayment.scannerConnect.evolute;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.digitalindiapayment.R;
import com.digitalindiapayment.application.MyApplication;
import com.esys.impressivedemo.ImpressSetup;
import com.esys.impressivedemo.bluetooth.BluetoothComm;
import com.impress.api.FPS;
import com.impress.api.FpsConfig;
import com.impress.api.HexString;

/**
 * Created by rajeshpandya on 26/04/17.
 */

public class EvoluteScanner extends Activity {

    private static final int DEVICE_NOTCONNECTED = -100;
    private FPS fps;
    private ProgressDialog dlgPg;
    private Button btn_print;
    private byte[] bMinutiaeData = {};
    private byte[] bufvalue = {};
    private CaptureFingerAsyn textTask;
    private LinearLayout fingureprint;
    private TextView headerScanPrintText ;
    private ImageView imgFingerprint;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scan_print_receipt_activity);

        btn_print = (Button) findViewById(R.id.btn_print);

        headerScanPrintText =(TextView)findViewById(R.id.headerScanPrintText);
        imgFingerprint = (ImageView)findViewById(R.id.imgFingerprint);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            imgFingerprint.setBackground(getDrawable(R.drawable.img_fingur_print));
        }else {
            imgFingerprint.setBackground(getResources().getDrawable(R.drawable.img_fingur_print));
        }

        headerScanPrintText.setText(getString(R.string.scan_fingureprint));
        //

        dlgPg = new ProgressDialog(EvoluteScanner.this);
        dlgPg.setMessage(getString(R.string.place_your_fingure_on_scanner_message));
        dlgPg.setIndeterminate(true);
        dlgPg.setCancelable(false);





        btn_print.setText("SCAN");
        try {
            fps = new FPS(ImpressSetup.onCreate(getApplicationContext()), BluetoothComm.mosOut, BluetoothComm.misIn);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        btn_print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new CaptureFingerAsyn().execute(0);
            }
        });


    }

    public void showResult(int iRetVal) {


        if (iRetVal == DEVICE_NOTCONNECTED) {
            Toast.makeText(getApplicationContext(), "Device not connected", Toast.LENGTH_LONG).show();
            ((MyApplication) getApplicationContext()).closeConn();
            onBackPressed();

        } else if (iRetVal == FPS.SUCCESS) {
            Toast.makeText(getApplicationContext(), "Capture finger success", Toast.LENGTH_LONG).show();

            Intent returnIntent = new Intent();
            returnIntent.putExtra("DATA",Base64.encodeToString(bMinutiaeData, Base64.DEFAULT));
            setResult(Activity.RESULT_OK, returnIntent);
            finish();



        } else if (iRetVal == FPS.FPS_INACTIVE_PERIPHERAL) {
            Toast.makeText(getApplicationContext(), "Peripheral is inactive", Toast.LENGTH_LONG).show();

        } else if (iRetVal == FPS.TIME_OUT) {
            Toast.makeText(getApplicationContext(), "Capture finger time out", Toast.LENGTH_LONG).show();

        } else if (iRetVal == FPS.FAILURE) {
            Toast.makeText(getApplicationContext(), "Capture finger failed", Toast.LENGTH_LONG).show();

        } else if (iRetVal == FPS.PARAMETER_ERROR) {
            Toast.makeText(getApplicationContext(), "Parameter error", Toast.LENGTH_LONG).show();
        } else if (iRetVal == FPS.FPS_INVALID_DEVICE_ID) {
            Toast.makeText(getApplicationContext(), "Connected  device is not license authenticated.", Toast.LENGTH_LONG).show();
        } else if (iRetVal == FPS.FPS_ILLEGAL_LIBRARY) {

            Toast.makeText(getApplicationContext(), "Library not valid", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_CANCELED, returnIntent);
        finish();
    }



    public class CaptureFingerAsyn extends AsyncTask<Integer, Integer, Integer> {
        /* displays the progress dialog until background task is completed */

        private FpsConfig fpsconfig = new FpsConfig();
        private int iRetVal;

        @Override
        protected void onPreExecute() {
            dlgPg.show();

            super.onPreExecute();
        }

        /* Task of CaptureFinger performing in the background */
        @Override
        protected Integer doInBackground(Integer... params) {
            try {
                bufvalue = new byte[3500];
                fpsconfig = new FpsConfig(0, (byte) 20);
                bufvalue = fps.bFpsCaptureTemplate(fpsconfig);
                iRetVal = fps.iGetReturnCode();
                bMinutiaeData = fps.bGetMinutiaeData();
                String str1 = HexString.bufferToHex(bMinutiaeData);
                Log.i("Capture", "Finger Data:\n" + str1);
            } catch (NullPointerException e) {
                iRetVal = DEVICE_NOTCONNECTED;
                return iRetVal;
            }
            return iRetVal;
        }

        /*
         * This function sends message to handler to display the status messages
         * of Diagnose in the dialog box
         */
        @Override
        protected void onPostExecute(Integer result) {
            dlgPg.dismiss();
            showResult(iRetVal);

            super.onPostExecute(result);
        }
    }
}
