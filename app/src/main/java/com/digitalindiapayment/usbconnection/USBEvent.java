package com.digitalindiapayment.usbconnection;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.hardware.usb.UsbManager;

import com.digitalindiapayment.util.Util;
import com.morpho.android.usb.USBManager;
import com.morpho.sample.KeyFile;

import java.util.List;

/**
 * Created by Expert on 11-03-2017.
 */

public class USBEvent extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        // It Only works only when User is Selected Morpho Scanner option in Device Configuration Settings
        if (UsbManager.ACTION_USB_DEVICE_ATTACHED.equals(action)) {
            USBManager.getInstance().initialize(context, context.getPackageName() + ".USB_ACTION");
            try {
                this.abortBroadcast();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (isUserLoggedIn(context)) {
                Intent i = new Intent();
                if (Util.PREFERENCES.isRDEnabled(context)) {
                    i.setClassName(context.getPackageName(), "com.digitalindiapayment.usbconnection.RDUSBConnectionActivity");
                } else {
                    i.setClassName(context.getPackageName(), "com.digitalindiapayment.usbconnection.USBConnectionActivity");
                }
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
            }
        }
        if (UsbManager.ACTION_USB_DEVICE_DETACHED.equals(action)) {
            KeyFile.setDevise(context, "");
        }
    }

    public boolean isUserLoggedIn(Context mContext) {
        //ref to :  https://stackoverflow.com/a/31156585
        ActivityManager am = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);
        if (!tasks.isEmpty()) {
            ComponentName topActivity = tasks.get(0).topActivity;
            if (!topActivity.getPackageName().equals(mContext.getPackageName())) {
                return false;
            }
            // If Its LoginActivity and SplashActivity
            // then User is Not Loged in
            if (topActivity.getClassName().equals("com.digitalindiapayment.login.LoginActivity")) {
                return false;
            } else if (topActivity.getClassName().equals("com.digitalindiapayment.SplashActivity")) {
                return false;
            }
        }
        return true;
    }
}