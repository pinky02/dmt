package com.digitalindiapayment.usbconnection;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.digitalindiapayment.BuildConfig;
import com.digitalindiapayment.R;
import com.digitalindiapayment.deviceConnect.DeviceContract;
import com.digitalindiapayment.deviceConnect.DevicePresenter;
import com.digitalindiapayment.morphoScanConnect.availableMorphoes.DigitalPersonaScanner;
import com.digitalindiapayment.morphoScanConnect.availableMorphoes.ManagementClient;
import com.digitalindiapayment.morphoScanConnect.availableMorphoes.MantraScanner;
import com.digitalindiapayment.morphoScanConnect.availableMorphoes.MorphoL0SoftScanner;
import com.digitalindiapayment.morphoScanConnect.availableMorphoes.RDActions;
import com.digitalindiapayment.morphoScanConnect.availableMorphoes.SafronScanner;
import com.digitalindiapayment.morphoScanConnect.availableMorphoes.StartekScanner;
import com.digitalindiapayment.pojo.morphoXmlBeans.AdditionalInfo;
import com.digitalindiapayment.pojo.morphoXmlBeans.DeviceInfo;
import com.digitalindiapayment.pojo.morphoXmlBeans.Param;
import com.digitalindiapayment.pojo.morphoXmlBeans.RDService;
import com.digitalindiapayment.util.Util;
import com.morpho.android.usb.USBManager;
import com.morpho.morphosmart.sdk.MorphoDevice;
import com.morpho.sample.KeyFile;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;
import rx.Observer;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by admin on 28-11-2017.
 */

public class RDUSBConnectionActivity extends Activity implements DeviceContract.View {

    private static final String TAG = "MorphoConnectionScrn";
    private static final int rdServiceAttemptsLimit = 5;
    private static final int openManagementRequest = 202;

    MorphoDevice morphoDevice;
    @BindView(R.id.btn_register)
    Button btnRegister;
    DeviceContract.UserAction devicePresenter;
    IntentFilter morphoConnectionFilter;
    RDActions rdFingureScanner = null;
    Map<String, Object> scannerSelector = null;
    TextView textViewSensorName;
    String terminalId;
    @BindView(R.id.parentPanel)
    RelativeLayout parentPanel;
    String registeredDevice;
    private int rdServiceAttempts = 0;
    private String sensorName = "";
    private RelativeLayout btn_connectDevice;
    private TextView tv_connectDevice;
    private TextView deviceStatus;
    private Button btn_grantPermission;
    private Button btn_cancel;
    private MaterialProgressBar progressbar;
    private BroadcastReceiver morphoDetachEvent = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //unregisterReceiver(morphoDetachEvent);
            finish();
        }
    };
    private Dialog managementClientBuilder;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        if (KeyFile.isRebootSoft) {
            KeyFile.isRebootSoft = false;
            finish();
        }
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.morpho_connection_activity);
        ButterKnife.bind(this);
        textViewSensorName = (TextView) findViewById(R.id.textView_serialNumber);
        scannerSelector = new HashMap<>();
        scannerSelector.put(getString(R.string.device_startek_name), new StartekScanner(this));
        scannerSelector.put(getString(R.string.device_morpho_name), new SafronScanner(this));
        scannerSelector.put(getString(R.string.device_mantra_name), new MantraScanner(this));
        scannerSelector.put(getString(R.string.device_morphol0s_name),new MorphoL0SoftScanner(this));
        scannerSelector.put(getString(R.string.device_digital_persona_name), new DigitalPersonaScanner(this));
        registeredDevice = Util.PREFERENCES.getDeviceName(this);

        Log.d(TAG, "Selected Scanner :" + registeredDevice);
        morphoConnectionFilter = new IntentFilter(UsbManager.ACTION_USB_DEVICE_DETACHED);
        if (registeredDevice.equals(getString(R.string.device_evolute_name))) {
            final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
            alertDialog.setTitle(getResources().getString(R.string.app_name));
            alertDialog.setMessage(getString(R.string.default_device_selection_failure));
            alertDialog.setCancelable(false);
            alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
            alertDialog.show();
            return;
        }
        rdFingureScanner = (RDActions) scannerSelector.get(registeredDevice);
        btn_connectDevice = (RelativeLayout) findViewById(R.id.btn_connectDevice);
        tv_connectDevice = (TextView) findViewById(R.id.tv_connectDevice);
        btn_grantPermission = (Button) findViewById(R.id.btn_grantPermission);
        btn_cancel = (Button) findViewById(R.id.btn_cancel);
        progressbar = (MaterialProgressBar) findViewById(R.id.progressbar);
        deviceStatus = (TextView) findViewById(R.id.deviceStatus);
        ButtonClick buttonClick = new ButtonClick();
        btn_connectDevice.setOnClickListener(buttonClick);
        btn_grantPermission.setOnClickListener(buttonClick);
        btnRegister.setOnClickListener(buttonClick);
        btn_cancel.setOnClickListener(buttonClick);
        morphoDevice = new MorphoDevice();
        devicePresenter = new DevicePresenter(this, getApplicationContext());
        // Grant Permission not requred for Morpho RD Device
        btn_grantPermission.setVisibility(View.GONE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(morphoDetachEvent, morphoConnectionFilter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            unregisterReceiver(morphoDetachEvent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void grantPermission() {
        Log.d(TAG, "grantPermission" + getPackageName());
        USBManager.getInstance().initialize(this, getPackageName() + ".USB_ACTION");
    }

    public void finishDialog() {
        this.finish();
    }

    @Override
    public void showDeviceValidity(Boolean result, String message) {
        if (BuildConfig.DEBUG) {
            result = true;
        }
        Log.d(TAG, "showDeviceValidity " + result);
        if (message != null) {
            Util.showLongToast(getApplicationContext(), message);
        }
        progressbar.setVisibility(View.GONE);
        tv_connectDevice.setText("Connect");
        if (result) {
            Log.d(TAG, "Morpho Success");
        } else {
            KeyFile.setDevise(getApplication(), "");
        }
        finish();
    }

    @Override
    public void showLoader() {
        //Disable all Buttons till response received
        btn_connectDevice.setEnabled(false);
        btn_grantPermission.setEnabled(false);
        btn_cancel.setEnabled(false);
        // Show Loader for checking Device with DIPL Servers
        progressbar.setVisibility(View.VISIBLE);
        tv_connectDevice.setText("Connecting");
    }

    public void hideLoader() {
        btn_connectDevice.setEnabled(true);
        btn_cancel.setEnabled(true);
        // Show Loader for checking Device with DIPL Servers
        progressbar.setVisibility(View.GONE);
        tv_connectDevice.setText("Connect");
    }


    @Override
    public void onBackPressed() {
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Log.d(TAG, " requestCode " + requestCode);
            Bundle b = data.getExtras();
            if (b != null) {
                for (String key : b.keySet()) {
                    String obj = b.getString(key);   //later parse it as per your required type
                    Log.d(TAG, " KeyValue Pair " + key + " : " + obj);
                }
                String deviceInfo = b.getString("DEVICE_INFO", "");
                String rdServiceInfo = b.getString("RD_SERVICE_INFO", "");
                String dnc = b.getString("DNC", "");
                String dnr = b.getString("DNR", "");
                if (!dnc.isEmpty() || !dnr.isEmpty()) {
                    Log.d(TAG, "Device Info 001: " + dnc + dnr + " " + deviceInfo + rdServiceInfo);
                } else {
                    Log.d(TAG, "Device Info 002" + deviceInfo + rdServiceInfo);
                }
                Serializer serializer = new Persister();
                try {
                    DeviceInfo deviceInfoObject = serializer.read(DeviceInfo.class, deviceInfo);
                    RDService rdServiceObject = serializer.read(RDService.class, rdServiceInfo);

                    Util.PREFERENCES.setMorphoDeviceId(RDUSBConnectionActivity.this, deviceInfoObject.getMc());
                    if (rdServiceObject.isReady() && !deviceInfoObject.getDpId().equals("")) {
                        final ParamObserver pObserver = new ParamObserver();
                        String terminalId = "";
                        // bellow logic implemented for only mantra device to get terminal id from device info
                        if (Util.PREFERENCES.getDeviceName(getApplicationContext()).equalsIgnoreCase(getString(R.string.device_mantra_name))) {
                            //get additional info object from deviceInfo
                            AdditionalInfo info = deviceInfoObject.getAdditionalInfo();
                            if (info != null && info.getParams() != null) {
                                //rx reactive code to get serial number from List
                                rx.Observable.from(info.getParams())
                                        .subscribeOn(Schedulers.immediate())
                                        .observeOn(Schedulers.immediate())
                                        .filter(new Func1<Param, Boolean>() {
                                            @Override
                                            public Boolean call(Param param) {
                                                return param.getName().equalsIgnoreCase("srno");
                                            }
                                        })
                                        .subscribeOn(Schedulers.immediate())
                                        .observeOn(Schedulers.immediate())
                                        .subscribe(pObserver);

//                                for (Param p : info.getParams()) {
//                                    if (p.getName().equalsIgnoreCase("srno")) {
//                                        //get value where name == srno
//                                        terminalId = p.getValue();
//                                        break;
//                                    }
//                                }
                                terminalId = pObserver.getTerminalId();
                                Log.e("DeviceTerminalID", "mantra device = " + terminalId);
                            }
                        } else {
                            //this logic for to get terminal id from morpho,digital persona,startek,
                            terminalId = deviceInfoObject.getDpId() + ":" + deviceInfoObject.getMi() + ":" + deviceInfoObject.getDc();
                            Log.e("DeviceTerminalID", "other device = " + terminalId);

                        }
                        devicePresenter.checkDeviceTerminalId(terminalId);
                        textViewSensorName.setText(terminalId);
                        KeyFile.setDevise(getApplication(), terminalId);
                    } else {
                        // This issue due to Morpho Device firmware upgrade
                        // if device not ready then attempt multiple times
                        // if attempt is greter than limit then shows that firmware upgrade require
                        Log.d(TAG, " onActivityResult " + rdServiceAttempts);
                        if (rdServiceAttempts < rdServiceAttemptsLimit) {
                            rdFingureScanner.getDeviceInfo();
                            rdServiceAttempts++;
                        } else {
                            btn_connectDevice.setEnabled(true);
                            openManagementClientPopup();
                            hideLoader();
                            rdServiceAttempts = 0;
                            //UtilDmt.showShortToast(this, String.format(getString(R.string.please_update_firmware), UtilDmt.PREFERENCES.getDeviceName(RDUSBConnectionActivity.this)));
                        }
                    }
                    Log.d(TAG, " onActivityResult " + deviceInfoObject.toString() + " ::: " + rdServiceObject.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                    hideLoader();
                }
            }
        }
    }

    public void openManagementClientPopup() {
        if (scannerSelector.get(registeredDevice) instanceof ManagementClient) {
            final ManagementClient managementClient = (ManagementClient) scannerSelector.get(registeredDevice);
            if (managementClient != null) {
                managementClientBuilder = new Dialog(this);
                managementClientBuilder.setContentView(R.layout.layout_management_client_open);
                Window window = managementClientBuilder.getWindow();
                window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
                TextView message = (TextView) managementClientBuilder.findViewById(R.id.message);
                Button ok = (Button) managementClientBuilder.findViewById(R.id.ok);
                message.setText(String.format(getString(R.string.open_management_client_request), registeredDevice));
                try {

                    ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            if (managementClient != null) {
                                Log.d(TAG, "Under click" + managementClient.getManagementClientPackage());
                                //Intent intent = new Intent();
                                PackageManager pm = getPackageManager();
                                Intent intent = pm.getLaunchIntentForPackage(managementClient.getManagementClientPackage());
                                startActivityForResult(intent, openManagementRequest);

                            }
                        }
                    });
                } catch (Exception e) {
                }
                managementClientBuilder.show();
            }
        } else {
            Util.showShortToast(this, getString(R.string.device_not_ready));
        }
    }

    class ButtonClick implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btn_connectDevice:
                    if (rdFingureScanner.getDeviceInfo()) {
                        btn_connectDevice.setEnabled(false);
                        showLoader();
                    } else {
                        btn_connectDevice.setEnabled(true);
                        hideLoader();
                    }
                    break;
                case R.id.btn_grantPermission:
                    grantPermission();
                    break;
                case R.id.btn_cancel:
                    finishDialog();
                    break;
                case R.id.btn_register:
                    Log.d(TAG, " Btn Register:  ");
                    break;
            }
        }
    }

    class ParamObserver implements Observer<Param> {

        private String terminalId;

        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {

        }

        @Override
        public void onNext(Param param) {
            terminalId = param.getValue();
        }

        public String getTerminalId() {
            return terminalId;
        }
    }
}
