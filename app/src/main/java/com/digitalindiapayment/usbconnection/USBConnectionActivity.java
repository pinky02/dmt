// The present software is not subject to the US Export Administration Regulations (no exportation license required), May 2012
package com.digitalindiapayment.usbconnection;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.digitalindiapayment.BuildConfig;
import com.digitalindiapayment.R;
import com.digitalindiapayment.deviceConnect.DeviceContract;
import com.digitalindiapayment.deviceConnect.DevicePresenter;
import com.digitalindiapayment.morphoScanConnect.availableMorphoes.DeviceDescriptor;
import com.digitalindiapayment.morphoScanConnect.availableMorphoes.DigitalPersonaScanner;
import com.digitalindiapayment.morphoScanConnect.availableMorphoes.FingureScanner;
import com.digitalindiapayment.morphoScanConnect.availableMorphoes.MantraScanner;
import com.digitalindiapayment.morphoScanConnect.availableMorphoes.SafronScanner;
import com.digitalindiapayment.pojo.morphoXmlBeans.DeviceInfo;
import com.digitalindiapayment.pojo.morphoXmlBeans.RDService;
import com.digitalindiapayment.util.Util;
import com.morpho.android.usb.USBManager;
import com.morpho.morphosmart.sdk.MorphoDevice;
import com.morpho.sample.KeyFile;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;

public class USBConnectionActivity extends Activity implements DeviceContract.View {

    private static final String TAG = "MorphoConnectionScrn";

    MorphoDevice morphoDevice;
    @BindView(R.id.btn_register)
    Button btnRegister;

    private String sensorName = "";
    private RelativeLayout btn_connectDevice;
    private TextView tv_connectDevice;
    private TextView deviceStatus;
    private Button btn_grantPermission;
    private Button btn_cancel;

    private MaterialProgressBar progressbar;

    DeviceContract.UserAction devicePresenter;
    IntentFilter morphoConnectionFilter;

    FingureScanner fingureScanner = null;
    Map<String,Object> scannerSelector = null;
    TextView textViewSensorName;
    String terminalId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (KeyFile.isRebootSoft) {
            KeyFile.isRebootSoft = false;
            finish();
        }
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.morpho_connection_activity);
        ButterKnife.bind(this);

        textViewSensorName = (TextView) findViewById(R.id.textView_serialNumber);

        scannerSelector = new HashMap<>();
        scannerSelector.put(getString(R.string.device_morpho_name), new SafronScanner(USBConnectionActivity.this));
        scannerSelector.put(getString(R.string.device_mantra_name), new MantraScanner(USBConnectionActivity.this));
        scannerSelector.put(getString(R.string.device_digital_persona_name), new DigitalPersonaScanner(USBConnectionActivity.this));

        Log.d(TAG, "Selected Scanner :" + Util.PREFERENCES.getDeviceName(this));

        morphoConnectionFilter = new IntentFilter(UsbManager.ACTION_USB_DEVICE_DETACHED);

        String deviceName = Util.PREFERENCES.getDeviceName(this);
        if(deviceName.equals(getString(R.string.device_evolute_name)))
         {
            final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
            alertDialog.setTitle(getResources().getString(R.string.app_name));
            alertDialog.setMessage(getString(R.string.default_device_selection_failure));
            alertDialog.setCancelable(false);
            alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
            alertDialog.show();
            return;
        }
        fingureScanner = (FingureScanner) scannerSelector.get(Util.PREFERENCES.getDeviceName(this));

        fingureScanner.initializeDevice();

        btn_connectDevice = (RelativeLayout) findViewById(R.id.btn_connectDevice);
        tv_connectDevice = (TextView) findViewById(R.id.tv_connectDevice);
        btn_grantPermission = (Button) findViewById(R.id.btn_grantPermission);
        btn_cancel = (Button) findViewById(R.id.btn_cancel);
        progressbar = (MaterialProgressBar) findViewById(R.id.progressbar);
        deviceStatus = (TextView) findViewById(R.id.deviceStatus);
        ButtonClick buttonClick = new ButtonClick();
        btn_connectDevice.setOnClickListener(buttonClick);
        btn_grantPermission.setOnClickListener(buttonClick);
        btnRegister.setOnClickListener(buttonClick);
        btn_cancel.setOnClickListener(buttonClick);
        morphoDevice = new MorphoDevice();
        grantPermission();
        //if (USBManager.getInstance().isDevicesHasPermission() == true) {
            //btn_grantPermission.setEnabled(false);
        //}
        // Presenter handel Server validation of terminalID
        devicePresenter = new DevicePresenter(this, getApplicationContext());

    }

    private BroadcastReceiver morphoDetachEvent = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //unregisterReceiver(morphoDetachEvent);
            finish();
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(morphoDetachEvent, morphoConnectionFilter);

    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            unregisterReceiver(morphoDetachEvent);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        fingureScanner.stop();
    }

    public void grantPermission() {
        Log.d(TAG,"grantPermission"+getPackageName());
        USBManager.getInstance().initialize(this, getPackageName()+".USB_ACTION");
    }

    public void finishDialog() {
        this.finish();
    }

    @Override
    public void showDeviceValidity(Boolean result, String message) {
        if (BuildConfig.DEBUG) {
            result = true;
        }
        Log.d(TAG, "showDeviceValidity " + result);
        if (message != null) {
            Util.showLongToast(getApplicationContext(), message);
        }
        progressbar.setVisibility(View.GONE);
        tv_connectDevice.setText("Connect");
        if (result) {
            // to register Morpho Device to Server for whitlisting Morpho device
            Log.d(TAG,"Morpho Success");
            if(fingureScanner instanceof DeviceDescriptor){
                devicePresenter.registerDevice(((DeviceDescriptor)fingureScanner).getDeviceDetails());
            }
        } else {
            KeyFile.setDevise(getApplication(), "");
        }
        finish();
    }

    @Override
    public void showLoader() {
        //Disable all Buttons till response received
        btn_connectDevice.setEnabled(false);
        btn_grantPermission.setEnabled(false);
        btn_cancel.setEnabled(false);
        // Show Loader for checking Device with DIPL Servers
        progressbar.setVisibility(View.VISIBLE);
        tv_connectDevice.setText("Connecting");
    }


    class ButtonClick implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btn_connectDevice:
                    String sensorName = fingureScanner.getDeviseTerminalId();
                    //Log.d(TAG," sensorTerminalId "+sensorTerminalId);
                    if(sensorName.equals("")){
                        grantPermission();
                        Toast.makeText(USBConnectionActivity.this,R.string.MORPHOERR_DEVICE_DISCONNECT,Toast.LENGTH_LONG).show();
                    }else {
                        textViewSensorName.setText(sensorName);
                        KeyFile.setDevise(getApplication(), "" + sensorName);
                        terminalId = sensorName;
                        // For Morpho Device
                        if (sensorName.contains("-")) {
                            terminalId = sensorName.split("-")[1];
                        }
                        devicePresenter.checkDeviceTerminalId(terminalId);
                    }
                    break;
                case R.id.btn_grantPermission:
                    grantPermission();
                    break;
                case R.id.btn_cancel:
                    finishDialog();
                    break;
                case R.id.btn_register:
                    Log.d(TAG," Btn Register:  ");
                    fingureScanner.register();
                    break;


            }
        }
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Bundle b = data.getExtras();
            if (b != null) {

                for(String key : b.keySet()){
                    String obj = b.getString(key);   //later parse it as per your required type
                    Log.d(TAG," KeyValue Pair "+key +" : "+obj);
                }

                String deviceInfo = b.getString("DEVICE_INFO", "");
                String rdServiceInfo = b.getString("RD_SERVICE_INFO", "");
                String dnc = b.getString("DNC", "");
                String dnr = b.getString("DNR", "");
                if (!dnc.isEmpty() || !dnr.isEmpty()) {
                    Log.d(TAG, "Device Info 001: " + dnc + dnr + " " + deviceInfo + rdServiceInfo);

                } else {
                    Log.d(TAG, "Device Info 002" + deviceInfo + rdServiceInfo);

                }
                Serializer serializer = new Persister();

                try {
                    DeviceInfo deviceInfoObject = serializer.read(DeviceInfo.class, deviceInfo);
                    RDService rdServiceObject =  serializer.read(RDService.class, rdServiceInfo);
                    Util.PREFERENCES.setMorphoDeviceId(USBConnectionActivity.this,deviceInfoObject.getMc());

                    Log.d(TAG," onActivityResult "+deviceInfoObject.toString()+" ::: "+rdServiceObject.toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }
                //finish();

                //devicePresenter.checkDeviceTerminalId(terminalId);

            }

        }
    }

}
