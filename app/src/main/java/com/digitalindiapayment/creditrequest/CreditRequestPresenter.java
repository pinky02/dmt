package com.digitalindiapayment.creditrequest;

import android.content.Context;
import android.util.Log;

import com.digitalindiapayment.creditrequest.CreditRequestReport.CreditRequestReportActivity;
import com.digitalindiapayment.pojo.BankDetailsBean;
import com.digitalindiapayment.util.Util;
import com.digitalindiapayment.webservices.Api;
import com.digitalindiapayment.webservices.core.ApiFail;
import com.digitalindiapayment.webservices.core.ApiSuccess;
import com.digitalindiapayment.webservices.core.HttpErrorResponse;
import com.digitalindiapayment.webservices.requestpojo.CreditAPIRequest;
import com.digitalindiapayment.webservices.responsepojo.BankAccountsResponse;
import com.digitalindiapayment.webservices.responsepojo.GetBanksResponse;
import com.digitalindiapayment.webservices.responsepojo.GetModeofTransferResponse;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class CreditRequestPresenter  implements CreditRequestContract.UserActions{

    private static final String TAG = "CreditReqPresenter";

    private CreditRequestContract.View view;
    private Context context;

    public CreditRequestPresenter(CreditRequestContract.View view, Context context) {
        this.view = view;
        this.context = context;
    }

    public CreditRequestPresenter(CreditRequestReportActivity creditRequestReportActivity, Context applicationContext) {
    }

    @Override
    public void loadDIPLBankDetails() {view.showLoading();
        Api.banksDetails().getDIPLAccountInfo(Util.PREFERENCES.getToken(context))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ApiSuccess<BankAccountsResponse>() {
                    @Override
                    public void call(BankAccountsResponse getBanksResponse) {
                        view.hideLoading();

                        //List<BankDetailsBean> banks = new ArrayList<>();
                        //banks.add(getBanksResponse);

                        Log.d(TAG,getBanksResponse.getBankAccounts().get(0).getAccountName()+" :: "+getBanksResponse.getBankAccounts().get(0).getBankId());
                        view.showDIPLBankDetails(getBanksResponse.getBankAccounts());
                    }
                }, new ApiFail() {
                    @Override
                    public void httpStatus(HttpErrorResponse response) {
                        view.hideLoading();

                    }

                    @Override
                    public void noNetworkError() {
                        view.hideLoading();
                        view.noInternet();

                    }

                    @Override
                    public void unknownError(Throwable e) {
                        view.hideLoading();

                    }
                });
    }

    @Override
    public void loadTransactionTypes() {view.showLoading();
        Api.banksDetails().getModeOfTransafer(Util.PREFERENCES.getToken(context))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ApiSuccess<GetModeofTransferResponse>() {
                    @Override
                    public void call(GetModeofTransferResponse getModeofTransferResponse) {
                        view.hideLoading();
                        view.showTransactionTypes(getModeofTransferResponse.getModeofTransfer());
                    }
                }, new ApiFail() {
                    @Override
                    public void httpStatus(HttpErrorResponse response) {
                        view.hideLoading();

                    }

                    @Override
                    public void noNetworkError() {
                        view.hideLoading();


                    }

                    @Override
                    public void unknownError(Throwable e) {
                        view.hideLoading();

                    }
                });
    }

    @Override
    public void submitCreditRequest(CreditAPIRequest apiRequest) {
        view.showLoading();
        Api.banksDetails().walletCreditRequest(Util.PREFERENCES.getToken(context),apiRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ApiSuccess<String>() {
                    @Override
                    public void call(String stringStringMap) {
                        view.hideLoading();
                        view.showWalletCreditRequestResult();
                        //view.success(context.getString(R.string.wallet_request_submitted);
                        // view.showLoading();
                    }
                }, new ApiFail() {
                    @Override
                    public void httpStatus(HttpErrorResponse response) {
                        view.hideLoading();
                        view.fail(response.getResponse().message());
                    }

                    @Override
                    public void noNetworkError() {
                        view.hideLoading();
                        view.noInternet();

                    }

                    @Override
                    public void unknownError(Throwable e) {
                        view.hideLoading();
                        view.exception(e);
                    }
                });
    }

}
