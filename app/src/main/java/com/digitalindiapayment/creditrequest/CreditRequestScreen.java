package com.digitalindiapayment.creditrequest;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.digitalindiapayment.BaseActivity;
import com.digitalindiapayment.R;
import com.digitalindiapayment.adapter.MyArrayAdapter;
import com.digitalindiapayment.pojo.BankDetailsBean;
import com.digitalindiapayment.pojo.ModeofTransfer;
import com.digitalindiapayment.util.Util;
import com.digitalindiapayment.webservices.requestpojo.CreditAPIRequest;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;


public class CreditRequestScreen extends BaseActivity implements CreditRequestContract.View {
    private static final String TAG = "CreditRequestScreen";
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.textView6)
    TextView textView6;
    @BindView(R.id.digital_india_payment_layout1)
    LinearLayout digitalIndiaPaymentLayout1;
    @BindView(R.id.layout01)
    RelativeLayout layout01;
    @BindView(R.id.layout0)
    RelativeLayout layout0;
    @BindView(R.id.account_name)
    TextView accountName;
    @BindView(R.id.bank_name)
    Spinner bankName;
    @BindView(R.id.branch_name)
    TextView branchName;
    @BindView(R.id.account_no)
    TextView accountNo;
    @BindView(R.id.ifsc_no)
    TextView ifscNo;
    @BindView(R.id.digital_india_payment_layout2)
    LinearLayout digitalIndiaPaymentLayout2;
    @BindView(R.id.request_amount)
    EditText requestAmount;
    @BindView(R.id.mode_of_transfer_spinner)
    Spinner modeOfTransferSpinner;
    @BindView(R.id.bank_account_spinner)
    Spinner bankAccountSpinner;
    @BindView(R.id.request_branch)
    EditText requestBranch;
    @BindView(R.id.request_ref_no)
    EditText requestRefNo;
    @BindView(R.id.submitt_request)
    Button submittRequest;
    @BindView(R.id.message)
    TextView message;
    @BindView(R.id.noInternetImg)
    ImageView noInternetImg;
    @BindView(R.id.noInternetMsg)
    TextView noInternetMsg;
    @BindView(R.id.refreshButton)
    ImageView refreshButton;
    @BindView(R.id.noInternetContainer)
    LinearLayout noInternetContainer;
    @BindView(R.id.progressdialogdip)
    RelativeLayout progressdialogdip;
    @BindView(R.id.exception_handle)
    RelativeLayout exceptionHandle;
    @BindView(R.id.textView7)
    TextView textView7;
    @BindView(R.id.homeWalletRequest)
    LinearLayout homeWalletRequest;


    private CreditRequestPresenter creditRequestPresenter;
    private MyArrayAdapter<BankDetailsBean> adapter, bankSpinnerAdapter;
    private MyArrayAdapter<ModeofTransfer> modeofTransferAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wallet_credit_request);
        ButterKnife.bind(this);

        noInternetContainer.setVisibility(View.GONE);
        progressdialogdip.setVisibility(View.GONE);
        exceptionHandle.setVisibility(View.GONE);
        homeWalletRequest.setVisibility(View.VISIBLE);


        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        creditRequestPresenter = new CreditRequestPresenter(this, getApplicationContext());

        adapter = new MyArrayAdapter<BankDetailsBean>(getApplicationContext(),
                R.layout.textviewforspinner_whitetext, new ArrayList<BankDetailsBean>());
        bankName.setAdapter(adapter);

        bankSpinnerAdapter = new MyArrayAdapter<BankDetailsBean>(getApplicationContext(),
                R.layout.textviewforspinner, new ArrayList<BankDetailsBean>());
        bankAccountSpinner.setAdapter(bankSpinnerAdapter);

        modeofTransferAdapter = new MyArrayAdapter<ModeofTransfer>(getApplicationContext(),
                R.layout.textviewforspinner, new ArrayList<ModeofTransfer>());
        modeOfTransferSpinner.setAdapter(modeofTransferAdapter);

        creditRequestPresenter.loadDIPLBankDetails();
        creditRequestPresenter.loadTransactionTypes();
//        creditRequestPresenter.getBanksList();

        requestRefNo.setFilters(new InputFilter[]{
                new InputFilter() {
                    public CharSequence filter(CharSequence src, int start,
                                               int end, Spanned dst, int dstart, int dend) {
                        if (src.equals("")) { // for backspace
                            return src;
                        }
                        if (src.toString().matches("[\\x00-\\x7F]+")) {
                            return src;
                        }
                        return "";
                    }
                }
        });

    }


    @Override
    protected void onResume() {
        super.onResume();
        showLoading();
        creditRequestPresenter.loadDIPLBankDetails();
        creditRequestPresenter.loadTransactionTypes();
    }

    @OnClick(R.id.refreshButton)
    public void onClickReferesh(View view) {
        showLoading();
        creditRequestPresenter.loadDIPLBankDetails();
        creditRequestPresenter.loadTransactionTypes();
    }


    @Override
    public void noInternet() {
        noInternetContainer.setVisibility(View.VISIBLE);
        homeWalletRequest.setVisibility(View.GONE);
        hideLoading();

        exceptionHandle.setVisibility(View.VISIBLE);

    }

    @Override
    public void exception(Throwable e) {
        Util.showLongToast(getApplicationContext(), e.getMessage());
        hideLoading();
    }

    @Override
    public void fail(String message) {
        Util.showLongToast(getApplicationContext(), message);
        hideLoading();
    }

    @Override
    public void success(String message) {
        Util.showLongToast(getApplicationContext(), message);
        finish();
    }

    @Override
    public void showLoading() {
        progressdialogdip.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        progressdialogdip.setVisibility(View.GONE);
    }

    @Override
    public void showDIPLBankDetails(List<BankDetailsBean> bankDetailsList) {
        homeWalletRequest.setVisibility(View.VISIBLE);
        exceptionHandle.setVisibility(View.GONE);
        adapter.clear();
        bankDetailsList.add(0, new BankDetailsBean(0, getString(R.string.selectbank)));
        adapter.addAll(bankDetailsList);
        adapter.notifyDataSetChanged();
        bankSpinnerAdapter.clear();
        bankSpinnerAdapter.addAll(bankDetailsList);
        bankSpinnerAdapter.notifyDataSetChanged();

        Log.d(TAG,"DIPL Account:"+bankDetailsList.get(0).getBankName()+" ::: "+bankDetailsList.get(0).getBankId());



    }

    @Override
    public void showTransactionTypes(List<ModeofTransfer> modes) {
        homeWalletRequest.setVisibility(View.VISIBLE);
        exceptionHandle.setVisibility(View.GONE);
        modeofTransferAdapter.clear();
        modes.add(0, new ModeofTransfer("0", getString(R.string.select_transfer_mode)));
        modeofTransferAdapter.addAll(modes);
        modeofTransferAdapter.notifyDataSetChanged();
    }

    @Override
    public void showWalletCreditRequestResult() {
        Util.showShortToast(getApplicationContext(), getString(R.string.wallet_request_submitted));
        finish();
    }


    @OnItemSelected(R.id.bank_name)
    public void bankSpinnerClick() {

        BankDetailsBean bankDetailsBean = adapter.getItem(bankName.getSelectedItemPosition());
        branchName.setText(bankDetailsBean.getBranch());
        accountNo.setText(bankDetailsBean.getAccountNo());
        ifscNo.setText(bankDetailsBean.getIfscCode());
    }

    @OnClick(R.id.submitt_request)
    public void submitCreditRequest() {
        if (requestAmount.getText().toString().equalsIgnoreCase("")) {
            Util.showShortToast(getApplicationContext(), getString(R.string.pls_enter_amount));
            return;
        }
        if (requestRefNo.getText().toString().equalsIgnoreCase("")) {
            Util.showShortToast(getApplicationContext(), getString(R.string.pls_enter_refrenceno));
            return;
        }

        if (modeOfTransferSpinner.getSelectedItemPosition() < 1) {
            Util.showShortToast(getApplicationContext(), getString(R.string.pls_select_transfermode));
            return;
        }
        if (bankAccountSpinner.getSelectedItemPosition() < 1) {
            Util.showShortToast(getApplicationContext(), getString(R.string.pls_select_bank));
            return;
        }
        if (Integer.parseInt(requestAmount.getText().toString()) < 100) {
            Util.showShortToast(getApplicationContext(), getString(R.string.min_amount_100));
            return;
        }
        if (requestBranch.getText().toString().equalsIgnoreCase("")) {
            Util.showShortToast(getApplicationContext(), getString(R.string.pls_enter_branch));
            return;
        }


        CreditAPIRequest apiRequest = new CreditAPIRequest();
        apiRequest.amount = requestAmount.getText().toString();
        apiRequest.referenceNumber = requestRefNo.getText().toString();
        apiRequest.bankBranch = requestBranch.getText().toString();
        apiRequest.bankCode = String.valueOf(bankSpinnerAdapter.getItem(bankAccountSpinner.getSelectedItemPosition()).getBankName());
        apiRequest.modeOfTransfer = modeofTransferAdapter.getItem(modeOfTransferSpinner.getSelectedItemPosition()).code;
        apiRequest.bankId = String.valueOf(bankSpinnerAdapter.getItem(bankAccountSpinner.getSelectedItemPosition()).getBankId());

        //Log.d(TAG,""+bankSpinnerAdapter.getItem(bankAccountSpinner.getSelectedItemPosition()).getBankId());

        creditRequestPresenter.submitCreditRequest(apiRequest);
    }
}
