package com.digitalindiapayment.creditrequest.CreditRequestReport;

import com.digitalindiapayment.pojo.RequestReportBean;
import com.digitalindiapayment.pojo.TransactionReportBean;
import com.digitalindiapayment.webservices.responsepojo.CreditResponseBean;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Mayur on 11-04-2017.
 */

public class CreditRequestReport {


    @SerializedName("report")

    private List<CreditResponseBean> report = null;


    public List<CreditResponseBean> getReport() {
        return report;
    }

    public void setReport(List<CreditResponseBean> report) {
        this.report = report;
    }
}
