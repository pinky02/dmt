package com.digitalindiapayment.creditrequest.CreditRequestReport;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.digitalindiapayment.R;
import com.digitalindiapayment.pojo.RequestReportBean;
import com.digitalindiapayment.webservices.responsepojo.CreditResponseBean;

import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
/**
 * Created by Mayur on 11-04-2017.
 */

public class CreditRequestReportAdapter extends RecyclerView.Adapter<CreditRequestReportAdapter.CreditRequestReportViewHolder> {

    private List<CreditResponseBean> reports = null;
    private Context context;

    public CreditRequestReportAdapter(List<CreditResponseBean> list, Context context) {
        this.context = context;
        this.reports = list;
    }

    @Override
    public CreditRequestReportViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View ItemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycle_request_report, parent, false);
        return new CreditRequestReportViewHolder(ItemView);
    }

    @Override
    public void onBindViewHolder(CreditRequestReportViewHolder holder, int position) {
        final CreditResponseBean requestReportBean = reports.get(position);
        holder.transId.setText(requestReportBean.getId());
        holder.date.setText(requestReportBean.getTimestamp());
        holder.amountWord.setText(requestReportBean.getAmount());
        holder.branch.setText(requestReportBean.getBranch());
        holder.status.setText(requestReportBean.getStatus());
        holder.referenceno.setText(requestReportBean.getReferenceNo());
        holder.modeOfTran.setText(requestReportBean.getModeOfTransfer());
    }

    @Override
    public int getItemCount() {
        return reports.size();

    }

    public void setReportList(List<CreditResponseBean> reports) {
        this.reports = reports;
        notifyDataSetChanged();
    }

    public class CreditRequestReportViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.amount_word)
        TextView amountWord;
        @BindView(R.id.branch)
        TextView branch;
        @BindView(R.id.status)
        TextView status;
        @BindView(R.id.mode_of_tran)
        TextView modeOfTran;
        @BindView(R.id.referenceno)
        TextView referenceno;
        @BindView(R.id.trans_id)
        TextView transId;
        @BindView(R.id.date)
        TextView date;

        public CreditRequestReportViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public List<CreditResponseBean> getReportList() {
            return reports;
        }
    }
}

