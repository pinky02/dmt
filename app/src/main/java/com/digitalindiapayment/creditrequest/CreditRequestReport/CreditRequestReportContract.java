package com.digitalindiapayment.creditrequest.CreditRequestReport;

import com.digitalindiapayment.mvp.BaseView;
import com.digitalindiapayment.pojo.RequestReportBean;
import com.digitalindiapayment.pojo.TransactionReportBean;
import com.digitalindiapayment.webservices.responsepojo.CreditResponseBean;

import java.util.List;

/**
 * Created by Mayur on 11-04-2017.
 */

public class CreditRequestReportContract {


    public interface View extends BaseView {
        void showTransaction(List<CreditResponseBean> RequestReportBean);
    }

    public interface UserAction {
        void generateWalletRequestReport();

    }

}
