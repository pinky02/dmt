package com.digitalindiapayment.creditrequest.CreditRequestReport;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.digitalindiapayment.R;
import com.digitalindiapayment.pojo.RequestReportBean;
import com.digitalindiapayment.webservices.responsepojo.CreditResponseBean;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;

/**
 * Created by Mayur on 11-04-2017.
 */

public class CreditRequestReportActivity extends AppCompatActivity implements CreditRequestReportContract.View {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.noInternetImg)
    ImageView noInternetImg;
    @BindView(R.id.noInternetMsg)
    TextView noInternetMsg;
    @BindView(R.id.noInternetContainer)
    LinearLayout noInternetContainer;
    @BindView(R.id.progressdialogdip)
    RelativeLayout progressdialogdip;
    @BindView(R.id.message)
    TextView message;
    @BindView(R.id.recyclerviewrequestransaction)
    RecyclerView recyclerviewrequestransaction;
    private List<CreditResponseBean> reports = new ArrayList<CreditResponseBean>();
    private CreditRequestReportAdapter creditRequestReportAdapter = null;
    private CreditRequestReportPresenter creditRequestReportPresenter= null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.request_report);
        ButterKnife.bind(this);

        noInternetContainer.setVisibility(View.GONE);
        progressdialogdip.setVisibility(View.GONE);


        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        //get data from server
        creditRequestReportPresenter = new CreditRequestReportPresenter(this, getApplicationContext());
        creditRequestReportPresenter.generateWalletRequestReport();

        //setting the adapter
        creditRequestReportAdapter = new CreditRequestReportAdapter(reports,getApplicationContext());
        RecyclerView.LayoutManager mlayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerviewrequestransaction.setLayoutManager(mlayoutManager);
        recyclerviewrequestransaction.setItemAnimator(new DefaultItemAnimator());
        recyclerviewrequestransaction.setAdapter(creditRequestReportAdapter);
    }


    @Override
    public void noInternet() {
        noInternetContainer.setVisibility(View.VISIBLE);
        progressdialogdip.setVisibility(View.GONE);
    }

    @Override
    public void exception(Throwable e) {
        hideLoading();
    }

    @Override
    public void fail(String message) {

    }

    @Override
    public void success(String message) {

    }

    @Override
    public void showLoading() {
        noInternetContainer.setVisibility(View.GONE);
        progressdialogdip.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {

        progressdialogdip.setVisibility(View.GONE);

    }

    @Override
    public void showTransaction(List<CreditResponseBean> requestReportBeen) {
        hideLoading();
        noInternetContainer.setVisibility(View.GONE);


        this.reports = requestReportBeen;

        creditRequestReportAdapter.setReportList(requestReportBeen);

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


}
