package com.digitalindiapayment.creditrequest.CreditRequestReport;

import android.content.Context;
import android.util.Log;

import com.digitalindiapayment.util.Util;
import com.digitalindiapayment.webservices.Api;
import com.digitalindiapayment.webservices.core.ApiFail;
import com.digitalindiapayment.webservices.core.ApiSuccess;
import com.digitalindiapayment.webservices.core.HttpErrorResponse;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Mayur on 11-04-2017.
 */

public class CreditRequestReportPresenter implements CreditRequestReportContract.UserAction {

    private final CreditRequestReportContract.View view;
    private final Context context;

    public CreditRequestReportPresenter(CreditRequestReportContract.View view, Context context) {
        this.view = view;
        this.context = context;
    }


    @Override
    public void generateWalletRequestReport() {
        view.showLoading();

        Log.i("Msg1", "Feteching Credit Request Report");

        Api.reports().getCreditRequestReport(Util.PREFERENCES.getToken(context))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ApiSuccess<CreditRequestReport>() {
                    @Override
                    public void call(CreditRequestReport report) {
                        view.showTransaction(report.getReport());

                    }
                }, new ApiFail() {
                    @Override
                    public void httpStatus(HttpErrorResponse response) {
                        view.hideLoading();
                    }

                    @Override
                    public void noNetworkError() {
                        view.noInternet();
                    }

                    @Override
                    public void unknownError(Throwable e) {
                        view.hideLoading();
                        view.fail(e.getMessage());
                    }
                });

        Log.i("Msg1", "Feteching Credit Request Report Completed");
    }


}




