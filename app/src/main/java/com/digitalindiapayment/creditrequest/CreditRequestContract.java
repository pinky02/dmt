package com.digitalindiapayment.creditrequest;

import com.digitalindiapayment.mvp.BaseView;
import com.digitalindiapayment.pojo.BankDetailsBean;
import com.digitalindiapayment.pojo.ModeofTransfer;
import com.digitalindiapayment.webservices.requestpojo.CreditAPIRequest;

import java.util.List;

public class CreditRequestContract {

    interface View extends BaseView {
        void showDIPLBankDetails(List<BankDetailsBean> bankDetailsList);
        void showTransactionTypes(List<ModeofTransfer> modes);
        void showWalletCreditRequestResult();
    }

    interface UserActions{
        void loadDIPLBankDetails();
        void loadTransactionTypes();
        void submitCreditRequest(CreditAPIRequest apiRequest);

    }
}
