package com.digitalindiapayment.settings;

import android.content.Context;

import com.digitalindiapayment.R;
import com.digitalindiapayment.util.Util;
import com.digitalindiapayment.webservices.Api;
import com.digitalindiapayment.webservices.core.ApiFail;
import com.digitalindiapayment.webservices.core.ApiSuccess;
import com.digitalindiapayment.webservices.core.HttpErrorResponse;
import com.digitalindiapayment.webservices.requestpojo.TicketLoginRequest;
import com.digitalindiapayment.webservices.responsepojo.TicketLoginResponse;
import com.google.firebase.messaging.FirebaseMessaging;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by admin on 13-05-2017.
 */

public class SettingsPresenter implements SettingsContract.UserAction {
    private SettingsContract.View view;
    private Context context;

    public SettingsPresenter(SettingsContract.View view, Context context) {
        this.view = view;
        this.context = context;
    }

    @Override
    public void logout() {
        //view.onLoggout(true);
        Api.userManagement().logout(Util.PREFERENCES.getToken(context))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ApiSuccess<String>() {
                    @Override
                    public void call(String stringStringMap) {
                        //UtilDmt.showShortToast(context.getApplicationContext(),stringStringMap.get(context.getString(R.string.server_reson_key)));
                        view.onLoggout(true, context.getString(R.string.user_logout_success_message));//stringStringMap.get(context.getString(R.string.server_reson_key)));

                        FirebaseMessaging.getInstance().unsubscribeFromTopic(Util.KEYS.DEFAULT_NOTIFICATION_TOPIC);

                    }
                }, new ApiFail() {
                    @Override
                    public void httpStatus(HttpErrorResponse response) {
                        view.onLoggout(false, context.getString(R.string.OP_FAILED));

                    }

                    @Override
                    public void noNetworkError() {
                        view.onLoggout(false, context.getString(R.string.OP_FAILED));
                    }

                    @Override
                    public void unknownError(Throwable e) {
                        view.onLoggout(false, context.getString(R.string.OP_FAILED));

                    }
                });


    }

    @Override
    public void submitFeedback(String feedback) {

        Api.userManagement().feedback(Util.PREFERENCES.getToken(context), feedback)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ApiSuccess<String>() {
                    @Override
                    public void call(String stringStringMap) {
                        view.onFeedbackSubmit(stringStringMap);


                    }
                }, new ApiFail() {
                    @Override
                    public void httpStatus(HttpErrorResponse response) {
                        view.onFeedbackSubmit(response.getError());

                    }

                    @Override
                    public void noNetworkError() {
                        view.onLoggout(false, context.getString(R.string.OP_FAILED));
                    }

                    @Override
                    public void unknownError(Throwable e) {
                        view.onLoggout(false, context.getString(R.string.OP_FAILED));

                    }
                });
    }


}
