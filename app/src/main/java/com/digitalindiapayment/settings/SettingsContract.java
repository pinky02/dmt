package com.digitalindiapayment.settings;

import com.digitalindiapayment.webservices.responsepojo.TicketLoginResponse;

/**
 * Created by admin on 13-05-2017.
 */

public class SettingsContract {

    interface View{
        public void onLoggout(Boolean isLogoutSuccessful,String message);
        public void onFeedbackSubmit(String message);

    }

    interface UserAction{
        public void logout();
        public void submitFeedback(String feedback);

    }
}
