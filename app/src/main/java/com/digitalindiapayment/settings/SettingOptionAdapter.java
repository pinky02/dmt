package com.digitalindiapayment.settings;

import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.digitalindiapayment.R;
import com.digitalindiapayment.pojo.SettingOption;
import com.pnikosis.materialishprogress.ProgressWheel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by admin on 12-07-2017.
 */

public class SettingOptionAdapter extends RecyclerView.Adapter<SettingOptionAdapter.MyViewHolder> {
    private static final String TAG = "SettingOptionAdapter";

    List<SettingOption> settingOptions;
    Context context;
    OnSettingOptionClick onSettingOptionClick;

    SettingOptionAdapter(List<SettingOption> settingOptions,Context context) {
        this.settingOptions = settingOptions;
        this.context = context ;
        //this.onSettingOptionClick = onSettingOptionClick;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, final int viewType) {
        Log.d(TAG,"MyViewHolder");
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.settings_option_row, parent, false);

        final MyViewHolder  myViewHolder = new MyViewHolder(itemView);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final int position = viewType ;//myViewHolder.getLayoutPosition();
                Log.d(TAG,"MyViewHolder Inside onClick position : "+position);
                settingOptions.get(position).optionClick(position);
            }
        });

        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Log.d(TAG,"onBindViewHolder");
        SettingOption option = settingOptions.get(position);

        // Set Icon
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            holder.settingIcon.setBackground(context.getDrawable(option.getIcon()));
        }else {
            holder.settingIcon.setBackground(context.getResources().getDrawable(option.getIcon()));
        }

        holder.settingTitle.setText(option.getTitle());
        holder.navigation.setVisibility(option.getHasNavigation() ? View.VISIBLE : View.INVISIBLE );

        if(option.getHasLoader()) {
            holder.progressWheel.setVisibility(option.getProgressStatus() ? View.VISIBLE : View.INVISIBLE);
        }

    }

    @Override
    public int getItemCount() {
        Log.d(TAG,"getItemCount "+settingOptions.size());
        return settingOptions.size();
    }

    @Override
    public int getItemViewType(int position) {
        //super.getItemViewType(position);

        return position;
    }

    public class MyViewHolder extends ViewHolder {

        @BindView(R.id.settingIcon)
        ImageView settingIcon;
        @BindView(R.id.settingTitle)
        TextView settingTitle;
        @BindView(R.id.navigation)
        ImageView navigation;
        @BindView(R.id.progressWheel)
        ProgressWheel progressWheel;
        @BindView(R.id.deviceConfiguration)
        RelativeLayout deviceConfiguration;

        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            Log.d(TAG,"MyViewHolder");

        }

    }
}
