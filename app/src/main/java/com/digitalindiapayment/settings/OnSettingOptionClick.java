package com.digitalindiapayment.settings;

import android.view.View;

/**
 * Created by admin on 12-07-2017.
 */

public interface OnSettingOptionClick<T> {

    public void optionClick(T data);

}
