package com.digitalindiapayment.settings;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.digitalindiapayment.BaseActivity;
import com.digitalindiapayment.R;
import com.digitalindiapayment.login.UserProfile;
import com.digitalindiapayment.pojo.SettingOption;
import com.digitalindiapayment.profile.ProfileActivity;
import com.digitalindiapayment.settings.CommonWebView.WebViewActivity;
import com.digitalindiapayment.settings.deviceConfiguration.DeviceConfiguration;
import com.digitalindiapayment.util.Util;
import com.rdservice.helper.Printer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

//import static com.digitalindiapayment.R.id.progressWheelSignOut;

//import agentapp.digitalindiapayment.com.digitalindiapayment.debug.R;

public class SettingsActivity extends BaseActivity implements SettingsContract.View {


    private static final String TAG = "SettingsActivity";
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    SettingsContract.UserAction settingPresenter;
    @BindView(R.id.version)
    TextView version;
    @BindView(R.id.settingOptions)
    RecyclerView settingOptions;

    private SettingOptionAdapter settingOptionAdapter;
    private List<SettingOption> settingOptionMenus;
    private SettingOption currentSelectedSettingOption;

    private Dialog feedbackDialogBuilder;
    private UserProfile userProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.settings);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        settingPresenter = new SettingsPresenter(SettingsActivity.this, getApplicationContext());

        if (getIntent().hasExtra(Util.KEYS.DATA))
            userProfile = getIntent().getParcelableExtra(Util.KEYS.DATA);

        version.setText(getString(R.string.verion) + " " + Util.getVersionName(getApplicationContext()));

        settingOptionMenus = new ArrayList<SettingOption>();
        settingOptionAdapter = new SettingOptionAdapter(settingOptionMenus, getApplicationContext());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        settingOptions.setLayoutManager(mLayoutManager);
        settingOptions.setItemAnimator(new DefaultItemAnimator());
        settingOptions.setAdapter(settingOptionAdapter);

        settingOptions();

    }

    public void settingOptions() {

        SettingNavigator settingNavigator = new SettingNavigator();

        //Device Configuration Option
        SettingOption deviceConfiguration = new SettingOption(getString(R.string.device_configuration), R.drawable.ic_configuration);
        deviceConfiguration.addOnOptionClickListener(settingNavigator);

        settingOptionMenus.add(deviceConfiguration);

/*
        // FAQ
        SettingOption faq = new SettingOption(getString(R.string.faq), R.drawable.ic_faq);
        faq.addOnOptionClickListener(settingNavigator);
        settingOptionMenus.add(faq);


        // Contacts
        SettingOption contacts = new SettingOption(getString(R.string.contact_us), R.drawable.ic_contact_us);
        contacts.addOnOptionClickListener(settingNavigator);
        settingOptionMenus.add(contacts);


        // Privacy Policy
        SettingOption privacyPolicy = new SettingOption(getString(R.string.privacy_policy), R.drawable.ic_police);
        privacyPolicy.addOnOptionClickListener(settingNavigator);
        settingOptionMenus.add(privacyPolicy);

        // Feedback
        SettingOption feedback = new SettingOption(getString(R.string.feedback), R.drawable.ic_feedback);
        feedback.setHasLoader(true);
        feedback.setHasNavigation(false);
        feedback.addOnOptionClickListener(new Feedback());
        settingOptionMenus.add(feedback);
*/
        //Help & Support
        SettingOption profileOption = new SettingOption(getString(R.string.profile), R.drawable.ic_profile_light);
        profileOption.setHasLoader(false);
        profileOption.setHasNavigation(true);
        profileOption.addOnOptionClickListener(new Profile());
        settingOptionMenus.add(profileOption);

        // Logout Option
        SettingOption logOut = new SettingOption(getString(R.string.action_log_out), R.drawable.ic_logout);
        logOut.setHasLoader(true);
        logOut.addOnOptionClickListener(new Logout());
        settingOptionMenus.add(logOut);

        settingOptionAdapter.notifyDataSetChanged();

    }


    @Override
    public void onLoggout(Boolean isLogoutSuccessful, String message) {
        //progressWheelSignOut.setVisibility(View.GONE);
        //logOut.setInProgress(false);
        //logOutProgress.dismiss();
        currentSelectedSettingOption.setProgressStatus(false);
        settingOptionAdapter.notifyDataSetChanged();
        Util.showShortToast(SettingsActivity.this, message);
        // clear all the shared preference
        // reset device
        // clear all the activites
        // Start the login activity

        if (isLogoutSuccessful) {
            if (Util.PREFERENCES.getDeviceName(this).equalsIgnoreCase(getString(R.string.device_evolute_name))) {
                String bluetoothName = Util.PREFERENCES.getEvoluteBluetoothName(getApplicationContext());
                String bluetoothMac = Util.PREFERENCES.getEvoluteBluetoothMac(getApplicationContext());
                disconnectEvoluteBtConn(bluetoothName, bluetoothMac);
            } else {
                Util.PREFERENCES.clearLoginSession(SettingsActivity.this);
                redirectToLogin();
            }

        }

    }


    public class Logout implements OnSettingOptionClick {
        @Override
        public void optionClick(Object index) {

            currentSelectedSettingOption = settingOptionMenus.get((Integer) index);

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SettingsActivity.this);
            alertDialogBuilder.setTitle(getString(R.string.action_log_out));
            alertDialogBuilder.setMessage(getString(R.string.logout_confirm_message)).setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // Logic to Print
                            // Loader
                            currentSelectedSettingOption.setProgressStatus(true);
                            settingOptionAdapter.notifyDataSetChanged();
                            settingPresenter.logout();
                            //new EnterTextTask).execute(0);
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    });
            alertDialogBuilder.show();
        }
    }

    public class SettingNavigator implements OnSettingOptionClick {

        private Map<String, Class> screenSelector;
        private Map<String, String> urls;

        SettingNavigator() {
            screenSelector = new HashMap<>();
            screenSelector.put(getString(R.string.device_configuration), DeviceConfiguration.class);
            screenSelector.put(getString(R.string.faq), WebViewActivity.class);
            screenSelector.put(getString(R.string.contact_us), WebViewActivity.class);
            screenSelector.put(getString(R.string.privacy_policy), WebViewActivity.class);

            urls = new HashMap<>();
            urls.put(getString(R.string.device_configuration), "");
            urls.put(getString(R.string.faq), getString(R.string.url_terms_condition));
            urls.put(getString(R.string.contact_us), getString(R.string.url_contact_us));
            urls.put(getString(R.string.privacy_policy), getString(R.string.url_privacy_policy));

        }

        @Override
        public void optionClick(Object index) {

            currentSelectedSettingOption = settingOptionMenus.get((Integer) index);

            try {
                Intent settingOptionScreen = new Intent(SettingsActivity.this, screenSelector.get(currentSelectedSettingOption.getTitle()));
                settingOptionScreen.putExtra(Util.KEYS.TITAL, currentSelectedSettingOption.getTitle());
                settingOptionScreen.putExtra(Util.KEYS.DATA, urls.get(currentSelectedSettingOption.getTitle()));
                startActivity(settingOptionScreen);
            } catch (Exception e) {
                Log.d(TAG, "SettingNavigator optionClick " + e.toString());
            }
        }
    }

    @Override
    public void onFeedbackSubmit(String message) {
        feedbackDialogBuilder.dismiss();
        Util.showShortToast(SettingsActivity.this, message);
    }


    public class Feedback implements OnSettingOptionClick {

        @BindView(R.id.etFeedback)
        EditText etFeedback;
        @BindView(R.id.wordCount)
        TextView wordCount;
        @BindView(R.id.submit)
        Button submit;

        @Override
        public void optionClick(Object index) {
            currentSelectedSettingOption = settingOptionMenus.get((Integer) index);

            feedbackDialogBuilder = new Dialog(SettingsActivity.this);
            feedbackDialogBuilder.setContentView(R.layout.layout_feedback);
            ButterKnife.bind(this, feedbackDialogBuilder);

            Window window = feedbackDialogBuilder.getWindow();
            window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);


            final int limit = Integer.parseInt(getString(R.string.feedback_limit));

            etFeedback.setFilters(new InputFilter[]{new InputFilter.LengthFilter(limit)});

            submit.setEnabled(false);
            etFeedback.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    wordCount.setText("" + (limit - s.length()));
                    if (s.length() < 0) {
                        submit.setEnabled(false);
                    } else {
                        submit.setEnabled(true);
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });

            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    submit.setText(getString(R.string.processing));
                    feedbackDialogBuilder.setCancelable(false);
                    settingPresenter.submitFeedback(etFeedback.getText().toString());
                }
            });

            feedbackDialogBuilder.show();
        }
    }


    private class Profile implements OnSettingOptionClick {
        @Override
        public void optionClick(Object data) {
            currentSelectedSettingOption = settingOptionMenus.get((Integer) data);
            settingOptionAdapter.notifyDataSetChanged();
            Intent intent = new Intent(SettingsActivity.this, ProfileActivity.class);
            intent.putExtra(Util.KEYS.DATA, userProfile);
            startActivity(intent);
        }
    }


    public void disconnectEvoluteBtConn(String bluetoothName, String bluetoothMac) {
        try {
            Printer ptr = new Printer(Util.isBluetoothConnected);
            String value = ptr.sDisconnect("dipl", bluetoothName, bluetoothMac);
            Intent act = new Intent(Printer.URI);
            act.putExtra(Printer.DataTAG, value);
            PackageManager packageManager = getPackageManager();
            List activities = packageManager.queryIntentActivities(act, PackageManager.MATCH_DEFAULT_ONLY);
            final boolean isIntentSafe = activities.size() > 0;
            Log.d(TAG, "Boolean check for activities = " + isIntentSafe);
            if (!isIntentSafe) {
                Toast.makeText(getApplicationContext(), "No RD Service Available", Toast.LENGTH_SHORT).show();
            }
            Log.d(TAG, "No of activities = " + activities.size());
            Log.e(TAG, "**** After sending parcelable class via intent");
            startActivityForResult(act, 102);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 102 && resultCode == Activity.RESULT_OK) {
            Util.PREFERENCES.clearLoginSession(SettingsActivity.this);
            redirectToLogin();
        }

    }
}
