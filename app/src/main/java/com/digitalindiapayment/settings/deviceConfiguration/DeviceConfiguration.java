package com.digitalindiapayment.settings.deviceConfiguration;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.digitalindiapayment.BuildConfig;
import com.digitalindiapayment.R;
import com.digitalindiapayment.application.MyApplication;
import com.digitalindiapayment.util.Util;
import com.morpho.sample.KeyFile;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;


public class DeviceConfiguration extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.radioBtnMorpho)
    RadioButton radioBtnMorpho;
    @BindView(R.id.radioBtnEvolute)
    RadioButton radioBtnEvolute;
    @BindView(R.id.radioBtnHemen)
    RadioButton radioBtnHemen;
    @BindView(R.id.radioDevice)
    RadioGroup radioDevice;
    @BindView(R.id.scanDevices)
    LinearLayout scanDevices;
    @BindView(R.id.radioBtnEvolutePrinter)
    RadioButton radioBtnEvolutePrinter;
    @BindView(R.id.radioBtnHemenPrinter)
    RadioButton radioBtnHemenPrinter;
    @BindView(R.id.radioBtnNGXPrinter)
    RadioButton radioBtnNGXPrinter;
    @BindView(R.id.radioDevicePrinter)
    RadioGroup radioDevicePrinter;
    @BindView(R.id.printDevice)
    LinearLayout printDevice;
    @BindView(R.id.radioBtnStartek)
    RadioButton radioBtnStartek;
    @BindView(R.id.radioBtnMorphoL0S)
    RadioButton radioBtnMorphoL0S;

    Map<String, Integer> radioScanner;
    Map<String, Integer> radioPrinter;
    @BindView(R.id.radioBtnDigitalPersona)
    RadioButton radioBtnDigitalPersona;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_configuration);
        ButterKnife.bind(this);


        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getIntent().getExtras().getString(Util.KEYS.TITAL));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        if (!Util.PREFERENCES.isRDEnabled(this)) {
            RadioButton selectedRadioBtn;
            selectedRadioBtn = (RadioButton) findViewById(R.id.radioBtnMantra);
            selectedRadioBtn.setVisibility(View.GONE);
        }

        if (BuildConfig.DEBUG) {
            scanDevices.setVisibility(View.VISIBLE);
        }

        // Scanner
        radioScanner = new HashMap();
        radioScanner.put(getString(R.string.device_morpho_name), R.id.radioBtnMorpho);
        radioScanner.put(getString(R.string.device_evolute_name), R.id.radioBtnEvolute);
        radioScanner.put(getString(R.string.device_hemen_name), R.id.radioBtnHemen);
        radioScanner.put(getString(R.string.device_mantra_name), R.id.radioBtnMantra);
        radioScanner.put(getString(R.string.device_digital_persona_name), R.id.radioBtnDigitalPersona);
        radioScanner.put(getString(R.string.device_startek_name), R.id.radioBtnStartek);
        radioScanner.put(getString(R.string.device_morphol0s_name),R.id.radioBtnMorphoL0S);

        // Printer
        radioPrinter = new HashMap();
        radioPrinter.put(getString(R.string.device_hemen_printer), R.id.radioBtnHemenPrinter);
        radioPrinter.put(getString(R.string.device_evolute_printer), R.id.radioBtnEvolutePrinter);
        radioPrinter.put(getString(R.string.device_ngx_printer), R.id.radioBtnNGXPrinter);
        //seetingPresenter = new SettingsPresenter(SettingsActivity.this, getApplicationContext());


        String selectedDeviceName = Util.PREFERENCES.getDeviceName(DeviceConfiguration.this);
        if (!selectedDeviceName.equals("")) {
            RadioButton selectedRadioBtn = (RadioButton) findViewById(radioScanner.get(selectedDeviceName));
            selectedRadioBtn.setChecked(true);
        }

        String selectedPrinterName = Util.PREFERENCES.getDefaultPrinter(DeviceConfiguration.this);

        if (!selectedPrinterName.equals("")) {
            RadioButton selectedRadioBtn;
            selectedRadioBtn = (RadioButton) findViewById(radioPrinter.get(selectedPrinterName));
            selectedRadioBtn.setChecked(true);
        }


        radioDevice.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {


                RadioButton rb = (RadioButton) findViewById(checkedId);
                Util.PREFERENCES.setDeviceName(DeviceConfiguration.this, rb.getText().toString());
                ((MyApplication) getApplicationContext()).closeConn();
                unablePrinter(rb.getText().toString());
                KeyFile.setDevise(getApplicationContext(), "");

                // If i select Morpho it can select NGX and Evolute and hemen

                // If Evolute -> Evolute


                //UtilDmt.showLongToast(SettingsActivity.this, "You have selected default device as " + UtilDmt.PREFERENCES.getDeviceName(SettingsActivity.this));
            }
        });


        // Default Printer Name
        radioDevicePrinter.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {


                RadioButton rb = (RadioButton) findViewById(checkedId);
                Util.PREFERENCES.setDefaultPrinter(DeviceConfiguration.this, rb.getText().toString());

                ((MyApplication) getApplicationContext()).closeConn();


                //UtilDmt.showLongToast(SettingsActivity.this, "You have selected default device as " + UtilDmt.PREFERENCES.getDeviceName(SettingsActivity.this));

            }
        });


        unablePrinter(selectedDeviceName);

    }

    void unablePrinter(String scannerName) {


        // If Scanner is Evolute > Printer is also Evolute
        if (getString(R.string.device_evolute_name).equals(scannerName)) {


            radioBtnEvolutePrinter.setEnabled(true);
            radioBtnHemenPrinter.setEnabled(false);
            radioBtnNGXPrinter.setEnabled(false);

            radioBtnEvolutePrinter.setChecked(true);
            // If Scanner is Morpho and matra all Printer are enable
        } else if (getString(R.string.device_morpho_name).equals(scannerName) || getString(R.string.device_mantra_name).equals(scannerName)) {

            radioBtnHemenPrinter.setEnabled(true);
            radioBtnNGXPrinter.setEnabled(true);
            radioBtnEvolutePrinter.setEnabled(true);

            //radioBtnEvolutePrinter.setChecked(true);


        } else if (getString(R.string.device_hemen_name).equals(scannerName)) {
            // if hemen then only hemen is selected
            radioBtnEvolutePrinter.setEnabled(false);
            radioBtnHemenPrinter.setEnabled(true);
            radioBtnNGXPrinter.setEnabled(false);
            radioBtnHemenPrinter.setChecked(true);

        }

    }


}
