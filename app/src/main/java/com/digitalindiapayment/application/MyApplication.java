package com.digitalindiapayment.application;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.util.Log;
import com.crashlytics.android.Crashlytics;
import com.digitalindiapayment.morphoScanConnect.scannerdevice.Device;
import com.esys.impressivedemo.ImpressSetup;
import com.esys.impressivedemo.bluetooth.BluetoothComm;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.impress.api.Setup;
import com.digitalindiapayment.R;

import io.fabric.sdk.android.Fabric;

/**
 * Created by Aditya on 17-03-2017.
 */

public class MyApplication extends Application {
    /**
     * Bluetooth communication connection object
     */
    public BluetoothComm mBTcomm = null;
    private static GoogleAnalytics sAnalytics;
    private static Tracker sTracker;
    public static Device connectedDevice;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        sAnalytics = GoogleAnalytics.getInstance(this);
    }

    synchronized public Tracker getDefaultTracker() {
        // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
        if (sTracker == null) {
            sTracker = sAnalytics.newTracker(R.xml.global_tracker);
        }

        return sTracker;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }


    /**
     * Set up a Bluetooth connection
     *
     * @param sMac Bluetooth hardware address
     * @return Boolean
     */
    public boolean createConn(String sMac) {
        Log.e("Connect", "Create Connection");
        if (null == this.mBTcomm) {
            this.mBTcomm = new BluetoothComm(sMac);
            if (this.mBTcomm.createConn()) {
                return true;
            } else {
                this.mBTcomm = null;
                return false;
            }
        } else
            return true;
    }

    /**
     * Close and release the connection
     *
     * @return void
     */
    public void closeConn() {

        try {
            BluetoothComm.mosOut = null;
            BluetoothComm.misIn = null;
            ImpressSetup.nullify();
        } catch (NullPointerException e) {
        }
        if (null != this.mBTcomm) {
            this.mBTcomm.closeConn();
            this.mBTcomm = null;
        }
    }

    public Boolean isBluetoothDeviceConnected() {
        if (mBTcomm != null) {
            return mBTcomm.isConnect();
        }
        return false;
    }

}