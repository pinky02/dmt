package com.digitalindiapayment.help_support;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.digitalindiapayment.R;

import butterknife.BindView;
import butterknife.ButterKnife;


public class FullImageActivity extends AppCompatActivity {

    @BindView(R.id.imgView)
    ImageView imgView;
    private Uri imgUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_image);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        if (intent.hasExtra("imageUri")) {
            imgUri= intent.getParcelableExtra("imageUri");
        }
        if (imgUri != null) {
            imgView.setImageURI(imgUri);
        }

    }
}
