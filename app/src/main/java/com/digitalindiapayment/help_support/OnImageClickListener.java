package com.digitalindiapayment.help_support;

/**
 * Created by Nivrutti Pawar on 18-11-2017.
 */

public interface OnImageClickListener {
    void exception(Throwable e);

    void fail(String message);

    void onImageClick(int position);

    void onDeleteImage(int position);
}
