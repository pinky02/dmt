package com.digitalindiapayment.help_support.askquery;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.digitalindiapayment.R;
import com.digitalindiapayment.help_support.FullImageActivity;
import com.digitalindiapayment.help_support.ImageAdapter;
import com.digitalindiapayment.help_support.OnImageClickListener;
import com.digitalindiapayment.util.Util;
import com.digitalindiapayment.webservices.requestpojo.CreateTicketRequest;
import com.digitalindiapayment.webservices.requestpojo.ProblemType;
import com.digitalindiapayment.webservices.requestpojo.Project;
import com.digitalindiapayment.webservices.requestpojo.PublicReply;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;


public class AskQueryActivity extends AppCompatActivity implements OnImageClickListener, AskQueryContract.View, View.OnClickListener {

    private static final int RESULT_LOAD_IMAGE = 100;
    private static final int RESULT_CAMERA_IMAGE = 101;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;
    @BindView(R.id.tvProblem)
    TextView tvProblem;
    @BindView(R.id.edtTxtn)
    EditText edtTxtn;
    @BindView(R.id.edtDesc)
    EditText edtDesc;
    @BindView(R.id.imgCamera)
    ImageView imgCamera;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.noInternetImg)
    ImageView noInternetImg;
    @BindView(R.id.noInternetMsg)
    TextView noInternetMsg;
    @BindView(R.id.refreshButton)
    ImageView refreshButton;
    @BindView(R.id.noInternetContainer)
    LinearLayout noInternetContainer;
    @BindView(R.id.progressbar)
    MaterialProgressBar progressbar;
    @BindView(R.id.message)
    TextView message;
    @BindView(R.id.progressdialogdip)
    RelativeLayout progressdialogdip;
    @BindView(R.id.relativeMsg)
    RelativeLayout relativeMsg;
    @BindView(R.id.scrollView)
    ScrollView scrollView;
    @BindView(R.id.tvTxnIdMsg)
    TextView tvTxnIdMsg;

    List<Uri> mArrayUri = null;
    private String[] spinnerArray;
    private ArrayAdapter<String> spinnerAdapter;
    private int PERMISSION = 500;
    private GridLayoutManager mLayoutManager;
    private ImageAdapter mAdapter;
    private String problem, problemId;

    private AskQueryContract.UserAction presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ask_query);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.ask_query);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mLayoutManager = new GridLayoutManager(this, 3);
        recyclerView.setLayoutManager(mLayoutManager);

        presenter = new AskQueryPresenter(getApplicationContext(), AskQueryActivity.this);
        Intent intent = getIntent();
        if (intent.hasExtra(Util.KEYS.PROBLEM)) {
            problem = intent.getStringExtra(Util.KEYS.PROBLEM);
            problemId = intent.getStringExtra(Util.KEYS.PROBLEM_ID);
            if (problemId.equalsIgnoreCase("5a588f7ff8ae915374ab8427"))
                tvTxnIdMsg.setVisibility(View.VISIBLE);
            tvProblem.setText(problem);
        }

        btnSubmit.setOnClickListener(this);

    }


    private void openImageIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), RESULT_LOAD_IMAGE);
    }

    private void openCameraIntent() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, RESULT_CAMERA_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == RESULT_LOAD_IMAGE) {
                if (data.getData() != null) {

                    Uri mImageUri = data.getData();
                    mArrayUri = new ArrayList<>();
                    mArrayUri.add(mImageUri);

                } else {
                    if (data.getClipData() != null) {
                        ClipData mClipData = data.getClipData();
                        mArrayUri = new ArrayList<Uri>();
                        for (int i = 0; i < mClipData.getItemCount(); i++) {
                            ClipData.Item item = mClipData.getItemAt(i);
                            Uri uri = item.getUri();
                            mArrayUri.add(uri);
                        }
                        Log.v("LOG_TAG", "Selected Images" + mArrayUri.size());
                    }
                }


            }

            if (requestCode == RESULT_CAMERA_IMAGE) {

                try {
                    Bitmap bitmapImage = (Bitmap) data.getExtras().get("data");
                    Uri uri = getImageUri(getApplicationContext(), bitmapImage);
                    if (uri != null) {
                        mArrayUri = new ArrayList<>();
                        mArrayUri.add(uri);

                    } else {
                        Toast.makeText(this, "Error while capturing Image", Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    Toast.makeText(this, "Error while capturing Image", Toast.LENGTH_LONG).show();
                }
            }

            if (mArrayUri != null) {
                mAdapter = new ImageAdapter(mArrayUri, AskQueryActivity.this);
                recyclerView.setAdapter(mAdapter);
            }

        }
    }

    private void requestCameraStoragePermission() {

        int hasCameraPermission = 0;
        int hasStoragePermission = 0;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            hasCameraPermission = checkSelfPermission(Manifest.permission.CAMERA);
            hasStoragePermission = checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE);
        }

        if (hasCameraPermission != PackageManager.PERMISSION_GRANTED || hasStoragePermission != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION);
        } else {
            openCameraIntent();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                requestCameraStoragePermission();
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (shouldShowRequestPermissionRationale(permissions[0])) {
                        requestCameraStoragePermission();
                    } else showDenyAlert();
                }
            }
        }
    }

    protected void showDenyAlert() {
        //todo  open app setting for permission (not again deny)
        String title = "You have denied Permissions";
        String msg = "You need to allow the camera and storage Permissions to capture photo";

        android.support.v7.app.AlertDialog.Builder alertBuilder = new android.support.v7.app.AlertDialog.Builder(this);
        alertBuilder.setCancelable(true);
        alertBuilder.setTitle(Html.fromHtml(title));
        alertBuilder.setMessage(Html.fromHtml(msg));
        alertBuilder.setPositiveButton("Allow", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                intent.addCategory(Intent.CATEGORY_DEFAULT);
                intent.setData(Uri.parse("package:" + getPackageName()));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                startActivity(intent);
            }
        });

        android.support.v7.app.AlertDialog alert = alertBuilder.create();
        if (!alert.isShowing())
            alert.show();
    }

    @OnClick(R.id.imgCamera)
    public void getImage() {
        final String[] items = new String[]{getString(R.string.select_camera), getString(R.string.select_galaery)};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.select_dialog_item, items);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.select_image));
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) { //pick from camera
                if (item == 0) {
                    requestCameraStoragePermission();
                } else {
                    openImageIntent();
                }
            }
        });
        final AlertDialog dialog = builder.create();
        dialog.show();
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }


    @Override
    public void onImageClick(int position) {
        Intent intent = new Intent(this, FullImageActivity.class);
        Uri uri = mArrayUri.get(position);
        intent.putExtra("imageUri", uri);
        startActivity(intent);
    }

    @Override
    public void onDeleteImage(int position) {
        mArrayUri.remove(position);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void noInternet() {
        scrollView.setVisibility(View.GONE);
        relativeMsg.setVisibility(View.VISIBLE);
        noInternetContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void exception(Throwable e) {
        btnSubmit.setEnabled(true);
        btnSubmit.setText(getString(R.string.submit));
        Util.showShortToast(getApplicationContext(), e.getMessage());

    }

    @Override
    public void fail(String message) {
        btnSubmit.setEnabled(true);
        btnSubmit.setText(getString(R.string.submit));
        Util.showShortToast(getApplicationContext(), message);

    }

    @Override
    public void success(String message) {

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }


    @Override
    public void createTicketSuccess(String commentId) {
        hideSoftKeyboard();

        if (mArrayUri != null && mArrayUri.size() > 0)
            presenter.uploadImages(commentId, mArrayUri);
        else {
            Util.showShortToast(getApplicationContext(), "Ticket Created sucessfully");
            btnSubmit.setEnabled(true);
            btnSubmit.setText(getString(R.string.submit));
            edtTxtn.setText("");
            edtDesc.setText("");
            finish();
        }
    }

    @Override
    public void createTicketsFail() {

    }

    @Override
    public void uploadImagesSucess() {
        Util.showShortToast(getApplicationContext(), "Ticket Created sucessfully");
        btnSubmit.setEnabled(true);
        btnSubmit.setText(getString(R.string.submit));
        edtTxtn.setText("");
        edtDesc.setText("");
        finish();
    }

    @Override
    public void onClick(View view) {

        if (validate()) {
            CreateTicketRequest ticketRequest = new CreateTicketRequest();
            String subject = tvProblem.getText().toString();


            ticketRequest.setSubject(subject);
            ProblemType type = new ProblemType();
            PublicReply reply = new PublicReply();
            Project project = new Project();
            project.setName(getString(R.string.project_name));
            project.setCode(getString(R.string.project_code));
            reply.setBody(edtDesc.getText().toString());
            type.setCode(problemId);
            ticketRequest.setType(type);
            ticketRequest.setPublicReply(reply);
            ticketRequest.setProject(project);
            btnSubmit.setEnabled(false);
            btnSubmit.setText(getString(R.string.submitting_process));
            presenter.createTicket(ticketRequest);
        }

    }

    private boolean validate() {
        String problem = tvProblem.getText().toString();
        String desc = edtDesc.getText().toString().trim();
        if (problem.isEmpty()) {
            Util.showShortToast(getApplicationContext(), "Please select your problem");
            return false;
        } else if (desc.isEmpty()) {
            Util.showShortToast(getApplicationContext(), "Description cannot be empty");
            edtDesc.requestFocus();
            return false;
        } else {
            return true;
        }
    }

    @OnClick(R.id.refreshButton)
    public void onViewClicked() {
        scrollView.setVisibility(View.VISIBLE);
        relativeMsg.setVisibility(View.GONE);
        noInternetContainer.setVisibility(View.GONE);
        btnSubmit.setEnabled(true);
        btnSubmit.setText(getString(R.string.submit));
    }

    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        hideSoftKeyboard();
    }
}
