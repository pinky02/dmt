package com.digitalindiapayment.help_support.askquery;

import android.net.Uri;

import com.digitalindiapayment.mvp.BaseView;
import com.digitalindiapayment.webservices.requestpojo.CreateTicketRequest;

import java.util.List;

/**
 * Created by Nivrutti Pawar on 27-11-2017.
 */

public class AskQueryContract {

    public interface View extends BaseView {
        void createTicketSuccess(String commentId);

        void createTicketsFail();

        void uploadImagesSucess();
    }

    public interface UserAction {
        void createTicket(CreateTicketRequest ticketRequest);

        void uploadImages(String commentId, List<Uri> imageArray);
    }

}
