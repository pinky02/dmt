package com.digitalindiapayment.help_support.askquery;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.OpenableColumns;
import android.util.Log;

import com.digitalindiapayment.R;
import com.digitalindiapayment.util.Util;
import com.digitalindiapayment.webservices.Api;
import com.digitalindiapayment.webservices.core.ApiFail;
import com.digitalindiapayment.webservices.core.ApiSuccess;
import com.digitalindiapayment.webservices.core.HttpErrorResponse;
import com.digitalindiapayment.webservices.requestpojo.CreateTicketRequest;
import com.digitalindiapayment.webservices.responsepojo.CreateTicketResponse;
import com.digitalindiapayment.webservices.responsepojo.UploadImageResponse;

import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Nivrutti Pawar on 27-11-2017.
 */

public class AskQueryPresenter implements AskQueryContract.UserAction {

    Context context;
    AskQueryContract.View view;

    public AskQueryPresenter(Context context, AskQueryContract.View view) {
        this.context = context;
        this.view = view;
    }

    @Override
    public void createTicket(CreateTicketRequest ticketRequest) {
        String token = Util.PREFERENCES.getTicketToken(context);
        String appInstance = Util.PREFERENCES.getAppInstanceCode(context);
        Api.getTicket().createTicket(token, appInstance, ticketRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ApiSuccess<CreateTicketResponse>() {
                    @Override
                    public void call(CreateTicketResponse createTicketResponse) {
                        if (createTicketResponse.getStatus().equalsIgnoreCase(Util.KEYS.SUCCESS))
                            view.createTicketSuccess(createTicketResponse.getResponse().getCode());
                        else
                            view.fail(context.getString(R.string.someting_wrong));
                    }
                }, new ApiFail() {
                    @Override
                    public void httpStatus(HttpErrorResponse response) {
                        String message = response.getError();
                        view.fail(message);
                    }

                    @Override
                    public void noNetworkError() {
                        view.noInternet();
                    }

                    @Override
                    public void unknownError(Throwable e) {
                        view.fail(context.getString(R.string.error));
                    }
                });
    }

    @Override
    public void uploadImages(String commentId, List<Uri> imageArray) {
        String token = Util.PREFERENCES.getTicketToken(context);
        String appInstance = Util.PREFERENCES.getAppInstanceCode(context);
        MultipartBody.Part[] attachmentImages = new MultipartBody.Part[imageArray.size()];

        // RequestBody token = RequestBody.create(MediaType.parse("text/plain"),UtilDmt.PREFERENCES.getTicketToken(context));
        //RequestBody appInstance = RequestBody.create(MediaType.parse("text/plain"),UtilDmt.PREFERENCES.getAppInstanceCode(context));

        for (int i = 0; i < imageArray.size(); i++) {

            String file = getFileName(imageArray.get(i));


            // File file = new File(imageArray.get(i).getPath());
            RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), file);
            attachmentImages[i] = MultipartBody.Part.createFormData("attachments", file, requestBody);
            Log.e("okhttp", "" + attachmentImages[i].toString());
            Log.e("okhttp", "" + file);


        }

            Api.getTicket().uploadImages(appInstance, token, commentId, attachmentImages)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new ApiSuccess<UploadImageResponse>() {
                        @Override
                        public void call(UploadImageResponse uploadImageResponse) {
                            if (uploadImageResponse.getStatus().equalsIgnoreCase("success"))
                                view.uploadImagesSucess();
                            else
                                view.fail(uploadImageResponse.getError());
                        }
                    }, new ApiFail() {
                        @Override
                        public void httpStatus(HttpErrorResponse response) {
                            String message = response.getError();
                            view.fail(message);
                        }

                        @Override
                        public void noNetworkError() {
                            view.noInternet();
                        }

                        @Override
                        public void unknownError(Throwable e) {
                            view.fail(context.getString(R.string.error));
                        }
                    });


    }

    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }
}
