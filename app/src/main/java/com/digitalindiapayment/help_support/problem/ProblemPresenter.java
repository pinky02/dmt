package com.digitalindiapayment.help_support.problem;

import android.content.Context;

import com.digitalindiapayment.R;
import com.digitalindiapayment.util.Util;
import com.digitalindiapayment.webservices.Api;
import com.digitalindiapayment.webservices.core.ApiFail;
import com.digitalindiapayment.webservices.core.ApiSuccess;
import com.digitalindiapayment.webservices.core.HttpErrorResponse;
import com.digitalindiapayment.webservices.responsepojo.GetAllProblems;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Nivrutti Pawar on 2017/12/09.
 */

public class ProblemPresenter implements ProblemContract.UserAction {
    Context context;
    ProblemContract.View view;

    public ProblemPresenter(Context context, ProblemContract.View view) {
        this.context = context;
        this.view = view;
    }

    @Override
    public void getProblems(String projectId) {
        String token = Util.PREFERENCES.getTicketToken(context);
        String appInstance = Util.PREFERENCES.getAppInstanceCode(context);
        String agentId = Util.PREFERENCES.getAgentUserId(context);
        Api.getTicket().getProblems(token, appInstance, agentId,projectId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ApiSuccess<GetAllProblems>() {
                    @Override
                    public void call(GetAllProblems getAllProblems) {
                        if (getAllProblems.getStatus().equalsIgnoreCase(Util.KEYS.SUCCESS))
                            view.getAllProblemSuccess(getAllProblems.getTypeLists().get(0).getTypes());
                        else
                            view.fail("Data not present");
                    }
                }, new ApiFail() {
                    @Override
                    public void httpStatus(HttpErrorResponse response) {
                        String message = response.getError();
                        view.fail(message);
                    }

                    @Override
                    public void noNetworkError() {
                        view.noInternet();
                    }

                    @Override
                    public void unknownError(Throwable e) {
                        view.fail(context.getString(R.string.error));
                    }
                });
    }
}
