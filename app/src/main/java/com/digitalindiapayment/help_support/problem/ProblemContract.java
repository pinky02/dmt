package com.digitalindiapayment.help_support.problem;

import com.digitalindiapayment.mvp.BaseView;
import com.digitalindiapayment.webservices.responsepojo.IssueType;
import com.digitalindiapayment.webservices.responsepojo.TypeList;

import java.util.List;

/**
 * Created by Nivrutti Pawar on 2017/12/09.
 */

public class ProblemContract {
    public interface View extends BaseView {
        public void getAllProblemSuccess(List<IssueType> typeList);

    }

    public interface UserAction {
        public void getProblems(String projectId);
    }
}
