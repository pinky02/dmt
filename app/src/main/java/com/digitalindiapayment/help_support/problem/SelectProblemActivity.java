package com.digitalindiapayment.help_support.problem;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.digitalindiapayment.R;
import com.digitalindiapayment.help_support.OnItemClick;
import com.digitalindiapayment.help_support.askquery.AskQueryActivity;
import com.digitalindiapayment.util.Util;
import com.digitalindiapayment.webservices.responsepojo.IssueType;
import com.digitalindiapayment.webservices.responsepojo.TypeList;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;

public class SelectProblemActivity extends AppCompatActivity implements ProblemContract.View, OnItemClick {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.noInternetImg)
    ImageView noInternetImg;
    @BindView(R.id.noInternetMsg)
    TextView noInternetMsg;
    @BindView(R.id.refreshButton)
    ImageView refreshButton;
    @BindView(R.id.noInternetContainer)
    LinearLayout noInternetContainer;
    @BindView(R.id.progressbar)
    MaterialProgressBar progressbar;
    @BindView(R.id.message)
    TextView message;
    @BindView(R.id.progressdialogdip)
    RelativeLayout progressdialogdip;
    @BindView(R.id.tvNoData)
    TextView tvNoData;
    @BindView(R.id.relativeMsg)
    RelativeLayout relativeMsg;

    private LinearLayoutManager mLayoutManager;
    private ProblemAdapter mAdapter;

    private ProblemContract.UserAction presenter;
    private List<IssueType> typeList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_problem);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.select_problem);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        presenter = new ProblemPresenter(getApplicationContext(), SelectProblemActivity.this);
        mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
    }

    @Override
    protected void onResume() {
        super.onResume();
        showLoading();
        presenter.getProblems(getString(R.string.project_code));
    }

    @Override
    public void noInternet() {
        hideLoading();
        recyclerView.setVisibility(View.GONE);
        relativeMsg.setVisibility(View.VISIBLE);
        noInternetContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void exception(Throwable e) {
        hideLoading();
        Util.showShortToast(getApplicationContext(), e.getMessage());
    }

    @Override
    public void fail(String message) {
        hideLoading();
        Util.showShortToast(getApplicationContext(), message);
    }

    @Override
    public void success(String message) {

    }

    @Override
    public void showLoading() {
        relativeMsg.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
        progressdialogdip.setVisibility(View.VISIBLE);
        noInternetContainer.setVisibility(View.GONE);


    }

    @Override
    public void hideLoading() {
        progressdialogdip.setVisibility(View.GONE);
    }

    @Override
    public void getAllProblemSuccess(List<IssueType> typeList) {
        hideLoading();
        if (typeList!=null && typeList.size() > 0) {
            recyclerView.setVisibility(View.VISIBLE);
            tvNoData.setVisibility(View.GONE);
            this.typeList = typeList;
            mAdapter = new ProblemAdapter(this, typeList, this);
            recyclerView.setAdapter(mAdapter);
        }else {
            //empty list
            recyclerView.setVisibility(View.GONE);
            tvNoData.setVisibility(View.VISIBLE);

        }
    }

    @Override
    public void onRowClick(int position) {
        Intent intent = new Intent(this, AskQueryActivity.class);
        intent.putExtra(Util.KEYS.PROBLEM_ID, typeList.get(position).getCode());
        intent.putExtra(Util.KEYS.PROBLEM, typeList.get(position).getName());
        startActivity(intent);
    }

    @OnClick(R.id.refreshButton)
    public void onViewClicked() {
        showLoading();
        presenter.getProblems(getString(R.string.project_code));
    }

}
