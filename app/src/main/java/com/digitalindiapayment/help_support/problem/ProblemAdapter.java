package com.digitalindiapayment.help_support.problem;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.digitalindiapayment.R;
import com.digitalindiapayment.help_support.OnItemClick;
import com.digitalindiapayment.webservices.responsepojo.IssueType;
import com.digitalindiapayment.webservices.responsepojo.TypeList;

import java.util.List;

/**
 * Created by Nivrutti Pawar on 2017/12/09.
 */

public class ProblemAdapter extends RecyclerView.Adapter<ProblemAdapter.ViewHolder> {
    private Context mContext;
    private List<IssueType> typeList;
    private OnItemClick onItemClick;

    public ProblemAdapter(Context mContext, List<IssueType> typeList, OnItemClick onItemClick) {
        this.mContext = mContext;
        this.typeList = typeList;
        this.onItemClick = onItemClick;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_problem, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        holder.tvProblem.setText(typeList.get(position).getName());
        holder.linearMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClick.onRowClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return typeList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout linearMain;
        private TextView tvProblem;

        public ViewHolder(View itemView) {
            super(itemView);
            linearMain = (LinearLayout) itemView.findViewById(R.id.linearMain);
            tvProblem = (TextView) itemView.findViewById(R.id.tvProblem);
        }
    }
}
