package com.digitalindiapayment.help_support;

import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.digitalindiapayment.R;

import java.util.List;

/**
 * Created by Nivrutti Pawar on 14-11-2017.
 */

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ViewHolder> {
    private List<Uri> imageUri;
    private OnImageClickListener itemClick;

    public ImageAdapter(List<Uri> imageUri,OnImageClickListener itemClick) {
        this.imageUri = imageUri;
        this.itemClick=itemClick;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_image_adapter, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        if (imageUri != null && imageUri.size() > 0) {
            holder.imgView.setImageURI(imageUri.get(position));

        }
        holder.imgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClick.onImageClick(position);
            }
        });
        holder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClick.onDeleteImage(position);
            }
        });


    }

    @Override
    public int getItemCount() {
        return imageUri.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imgView,imgDelete;

        public ViewHolder(View itemView) {
            super(itemView);
            imgView=(ImageView)itemView.findViewById(R.id.imgView);
            imgDelete=(ImageView)itemView.findViewById(R.id.imgDelete);
        }
    }
}
