package com.digitalindiapayment.help_support.querydetails;

import com.digitalindiapayment.mvp.BaseView;
import com.digitalindiapayment.webservices.requestpojo.TicketUpdateRequest;
import com.digitalindiapayment.webservices.responsepojo.CommentList;
import com.digitalindiapayment.webservices.responsepojo.ConversationRes;
import com.digitalindiapayment.webservices.responsepojo.GetConversation;
import com.digitalindiapayment.webservices.responsepojo.GetTicketResponse;
import com.digitalindiapayment.webservices.responsepojo.TicketDetail;

import java.util.List;

/**
 * Created by Nivrutti Pawar on 27-11-2017.
 */

public class QueryDetailsContract {

    public interface View extends BaseView {
        public void ticketSuccess(GetTicketResponse ticketListResponse);

        public void ticketFail();

        public void updateTicketSuccess(ConversationRes conversationRes);

        public void updateTicketFail();

        void conversationSucess(List<CommentList> conversationList);

        void conversationFailed(String msg);

    }

    public interface UserAction {
        public void getTicket(String ticketCode);

        public void updateTicket(String ticketCode, TicketUpdateRequest updateRequest);

        void getConversation(String apikey,String ticketCode);

    }

}
