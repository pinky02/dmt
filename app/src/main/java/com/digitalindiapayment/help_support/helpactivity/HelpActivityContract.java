package com.digitalindiapayment.help_support.helpactivity;

import com.digitalindiapayment.mvp.BaseView;
import com.digitalindiapayment.webservices.responsepojo.TicketListResponse;

/**
 * Created by admin on 27-11-2017.
 */

public class HelpActivityContract {

    public interface View extends BaseView {
        public void ticketSuccess(TicketListResponse ticketListResponse);

        public void ticketFail();
    }

    public interface UserAction {
        public void getTickets();

    }

}
