package com.digitalindiapayment.help_support.helpactivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.digitalindiapayment.R;
import com.digitalindiapayment.help_support.OnItemClick;
import com.digitalindiapayment.help_support.problem.SelectProblemActivity;
import com.digitalindiapayment.help_support.querydetails.QueryDetailsActivity;
import com.digitalindiapayment.util.Util;
import com.digitalindiapayment.webservices.responsepojo.TicketList;
import com.digitalindiapayment.webservices.responsepojo.TicketListResponse;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;


public class HelpActivity extends AppCompatActivity implements OnItemClick, HelpActivityContract.View {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.btnAskQuery)
    Button btnAskQuery;
    @BindView(R.id.noInternetImg)
    ImageView noInternetImg;
    @BindView(R.id.noInternetMsg)
    TextView noInternetMsg;
    @BindView(R.id.refreshButton)
    ImageView refreshButton;
    @BindView(R.id.noInternetContainer)
    LinearLayout noInternetContainer;
    @BindView(R.id.progressbar)
    MaterialProgressBar progressbar;
    @BindView(R.id.message)
    TextView message;
    @BindView(R.id.progressdialogdip)
    RelativeLayout progressdialogdip;
    @BindView(R.id.relativeMain)
    RelativeLayout relativeMain;
    @BindView(R.id.tvNoData)
    TextView tvNoData;
    @BindView(R.id.relativeMsg)
    RelativeLayout relativeMsg;
    private HelpAdapter mAdapter;
    private LinearLayoutManager mLayoutManager;
    private HelpActivityContract.UserAction userAction;
    private List<TicketList> ticketList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.help);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);


        userAction = new HelpActivityPresenter(getApplicationContext(), HelpActivity.this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        showLoading();
        userAction.getTickets();
    }

    @OnClick(R.id.btnAskQuery)
    public void calAskQuery() {
        startActivity(new Intent(this, SelectProblemActivity.class));
        //finish();
    }

    @Override
    public void onRowClick(int position) {
        Intent intent = new Intent(this, QueryDetailsActivity.class);
        intent.putExtra(Util.KEYS.TICKET_CODE, ticketList.get(position).getCode());
        startActivity(intent);
    }

    @Override
    public void noInternet() {
        hideLoading();
        tvNoData.setVisibility(View.GONE);
        relativeMain.setVisibility(View.GONE);
        relativeMsg.setVisibility(View.VISIBLE);
        noInternetContainer.setVisibility(View.VISIBLE);

    }

    @Override
    public void exception(Throwable e) {
        hideLoading();
        Util.showShortToast(getApplicationContext(), e.getMessage());

    }

    @Override
    public void fail(String message) {
        hideLoading();
        Util.showShortToast(getApplicationContext(), message);
    }

    @Override
    public void success(String message) {

    }

    @Override
    public void showLoading() {
        relativeMsg.setVisibility(View.VISIBLE);
        relativeMain.setVisibility(View.GONE);
        progressdialogdip.setVisibility(View.VISIBLE);
        noInternetContainer.setVisibility(View.GONE);
    }

    @Override
    public void hideLoading() {
        progressdialogdip.setVisibility(View.GONE);
    }

    @Override
    public void ticketSuccess(TicketListResponse ticketListResponse) {
        hideLoading();
        if (ticketListResponse.getData() != null && ticketListResponse.getData().size() > 0) {
            tvNoData.setVisibility(View.GONE);
            relativeMain.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.VISIBLE);
            this.ticketList = ticketListResponse.getData();
            mAdapter = new HelpAdapter(this, ticketListResponse);
            recyclerView.setAdapter(mAdapter);
        } else {
            //empty list size
            relativeMain.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            btnAskQuery.setVisibility(View.VISIBLE);
            tvNoData.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void ticketFail() {
        Util.showShortToast(getApplicationContext(), getResources().getString(R.string.someting_wrong));
    }

    @OnClick(R.id.refreshButton)
    public void onViewClicked() {
        showLoading();
        userAction.getTickets();
    }
}
