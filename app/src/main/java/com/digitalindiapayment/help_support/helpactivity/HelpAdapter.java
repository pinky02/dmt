package com.digitalindiapayment.help_support.helpactivity;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.digitalindiapayment.R;
import com.digitalindiapayment.help_support.OnItemClick;
import com.digitalindiapayment.util.Util;
import com.digitalindiapayment.webservices.responsepojo.TicketList;
import com.digitalindiapayment.webservices.responsepojo.TicketListResponse;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by Nivrutti Pawar on 11-11-2017.
 */

public class HelpAdapter extends RecyclerView.Adapter<HelpAdapter.ViewHolder> {

    private OnItemClick itemClick;
    private TicketListResponse ticketListResponse;

    public HelpAdapter(OnItemClick itemClick, TicketListResponse ticketListResponse) {
        this.itemClick = itemClick;
        this.ticketListResponse = ticketListResponse;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_help_adapter, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.linearMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClick.onRowClick(position);
            }
        });
        TicketList bean = ticketListResponse.getData().get(position);
        holder.title.setText(bean.getSubject());
        holder.date.setText(Util.dateFormat(bean.getCreatedAt()));
        // holder.txnId.setText();
        holder.status.setText(ticketListResponse.getData().get(position).getCurrentStatus().getName());
        holder.tvTicketId.setText(ticketListResponse.getData().get(position).getCode());
    }

    @Override
    public int getItemCount() {
        return ticketListResponse.getData().size();//ticketList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout linearMain;
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.date)
        TextView date;
        @BindView(R.id.txnId)
        TextView txnId;
        @BindView(R.id.status)
        TextView status;
        @BindView(R.id.tvTicketId)
        TextView tvTicketId;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            linearMain = (LinearLayout) itemView.findViewById(R.id.linearMain);
        }
    }
}
