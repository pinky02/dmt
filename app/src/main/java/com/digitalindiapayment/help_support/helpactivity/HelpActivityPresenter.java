package com.digitalindiapayment.help_support.helpactivity;

import android.content.Context;

import com.digitalindiapayment.R;
import com.digitalindiapayment.util.Util;
import com.digitalindiapayment.webservices.Api;
import com.digitalindiapayment.webservices.core.ApiFail;
import com.digitalindiapayment.webservices.core.ApiSuccess;
import com.digitalindiapayment.webservices.core.HttpErrorResponse;
import com.digitalindiapayment.webservices.responsepojo.TicketListResponse;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by admin on 27-11-2017.
 */

public class HelpActivityPresenter implements HelpActivityContract.UserAction {

    Context context;
    HelpActivityContract.View view;

    public HelpActivityPresenter(Context context, HelpActivityContract.View view) {
        this.context = context;
        this.view = view;
    }

    @Override
    public void getTickets() {
        String token = Util.PREFERENCES.getTicketToken(context);
        String appInstance = Util.PREFERENCES.getAppInstanceCode(context);
        Api.getTicket().getTicketList(appInstance, token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ApiSuccess<TicketListResponse>() {
                    @Override
                    public void call(TicketListResponse ticketListResponse) {
                        if (ticketListResponse.getStatus().equalsIgnoreCase(Util.KEYS.SUCCESS))
                        view.ticketSuccess(ticketListResponse);
                        else {
                            view.ticketFail();
                        }

                    }
                }, new ApiFail() {
                    @Override
                    public void httpStatus(HttpErrorResponse response) {
                        String message = response.getError();
                        view.fail(message);
                    }

                    @Override
                    public void noNetworkError() {
                        view.noInternet();
                    }

                    @Override
                    public void unknownError(Throwable e) {
                        view.fail(context.getString(R.string.error));
                    }
                });
    }
}
