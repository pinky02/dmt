package com.digitalindiapayment.help_support;

import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.digitalindiapayment.R;
import com.digitalindiapayment.util.Util;
import com.digitalindiapayment.webservices.responsepojo.CommentList;
import com.digitalindiapayment.webservices.responsepojo.ConversationList;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Nivrutti Pawar on 20-11-2017.
 */

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.ViewHolder> {

    private Context context;
    private List<CommentList> conversationList;

    public CommentAdapter(Context context, List<CommentList> conversationList) {
        this.context = context;
        this.conversationList = conversationList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_comment_adapter, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {


        holder.tvTitle.setText(conversationList.get(position).getCreatedBy());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            holder.tvDescription.setText(Html.fromHtml(conversationList.get(position).getComment(), Html.FROM_HTML_MODE_COMPACT));
        else
            holder.tvDescription.setText(Html.fromHtml(conversationList.get(position).getComment()));
        holder.tvDate.setText(conversationList.get(position).getCreatedDate());

    }

    @Override
    public int getItemCount() {
        return conversationList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvTitle)
        TextView tvTitle;
        @BindView(R.id.tvDate)
        TextView tvDate;
        @BindView(R.id.tvDescription)
        TextView tvDescription;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
