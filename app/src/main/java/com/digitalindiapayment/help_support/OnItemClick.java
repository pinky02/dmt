package com.digitalindiapayment.help_support;

/**
 * Created by Nivrutti Pawar on 14-11-2017.
 */

public interface OnItemClick {
    public void onRowClick(int position);
}
