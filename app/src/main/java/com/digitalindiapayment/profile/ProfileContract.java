package com.digitalindiapayment.profile;

import com.digitalindiapayment.login.UserProfile;
import com.digitalindiapayment.mvp.BaseView;
import com.digitalindiapayment.pojo.BankDetailsBean;
import com.digitalindiapayment.webservices.requestpojo.GenerateTransactionIdRequest;
import com.digitalindiapayment.webservices.responsepojo.GenerateTransactionIdResponse;

import java.util.List;

/**
 * Created by admin on 13-07-2017.
 */

public class ProfileContract {

    interface View extends BaseView {
        void showInfo(UserProfile userProfile);
        void apiFail(String message);
    }

    interface UserActions{
        public void getUserInfo();
    }
}
