package com.digitalindiapayment.profile;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.digitalindiapayment.R;
import com.digitalindiapayment.login.UserProfile;
import com.digitalindiapayment.util.Util;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfileActivity extends AppCompatActivity implements ProfileContract.View {

    private static final String TAG = "ProfileActivity";
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.user_image)
    ImageView userImage;
    @BindView(R.id.userName)
    TextView userName;
    @BindView(R.id.userId)
    TextView userId;
    @BindView(R.id.emailIdName)
    TextView emailIdName;
    @BindView(R.id.mobileNumber)
    TextView mobileNumber;
    @BindView(R.id.dateOfJoining)
    TextView dateOfJoining;
    @BindView(R.id.linearLayout)
    LinearLayout linearLayout;
    @BindView(R.id.noInternetContainer)
    LinearLayout noInternetContainer;
    @BindView(R.id.progressdialogdip)
    RelativeLayout progressdialogdip;
    @BindView(R.id.exception_handle)
    RelativeLayout exceptionHandle;
    @BindView(R.id.profile_screen)
    ScrollView profileScreen;
    @BindView(R.id.balance)
    TextView balance;
    @BindView(R.id.terminalId)
    TextView terminalId;
    @BindView(R.id.diplUserId)
    TextView diplUserId;
    @BindView(R.id.status)
    TextView status;
    @BindView(R.id.registerDevice)
    TextView registerDevice;

    private ProfileContract.UserActions userActions;
    private UserProfile userProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        setSupportActionBar(toolbar);

        //Make the button enable and Onclick listner on that
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        getSupportActionBar().setTitle(getString(R.string.profile));
        toolbar.setTitle(getString(R.string.profile));


        userProfile = getIntent().getParcelableExtra(Util.KEYS.DATA);

        userId.setText(userProfile.getDipId());
        userName.setText(userProfile.getUserName());
        emailIdName.setText(userProfile.getEmail());
        mobileNumber.setText(userProfile.getMobileNumber());
        dateOfJoining.setText(userProfile.getJoiningDate());
        balance.setText("" + userProfile.getBalance());
        terminalId.setText(userProfile.getTerminalId());
        diplUserId.setText(userProfile.getId());
        status.setText(userProfile.getStatus() == 1 ? getString(R.string.active) : getString(R.string.inactive));

        registerDevice.setText(Util.PREFERENCES.getDeviceName(ProfileActivity.this));


        if (userProfile.getImage() != null) {
            if (!userProfile.getImage().trim().equals("")) {
                Glide.with(ProfileActivity.this)
                        .load(userProfile.getImage())
                        .centerCrop()
                        .crossFade()
                        .override(800, 500)
                        .placeholder(R.drawable.avatar)
                        .error(R.drawable.avatar)
                        .into(userImage);

            }

        }
    }

    @Override
    public void noInternet() {
    }

    @Override
    public void exception(Throwable e) {
    }

    @Override
    public void fail(String message) {
        Log.d(TAG, "fail " + message);
    }

    @Override
    public void success(String message) {

    }

    @Override
    public void showLoading() {
    }

    @Override
    public void hideLoading() {
    }


    @Override
    public void showInfo(UserProfile userProfile) {
    }

    @Override
    public void apiFail(String message) {
    }
}
