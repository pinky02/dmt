package com.digitalindiapayment.adhar;

import android.content.Intent;

import com.digitalindiapayment.mvp.BaseView;

/**
 * Created by admin on 24-05-2017.
 */

public class AdharContract {

    public interface View  {
        void setAdharDetails(String uId);

    }

    public interface UserAction {
        void showScanIntent();
        void processAdharDetails(int requestCode, int resultCode, Intent intent);
        //void setPermissions();
    }

}
