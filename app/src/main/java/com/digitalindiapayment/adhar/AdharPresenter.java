package com.digitalindiapayment.adhar;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.digitalindiapayment.R;
import com.digitalindiapayment.util.Util;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.StringReader;

/**
 * Created by admin on 24-05-2017.
 */

public class AdharPresenter implements AdharContract.UserAction {

    private final AdharContract.View view;
    private final Context context;

    public AdharPresenter(AdharContract.View view, Context context) {
        this.view = view;
        this.context = context;
    }

    @Override
    public void showScanIntent() {
        IntentIntegrator integrator = new IntentIntegrator((Activity) view);
        integrator.setCaptureActivity(AdharCaptureActivity.class);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
        integrator.setPrompt(context.getString(R.string.scan_your_adhar_qr));
        integrator.setOrientationLocked(true);
        //integrator.
        //integrator.setResultDisplayDuration(500);
        integrator.setCameraId(0);  // Use a specific camera of the device
        integrator.initiateScan();

    }


    @Override
    public void processAdharDetails(int requestCode, int resultCode, Intent intent) {
        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);

        if (scanningResult != null) {
            //we have a result
            String scanContent = scanningResult.getContents();
            String scanFormat = scanningResult.getFormatName();

            // process received data
            if (scanContent != null && !scanContent.isEmpty()) {
                processScannedData(scanContent);
            } else {
                Toast toast = Toast.makeText(context.getApplicationContext(), "Scan Cancelled", Toast.LENGTH_SHORT);
                toast.show();
            }

        } else {
            Toast toast = Toast.makeText(context.getApplicationContext(), "No scan data received!", Toast.LENGTH_SHORT);
            toast.show();
        }
    }


    /**
     * process xml string received from aadhaar card QR code
     *
     * @param scanData
     */
    protected void processScannedData(String scanData) {
        XmlPullParserFactory pullParserFactory;

        try {
            // init the parserfactory
            pullParserFactory = XmlPullParserFactory.newInstance();
            // get the parser
            XmlPullParser parser = pullParserFactory.newPullParser();

            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(new StringReader(scanData));

            // parse the XML
            int eventType = parser.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_DOCUMENT) {

                } else if (eventType == XmlPullParser.START_TAG && Util.AADHAAR.AADHAAR_DATA_TAG.equals(parser.getName())) {
                    // extract data from tag
                    //uid
                    String uid = parser.getAttributeValue(null, Util.AADHAAR.AADHAR_UID_ATTR);
                    if (uid != null && !uid.equals("")) {
                        Toast toast = Toast.makeText(context.getApplicationContext(), context.getString(R.string.aadhaar_suceess_message), Toast.LENGTH_SHORT);
                        toast.show();
                    } else {
                        uid = "";

                    }

                    view.setAdharDetails(uid);


                    //name
                    //name = parser.getAttributeValue(null,UtilDmt.AADHAAR.AADHAR_NAME_ATTR);
                    //gender
                    //gender = parser.getAttributeValue(null,UtilDmt.AADHAAR.AADHAR_GENDER_ATTR);
                    // year of birth
                    //yearOfBirth = parser.getAttributeValue(null,UtilDmt.AADHAAR.AADHAR_YOB_ATTR);
                    // care of
                    //careOf = parser.getAttributeValue(null,UtilDmt.AADHAAR.AADHAR_CO_ATTR);
                    // village Tehsil
                    //villageTehsil = parser.getAttributeValue(null,UtilDmt.AADHAAR.AADHAR_VTC_ATTR);
                    // Post Office
                    //postOffice = parser.getAttributeValue(null,UtilDmt.AADHAAR.AADHAR_PO_ATTR);
                    // district
                    //district = parser.getAttributeValue(null,UtilDmt.AADHAAR.AADHAR_DIST_ATTR);
                    // state
                    //state = parser.getAttributeValue(null,UtilDmt.AADHAAR.AADHAR_STATE_ATTR);
                    // Post Code
                    //postCode = parser.getAttributeValue(null,UtilDmt.AADHAAR.AADHAR_PC_ATTR);

                } else if (eventType == XmlPullParser.END_TAG) {
                    Log.d("DIPL", "End tag " + parser.getName());

                } else if (eventType == XmlPullParser.TEXT) {
                    Log.d("DIPL", "Text " + parser.getText());

                }
                // update eventType
                eventType = parser.next();
            }

            // display the data on screen
            //displayScannedData();
        } catch (XmlPullParserException e) {
            Toast toast = Toast.makeText(context.getApplicationContext(), context.getString(R.string.aadhaar_fail_message), Toast.LENGTH_SHORT);
            toast.show();
            e.printStackTrace();
        } catch (IOException e) {
            Toast toast = Toast.makeText(context.getApplicationContext(), context.getString(R.string.aadhaar_fail_message), Toast.LENGTH_SHORT);
            toast.show();
            e.printStackTrace();
        }

    }

}
