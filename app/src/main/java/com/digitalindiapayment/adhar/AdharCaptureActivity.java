package com.digitalindiapayment.adhar;

import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.Button;
import android.view.View;
import android.widget.ImageButton;

import com.digitalindiapayment.R;
import com.digitalindiapayment.screenanlytics.AppFirebaseAnalytics;
import com.digitalindiapayment.screenanlytics.IAnalytics;
import com.journeyapps.barcodescanner.CaptureManager;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;


public class AdharCaptureActivity extends AppCompatActivity implements
        DecoratedBarcodeView.TorchListener {

    private CaptureManager capture;
    private DecoratedBarcodeView barcodeScannerView;
    private ImageButton switchFlashlightButton;
    private Boolean isTorchOn;
    private IAnalytics iAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adhar_capture);

        barcodeScannerView = (DecoratedBarcodeView) findViewById(R.id.zxing_barcode_scanner);
        barcodeScannerView.setTorchListener(this);

        switchFlashlightButton = (ImageButton) findViewById(R.id.switch_flashlight);

        // if the device does not have flashlight in its camera,
        // then remove the switch flashlight button...
        if (!hasFlash()) {
            switchFlashlightButton.setVisibility(View.GONE);
        }

        capture = new CaptureManager(this, barcodeScannerView);
        capture.initializeFromIntent(getIntent(), savedInstanceState);
        capture.decode();
        isTorchOn = false;

        iAnalytics = new AppFirebaseAnalytics(AdharCaptureActivity.this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        capture.onResume();
        iAnalytics.trackScreen(getString(R.string.adhar_capture_screen));
    }

    @Override
    protected void onPause() {
        super.onPause();
        capture.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        capture.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        capture.onSaveInstanceState(outState);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return barcodeScannerView.onKeyDown(keyCode, event) || super.onKeyDown(keyCode, event);
    }

    private boolean hasFlash() {
        return getApplicationContext().getPackageManager()
                .hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
    }

    public void switchFlashlight(View view) {
        if (!isTorchOn) {
            barcodeScannerView.setTorchOn();

        } else {
            barcodeScannerView.setTorchOff();
        }
    }

    @Override
    public void onTorchOn() {
        isTorchOn = true;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            switchFlashlightButton.setBackground(getDrawable(R.drawable.ic_flash_torch));
        }else{
            switchFlashlightButton.setBackground(getResources().getDrawable(R.drawable.ic_flash_torch));
        }
    }

    @Override
    public void onTorchOff() {
        isTorchOn = false;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            switchFlashlightButton.setBackground(getDrawable(R.drawable.ic_flash_on_holo_light));
        }else{
            switchFlashlightButton.setBackground(getResources().getDrawable(R.drawable.ic_flash_on_holo_light));
        }

    }
}
