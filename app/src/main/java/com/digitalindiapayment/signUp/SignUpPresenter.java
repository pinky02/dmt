package com.digitalindiapayment.signUp;

import android.content.Context;
import android.util.Log;

import com.digitalindiapayment.util.Util;
import com.digitalindiapayment.webservices.Api;
import com.digitalindiapayment.webservices.core.ApiFail;
import com.digitalindiapayment.webservices.core.ApiSuccess;
import com.digitalindiapayment.webservices.core.HttpErrorResponse;
import com.digitalindiapayment.webservices.requestpojo.ForgotPasswordRequest;
import com.digitalindiapayment.webservices.requestpojo.RegisterUserRequest;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by admin on 12-09-2017.
 */

public class SignUpPresenter implements SignUpContract.UserAction {

    private static final String TAG = "SignUpPresenter";
    Context context;
    SignUpContract.View view;

    public SignUpPresenter(Context context, SignUpContract.View view) {
        this.context = context;
        this.view = view;
    }

    @Override
    public void submitDetails(RegisterUserRequest registerUserRequest) {
        Api.userManagement().registerUser(registerUserRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ApiSuccess<String>() {
                    @Override
                    public void call(String addMemberResponseResponse) {
                        Log.d(TAG, "subscribe > call");
                        //if(addMemberResponseResponse.equals(context.getString(R.string.api_success_key))){
                        view.submitDetailsSuccess();
                        //}
                    }
                }, new ApiFail() {
                    @Override
                    public void httpStatus(HttpErrorResponse response) {
                        view.submitDetailsFail(response.getError().toString().replace("\"",""));
                    }

                    @Override
                    public void noNetworkError() {
                        view.noInternet();
                    }

                    @Override
                    public void unknownError(Throwable e) {
                        view.fail(e.getMessage());
                    }
                });
    }
}
