package com.digitalindiapayment.signUp;

import com.digitalindiapayment.mvp.BaseView;
import com.digitalindiapayment.pojo.DashboardBean;
import com.digitalindiapayment.pojo.LastFiveTransaction;
import com.digitalindiapayment.webservices.requestpojo.RegisterUserRequest;

import java.util.List;

/**
 * Created by admin on 12-09-2017.
 */

public class SignUpContract {

    public interface View extends BaseView {

        void submitDetailsFail(String reason);
        void submitDetailsSuccess();
    }

    public interface UserAction {
        void submitDetails(RegisterUserRequest registerUserRequest);
    }
}
