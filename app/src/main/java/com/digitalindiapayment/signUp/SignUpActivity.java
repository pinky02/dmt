package com.digitalindiapayment.signUp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import android.support.v7.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import android.widget.Spinner;

import android.widget.Toast;

import com.digitalindiapayment.R;
import com.digitalindiapayment.util.Util;
import com.digitalindiapayment.webservices.requestpojo.RegisterUserRequest;

import java.util.ArrayList;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignUpActivity extends AppCompatActivity implements SignUpContract.View {


    private static final String TAG = "SignUpActivity";
    @BindView(R.id.container)
    ScrollView container;
    @BindView(R.id.noInternetContainer)
    LinearLayout noInternetContainer;
    @BindView(R.id.username)
    EditText username;
    @BindView(R.id.mobileno)
    EditText mobileno;
    @BindView(R.id.submit)
    Button submit;

    SignUpContract.UserAction userAction;
    ArrayList<String> listState = new ArrayList<String>();
    //@BindView(R.id.spStates)
    //Spinner spStates;
    String[] states;
    @BindView(R.id.autocomplet_state_list)
    AutoCompleteTextView autocompletStateList;
    ProgressDialog progressDialog;

    @BindView(R.id.toolbar)
    Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);

        toolbar.setTitle(R.string.sign_up);


        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               onBackPressed();
            }
        });
        userAction = new SignUpPresenter(getApplicationContext(), SignUpActivity.this);

        states = getResources().getStringArray(R.array.states);
        // Adapter for spinner
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, states);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //spStates.setAdapter(adapter);
        autocompletStateList.setAdapter(adapter);
    }

    @Override
    public void noInternet() {
        progressDialog.dismiss();
        //noInternetContainer.setVisibility(View.VISIBLE);
        //container.setVisibility(View.GONE);
    }

    @Override
    public void exception(Throwable e) {
        progressDialog.dismiss();
    }

    @Override
    public void fail(String message) {
        progressDialog.dismiss();
        Util.showLongToast(SignUpActivity.this, message);
    }

    @Override
    public void success(String message) {

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override

    public void submitDetailsFail(String reason) {
        progressDialog.dismiss();
        Util.showLongToast(SignUpActivity.this, reason);

        finish();
    }

    @Override
    public void submitDetailsSuccess() {
        progressDialog.dismiss();
        // S
        submit.setEnabled(true);
        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(getResources().getString(R.string.request_submitted));
        alertDialog.setMessage(getString(R.string.register_user_success));
        alertDialog.setCancelable(false);
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        alertDialog.show();
    }

    @OnClick(R.id.submit)
    public void onViewClicked() {

        if (mobileno.getText().toString().equalsIgnoreCase("")) {

            mobileno.requestFocus();
            mobileno.setError(getString(R.string.pls_enter_mobile_no));
            //Toast.makeText(getApplicationContext(), getString(R.string.pls_enter_mobile_no), Toast.LENGTH_SHORT).show();


            return;
        }
        Log.d(TAG, mobileno.getText().toString() + " lenght " + mobileno.getText().length());
        if (mobileno.getText().toString().length() != 10) {

            mobileno.requestFocus();
            mobileno.setError(getString(R.string.enter_10_digit_mobile));
            //Toast.makeText(getApplicationContext(), getString(R.string.enter_10_digit_mobile), Toast.LENGTH_SHORT).show();


            return;
        }
        if (username.getText().toString().equalsIgnoreCase("")) {

            username.requestFocus();
            username.setError(getString(R.string.pls_enter_username));
            //Toast.makeText(getApplicationContext(), getString(R.string.pls_enter_username), Toast.LENGTH_SHORT).show();
            return;
        }
        if (autocompletStateList.getText().toString().equals("")) {
            autocompletStateList.requestFocus();
            autocompletStateList.setError(getString(R.string.pls_select_state));
            //Toast.makeText(getApplicationContext(), getString(R.string.pls_select_state), Toast.LENGTH_SHORT).show();
            return;
        }
        if (!Arrays.asList(states).contains(autocompletStateList.getText().toString().trim())) {
            autocompletStateList.requestFocus();
            autocompletStateList.setError(getString(R.string.pls_enter_valid_state));
            //Toast.makeText(getApplicationContext(), getString(R.string.pls_enter_valid_state), Toast.LENGTH_SHORT).show();
            return;
        }


        submit.setEnabled(false);

        progressDialog = new ProgressDialog(SignUpActivity.this);
        progressDialog.setMessage("loading");
        progressDialog.setCancelable(false);
        progressDialog.show();

        RegisterUserRequest registerUserRequest = new RegisterUserRequest();
        registerUserRequest.setMobileNo(mobileno.getText().toString());
        registerUserRequest.setName(username.getText().toString());
        registerUserRequest.setState(autocompletStateList.getText().toString());
        userAction.submitDetails(registerUserRequest);

    }
}
