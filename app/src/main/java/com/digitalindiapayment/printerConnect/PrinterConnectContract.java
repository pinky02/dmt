package com.digitalindiapayment.printerConnect;

import com.digitalindiapayment.pojo.TransactionDetailsBean;

/**
 * Created by admin on 03-06-2017.
 */

public class PrinterConnectContract {
    public interface View {
        public void onPrintCompletion(String message);
    }
    public interface UserAction {
        public void printReceipt(TransactionDetailsBean bean,String printerName);
        public Boolean isBluetoothPrinterConnected();
    }
}
