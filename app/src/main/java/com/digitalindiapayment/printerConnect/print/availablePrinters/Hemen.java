package com.digitalindiapayment.printerConnect.print.availablePrinters;


import android.content.Context;
import android.os.AsyncTask;

import com.digitalindiapayment.R;
import com.digitalindiapayment.pojo.TransactionDetailsBean;
import com.digitalindiapayment.pojo.TransactionReportBean;
import com.digitalindiapayment.printerConnect.print.PrintContract;
import com.esys.impressivedemo.bluetooth.BluetoothComm;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by admin on 06-06-2017.
 */

public class Hemen implements PrintContract.Printer {

    private TransactionDetailsBean bean;

    private Context context;
    private PrintContract.PrintEventPrsenter printEventPrsenter;
    public static final int DEVICE_NOTCONNECTED = -100;
    int totDataLen =0;
    OutputStream mmOutStream;
    InputStream mmInStream;

    /*
    Hardcoded logo
     */
    private byte[] Packet1 = {
            (byte) 0x7E, (byte) 0xB5, (byte) 0xB8, (byte) 0x65, (byte) 0x1, (byte) 0x5E, (byte) 0x18, (byte) 0x25,
            (byte) 0x2, (byte) 0x24, (byte) 0x4, (byte) 0x00, (byte) 0x01, (byte) 0xFF, (byte) 0xC0, (byte) 0x5E, (byte) 0x14, (byte) 0x25,
            (byte) 0x1, (byte) 0x24, (byte) 0x4, (byte) 0x00, (byte) 0x07, (byte) 0xFF, (byte) 0xF0, (byte) 0x5E, (byte) 0x14, (byte) 0x25,
            (byte) 0x1, (byte) 0x24, (byte) 0x4, (byte) 0x00, (byte) 0x0F, (byte) 0xFF, (byte) 0xFC, (byte) 0x5E, (byte) 0x8, (byte) 0x24, (byte) 0x1, (byte) 0x78, (byte) 0x5E, (byte) 0xB, (byte) 0x25,
            (byte) 0x1, (byte) 0x24, (byte) 0x4, (byte) 0x00, (byte) 0x1F, (byte) 0xFF, (byte) 0xFE, (byte) 0x5E, (byte) 0x8, (byte) 0x24, (byte) 0x1, (byte) 0x78, (byte) 0x5E, (byte) 0xB, (byte) 0x25,
            (byte) 0x1, (byte) 0x24, (byte) 0x4, (byte) 0x00, (byte) 0x3F, (byte) 0xFF, (byte) 0x03, (byte) 0x5E, (byte) 0x8, (byte) 0x24, (byte) 0x1, (byte) 0x78, (byte) 0x5E, (byte) 0xB, (byte) 0x25,
            (byte) 0x1, (byte) 0x24, (byte) 0x5, (byte) 0x00, (byte) 0x7F, (byte) 0xFC, (byte) 0x00, (byte) 0x80, (byte) 0x5E, (byte) 0x7, (byte) 0x24, (byte) 0x1, (byte) 0x78, (byte) 0x5E, (byte) 0xB, (byte) 0x25,
            (byte) 0x1, (byte) 0x24, (byte) 0x5, (byte) 0x00, (byte) 0xFF, (byte) 0xE0, (byte) 0x00, (byte) 0x40, (byte) 0x5E, (byte) 0x7, (byte) 0x24, (byte) 0x1, (byte) 0x78, (byte) 0x5E, (byte) 0xB, (byte) 0x25, (byte) 0x04, (byte) 0xEC
    };
    private byte[] Packet2 = {
            (byte) 0x7E, (byte) 0xB5, (byte) 0xB8, (byte) 0x66, (byte) 0x1, (byte) 0x24, (byte) 0x3, (byte) 0x01, (byte) 0xFF, (byte) 0xCC, (byte) 0x5E, (byte) 0x9, (byte) 0x24, (byte) 0x2, (byte) 0x7B, (byte) 0xB8, (byte) 0x5E, (byte) 0xA, (byte) 0x25,
            (byte) 0x1, (byte) 0x24, (byte) 0x5, (byte) 0x01, (byte) 0xFF, (byte) 0x82, (byte) 0x00, (byte) 0x20, (byte) 0x5E, (byte) 0x7, (byte) 0x24, (byte) 0x2, (byte) 0x7F, (byte) 0xBE, (byte) 0x5E, (byte) 0xA, (byte) 0x25,
            (byte) 0x1, (byte) 0x24, (byte) 0x5, (byte) 0x03, (byte) 0xFF, (byte) 0x9C, (byte) 0x00, (byte) 0x20, (byte) 0x5E, (byte) 0x7, (byte) 0x24, (byte) 0x2, (byte) 0x7F, (byte) 0xBF, (byte) 0x5E, (byte) 0xA, (byte) 0x25,
            (byte) 0x2, (byte) 0x24, (byte) 0x5, (byte) 0x01, (byte) 0xFF, (byte) 0x63, (byte) 0x00, (byte) 0x20, (byte) 0x5E, (byte) 0x7, (byte) 0x24, (byte) 0x3, (byte) 0x7F, (byte) 0xBF, (byte) 0x80, (byte) 0x5E, (byte) 0x9, (byte) 0x25,
            (byte) 0x1, (byte) 0x24, (byte) 0x5, (byte) 0x05, (byte) 0xFF, (byte) 0x40, (byte) 0xD0, (byte) 0x20, (byte) 0x5E, (byte) 0x7, (byte) 0x24, (byte) 0x3, (byte) 0x7F, (byte) 0x1F, (byte) 0x80, (byte) 0x5E, (byte) 0x9, (byte) 0x25,
            (byte) 0x1, (byte) 0x24, (byte) 0x4, (byte) 0x01, (byte) 0xFE, (byte) 0x9E, (byte) 0x40, (byte) 0x5E, (byte) 0x8, (byte) 0x24, (byte) 0x3, (byte) 0x7C, (byte) 0x07, (byte) 0xC0, (byte) 0x5E, (byte) 0x9, (byte) 0x25, (byte) 0x04, (byte) 0x49
    };
    private byte[] Packet3 = {
            (byte) 0x7E, (byte) 0xB5, (byte) 0xB8, (byte) 0x6E, (byte) 0x1, (byte) 0x24, (byte) 0x4, (byte) 0x01, (byte) 0xFC, (byte) 0x8C, (byte) 0x07, (byte) 0x5E, (byte) 0x8, (byte) 0x24, (byte) 0x3, (byte) 0x7C, (byte) 0x07, (byte) 0xC0, (byte) 0x5E, (byte) 0x9, (byte) 0x25,
            (byte) 0x1, (byte) 0x24, (byte) 0x5, (byte) 0x01, (byte) 0x38, (byte) 0x10, (byte) 0x5F, (byte) 0xC0, (byte) 0x5E, (byte) 0x7, (byte) 0x24, (byte) 0x3, (byte) 0x78, (byte) 0x03, (byte) 0xC0, (byte) 0x5E, (byte) 0x9, (byte) 0x25,
            (byte) 0x1, (byte) 0x24, (byte) 0x5, (byte) 0x01, (byte) 0x00, (byte) 0x0C, (byte) 0x7F, (byte) 0xC0, (byte) 0x5E, (byte) 0x7, (byte) 0x24, (byte) 0xC, (byte) 0x78, (byte) 0x03, (byte) 0xC0, (byte) 0xF0, (byte) 0x48, (byte) 0x00, (byte) 0xF0, (byte) 0x30, (byte) 0x42, (byte) 0x01, (byte) 0x00, (byte) 0x00, (byte) 0x25,
            (byte) 0x1, (byte) 0x24, (byte) 0x5, (byte) 0x00, (byte) 0xE0, (byte) 0xCC, (byte) 0xBF, (byte) 0xC0, (byte) 0x5E, (byte) 0x7, (byte) 0x24, (byte) 0xC, (byte) 0x78, (byte) 0x03, (byte) 0xC0, (byte) 0xFC, (byte) 0xFC, (byte) 0xC0, (byte) 0xFC, (byte) 0x30, (byte) 0xE6, (byte) 0x33, (byte) 0x00, (byte) 0x00, (byte) 0x25,
            (byte) 0x1, (byte) 0x24, (byte) 0x5, (byte) 0x00, (byte) 0x90, (byte) 0x61, (byte) 0x7C, (byte) 0xC0, (byte) 0x5E, (byte) 0x7, (byte) 0x24, (byte) 0xC, (byte) 0x7C, (byte) 0x07, (byte) 0xC0, (byte) 0xCC, (byte) 0xCE, (byte) 0xE0, (byte) 0xCC, (byte) 0x38, (byte) 0xE6, (byte) 0x36, (byte) 0x00, (byte) 0x00, (byte) 0x25, (byte) 0x04, (byte) 0xEE
    };
    private byte[] Packet4 = {
            (byte) 0x7E, (byte) 0xB5, (byte) 0xB8, (byte) 0x63, (byte) 0x1, (byte) 0x24, (byte) 0x5, (byte) 0x00, (byte) 0xC8, (byte) 0x1E, (byte) 0x78, (byte) 0x80, (byte) 0x5E, (byte) 0x7, (byte) 0x24, (byte) 0xC, (byte) 0x7E, (byte) 0x07, (byte) 0xC0, (byte) 0xCC, (byte) 0xCC, (byte) 0xC0, (byte) 0xCC, (byte) 0x68, (byte) 0xF6, (byte) 0x3C, (byte) 0x00, (byte) 0x00, (byte) 0x25,
            (byte) 0x1, (byte) 0x24, (byte) 0x5, (byte) 0x00, (byte) 0x80, (byte) 0x00, (byte) 0xF9, (byte) 0x80, (byte) 0x5E, (byte) 0x7, (byte) 0x24, (byte) 0xC, (byte) 0x7F, (byte) 0x1F, (byte) 0x80, (byte) 0xDC, (byte) 0xFC, (byte) 0xE0, (byte) 0xF8, (byte) 0x48, (byte) 0xD6, (byte) 0x3C, (byte) 0x00, (byte) 0x00, (byte) 0x25,
            (byte) 0x2, (byte) 0x24, (byte) 0x5, (byte) 0x04, (byte) 0x24, (byte) 0x01, (byte) 0xF1, (byte) 0x80, (byte) 0x5E, (byte) 0x7, (byte) 0x24, (byte) 0xC, (byte) 0x7F, (byte) 0xFF, (byte) 0x80, (byte) 0xF8, (byte) 0xEE, (byte) 0xE0, (byte) 0xCC, (byte) 0xCC, (byte) 0xDE, (byte) 0x3E, (byte) 0x00, (byte) 0x00, (byte) 0x25,
            (byte) 0x1, (byte) 0x24, (byte) 0x4, (byte) 0x04, (byte) 0x50, (byte) 0x03, (byte) 0xF3, (byte) 0x5E, (byte) 0x8, (byte) 0x24, (byte) 0xC, (byte) 0x7F, (byte) 0xFF, (byte) 0x00, (byte) 0xDC, (byte) 0xC6, (byte) 0xE0, (byte) 0xC6, (byte) 0xFC, (byte) 0xCE, (byte) 0x33, (byte) 0x00, (byte) 0x00, (byte) 0x25, (byte) 0x04, (byte) 0x82
    };
    private byte[] Packet5 = {
            (byte) 0x7E, (byte) 0xB5, (byte) 0xB8, (byte) 0x6A, (byte) 0x1, (byte) 0x24, (byte) 0x5, (byte) 0x00, (byte) 0x32, (byte) 0x0F, (byte) 0xE3, (byte) 0x10, (byte) 0x5E, (byte) 0x7, (byte) 0x24, (byte) 0xC, (byte) 0x7F, (byte) 0xFE, (byte) 0x00, (byte) 0xCE, (byte) 0xFE, (byte) 0xFC, (byte) 0xCC, (byte) 0x84, (byte) 0xCE, (byte) 0x33, (byte) 0x00, (byte) 0x00, (byte) 0x25,
            (byte) 0x1, (byte) 0x24, (byte) 0x4, (byte) 0x02, (byte) 0x11, (byte) 0x7F, (byte) 0xC6, (byte) 0x5E, (byte) 0x8, (byte) 0x24, (byte) 0xC, (byte) 0x79, (byte) 0xF8, (byte) 0x00, (byte) 0xCE, (byte) 0xFC, (byte) 0xFC, (byte) 0xFD, (byte) 0x86, (byte) 0xC6, (byte) 0x31, (byte) 0x80, (byte) 0x00, (byte) 0x25,
            (byte) 0x1, (byte) 0x24, (byte) 0x5, (byte) 0x00, (byte) 0x00, (byte) 0xFF, (byte) 0x80, (byte) 0x20, (byte) 0x5E, (byte) 0x7, (byte) 0x24, (byte) 0x1, (byte) 0x78, (byte) 0x5E, (byte) 0xB, (byte) 0x25,
            (byte) 0x1, (byte) 0x24, (byte) 0x5, (byte) 0x00, (byte) 0x00, (byte) 0x3F, (byte) 0x00, (byte) 0x40, (byte) 0x5E, (byte) 0x13, (byte) 0x25,
            (byte) 0x1, (byte) 0x24, (byte) 0x5, (byte) 0x00, (byte) 0x00, (byte) 0x3F, (byte) 0x00, (byte) 0x80, (byte) 0x5E, (byte) 0x13, (byte) 0x25,
            (byte) 0x1, (byte) 0x24, (byte) 0x4, (byte) 0x00, (byte) 0x00, (byte) 0x3F, (byte) 0x01, (byte) 0x5E, (byte) 0x14, (byte) 0x25,
            (byte) 0x1, (byte) 0x24, (byte) 0x3, (byte) 0x00, (byte) 0x00, (byte) 0x21, (byte) 0x5E, (byte) 0x15, (byte) 0x25, (byte) 0x04, (byte) 0x60
    };
    private byte[] Packet6 = {
            (byte) 0x7E, (byte) 0xB5, (byte) 0xB8, (byte) 0x2E, (byte) 0x2, (byte) 0x24, (byte) 0x3, (byte) 0x00, (byte) 0x00, (byte) 0x20, (byte) 0x5E, (byte) 0x15, (byte) 0x25,
            (byte) 0x1, (byte) 0x24, (byte) 0x4, (byte) 0x00, (byte) 0x00, (byte) 0x20, (byte) 0x04, (byte) 0x5E, (byte) 0x14, (byte) 0x25,
            (byte) 0x1, (byte) 0x5E, (byte) 0x18, (byte) 0x25,
            (byte) 0x2, (byte) 0x24, (byte) 0x3, (byte) 0x00, (byte) 0x00, (byte) 0x01, (byte) 0x5E, (byte) 0x15, (byte) 0x25,
            (byte) 0x1, (byte) 0x24, (byte) 0x4, (byte) 0x00, (byte) 0x00, (byte) 0x40, (byte) 0x04, (byte) 0x5E, (byte) 0x14, (byte) 0x25,
            (byte) 0xA, (byte) 0x5E, (byte) 0x18, (byte) 0x25, (byte) 0x04, (byte) 0x6D
    };


    public Hemen(Context context, PrintContract.PrintEventPrsenter printEventPrsenter) {
        this.context = context;
        this.printEventPrsenter = printEventPrsenter;

    }

    @Override
    public void print(TransactionDetailsBean bean) {
        this.bean = bean;

        mmOutStream = BluetoothComm.mosOut ;
        mmInStream = BluetoothComm.misIn;

        new EnterTextTask().execute(0);

    }

    private int PrinterData(String data, int Font) {
        int i = 0, j = 0, col = 0, la = 0;
        char[] tempBuffer = new char[1000];
        int[] dataBuffer = new int[5000];
        //  byte[] b = new byte[2];
        //System.out.println(data);
        data.getChars(0, data.length(), tempBuffer, col);
        totDataLen = data.length();
/////////////////////// PACKET FORMAT /////////////////////
        for (i = 0; la < data.length(); ) {
            if (Font == 1 || Font == 2) {
                if (Font == 1)
                    dataBuffer[i++] = 0xf0;
                else if (Font == 2)
                    dataBuffer[i++] = 0xf1;
                for (j = 0; j < 24; j++) {
                    if (la < data.length()) {
                        dataBuffer[i++] = tempBuffer[la++];
                    } else
                        break;
                }
            } else if (Font == 3 || Font == 4) {
                if (Font == 3)
                    dataBuffer[i++] = 0xf2;
                else if (Font == 4)
                    dataBuffer[i++] = 0xf3;
                for (j = 0; j < 42; j++) {
                    if (la < data.length()) {
                        dataBuffer[i++] = tempBuffer[la++];
                    } else
                        break;
                }
            }
        }
/////////////////////// PACKET FORMAT /////////////////////
        try {
            SendPrint(dataBuffer, i, Font);
        } catch (IOException ex) {

        }
        return 1;
    }

    public int SendPrint(int[] pData, int pLength, int Font) throws IOException {
        int[] pBuff = new int[1000];
        int n, lrc = 0, space = 0, i = 0, iBreak = 0, dataPack = 0, pPos = 0, pInc = 0;
        int iEvLength = 0;

        if (Font == 1 || Font == 2)
            dataPack = pLength / 125 + 1;
        else if (Font == 3 || Font == 4)
            dataPack = pLength / 86 + 1;

        System.out.println(dataPack);
        if (Font == 1 || Font == 2)
            space = pLength % 125;
        else if (Font == 3 || Font == 4)
            space = pLength % 86;
/////////////////// APPENDING SPACES///////////////////
        if (Font == 1 || Font == 2) {
            for (i = 0; ; i++) {
                iBreak = space % 25;
                if (iBreak == 0)
                    break;
                pData[pLength++] = 0x20;
                space++;
            }
        } else if (Font == 3 || Font == 4) {
            for (i = 0; ; i++) {
                iBreak = space % 43;
                if (iBreak == 0)
                    break;
                pData[pLength++] = 0x20;
                space++;
            }
        }
/////////////////// APPENDING SPACES///////////////////
        while (dataPack > 0) {
            pBuff[0] = 0x7e;
            pBuff[1] = 0xb2;
            if (Font == 1 || Font == 2) {
                pPos = pInc * 125;
                if (dataPack > 1)
                    iEvLength = 125;
                else
                    iEvLength = pLength - pPos;
            } else if (Font == 3 || Font == 4) {
                pPos = pInc * 86;
                if (dataPack > 1)
                    iEvLength = 86;
                else
                    iEvLength = pLength - pPos;
            }
            System.out.println("iEvLength=" + iEvLength + "Pos=" + pPos);
            System.arraycopy(pData, pPos, pBuff, 2, iEvLength);
            iEvLength = iEvLength + 2;
            pBuff[iEvLength++] = 0x04;


            for (n = 1; n < iEvLength; n++) {
                lrc ^= pBuff[n];
            }
            pBuff[iEvLength++] = lrc;
            try {
                for (int k = 0; k < iEvLength; k++)
                    mmOutStream.write(pBuff[k]);
                mmOutStream.flush();
                ReadPrinterresp();
            } catch (IOException ex) {
                ex.printStackTrace();
            }

            dataPack--;
            pInc++;
            lrc = 0;
        }
        return 1;
    }

    public int ReadPrinterresp() {
        int iResp = 0;
        String message = "";
        try {
            iResp = mmInStream.read();
            if (iResp != 128 || iResp != 0) {
                switch (iResp) {
                    case 65:
                        message = "ERROR...PAPER OUT";
                        break;
                    case 66:
                        message = "ERROR...PLATEN OPEN";
                        break;
                    case 72:
                        message = "ERROR..TEMP HIGH";
                        break;
                    case 80:
                        message =  "ERROR..TEMP LOW";
                        break;
                    case 96:
                        message =  "ERROR..Improper Voltage";
                        break;
                }

            }
        } catch (Exception e) {
        }
        return iResp;
    }

    public int logoprint()
    {
        int i=0,k=0;

        try {
            Thread.sleep(2000);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            for(k=0;k<(Packet1).length;k++)
                mmOutStream.write(Packet1[k]);
            mmOutStream.flush();
            i = ReadPrinterresp();
            if(i != 128)
                return 1;

            for(k=0;k<(Packet2).length;k++)
                mmOutStream.write(Packet2[k]);
            mmOutStream.flush();
            i=   ReadPrinterresp();
            if(i != 128)
                return 1;

            for(k=0;k<(Packet3).length;k++)
                mmOutStream.write(Packet3[k]);
            mmOutStream.flush();
            i=   ReadPrinterresp();
            if(i != 128)
                return 1;

            for(k=0;k<(Packet4).length;k++)
                mmOutStream.write(Packet4[k]);
            mmOutStream.flush();
            i=   ReadPrinterresp();
            if(i != 128)
                return 1;

            for(k=0;k<(Packet5).length;k++)
                mmOutStream.write(Packet5[k]);
            mmOutStream.flush();
            i=   ReadPrinterresp();
            if(i != 128)
                return 1;

            for(k=0;k<(Packet6).length;k++)
                mmOutStream.write(Packet6[k]);
            mmOutStream.flush();
            i=   ReadPrinterresp();
            if(i != 128)
                return 1;
            /*
            for(k=0;k<(Packet7).length;k++)
                mmOutStream.write(Packet7[k]);
            mmOutStream.flush();
            i=   ReadPrinterresp();
            if(i != 128)
                return 1;

            for(k=0;k<(Packet8).length;k++)
                mmOutStream.write(Packet8[k]);
            mmOutStream.flush();
            i=   ReadPrinterresp();
            if(i != 128)
                return 1;

            for(k=0;k<(Packet9).length;k++)
                mmOutStream.write(Packet9[k]);
            mmOutStream.flush();
            i=   ReadPrinterresp();
            if(i != 128)
                return 1;

            for(k=0;k<(Packet10).length;k++)
                mmOutStream.write(Packet10[k]);
            mmOutStream.flush();
            i=   ReadPrinterresp();
            if(i != 128)
                return 1;
            for(k=0;k<(Packet11).length;k++)
                mmOutStream.write(Packet11[k]);
            mmOutStream.flush();
            i=   ReadPrinterresp();
            if(i != 128)
                return 1;
            for(k=0;k<(Packet12).length;k++)
                mmOutStream.write(Packet12[k]);
            mmOutStream.flush();
            i=   ReadPrinterresp();
            if(i != 128)
                return 1;
            for(k=0;k<(Packet13).length;k++)
                mmOutStream.write(Packet13[k]);
            mmOutStream.flush();
            i=   ReadPrinterresp();
            if(i != 128)
                return 1;
            for(k=0;k<(Packet14).length;k++)
                mmOutStream.write(Packet14[k]);
            mmOutStream.flush();
            i=   ReadPrinterresp();
            if(i != 128)
                return 1;
            for(k=0;k<(Packet15).length;k++)
                mmOutStream.write(Packet15[k]);
            mmOutStream.flush();
            i=   ReadPrinterresp();
            if(i != 128)
                return 1;
            for(k=0;k<(Packet16).length;k++)
                mmOutStream.write(Packet16[k]);
            mmOutStream.flush();
            i=   ReadPrinterresp();
            if(i != 128)
                return 1;

            */

            //PrinterData(" ", 1);
            // PrinterData(" ",1);
            //PrinterData(" ",1);
            // PrinterData(" ",1);
            // PrinterData(" ",1);

            //cancel();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return 1;
    }


    private class EnterTextTask extends AsyncTask<Integer, Integer, Integer> {
        /* displays the progress dialog until background task is completed*/
        private int iRetVal;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        /* Task of EnterTextAsyc performing in the background*/
        @Override
        protected Integer doInBackground(Integer... params) {
            try {

                String line0 = "     Digital India Payments ltd        ";
                String line1 = "  Transaction Receipt  ";
                String line2 = "Customer Copy - " + bean.serviceMod;
                String line3 = "Date & Time    : " + bean.timestamp;
                String line4 = "Terminal ID    : " + bean.terminalId;
                String line5 = "Agent ID       : " + bean.agentId;
                String line6 = "BC Name        : " + bean.BCName;
                String line7 = "mATN req ID    : " + bean.mATNreqID;
                String line8 = "Txn ID         : " + bean.transactionId;
                String line9 = "UIDAI Auth Code: " + bean.aadhaarNo;
                String line10 = "Txn Status     : " + bean.remark;
                String line11 = "Txn Amt        : " + bean.amount;
                String line12 = "A/c Bal        : " + bean.balance;
                String line13 = "   Note: Pls do not pay any charges/fee   " +
                        "               for this txn               ";
                String newLine = "\n";

                PrinterData(line0, 3);
                PrinterData(" ", 1);
                PrinterData(" ", 1);
                logoprint();
                PrinterData(line1, 2);
                PrinterData(line2, 3);
                PrinterData(line3, 3);
                PrinterData(line4, 3);
                PrinterData(line5, 3);
                PrinterData(line6, 3);
                PrinterData(line7, 3);
                PrinterData(line8, 3);
                PrinterData(line10, 3);
                PrinterData(line11, 3);
                PrinterData(line12, 3);
                PrinterData(line13, 3);
                PrinterData(" ", 1);
                PrinterData(" ", 1);
                PrinterData(" ", 1);
                PrinterData(" ", 1);


            } catch (Exception e) {
                iRetVal = DEVICE_NOTCONNECTED;
                return iRetVal;
            }
            return iRetVal;
        }

        /* This displays the status messages of EnterTextAsyc in the dialog box */
        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            printEventPrsenter.onProcessCompletion("Print Success");


            //showResult(iRetVal);
        }
    }


}
