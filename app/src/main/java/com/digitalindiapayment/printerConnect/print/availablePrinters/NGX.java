package com.digitalindiapayment.printerConnect.print.availablePrinters;


import android.content.ComponentName;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;

import com.digitalindiapayment.R;
import com.digitalindiapayment.pojo.TransactionDetailsBean;
import com.digitalindiapayment.printerConnect.print.PrintContract;
import com.esys.impressivedemo.bluetooth.BluetoothComm;
import com.ngx.BluetoothPrinter;

import com.ngx.DebugLog;


import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

/**
 * Created by admin on 06-06-2017.
 */


public class NGX implements PrintContract.Printer {

    private Context context;
    private PrintContract.PrintEventPrsenter printEventPrsenter;
    private BluetoothPrinter mBtp ;

    private TransactionDetailsBean bean;
    InputStream mmInStream;
    OutputStream mmOutStream;


    // Byte Commands
    // --------- FONTS
    private static byte[] FONT_ONE = new byte[]{27, 116, 0};
    private static byte[] FONT_TWO = new byte[]{27, 116, 1};
    private static byte[]  FONT_THREE = new byte[]{27, 116, 10};

    // ---------- Font - Size
    private static byte[] FONT_SIZE_ONE = new byte[]{27, 33, 0};
    private static byte[] FONT_SIZE_TWO = new byte[]{27, 33, 15};
    private static byte[] FONT_SIZE_THREE = new byte[]{27, 33, -16};
    private static byte[] FONT_SIZE_FOUR = new byte[]{27, 33, -1};

    // Line feed : **********************************
    private static byte[] PRINT_LINE_FEED = new byte[]{10};

    // Print Logo
    private static byte[] PRINT_LOGO = new byte[]{27, 47, 0};


    // Image Related Prop
    private boolean bInvertBitmap = false;
    private boolean bIgnoreAlpha = true;
    private boolean bImageSelected = false;
    private int threshold = 127;
    private static byte[] o = new byte[]{10, 27, 47};







    public NGX(Context context,PrintContract.PrintEventPrsenter printEventPrsenter) {
        this.context = context;
        this.printEventPrsenter = printEventPrsenter;
        this.mBtp = BluetoothPrinter.INSTANCE;
        mmOutStream = BluetoothComm.mosOut;
        mmInStream = BluetoothComm.misIn;
    }

    @Override
    public void print(TransactionDetailsBean bean) {
        this.bean = bean;
        //mBtp.initService(this, mHandler);


        new EnterTextTask().execute(0);

        //mmOutStream.write();

    }
    private Boolean printTextLine(String var1) throws IOException {
        if(var1 != null && var1.length() != 0) {
            int var2 = var1.length() / 250;

            for(int var4 = 0; var4 < var2; ++var4) {
                byte[] var3 = var1.substring(var4 * 250, 250 * (var4 + 1)).getBytes();
                mmOutStream.write(var3);
                //Thread.sleep(1000L);
            }

            if(var1.length() % 250 != 0) {
                byte[] var5 = var1.substring(var2 * 250).getBytes();
                mmOutStream.write(var5);
            }

            //this.a("Print Text - OK");
            return true;
        }
        return true;

    };

    private class EnterTextTask extends AsyncTask<Integer, Integer, Integer> {
        /* displays the progress dialog until background task is completed*/


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        /* Task of EnterTextAsyc performing in the background*/
        @Override
        protected Integer doInBackground(Integer... params) {
            try {

                try {
                    //mBtp.initService(context.getApplicationContext());
                    String path = "android.resource://"+context.getPackageName()+"/raw/" + R.raw.logo ;
                    //mBtp.printImage(path);
                    //setPrintFontSize(BtpCommands.FONT_SIZE_NORMAL);
                    //Uri path = Uri.parse("android.resource://"+context.getPackageName()+"/raw/logo");
                    //String path1 = "file:///android_asset/logo";
                    mmOutStream.write(FONT_TWO);

                    //(context.getApplicationContext().getResources().openRawResource(R.raw.logo)).
                    //String path = "android.resource://com.digitalindiapayment/raw/" + R.raw.logo ;
                    //Bitmap b = NgxImageFactory.LoadLogo(path.getPath());
                    //Bitmap c =  BitmapFactory.decodeResource(context.getResources(), R.drawable.logo);
                    //InputStream is = context.getResources().openRawResource(R.raw.logo);
                    //Bitmap b = BitmapFactory.decodeStream(is);
                    //c.setWidth(230);
                    //Bitmap b = Bitmap.createScaledBitmap(c,224,56,false);
                    //setLogo(b.getWidth(),b.getHeight(),a(b));
                    //threshold = NgxImageFactory.getThreshold();

                    //mBtp.printImage();
                    //setLogo(path.getPath(), bIgnoreAlpha, bInvertBitmap, threshold);
                    mmOutStream.write(PRINT_LOGO);

                    printTextLine("\n Digital India Payments ltd        ");
                    mmOutStream.write(FONT_SIZE_ONE);


                    printTextLine("\n Transaction Receipt ");
                    mmOutStream.write(FONT_SIZE_THREE);

                    printTextLine(" ");
                    mmOutStream.write(FONT_SIZE_ONE);
                    printTextLine("\nCustomer Copy - " + bean.serviceMod);

                    printTextLine("\nDate & Time    : " + bean.timestamp);
                    printTextLine("\nTerminal ID    : " + bean.terminalId);
                    printTextLine("\nAgent ID       : " + bean.agentId);

                    printTextLine("\nBC Name        : " + bean.BCName);
                    printTextLine("\nmATN req ID    : " + bean.mATNreqID);
                    printTextLine("\nTxn ID         : " + bean.transactionId);
                    printTextLine("\nUIDAI Auth Code: " + bean.aadhaarNo);
                    printTextLine("\nTxn Status     : " + bean.remark);
                    printTextLine("\nTxn Amt        : " + bean.amount);
                    printTextLine("\nA/c Bal        : " + bean.balance);
                    printTextLine("\n  Note: Pls do not pay any charges/fee   ");
                    printTextLine("          for this txn  \n\n\n\n");
                    mmOutStream.write(PRINT_LINE_FEED);

                } catch (IOException e) {
                    e.printStackTrace();
                }



            } catch (Exception e) {
                e.printStackTrace();
                //return iRetVal;
            }
            return 1;
        }

        /* This displays the status messages of EnterTextAsyc in the dialog box */
        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);

        }
    }


    protected static byte[] a(Bitmap var0) {
        int var2 = var0.getWidth();
        int var3 = var0.getHeight();
        int var1 = var2 * (var3 / 2) / 8;
        if(var2 <= 240 && var2 >= 8) {
            if(var3 <= 500 && var3 >= 8) {
                if(var1 % 8 != 0) {
                    throw new IllegalArgumentException("Image size should be a multiple of 8");
                } else {
                    byte[] var4 = new byte[var1];
                    byte var5 = 0;
                    int var6 = 7;
                    int var7 = 0;

                    for(int var8 = 0; var8 < var3; ++var8) {
                        for(int var9 = 0; var9 < var2; ++var9) {
                            if(Color.red(var0.getPixel(var9, var8)) > 0) {
                                var5 = (byte)(var5 | 1 << var6);
                            }

                            if(var6 == 0) {
                                var6 = 7;
                                if(var8 % 2 == 0) {
                                    var4[var7] = var5;
                                    ++var7;
                                }

                                var5 = 0;
                            } else {
                                --var6;
                            }
                        }
                    }

                    return var4;
                }
            } else {
                throw new IllegalArgumentException("Image Height out of bounds, should be within 8 and 255 pixels");
            }
        } else {
            throw new IllegalArgumentException("Image Width out of bounds, should be within 8 and 240 pixels");
        }
    }

    public final boolean setLogo(int var1, int var2, byte[] var3) {
        DebugLog.logTrace();
        if(var1 <= 255 && var1 >= 8 && var1 % 8 == 0) {
            if(var2 <= 255 && var2 > 0 && var2 % 8 == 0) {
                if(false) {
                    return false;
                } else {
                    byte[] var4;
                    (var4 = new byte[6])[0] = 27;
                    var4[1] = 42;
                    var4[2] = (byte)var1;
                    var4[3] = (byte)(var2 / 2);
                    byte[] var10;
                    (var10 = new byte[4])[2] = var4[2];
                    var10[3] = var4[3];
                    byte[] var11 = new byte[var3.length + 4];
                    System.arraycopy(var10, 0, var11, 0, 4);
                    System.arraycopy(var3, 0, var11, 4, var3.length);
                    short var12 = 0;
                    byte[] var5 = new byte[2];
                    int var6 = (var10 = var11).length;

                    for(int var7 = 0; var7 < var6; ++var7) {
                        byte var8 = var10[var7];
                        var12 = (short)(var12 + (var8 & 255));
                    }

                    var1 = var12 & '\uffff';
                    var5[0] = (byte)var1;
                    var1 >>= 8;
                    var5[1] = (byte)var1;
                    DebugLog.logTrace("Image Checksum : " + DebugLog.bytesToHex(var5));
                    DebugLog.bytesToHex(var5);
                    var4[4] = var5[0];
                    var4[5] = var5[1];
                    try {
                        mmOutStream.write(var4);
                        try {
                            Thread.sleep(100L);
                        } catch (InterruptedException var9) {
                            var9.printStackTrace();
                        }

                        mmOutStream.write(var3);
                        mmOutStream.write(o);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }




                    return true;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

}
