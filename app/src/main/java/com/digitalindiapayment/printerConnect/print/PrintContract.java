package com.digitalindiapayment.printerConnect.print;

import com.digitalindiapayment.pojo.TransactionDetailsBean;
import com.digitalindiapayment.pojo.TransactionReportBean;

/**
 * Created by admin on 06-06-2017.
 */

public class PrintContract {
    public interface Printer{
        public void print(TransactionDetailsBean bean);
    }
    public  interface PrintEventPrsenter{
        public void onProcessCompletion(String message);
    }
}
