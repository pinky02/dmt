package com.digitalindiapayment.printerConnect.print.availablePrinters;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.digitalindiapayment.R;
import com.digitalindiapayment.application.MyApplication;
import com.digitalindiapayment.pojo.TransactionDetailsBean;
import com.digitalindiapayment.printerConnect.print.PrintContract;
import com.digitalindiapayment.util.Util;
import com.esys.impressivedemo.ImpressSetup;
import com.esys.impressivedemo.bluetooth.BluetoothComm;
import com.impress.api.Printer;


/**
 * Created by admin on 06-06-2017.
 */

public class Evolute implements PrintContract.Printer {
    private byte bFontstyleSmall = 0x03;
    private byte bFontstyleBig = 0x02;
    private byte bFontstyleSmallLine = 0x07;
    private com.impress.api.Printer ptr;
    public static final int DEVICE_NOTCONNECTED = -100;
    PrintContract.PrintEventPrsenter PrintEventPresenter;


    private Context context;
    private TransactionDetailsBean bean;

    public Evolute(Context context, PrintContract.PrintEventPrsenter PrintEventPresenter) {
        this.context = context;
        this.PrintEventPresenter = PrintEventPresenter;

    }

    @Override
    public void print(TransactionDetailsBean bean) {
        this.bean = bean;


        new EnterTextTask().execute(0);


    }

    private class EnterTextTask extends AsyncTask<Integer, Integer, Integer> {
        /* displays the progress dialog until background task is completed*/
        private int iRetVal;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        /* Task of EnterTextAsyc performing in the background*/
        @Override
        protected Integer doInBackground(Integer... params) {


            try {
                ptr = new Printer(ImpressSetup.onCreate(context.getApplicationContext()), BluetoothComm.mosOut, BluetoothComm.misIn);
                if (!ptr.sGetSerialNumber().equals("")) {

                        String line0 = "        Digital India Payments ltd        ";
                        String line1 = "  Transaction Receipt  ";
                        String line2 = "Customer Copy - " + bean.serviceMod;
                        String line3 = "Date & Time    : " + bean.timestamp;
                        String line4 = "Terminal ID    : " + bean.terminalId;
                        String line5 = "Agent ID       : " + bean.agentId;
                        String line6 = "BC Name        : " + bean.BCName;
                        String line7 = "mATN req ID    : " + bean.mATNreqID;
                        String line8 = "Txn ID         : " + bean.transactionId;
                        String line9 = "UIDAI Auth Code: " + bean.aadhaarNo;
                        String line10 = "Txn Status     : " + bean.remark;
                        String line11 = "Txn Amt        : " + bean.amount;
                        String line12 = "A/c Bal        : " + bean.balance;
                        String line13 = "   Note: Pls do not pay any charges/fee   " +
                                "               for this txn               ";
                        String newLine = "\n";

                        ptr.iFlushBuf();


                        ptr.iPrinterAddData(bFontstyleBig, newLine);

                        ptr.iPrinterAddData(bFontstyleSmallLine, line0);
                        iRetVal = ptr.iStartPrinting(1);
                        ptr.iFlushBuf();


                        iRetVal = ptr.iBmpPrint(context.getApplicationContext(), R.raw.logo);

                        ptr.iFlushBuf();


                        ptr.iPrinterAddData(bFontstyleBig, line1);

                        //ptr.iPrinterAddData(bFontstyleSmall, newLine);
                        ptr.iPrinterAddData(bFontstyleSmall, line2);
                        //ptr.iPrinterAddData(bFontstyleSmall, newLine);
                        ptr.iPrinterAddData(bFontstyleSmall, line3);
                        //ptr.iPrinterAddData(bFontstyleSmall, newLine);
                        ptr.iPrinterAddData(bFontstyleSmall, line4);
                        //ptr.iPrinterAddData(bFontstyleSmall, newLine);
                        ptr.iPrinterAddData(bFontstyleSmall, line5);
                        //ptr.iPrinterAddData(bFontstyleSmall, newLine);
                        ptr.iPrinterAddData(bFontstyleSmall, line6);
                        //ptr.iPrinterAddData(bFontstyleSmall, newLine);
                        ptr.iPrinterAddData(bFontstyleSmall, line7);
                        //ptr.iPrinterAddData(bFontstyleSmall, newLine);
                        ptr.iPrinterAddData(bFontstyleSmall, line8);
                        //ptr.iPrinterAddData(bFontstyleSmall, newLine);
                        ptr.iPrinterAddData(bFontstyleSmall, line9);
                        //ptr.iPrinterAddData(bFontstyleSmall, newLine);
                        ptr.iPrinterAddData(bFontstyleSmall, line10);
                        //ptr.iPrinterAddData(bFontstyleSmall, newLine);
                        ptr.iPrinterAddData(bFontstyleSmall, line11);
                        //ptr.iPrinterAddData(bFontstyleSmall, newLine);
                        ptr.iPrinterAddData(bFontstyleSmall, line12);
                        ptr.iPrinterAddData(bFontstyleSmall, line13);
                        ptr.iPrinterAddData(bFontstyleSmall, newLine + newLine + newLine);
                        ptr.iPrinterAddData(bFontstyleSmall, newLine + newLine);


                        iRetVal = ptr.iStartPrinting(1);

                }
            } catch (Exception e) {
                iRetVal = DEVICE_NOTCONNECTED;
                //PrintEventPresenter.onProcessCompletion(context.getResources().getString(R.string.invaliddevice));
                return iRetVal;
                //e.printStackTrace();
            }

            return iRetVal;
        }

        /* This displays the status messages of EnterTextAsyc in the dialog box */
        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            showResult(iRetVal);
        }
    }

    private void showResult(int iRetVal) {
        String message = "";
        if (iRetVal == DEVICE_NOTCONNECTED) {
            message = "Device not connected";
            ((MyApplication) context.getApplicationContext()).closeConn();

        } else if (iRetVal == com.impress.api.Printer.PR_SUCCESS) {
            message = context.getString(R.string.print_success);
            //Activity_RESULT = RESULT_OK;

        } else if (iRetVal == com.impress.api.Printer.PR_PLATEN_OPEN) {
            message = "Printer platen is open";
        } else if (iRetVal == com.impress.api.Printer.PR_PAPER_OUT) {
            message = "Printer paper is out";
        } else if (iRetVal == com.impress.api.Printer.PR_IMPROPER_VOLTAGE) {
            message = "Printer improper voltage";
        } else if (iRetVal == com.impress.api.Printer.PR_FAIL) {
            message = "Printer failed";
        } else if (iRetVal == com.impress.api.Printer.PR_PARAM_ERROR) {
            message = "Printer param error";
        } else if (iRetVal == com.impress.api.Printer.PR_NO_RESPONSE) {
            message = "No response from impress device";
        } else if (iRetVal == com.impress.api.Printer.PR_DEMO_VERSION) {
            message = "Library is in demo version";
        } else if (iRetVal == com.impress.api.Printer.PR_INVALID_DEVICE_ID) {
            message = "Connected  device is not license authenticated";
        } else if (iRetVal == com.impress.api.Printer.PR_ILLEGAL_LIBRARY) {
            message = "Library not valid";

        }
        PrintEventPresenter.onProcessCompletion(message);

    }

}
