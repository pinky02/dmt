package com.digitalindiapayment.printerConnect.print.availablePrinters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.digitalindiapayment.R;
import com.digitalindiapayment.application.MyApplication;
import com.digitalindiapayment.pojo.TransactionDetailsBean;
import com.digitalindiapayment.printerConnect.print.PrintContract;
import com.rdservice.helper.Printer;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;


/**
 * Created by amolbhanushali on 2017/12/21.
 */

public class EvoluteRDPrint extends Activity implements PrintContract.Printer {
    private byte bFontstyleSmall = 0x03;
    private byte bFontstyleBig = 0x02;
    private byte bFontstyleSmallLine = 0x07;
    private com.rdservice.helper.Printer ptr;
    public static final int DEVICE_NOTCONNECTED = -100;
    PrintContract.PrintEventPrsenter PrintEventPresenter;
    private final String TAG = "EvoluteRDPrint";


    private Context context;
    private TransactionDetailsBean bean;

    public EvoluteRDPrint(Context context, PrintContract.PrintEventPrsenter PrintEventPresenter) {
        this.context = context;
        this.PrintEventPresenter = PrintEventPresenter;

    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.empty_layout);

    }

    @Override
    public void print(TransactionDetailsBean bean) {
        this.bean = bean;

        String printValue;

        /// new EvoluteRDPrint.EnterTextTask().execute(0);
        try {
            ptr = new com.rdservice.helper.Printer(true);
            if (ptr != null) {

                String line0 = "        Digital India Payments ltd        ";
                String line1 = "  Transaction Receipt  ";
                String line2 = "Customer Copy - " + bean.serviceMod;
                String line3 = "Date & Time    : " + bean.timestamp;
                String line4 = "Terminal ID    : " + bean.terminalId;
                String line5 = "Agent ID       : " + bean.agentId;
                String line6 = "BC Name        : " + bean.BCName;
                String line7 = "mATN req ID    : " + bean.mATNreqID;
                String line8 = "Txn ID         : " + bean.transactionId;
                String line9 = "UIDAI Auth Code: " + bean.aadhaarNo;
                String line10 = "Txn Status     : " + bean.remark;
                String line11 = "Txn Amt        : " + bean.amount;
                String line12 = "A/c Bal        : " + bean.balance;
                String line13 = "   Note: Pls do not pay any charges/fee   " +
                        "               for this txn               ";
                String newLine = "\n";

                ptr.iFlushBuf();


                ptr.iPrinterAddData(bFontstyleBig, newLine);

                ptr.iPrinterAddData(bFontstyleSmallLine, line0);
                printValue = ptr.sStartPrinting("dipl", "ESYS63500", "00:06:66:81:3F:D1");
                ptr.iFlushBuf();


                //iRetVal = ptr.iBmpPrint(context.getApplicationContext(), R.raw.logo);

                //ptr.iFlushBuf();


                ptr.iPrinterAddData(bFontstyleBig, line1);

                //ptr.iPrinterAddData(bFontstyleSmall, newLine);
                ptr.iPrinterAddData(bFontstyleSmall, line2);
                //ptr.iPrinterAddData(bFontstyleSmall, newLine);
                ptr.iPrinterAddData(bFontstyleSmall, line3);
                //ptr.iPrinterAddData(bFontstyleSmall, newLine);
                ptr.iPrinterAddData(bFontstyleSmall, line4);
                //ptr.iPrinterAddData(bFontstyleSmall, newLine);
                ptr.iPrinterAddData(bFontstyleSmall, line5);
                //ptr.iPrinterAddData(bFontstyleSmall, newLine);
                ptr.iPrinterAddData(bFontstyleSmall, line6);
                //ptr.iPrinterAddData(bFontstyleSmall, newLine);
                ptr.iPrinterAddData(bFontstyleSmall, line7);
                //ptr.iPrinterAddData(bFontstyleSmall, newLine);
                ptr.iPrinterAddData(bFontstyleSmall, line8);
                //ptr.iPrinterAddData(bFontstyleSmall, newLine);
                ptr.iPrinterAddData(bFontstyleSmall, line9);
                //ptr.iPrinterAddData(bFontstyleSmall, newLine);
                ptr.iPrinterAddData(bFontstyleSmall, line10);
                //ptr.iPrinterAddData(bFontstyleSmall, newLine);
                ptr.iPrinterAddData(bFontstyleSmall, line11);
                //ptr.iPrinterAddData(bFontstyleSmall, newLine);
                ptr.iPrinterAddData(bFontstyleSmall, line12);
                ptr.iPrinterAddData(bFontstyleSmall, line13);
                ptr.iPrinterAddData(bFontstyleSmall, newLine + newLine + newLine);
                ptr.iPrinterAddData(bFontstyleSmall, newLine + newLine);


                printValue = ptr.sStartPrinting("Dipl", "ESYS63500", "00:06:66:81:3F:D1");
                if (printValue == null) {
                    ptr.iGetReturnCode();
                } else {
                    try {
                        Log.e(TAG, " inXml Length is :" + printValue.length());
                        Log.e(TAG, "++++ xml Data = " + printValue);
                        Log.e(TAG, "**** Sending data via intent");
                        Intent act = new Intent(Printer.URI);
                        act.putExtra(Printer.DataTAG, printValue);
                        PackageManager packageManager = getApplicationContext().getPackageManager();
                        List activities = packageManager.queryIntentActivities(act, PackageManager.MATCH_DEFAULT_ONLY);
                        final boolean isIntentSafe = activities.size() > 0;
                        Log.d(TAG, "Boolean check for activities = " + isIntentSafe);
                        if (!isIntentSafe) {
                            Toast.makeText(getApplicationContext(), "No RD Service Available", Toast.LENGTH_SHORT).show();
                        }
                        Log.d(TAG, "No of activities = " + activities.size());
                        Log.e(TAG, "**** After sending parcelable class via intent");
                        startActivityForResult(act, 2);
                    } catch (Exception e) {
                        Log.e(TAG, "Error while connecting to RDService");
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
            //iRetVal = DEVICE_NOTCONNECTED;
            //PrintEventPresenter.onProcessCompletion(context.getResources().getString(R.string.invaliddevice));
            //return iRetVal;
            //e.printStackTrace();
            e.printStackTrace();
        }


    }

//    private class EnterTextTask extends AsyncTask<Integer, Integer, Integer> {
//        /* displays the progress dialog until background task is completed*/
//        private String  printValue;
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//        }
//
//        /* Task of EnterTextAsyc performing in the background*/
//        @Override
//        protected Integer doInBackground(Integer... params) {
//
//
//
//            return iRetVal;
//        }
//
//        /* This displays the status messages of EnterTextAsyc in the dialog box */
//        @Override
//        protected void onPostExecute(Integer result) {
//            super.onPostExecute(result);
//            showResult(iRetVal);
//        }
//    }

    private void showResult(int iRetVal) {
        String message = "";
        if (iRetVal == DEVICE_NOTCONNECTED) {
            message = "Device not connected";
            ((MyApplication) context.getApplicationContext()).closeConn();

        } else if (iRetVal == com.impress.api.Printer.PR_SUCCESS) {
            message = context.getString(R.string.print_success);
            //Activity_RESULT = RESULT_OK;

        } else if (iRetVal == com.impress.api.Printer.PR_PLATEN_OPEN) {
            message = "Printer platen is open";
        } else if (iRetVal == com.impress.api.Printer.PR_PAPER_OUT) {
            message = "Printer paper is out";
        } else if (iRetVal == com.impress.api.Printer.PR_IMPROPER_VOLTAGE) {
            message = "Printer improper voltage";
        } else if (iRetVal == com.impress.api.Printer.PR_FAIL) {
            message = "Printer failed";
        } else if (iRetVal == com.impress.api.Printer.PR_PARAM_ERROR) {
            message = "Printer param error";
        } else if (iRetVal == com.impress.api.Printer.PR_NO_RESPONSE) {
            message = "No response from impress device";
        } else if (iRetVal == com.impress.api.Printer.PR_DEMO_VERSION) {
            message = "Library is in demo version";
        } else if (iRetVal == com.impress.api.Printer.PR_INVALID_DEVICE_ID) {
            message = "Connected  device is not license authenticated";
        } else if (iRetVal == com.impress.api.Printer.PR_ILLEGAL_LIBRARY) {
            message = "Library not valid";

        }
        PrintEventPresenter.onProcessCompletion(message);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode==2){
            PrintEventPresenter.onProcessCompletion("sucess");

            String  postAuth;
            if (resultCode == Activity.RESULT_OK) {
                try {
                    postAuth = data.getStringExtra(Printer.ResponseDataTAG);
                    Log.e(TAG, "result data:" + postAuth);
                    if (postAuth != null) {

                        getResult(postAuth);
                    } else {
                        Log.e(TAG, "PostAuth is null inside in result");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void getResult( String postAuth2) {
        try {
            Log.e(TAG, " Test XML parsing start and fetching BTmac ");
            InputStream is = new ByteArrayInputStream(postAuth2.getBytes());
            DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
            domFactory.setIgnoringComments(true);
            DocumentBuilder builder = domFactory.newDocumentBuilder();
            Document doc = builder.parse(is);

            NodeList nodeList = doc.getElementsByTagName("*");
            ArrayList<String> elmntarrayList = new ArrayList<String>();
            for (int i = 0; i < nodeList.getLength(); i++) {
                Element element = (Element) nodeList.item(i);
                String postAuth = element.getNodeName();
                Log.e(TAG, "PostAuthElement = " + postAuth);

                elmntarrayList.add(postAuth);
                Log.e(TAG, "elmntarrayList = " + elmntarrayList);
                Log.e(TAG, "elmntarrayList = " + elmntarrayList.size());
            }

            ArrayList<String> custarrayList = new ArrayList<String>();
            NodeList custElmntLst = doc.getElementsByTagName("CustOpts");
            Element baseElmntcust = (Element) custElmntLst.item(0);

            if (baseElmntcust == null) {
                Log.e(TAG, "baseElmntcust = " + baseElmntcust);
            } else {
                NamedNodeMap custElmnattrattr = baseElmntcust.getAttributes();
                for (int i = 0; i < custElmnattrattr.getLength(); ++i) {
                    Node attr = custElmnattrattr.item(i);
                    String demoattributes = attr.getNodeName();
                    custarrayList.add(demoattributes);
                    Log.e(TAG, "custarrayList = " + custarrayList);
                }
                Log.e(TAG, "custarrayList = " + custarrayList.size());
            }

            ArrayList<String> paramarrayList = new ArrayList<String>();
            NodeList paramElmntLst = doc.getElementsByTagName("Param");
            Element baseElmntparam = (Element) paramElmntLst.item(0);

            if (baseElmntparam == null) {
                Log.e(TAG, "baseElmntparam = " + baseElmntparam);
            } else {
                NamedNodeMap paramElmnattrattr = baseElmntparam.getAttributes();
                for (int i = 0; i < paramElmnattrattr.getLength(); ++i) {
                    Node attr = paramElmnattrattr.item(i);
                    String paramattributes = attr.getNodeName();
                    paramarrayList.add(paramattributes);
                }
            }
            if (elmntarrayList.contains("PostAuth")) {
                Log.e(TAG, "PostAuth present");
            } else {
                Log.e(TAG, "PostAuth notpresent");
            }
            if (elmntarrayList.contains("CustOpts")) {
                Log.e(TAG, "CustOpts present");
                if (paramarrayList.contains("name")) {
                    String resultValueName = doc.getElementsByTagName("Param").item(0).getAttributes().getNamedItem("name").getTextContent();
                    Log.e(TAG, "resultValueName = " + resultValueName);

                }
                if (paramarrayList.contains("value")) {
                    String resultIntValue = doc.getElementsByTagName("Param").item(0).getAttributes().getNamedItem("value").getTextContent();
                    Log.e(TAG, "resultIntValue = " + resultIntValue);
                }
                if (paramarrayList.contains("name")) {
                    String resultDataName = doc.getElementsByTagName("Param").item(1).getAttributes().getNamedItem("name").getTextContent();
                    Log.e(TAG, " resultDataName = " + resultDataName);

                }
                if (paramarrayList.contains("value")) {
                    String resultData = doc.getElementsByTagName("Param").item(1).getAttributes().getNamedItem("value").getTextContent();
                    Log.e(TAG, "resultData = " + resultData);
                }
                if (elmntarrayList.size() > 4) {
                    if (paramarrayList.contains("name")) {
                        String Tracknameone = doc.getElementsByTagName("Param").item(2).getAttributes().getNamedItem("name").getTextContent();
                        Log.e(TAG, " Trackname = " + Tracknameone);

                    }
                    if (paramarrayList.contains("value")) {
                        String track_one_data = doc.getElementsByTagName("Param").item(2).getAttributes().getNamedItem("value").getTextContent();
                        Log.e(TAG, "Trackdata = " + track_one_data);
//                        txt_result.append("\n"+track_one_data);
                    }
                    if (paramarrayList.contains("name")) {
                        String Tracknametwo = doc.getElementsByTagName("Param").item(3).getAttributes().getNamedItem("name").getTextContent();
                        Log.e(TAG, " Trackname = " + Tracknametwo);

                    }
                    if (paramarrayList.contains("value")) {
                        String track_two_data = doc.getElementsByTagName("Param").item(3).getAttributes().getNamedItem("value").getTextContent();
                        Log.e(TAG, "Trackdata = " + track_two_data);
//                        txt_result.append(" "+track_two_data);
                    }
                    if (paramarrayList.contains("name")) {
                        String Tracknamethree = doc.getElementsByTagName("Param").item(4).getAttributes().getNamedItem("name").getTextContent();
                        Log.e(TAG, " Trackname  = " + Tracknamethree);

                    }
                    if (paramarrayList.contains("value")) {
                        String track_three_data = doc.getElementsByTagName("Param").item(4).getAttributes().getNamedItem("value").getTextContent();
                        Log.e(TAG, "Trackdata " + track_three_data);
//                        txt_result.append(" "+track_three_data);
                    }
                }

            } else {
                Log.e(TAG, "CustOpts notpresent");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}

