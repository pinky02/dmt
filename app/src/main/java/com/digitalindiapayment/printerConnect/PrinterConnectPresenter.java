package com.digitalindiapayment.printerConnect;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.digitalindiapayment.R;
import com.digitalindiapayment.application.MyApplication;
import com.digitalindiapayment.pojo.TransactionDetailsBean;
import com.digitalindiapayment.printerConnect.PrinterConnectContract.UserAction;
import com.digitalindiapayment.printerConnect.print.PrintContract;
import com.digitalindiapayment.printerConnect.print.availablePrinters.Evolute;
import com.digitalindiapayment.printerConnect.print.availablePrinters.EvoluteRDPrint;
import com.digitalindiapayment.printerConnect.print.availablePrinters.Hemen;
import com.digitalindiapayment.util.Util;
import com.impress.api.Printer;

/**
 * Created by admin on 03-06-2017.
 */

public class PrinterConnectPresenter implements UserAction, PrintContract.PrintEventPrsenter {

    private final PrinterConnectContract.View view;
    private final Context context;
    private TransactionDetailsBean bean;

    public static final int DEVICE_NOTCONNECTED = -100;
    public Printer ptr;
    int Activity_RESULT = Activity.RESULT_CANCELED;
    private ProgressDialog dlgPg;
    PrintContract.Printer Printer;


    public PrinterConnectPresenter(PrinterConnectContract.View view, Context context) {
        this.view = view;
        this.context = context;
    }

    /*
    *** printReceipt
        check for the Bluetooth device is connected or not
        then give Alert dialog
        wether user is sure about printing
        If Connected then call Async task to print

     */
    @Override
    public void printReceipt(final TransactionDetailsBean bean, String printerName) {
        if (bean != null) {
            this.bean = bean;
        }

        //00:06:66:81:3F:D1   mac
        //ESYS63500
        if (printerName.equals(context.getString(R.string.device_evolute_printer))
                && Util.PREFERENCES.getDeviceName(context).equalsIgnoreCase(context.getString(R.string.device_evolute_name))) {
            Printer = new EvoluteRDPrint(context.getApplicationContext(),PrinterConnectPresenter.this);

        } else if (printerName.equals(context.getString(R.string.device_evolute_printer))) {
            Printer = new Evolute(context.getApplicationContext(), PrinterConnectPresenter.this);
        } else if (printerName.equals(context.getString(R.string.device_hemen_printer))) {
            Printer = new Hemen(context.getApplicationContext(), PrinterConnectPresenter.this);
        }


        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder((Activity) view);
        alertDialogBuilder.setTitle(context.getString(R.string.print_receipt));
        alertDialogBuilder.setMessage(context.getString(R.string.print_confirm_message)).setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Logic to Print
                        // Loader
                        dlgPg = new ProgressDialog((Activity) view);
                        dlgPg.setMessage("Please Wait...");
                        dlgPg.setCancelable(false);
                        dlgPg.show();
                        Printer.print(bean);
                        //new EnterTextTask).execute(0);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //new EnterTextTask().execute(0);
                    }
                });
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();

    }

    @Override
    public Boolean isBluetoothPrinterConnected() {
        return ((MyApplication) context.getApplicationContext()).isBluetoothDeviceConnected();
    }


    @Override
    public void onProcessCompletion(String message) {
        dlgPg.dismiss();
        view.onPrintCompletion(message);
    }
}
