package com.digitalindiapayment.printerConnect.ngxprinter;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Layout;
import android.text.TextPaint;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.digitalindiapayment.R;
import com.digitalindiapayment.pojo.TransactionDetailsBean;
import com.digitalindiapayment.printerConnect.print.availablePrinters.Evolute;
import com.digitalindiapayment.util.Util;
import com.ngx.BluetoothPrinter;
import com.ngx.DebugLog;

import butterknife.BindView;
import butterknife.ButterKnife;


public class NGXPrintActivity extends AppCompatActivity {
    @BindView(R.id.deviceStatus)
    TextView deviceStatus;
    @BindView(R.id.btn_print)
    Button btnPrint;
    @BindView(R.id.btn_connect)
    Button btnConnect;

    private TransactionDetailsBean bean;
    public static BluetoothPrinter mBtp = BluetoothPrinter.INSTANCE;
    public static SharedPreferences mSp;

    private String mConnectedDeviceName = "";

    public static final String title_connecting = "Connecting...";
    public static final String title_connected_to = "Connected: ";
    public static final String title_not_connected = "Not Connected";

    private Typeface tf = null;
    private int fontSize = 20;
    TextPaint tp;


    @SuppressLint("HandlerLeak")
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Log.d("Handler",msg.what+"  "+msg.arg1);
            switch (msg.what) {
                case BluetoothPrinter.MESSAGE_STATE_CHANGE:
                    switch (msg.arg1) {
                        case BluetoothPrinter.STATE_CONNECTED:
                            deviceStatus.setText(title_connected_to);
                            deviceStatus.append(mConnectedDeviceName);
                            setConnectEnable(true);

                            break;
                        case BluetoothPrinter.STATE_CONNECTING:
                            deviceStatus.setText(title_connecting);
                            setConnectEnable(false);
                            break;
                        case BluetoothPrinter.STATE_LISTEN:
                        case BluetoothPrinter.STATE_NONE:
                            setConnectEnable(true);
                            deviceStatus.setText(title_not_connected);
                            break;
                        case BluetoothPrinter.PRINTER_SWITCHED_OFF:break;

                        }
                    break;
                case BluetoothPrinter.MESSAGE_DEVICE_NAME:
                    // save the connected device's name
                    mConnectedDeviceName = msg.getData().getString(BluetoothPrinter.DEVICE_NAME);
                    setConnectEnable(false);
                    break;
                case BluetoothPrinter.MESSAGE_STATUS:
                    setConnectEnable(true);
                    deviceStatus.setText(msg.getData().getString(BluetoothPrinter.STATUS_TEXT));
                    break;
                case BluetoothPrinter.PRINT_FINISH:
                    //String printStatus = msg.getData().getString(BluetoothPrinter.PRINT_STATUS);
                    Util.showShortToast(getApplicationContext(),getResources().getString(R.string.print_success));
                    finish();
                    break;

                default:
                    break;
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ngxprint);
        ButterKnife.bind(this);
        tp = new TextPaint();
        tp.setColor(Color.BLACK);
        bean = getIntent().getParcelableExtra("DATA");

        mSp = PreferenceManager.getDefaultSharedPreferences(this);

        try {
            mBtp.initService(this, mHandler);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //mBtp.getPrinterStatus();
        //mBtp.getState();
        //mBtp.connectToPrinter();
        mBtp.showDeviceList(NGXPrintActivity.this);
        //mBtp.


        btnConnect.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDarkSelection));

        btnConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBtp.showDeviceList(NGXPrintActivity.this);
            }
        });

        btnPrint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //tp.setTypeface(tf);
                btnPrint.setEnabled(false);
                new EnterTextTask().execute(0);

                //mBtp.add



            }
        });

    }



    @Override
    public void onPause() {
        mBtp.onActivityPause();
        super.onPause();
    }

    @Override
    public void onResume() {
        mBtp.onActivityResume();
        DebugLog.logTrace("onResume");
        super.onResume();
    }

    @Override
    public void onDestroy() {
        mBtp.onActivityDestroy();
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mBtp.onActivityResult(requestCode, resultCode, this);
    }
/*
* Set Connect btn Enable and disables
* On Printer connection status
* and
* Enables and disables Print Button
* */
    private void setConnectEnable(Boolean isEnable)
    {
        btnConnect.setEnabled(isEnable);
        int selectionColor = isEnable ? R.color.colorPrimaryDark :R.color.colorPrimaryDarkSelection ;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            btnConnect.setBackgroundColor(getColor(selectionColor));
        }else {
            btnConnect.setBackgroundColor(getResources().getColor(selectionColor));
        }
        //btnPrint.setEnabled(!isEnable);

    }




    private class EnterTextTask extends AsyncTask<Integer, Integer, Integer> {
        /* displays the progress dialog until background task is completed*/
        private int iRetVal;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        /* Task of EnterTextAsyc performing in the background*/
        @Override
        protected Integer doInBackground(Integer... params) {
            tp.setTextSize(24);

            mBtp.addText("Digital India Payments ltd", Layout.Alignment.ALIGN_CENTER, tp);
            try {
                Bitmap logo = BitmapFactory.decodeStream(getResources().openRawResource(R.raw.logo));
                mBtp.addImage(logo.copy(logo.getConfig(), true));

            } catch (Exception e) {
                e.printStackTrace();
            }
            tp.setTextSize(28);
            mBtp.addText("Transaction Receipt", Layout.Alignment.ALIGN_CENTER, tp);
            tp.setTextSize(20);


            String line0 = "Customer Copy - " + bean.serviceMod+"\n";
            line0 += "Date & Time \t\t\t: " + bean.timestamp+"\n";
            line0 += "Terminal ID \t\t\t: " + bean.terminalId+"\n";
            line0 += "Agent ID\t\t\t\t\t: " + bean.agentId+"\n";
            line0 += "BC Name\t\t\t\t: " + bean.BCName+"\n";
            line0 += "mATN req ID \t\t\t: " + bean.mATNreqID+"\n";
            line0 += "Txn ID\t\t\t\t\t\t: " + bean.transactionId+"\n";
            line0 += "UIDAI Auth Code \t: " + bean.aadhaarNo+"\n";
            line0 += "Txn Status\t\t\t\t: " + bean.remark+"\n";
            line0 += "Txn Amt\t\t\t\t\t: " + bean.amount+"\n";
            line0 += "A/c Bal \t\t\t\t\t: " + bean.balance+"\n";


            mBtp.addText(line0, Layout.Alignment.ALIGN_NORMAL, tp);
            mBtp.addText("Note: Pls do not pay any charges/fee", Layout.Alignment.ALIGN_CENTER, tp);
            mBtp.addText("for this txn", Layout.Alignment.ALIGN_CENTER, tp);
            mBtp.addText(" ");
            mBtp.addText(" ");
            mBtp.addText(" ");
            mBtp.print();
            //Log.d("NGX", ""+mBtp.getPrinterStatus());
            iRetVal = mBtp.getPrinterStatus();
            /*if(true) {
                while (true) {
                    //Log.d("NGX", ""+mBtp.getPrinterStatus());

                }
            }*/


            return iRetVal;
        }

        /* This displays the status messages of EnterTextAsyc in the dialog box */
        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            //UtilDmt.showShortToast(getApplicationContext(),getResources().getString(R.string.print_success));
            //finish();
            //showResult(iRetVal);
            /*try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            UtilDmt.showShortToast(getApplicationContext(),getResources().getString(R.string.print_success));
            finish();
            */
        }
    }

}
