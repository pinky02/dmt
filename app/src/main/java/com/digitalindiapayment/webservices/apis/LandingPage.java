package com.digitalindiapayment.webservices.apis;


import com.digitalindiapayment.pojo.DashboardBean;
import com.digitalindiapayment.webservices.URLS;
import com.digitalindiapayment.webservices.responsepojo.TransactionsResponse;

import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import rx.Observable;

public interface LandingPage {

    @GET(URLS.ROOTPATH + "test/agent/dashboard")
    Observable<DashboardBean> dashboardData(@Header("auth") String id);

    @GET(URLS.ROOTPATH + "agent/transaction")
    Observable<TransactionsResponse> getTransctions(@Header("auth") String id);
}
