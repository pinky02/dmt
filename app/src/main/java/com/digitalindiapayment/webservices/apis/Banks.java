package com.digitalindiapayment.webservices.apis;


import com.digitalindiapayment.pojo.BankDetailsBean;
import com.digitalindiapayment.pojo.DeviceDetails;
import com.digitalindiapayment.webservices.requestpojo.BankTransactionRequest;
import com.digitalindiapayment.webservices.requestpojo.CreditAPIRequest;
import com.digitalindiapayment.webservices.requestpojo.GenerateDepositWithdrawTransIdRequest;
import com.digitalindiapayment.webservices.requestpojo.GenerateTransactionIdRequest;
import com.digitalindiapayment.webservices.requestpojo.ConfirmTransactionRequest;
import com.digitalindiapayment.webservices.requestpojo.RDFingurePrintRequest;
import com.digitalindiapayment.webservices.responsepojo.BankAccountsResponse;
import com.digitalindiapayment.webservices.responsepojo.BankTransactionResponse;
import com.digitalindiapayment.webservices.responsepojo.GenerateDepositWithdrawTransIdResponse;
import com.digitalindiapayment.webservices.responsepojo.GenerateTransactionIdResponse;
import com.digitalindiapayment.webservices.responsepojo.GetBanksResponse;
import com.digitalindiapayment.webservices.URLS;
import com.digitalindiapayment.webservices.responsepojo.GetModeofTransferResponse;

import java.util.List;
import java.util.Map;

import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import rx.Observable;

public interface Banks {

    @GET(URLS.ROOTPATH + "banks")
    Observable<GetBanksResponse> getBank(@Header("auth") String id);

    @POST(URLS.ROOTPATH + "balanceinquiry/confirmtransaction")
    Observable<ConfirmTransactionRequest> confirmBalance(@Header("auth") String id,
                                                         @Body RDFingurePrintRequest request);

    @POST(URLS.ROOTPATH + "transaction/status")
    Observable<BankTransactionResponse> getTransactionStatus(@Header("auth") String id,
                                                             @Body ConfirmTransactionRequest request);


    // RD Service Confirmation
    @POST(URLS.ROOTPATH + "{service}/confirmtransaction")
    Observable<BankTransactionResponse> transaction(@Header("auth") String id,
                                                    @Path("service") String service,
                                                    @Body RDFingurePrintRequest request);

    // Without RD confirmation
    @POST(URLS.ROOTPATH + "{service}/confirmtransaction")
    Observable<BankTransactionResponse> transaction(@Header("auth") String id,
                                                    @Path("service") String service,
                                                    @Body BankTransactionRequest request);


    @GET(URLS.ROOTPATH + "bankaccountinfo")
    Observable<BankAccountsResponse> getDIPLAccountInfo(@Header("auth") String id);

    @GET(URLS.ROOTPATH + "modeoftransfer")
    Observable<GetModeofTransferResponse> getModeOfTransafer(@Header("auth") String id);

    @POST(URLS.ROOTPATH + "agent/wallet/creditrequest")
    Observable<String> walletCreditRequest(@Header("auth") String id,
                                           @Body CreditAPIRequest apiRequest);


    @POST(URLS.ROOTPATH + "balanceinquiry/generatetransaction")
    Observable<GenerateTransactionIdResponse> getBalanceTransactionId(@Header("auth") String id,
                                                                      @Body GenerateTransactionIdRequest apiRequest);


    @POST(URLS.ROOTPATH + "{service}/generatetransaction")
    Observable<GenerateDepositWithdrawTransIdResponse> getTransactionId(@Header("auth") String id,
                                                                        @Path("service") String service,
                                                                        @Body GenerateDepositWithdrawTransIdRequest apiRequest);


    @GET(URLS.ROOTPATH + "agent/validateTerminalID")
    Observable<String> validateTerminalID(@Header("auth") String id,
                                          @Header("terminalId") String apiRequest);

    @GET(URLS.ROOTPATH + "agent/validateDCcode")
    Observable<String> validateRDTerminalID(@Header("auth") String id,
                                            @Header("terminalId") String apiRequest);

    @POST(URLS.ROOTPATH + "device/details")
    Observable<String> registerDevice(@Header("auth") String id, @
            Body DeviceDetails deviceDetails);


}
