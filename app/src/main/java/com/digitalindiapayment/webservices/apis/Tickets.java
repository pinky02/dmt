package com.digitalindiapayment.webservices.apis;

import com.digitalindiapayment.webservices.URLS;
import com.digitalindiapayment.webservices.requestpojo.CreateTicketRequest;
import com.digitalindiapayment.webservices.requestpojo.GenerateTicketRequest;
import com.digitalindiapayment.webservices.requestpojo.InsertCommentRequest;
import com.digitalindiapayment.webservices.requestpojo.TicketLoginRequest;
import com.digitalindiapayment.webservices.requestpojo.TicketRequest;
import com.digitalindiapayment.webservices.requestpojo.TicketUpdateRequest;
import com.digitalindiapayment.webservices.responsepojo.AllTicketListChatResponse;
import com.digitalindiapayment.webservices.responsepojo.AllTicketListResponse;
import com.digitalindiapayment.webservices.responsepojo.CreateTicketResponse;
import com.digitalindiapayment.webservices.responsepojo.GenerateTicketResponse;
import com.digitalindiapayment.webservices.responsepojo.GetAllProblems;
import com.digitalindiapayment.webservices.responsepojo.GetConversationsResponse;
import com.digitalindiapayment.webservices.responsepojo.GetTicketResponse;
import com.digitalindiapayment.webservices.responsepojo.IssueListResponse;
import com.digitalindiapayment.webservices.responsepojo.TicketList;
import com.digitalindiapayment.webservices.responsepojo.TicketListResponse;
import com.digitalindiapayment.webservices.responsepojo.TicketLoginResponse;
import com.digitalindiapayment.webservices.responsepojo.UpdateTicketRsponse;
import com.digitalindiapayment.webservices.responsepojo.UploadImageResponse;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import rx.Observable;

/**
 * Created by admin on 27-11-2017.
 */

public interface Tickets {
    @GET(URLS.TICKET_URL + "tickets/users/created")
    Observable<TicketListResponse> getTicketList(@Header("appInstanceCode") String appInstanceCode,
                                                 @Header("token") String token);

    @POST(URLS.TICKET_URL + "tickets")
    Observable<CreateTicketResponse> createTicket(@Header("token") String token,
                                                  @Header("appInstanceCode") String appInstanceCode,
                                                  @Body CreateTicketRequest ticketRequest);

    @PUT(URLS.TICKET_URL + "tickets/{code}/updateConversation")
    Observable<UpdateTicketRsponse> updateTicket(@Header("token") String token,
                                                 @Header("appInstanceCode") String appInstanceCode,
                                                 @Path("code") String code,
                                                 @Body TicketUpdateRequest updateRequest);

    @GET(URLS.TICKET_URL + "projects/{id}/categories")
    Observable<GetAllProblems> getProblems(@Header("token") String token,
                                           @Header("appInstanceCode") String appInstanceCode,
                                           @Header("userId") String userId,
                                           @Path("id") String id);

    @GET(URLS.TICKET_URL + "tickets/{code}")
    Observable<GetTicketResponse> getTicket(@Header("appInstanceCode") String appInstanceCode,
                                            @Header("token") String token,
                                            @Path("code") String code);

    @POST(URLS.TICKET_URL + "dipl/agent")
    Observable<TicketLoginResponse> ticketLogin(@Header("appInstanceCode") String appInstanceCode,
                                                @Body TicketLoginRequest request);

    @GET(URLS.TICKET_URL + "tickets/{code}/conversations")
    Observable<GetConversationsResponse> getConversation(@Header("appInstanceCode") String appInstanceCode,
                                                         @Header("token") String token,
                                                         @Path("code") String code);

    @Multipart
    @PUT(URLS.TICKET_URL + "tickets/{commentId}/attachments")
    Observable<UploadImageResponse> uploadImages(@Header("appInstanceCode") String appInstanceCode,
                                                 @Header("token") String token,
                                                 @Path("commentId") String commentId,
                                                 @Part MultipartBody.Part[] attachmentImages);

    @FormUrlEncoded
    @POST(URLS.TICKET_URL + "customer_api/add_ticket")
    Observable<List<GenerateTicketResponse>> createTicketNew(@Field("data") String ticketRequests);

    @FormUrlEncoded
    @POST(URLS.TICKET_URL + "product_api/get_issues")
    Observable<IssueListResponse> getIssueList(@Field("api_key") String apiKey,
                                               @Field("product_name") String product);

    @FormUrlEncoded
    @POST(URLS.TICKET_URL + "ticket_api/get_all_ticket")
    Observable<AllTicketListChatResponse> getAllTicketListChat(@Field("api_key") String apiKey,
                                                               @Field("ticket_no") String ticketNumber);


    @FormUrlEncoded
    @POST(URLS.TICKET_URL + "ticket_api/get_ticket_details")
    Observable<AllTicketListResponse> getAllTicketList(@Field("api_key") String apiKey,
                                            @Field("customer_code") String customerCode);

    @FormUrlEncoded
    @POST(URLS.TICKET_URL + "ticket_api/insert_comment")
    Observable<String> insertComment(@Field("data") String insertCommentRequest);

}
