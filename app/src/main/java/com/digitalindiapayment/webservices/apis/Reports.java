package com.digitalindiapayment.webservices.apis;


import com.digitalindiapayment.creditrequest.CreditRequestReport.CreditRequestReport;
import com.digitalindiapayment.pojo.TransactionDetailsBean;
import com.digitalindiapayment.reports.TransactionReport.Transaction;
import com.digitalindiapayment.reports.WalletReport.WalletReport;
import com.digitalindiapayment.webservices.URLS;
import com.digitalindiapayment.webservices.requestpojo.BankTransactionRequest;
import com.digitalindiapayment.webservices.requestpojo.TransactionDetailsRequest;
import com.digitalindiapayment.webservices.responsepojo.GetBanksResponse;

import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import rx.Observable;

public interface Reports {


    @GET(URLS.ROOTPATH + "agent/transaction")
    Observable<Transaction> getTransactionReport(@Header("auth") String id);

    @GET(URLS.ROOTPATH + "agent/creditrequests")
    Observable<CreditRequestReport> getCreditRequestReport(@Header("auth") String id);

    @GET(URLS.ROOTPATH + "agent/wallet/transaction")
    Observable<WalletReport> getWalletReport(@Header("auth") String id);

    @POST(URLS.ROOTPATH + "agent/transactionDetails")
    Observable<TransactionDetailsBean> getTransactionDetails(@Header("auth") String auth,
                                                             @Body TransactionDetailsRequest body);

}


