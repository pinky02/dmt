package com.digitalindiapayment.webservices.apis;

import com.digitalindiapayment.login.UserProfile;
import com.digitalindiapayment.pojo.ChangePassword;
import com.digitalindiapayment.pojo.Login;
import com.digitalindiapayment.webservices.URLS;
import com.digitalindiapayment.webservices.requestpojo.DeviceInfoRequest;
import com.digitalindiapayment.webservices.requestpojo.ForgotPasswordRequest;
import com.digitalindiapayment.webservices.requestpojo.RegisterUserRequest;
import com.digitalindiapayment.webservices.requestpojo.ResetSessionRequest;
import com.digitalindiapayment.webservices.responsepojo.AgentDeviceResponse;

import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import rx.Observable;

/**
 * Created by amolbhanushali on 2017/03/03.
 */

public interface UserManagement {


    @POST(URLS.ROOTPATH + "login/agent")
    Observable<Response<String>> userLogin(@Body Login login);

    @POST(URLS.ROOTPATH + "test/login/{role}")
    Observable<Response<String>> userLogin(@Path("role") String role,
                                           @Body Login login);

    @GET(URLS.ROOTPATH + "profile")
    Observable<UserProfile> profile(@Header("auth") String token,
                                    @Header("fcmtoken") String fcmtoken);

    @GET(URLS.ROOTPATH + "user/logout")
    Observable<String> logout(@Header("auth") String id);

    @GET(URLS.ROOTPATH + "agent/devices")
    Observable<AgentDeviceResponse> getDevices(@Header("auth") String id);

    @POST(URLS.ROOTPATH + "user/forgotpassword")
    Observable<String> forgotPassword(@Body ForgotPasswordRequest forgotPasswordRequest);

    @POST(URLS.ROOTPATH + "user/forgotpassword/newpassword")
    Observable<String> resetPassword(@Body ChangePassword resetPasswordRequest);

    @POST(URLS.ROOTPATH + "feedback")
    Observable<String> feedback(@Header("auth") String id,
                                @Body String feedback);

    @POST(URLS.ROOTPATH + "user/reset-session/request")
    Observable<String> resetSession(@Header("auth") String id,
                                    @Body ForgotPasswordRequest forgotPasswordRequest);

    @POST(URLS.ROOTPATH + "createPlayStoreUsers")
    Observable<String> registerUser(@Body RegisterUserRequest forgotPasswordRequest);


    @POST(URLS.ROOTPATH + "user/reset-session")
    Observable<Response<String>> validateResetSessionOtp(@Header("auth") String id,
                                                         @Body ResetSessionRequest resetSessionRequest);

    @POST(URLS.ROOTPATH + "agent/deviceLocationInfo")
    Observable<Response<String>> deviceLocationInfo(@Header("auth") String auth,
                                                    @Body DeviceInfoRequest request);
}
