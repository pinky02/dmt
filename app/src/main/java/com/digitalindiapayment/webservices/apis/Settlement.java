package com.digitalindiapayment.webservices.apis;

import com.digitalindiapayment.webservices.URLS;
import com.digitalindiapayment.webservices.requestpojo.AddSettlementDetailsRequest;
import com.digitalindiapayment.webservices.requestpojo.SettlementRequest;
import com.digitalindiapayment.webservices.responsepojo.SettlementAccountResponse;
import com.digitalindiapayment.webservices.responsepojo.SettlementReportResponse;
import com.digitalindiapayment.webservices.responsepojo.SettlementStatusResponse;

import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by Nivrutti Pawar on 04-11-2017.
 */

public interface Settlement {
    @GET(URLS.ROOTPATH + "settlement/account")
    Observable<SettlementAccountResponse> getSettlementDetails(@Header("auth") String id);

    @GET(URLS.ROOTPATH + "settlement/status")
    Observable<SettlementStatusResponse> getSettlementRequest(@Header("auth") String id);


    //need to replace body int type to class
    @POST(URLS.ROOTPATH + "settlement/request")
    Observable<String> sendSettlementRequest(@Header("auth") String id,
                                             @Body SettlementRequest settlementRequest);

    @GET(URLS.ROOTPATH + "settlement")
    Observable<SettlementReportResponse> getSettlementReport(@Header("auth") String id);

    @POST(URLS.ROOTPATH +"settlement/details")
    Observable<String> addSettlmentDetail(@Header("auth") String id, @Body AddSettlementDetailsRequest addSettlementDetailsRequest);

}
