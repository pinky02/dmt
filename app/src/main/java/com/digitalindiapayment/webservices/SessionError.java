package com.digitalindiapayment.webservices;

import retrofit2.Response;

/**
 * Created by admin on 21-07-2017.
 */

public class SessionError extends RuntimeException {
    private Response retrofitResponse;

    public SessionError(Response retrofitResponse) {
        this.retrofitResponse = retrofitResponse;
    }

    public Response getRetrofitResponse() {
        return retrofitResponse;
    }
}
