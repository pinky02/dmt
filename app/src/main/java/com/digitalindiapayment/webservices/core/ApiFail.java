package com.digitalindiapayment.webservices.core;


import android.util.Log;

import com.digitalindiapayment.webservices.InValidToken;
import com.digitalindiapayment.webservices.SessionError;
import com.digitalindiapayment.webservices.responsepojo.ErrorResponse;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.net.SocketTimeoutException;

import retrofit2.adapter.rxjava.HttpException;
import rx.functions.Action1;

/**
 * Created by ashwinnbhanushali on 04/11/16.
 */

public abstract class ApiFail implements Action1<Throwable> {
    @Override
    public void call(Throwable e) {
        if (e instanceof HttpException) {
            HttpException http = (HttpException) e;
            HttpErrorResponse response = new HttpErrorResponse(http.code(), http.response());
            response.setError(http.message());
            // Error response Should be read from error body ( reason field )
            try {
                String body = http.response().errorBody().string().trim();
                Log.d("ApiFail", body);
                if (body.contains("message")) {
                    Gson gson = new Gson();
                    ErrorResponse errorResponse = gson.fromJson(body, ErrorResponse.class);
                    body = errorResponse.getMessage();
                }
                if (body.contains("<html>")) {
                    body = "Something went wrong";
                }
                response.setError(body);
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            if (response.getHttpStatusCode() == 444) {
                InValidToken token = new InValidToken();
                EventBus.getDefault().post(token);
            } else {
                httpStatus(response);
            }
        } else if (e instanceof SocketTimeoutException) {
            EventBus.getDefault().post(e);
        } else if (e instanceof IOException) {
            noNetworkError();
        } else if (e instanceof SessionError) {
            HttpErrorResponse response = new HttpErrorResponse(409, ((SessionError) e).getRetrofitResponse());
            httpStatus(response);
        } else {
            unknownError(e);
        }
    }

    public abstract void httpStatus(HttpErrorResponse response);

    public abstract void noNetworkError();

    public abstract void unknownError(Throwable e);
}
