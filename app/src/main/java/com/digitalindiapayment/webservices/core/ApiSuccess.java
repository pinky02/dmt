package com.digitalindiapayment.webservices.core;

import rx.functions.Action1;

/**
 * Created by ashwinnbhanushali on 04/11/16.
 */

public abstract class ApiSuccess<T> implements Action1<T> {
}
