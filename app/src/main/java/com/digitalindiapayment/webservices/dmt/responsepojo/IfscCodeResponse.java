package com.digitalindiapayment.webservices.dmt.responsepojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Pinky on 22-05-2018.
 */

public class IfscCodeResponse {
    @SerializedName("bank_branch")
    @Expose
    private String bankBranch;
    @SerializedName("bank_name")
    @Expose
    private String bankName;
    @SerializedName("bank_address")
    @Expose
    private String bankAddress;
    @SerializedName("status")
    @Expose
    private String status;

    public String getBankBranch() {
        return bankBranch;
    }

    public void setBankBranch(String bankBranch) {
        this.bankBranch = bankBranch;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankAddress() {
        return bankAddress;
    }

    public void setBankAddress(String bankAddress) {
        this.bankAddress = bankAddress;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
