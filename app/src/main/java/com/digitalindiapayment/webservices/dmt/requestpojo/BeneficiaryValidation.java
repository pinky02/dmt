package com.digitalindiapayment.webservices.dmt.requestpojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Pinky on 28-06-2018.
 */

public class BeneficiaryValidation {
    @SerializedName("sessiontoken")
    @Expose
    private String sessiontoken;
    @SerializedName("remitterid")
    @Expose
    private String remitterid;
    @SerializedName("beneficiaryid")
    @Expose
    private String beneficiaryid;
    @SerializedName("verficationcode")
    @Expose
    private String verficationcode;

    public String getSessiontoken() {
        return sessiontoken;
    }

    public void setSessiontoken(String sessiontoken) {
        this.sessiontoken = sessiontoken;
    }

    public String getRemitterid() {
        return remitterid;
    }

    public void setRemitterid(String remitterid) {
        this.remitterid = remitterid;
    }

    public String getBeneficiaryid() {
        return beneficiaryid;
    }

    public void setBeneficiaryid(String beneficiaryid) {
        this.beneficiaryid = beneficiaryid;
    }

    public String getVerficationcode() {
        return verficationcode;
    }

    public void setVerficationcode(String verficationcode) {
        this.verficationcode = verficationcode;
    }
}
