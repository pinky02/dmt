package com.digitalindiapayment.webservices.dmt.requestpojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Pinky on 24-04-2018.
 */

public class ViewSenderRequest {
    @SerializedName("bc_agent_id")
    @Expose
    private String bcAgentId;

    public String getBcAgentId() {
        return bcAgentId;
    }

    public void setBcAgentId(String bcAgentId) {
        this.bcAgentId = bcAgentId;
    }
}
