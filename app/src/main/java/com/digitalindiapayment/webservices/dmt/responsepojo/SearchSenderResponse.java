package com.digitalindiapayment.webservices.dmt.responsepojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * Created by Pinky on 01-05-2018.
 */

public class SearchSenderResponse {
    @SerializedName("response")
    @Expose
    private Response response;
    @SerializedName("bene_count")
    @Expose
    private String beneCount;

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    public String getBeneCount() {
        return beneCount;
    }

    public void setBeneCount(String beneCount) {
        this.beneCount = beneCount;
    }

    public class Response {

        @SerializedName("remitterdetail")
        @Expose
        private Remitterdetail remitterdetail;
        @SerializedName("beneficiarydetail")
        @Expose
        private Beneficiarydetail beneficiarydetail;
        @SerializedName("bcagent")
        @Expose
        private String bcagent;
        @SerializedName("status")
        @Expose
        private String status;

        public Remitterdetail getRemitterdetail() {
            return remitterdetail;
        }

        public void setRemitterdetail(Remitterdetail remitterdetail) {
            this.remitterdetail = remitterdetail;
        }

        public Beneficiarydetail getBeneficiarydetail() {
            return beneficiarydetail;
        }

        public void setBeneficiarydetail(Beneficiarydetail beneficiarydetail) {
            this.beneficiarydetail = beneficiarydetail;
        }

        public String getBcagent() {
            return bcagent;
        }

        public void setBcagent(String bcagent) {
            this.bcagent = bcagent;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}