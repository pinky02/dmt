package com.digitalindiapayment.webservices.dmt.responsepojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Pinky on 05-04-2018.
 */

public class LoginResponse {
    @SerializedName("user_app_services")
    @Expose
    private String userAppServices;
    @SerializedName("user_name")
    @Expose
    private String userName;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("loginstatus")
    @Expose
    private Integer loginstatus;
    @SerializedName("mobileno")
    @Expose
    private String mobileno;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("userid")
    @Expose
    private Integer userid;
    @SerializedName("parentid")
    @Expose
    private Integer parentid;
    @SerializedName("user_type")
    @Expose
    private String userType;
    @SerializedName("login_time")
    @Expose
    private String loginTime;
    @SerializedName("bcagent")
    @Expose
    private String bcagent;
    @SerializedName("sessiontoken")
    @Expose
    private String sessiontoken;
    @SerializedName("INTERRESP")
    @Expose
    private Integer iNTERRESP;
    @SerializedName("status")
    @Expose
    private Integer status;

    public String getUserAppServices() {
        return userAppServices;
    }

    public void setUserAppServices(String userAppServices) {
        this.userAppServices = userAppServices;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getLoginstatus() {
        return loginstatus;
    }

    public void setLoginstatus(Integer loginstatus) {
        this.loginstatus = loginstatus;
    }

    public String getMobileno() {
        return mobileno;
    }

    public void setMobileno(String mobileno) {
        this.mobileno = mobileno;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public Integer getParentid() {
        return parentid;
    }

    public void setParentid(Integer parentid) {
        this.parentid = parentid;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(String loginTime) {
        this.loginTime = loginTime;
    }

    public String getBcagent() {
        return bcagent;
    }

    public void setBcagent(String bcagent) {
        this.bcagent = bcagent;
    }

    public String getSessiontoken() {
        return sessiontoken;
    }

    public void setSessiontoken(String sessiontoken) {
        this.sessiontoken = sessiontoken;
    }

    public Integer getINTERRESP() {
        return iNTERRESP;
    }

    public void setINTERRESP(Integer iNTERRESP) {
        this.iNTERRESP = iNTERRESP;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

}
