package com.digitalindiapayment.webservices.dmt.responsepojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Objects;

/**
 * Created by Pinky on 26-06-2018.
 */

public class TransactionResponse  {
    @SerializedName("NPCIResponsecode")
    @Expose
    private Integer nPCIResponsecode;
    @SerializedName("amount")
    @Expose
    private Integer amount;
    @SerializedName("channelpartnerrefno")
    @Expose
    private String channelpartnerrefno;
    @SerializedName("RBLtransactionid")
    @Expose
    private String rBLtransactionid;
    @SerializedName("transaction_Group_id")
    @Expose
    private String transactionGroupId;
    @SerializedName("bankrefno")
    @Expose
    private Objects bankrefno;
    @SerializedName("kycstatus")
    @Expose
    private Integer kycstatus;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("grossamount")
    @Expose
    private Integer grossamount;
    @SerializedName("remarks")
    @Expose
    private String remarks;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("servicecharge")
    @Expose
    private Integer servicecharge;

    public Integer getNPCIResponsecode() {
        return nPCIResponsecode;
    }

    public void setNPCIResponsecode(Integer nPCIResponsecode) {
        this.nPCIResponsecode = nPCIResponsecode;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getChannelpartnerrefno() {
        return channelpartnerrefno;
    }

    public void setChannelpartnerrefno(String channelpartnerrefno) {
        this.channelpartnerrefno = channelpartnerrefno;
    }

    public String getRBLtransactionid() {
        return rBLtransactionid;
    }

    public void setRBLtransactionid(String rBLtransactionid) {
        this.rBLtransactionid = rBLtransactionid;
    }

    public String getTransactionGroupId() {
        return transactionGroupId;
    }

    public void setTransactionGroupId(String transactionGroupId) {
        this.transactionGroupId = transactionGroupId;
    }

    public Objects getBankrefno() {
        return bankrefno;
    }

    public void setBankrefno(Objects bankrefno) {
        this.bankrefno = bankrefno;
    }

    public Integer getKycstatus() {
        return kycstatus;
    }

    public void setKycstatus(Integer kycstatus) {
        this.kycstatus = kycstatus;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getGrossamount() {
        return grossamount;
    }

    public void setGrossamount(Integer grossamount) {
        this.grossamount = grossamount;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getServicecharge() {
        return servicecharge;
    }

    public void setServicecharge(Integer servicecharge) {
        this.servicecharge = servicecharge;
    }

}
