package com.digitalindiapayment.webservices.dmt.requestpojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Pinky on 20-04-2018.
 */

public class PincodeRequest {
    @SerializedName("pincode")
    @Expose
    private String pincode;

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }
}
