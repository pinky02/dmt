package com.digitalindiapayment.webservices.dmt.requestpojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Pinky on 22-05-2018.
 */

public class IfscCodeRequest {
    @SerializedName("ifsc_code")
    @Expose
    private String ifscCode;

    public String getIfscCode() {
        return ifscCode;
    }

    public void setIfscCode(String ifscCode) {
        this.ifscCode = ifscCode;
    }
}
