package com.digitalindiapayment.webservices.dmt.requestpojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Pinky on 01-05-2018.
 */

public class SearchSenderRequest {
    @SerializedName("sessiontoken")
    @Expose
    private String sessiontoken;
    @SerializedName("bcagent")
    @Expose
    private String bcagent;
    @SerializedName("mobilenumber")
    @Expose
    private String mobilenumber;

    public String getSessiontoken() {
        return sessiontoken;
    }

    public void setSessiontoken(String sessiontoken) {
        this.sessiontoken = sessiontoken;
    }

    public String getBcagent() {
        return bcagent;
    }

    public void setBcagent(String bcagent) {
        this.bcagent = bcagent;
    }

    public String getMobilenumber() {
        return mobilenumber;
    }

    public void setMobilenumber(String mobilenumber) {
        this.mobilenumber = mobilenumber;
    }

}
