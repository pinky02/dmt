package com.digitalindiapayment.webservices.dmt.responsepojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Pinky on 20-04-2018.
 */

public class PincodeResponse {
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("postal_region")
    @Expose
    private String postalRegion;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("status")
    @Expose
    private String status;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostalRegion() {
        return postalRegion;
    }

    public void setPostalRegion(String postalRegion) {
        this.postalRegion = postalRegion;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
