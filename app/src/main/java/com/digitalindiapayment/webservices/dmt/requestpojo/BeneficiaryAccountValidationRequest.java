package com.digitalindiapayment.webservices.dmt.requestpojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Pinky on 26-06-2018.
 */

public class BeneficiaryAccountValidationRequest {
    @SerializedName("sessiontoken")
    @Expose
    private String sessiontoken;
    @SerializedName("bcagent")
    @Expose
    private String bcagent;
    @SerializedName("remitterid")
    @Expose
    private String remitterid;
    @SerializedName("beneficiaryname")
    @Expose
    private String beneficiaryname;
    @SerializedName("beneficiarymobilenumber")
    @Expose
    private String beneficiarymobilenumber;
    @SerializedName("accountnumber")
    @Expose
    private String accountnumber;
    @SerializedName("ifscode")
    @Expose
    private String ifscode;
    @SerializedName("channelpartnerrefno")
    @Expose
    private String channelpartnerrefno;
    @SerializedName("userid")
    @Expose
    private String userid;

    public String getSessiontoken() {
        return sessiontoken;
    }

    public void setSessiontoken(String sessiontoken) {
        this.sessiontoken = sessiontoken;
    }

    public String getBcagent() {
        return bcagent;
    }

    public void setBcagent(String bcagent) {
        this.bcagent = bcagent;
    }

    public String getRemitterid() {
        return remitterid;
    }

    public void setRemitterid(String remitterid) {
        this.remitterid = remitterid;
    }

    public String getBeneficiaryname() {
        return beneficiaryname;
    }

    public void setBeneficiaryname(String beneficiaryname) {
        this.beneficiaryname = beneficiaryname;
    }

    public String getBeneficiarymobilenumber() {
        return beneficiarymobilenumber;
    }

    public void setBeneficiarymobilenumber(String beneficiarymobilenumber) {
        this.beneficiarymobilenumber = beneficiarymobilenumber;
    }

    public String getAccountnumber() {
        return accountnumber;
    }

    public void setAccountnumber(String accountnumber) {
        this.accountnumber = accountnumber;
    }

    public String getIfscode() {
        return ifscode;
    }

    public void setIfscode(String ifscode) {
        this.ifscode = ifscode;
    }

    public String getChannelpartnerrefno() {
        return channelpartnerrefno;
    }

    public void setChannelpartnerrefno(String channelpartnerrefno) {
        this.channelpartnerrefno = channelpartnerrefno;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

}
