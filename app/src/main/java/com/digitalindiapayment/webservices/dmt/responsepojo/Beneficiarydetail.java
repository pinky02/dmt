package com.digitalindiapayment.webservices.dmt.responsepojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Pinky on 02-05-2018.
 */

public class Beneficiarydetail {
    @SerializedName("beneficiary")
    @Expose
    private List<Beneficiary> beneficiary = null;

    public List<Beneficiary> getBeneficiary() {
        return beneficiary;
    }

    public void setBeneficiary(List<Beneficiary> beneficiary) {
        this.beneficiary = beneficiary;
    }

}
