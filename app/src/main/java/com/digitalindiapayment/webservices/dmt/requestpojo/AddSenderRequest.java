package com.digitalindiapayment.webservices.dmt.requestpojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Pinky on 20-04-2018.
 */

public class AddSenderRequest {
    @SerializedName("remittermobilenumber")
    @Expose
    private String remittermobilenumber;
    @SerializedName("sessiontoken")
    @Expose
    private String sessiontoken;
    @SerializedName("bcagent")
    @Expose
    private String bcagent;
    @SerializedName("remittername")
    @Expose
    private String remittername;
    @SerializedName("remitteraddress")
    @Expose
    private String remitteraddress;
    @SerializedName("remitteraddress1")
    @Expose
    private String remitteraddress1;
    @SerializedName("remitteraddress2")
    @Expose
    private String remitteraddress2;
    @SerializedName("pincode")
    @Expose
    private String pincode;
    @SerializedName("cityname")
    @Expose
    private String cityname;
    @SerializedName("statename")
    @Expose
    private String statename;
    @SerializedName("alternatenumber")
    @Expose
    private String alternatenumber;
    @SerializedName("idproof")
    @Expose
    private String idproof;
    @SerializedName("idproofnumber")
    @Expose
    private String idproofnumber;
    @SerializedName("idproofissuedate")
    @Expose
    private String idproofissuedate;
    @SerializedName("idproofexpirydate")
    @Expose
    private String idproofexpirydate;
    @SerializedName("idproofissueplace")
    @Expose
    private String idproofissueplace;
    @SerializedName("lremitteraddress")
    @Expose
    private String lremitteraddress;
    @SerializedName("lpincode")
    @Expose
    private String lpincode;
    @SerializedName("lstatename")
    @Expose
    private String lstatename;
    @SerializedName("lcityname")
    @Expose
    private String lcityname;
    @SerializedName("user_id")
    @Expose
    private String userId;

    public String getRemittermobilenumber() {
        return remittermobilenumber;
    }

    public void setRemittermobilenumber(String remittermobilenumber) {
        this.remittermobilenumber = remittermobilenumber;
    }

    public String getSessiontoken() {
        return sessiontoken;
    }

    public void setSessiontoken(String sessiontoken) {
        this.sessiontoken = sessiontoken;
    }

    public String getBcagent() {
        return bcagent;
    }

    public void setBcagent(String bcagent) {
        this.bcagent = bcagent;
    }

    public String getRemittername() {
        return remittername;
    }

    public void setRemittername(String remittername) {
        this.remittername = remittername;
    }

    public String getRemitteraddress() {
        return remitteraddress;
    }

    public void setRemitteraddress(String remitteraddress) {
        this.remitteraddress = remitteraddress;
    }

    public String getRemitteraddress1() {
        return remitteraddress1;
    }

    public void setRemitteraddress1(String remitteraddress1) {
        this.remitteraddress1 = remitteraddress1;
    }

    public String getRemitteraddress2() {
        return remitteraddress2;
    }

    public void setRemitteraddress2(String remitteraddress2) {
        this.remitteraddress2 = remitteraddress2;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getCityname() {
        return cityname;
    }

    public void setCityname(String cityname) {
        this.cityname = cityname;
    }

    public String getStatename() {
        return statename;
    }

    public void setStatename(String statename) {
        this.statename = statename;
    }

    public String getAlternatenumber() {
        return alternatenumber;
    }

    public void setAlternatenumber(String alternatenumber) {
        this.alternatenumber = alternatenumber;
    }

    public String getIdproof() {
        return idproof;
    }

    public void setIdproof(String idproof) {
        this.idproof = idproof;
    }

    public String getIdproofnumber() {
        return idproofnumber;
    }

    public void setIdproofnumber(String idproofnumber) {
        this.idproofnumber = idproofnumber;
    }

    public String getIdproofissuedate() {
        return idproofissuedate;
    }

    public void setIdproofissuedate(String idproofissuedate) {
        this.idproofissuedate = idproofissuedate;
    }

    public String getIdproofexpirydate() {
        return idproofexpirydate;
    }

    public void setIdproofexpirydate(String idproofexpirydate) {
        this.idproofexpirydate = idproofexpirydate;
    }

    public String getIdproofissueplace() {
        return idproofissueplace;
    }

    public void setIdproofissueplace(String idproofissueplace) {
        this.idproofissueplace = idproofissueplace;
    }

    public String getLremitteraddress() {
        return lremitteraddress;
    }

    public void setLremitteraddress(String lremitteraddress) {
        this.lremitteraddress = lremitteraddress;
    }

    public String getLpincode() {
        return lpincode;
    }

    public void setLpincode(String lpincode) {
        this.lpincode = lpincode;
    }

    public String getLstatename() {
        return lstatename;
    }

    public void setLstatename(String lstatename) {
        this.lstatename = lstatename;
    }

    public String getLcityname() {
        return lcityname;
    }

    public void setLcityname(String lcityname) {
        this.lcityname = lcityname;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
