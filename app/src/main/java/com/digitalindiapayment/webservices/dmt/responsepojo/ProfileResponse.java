package com.digitalindiapayment.webservices.dmt.responsepojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Pinky on 23-04-2018.
 */

public class ProfileResponse {
    @SerializedName("distributorname")
    @Expose
    private String distributorname;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("wallet_balance")
    @Expose
    private Double walletBalance;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("mobileno")
    @Expose
    private String mobileno;
    @SerializedName("user_type")
    @Expose
    private Integer userType;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("zone")
    @Expose
    private String zone;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("PANNO")
    @Expose
    private String pANNO;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("status")
    @Expose
    private Integer status;

    public String getDistributorname() {
        return distributorname;
    }

    public void setDistributorname(String distributorname) {
        this.distributorname = distributorname;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Double getWalletBalance() {
        return walletBalance;
    }

    public void setWalletBalance(Double walletBalance) {
        this.walletBalance = walletBalance;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMobileno() {
        return mobileno;
    }

    public void setMobileno(String mobileno) {
        this.mobileno = mobileno;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPANNO() {
        return pANNO;
    }

    public void setPANNO(String pANNO) {
        this.pANNO = pANNO;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

}