package com.digitalindiapayment.webservices.dmt.apis;


import com.digitalindiapayment.webservices.URLS;
import com.digitalindiapayment.webservices.dmt.requestpojo.AddSenderRequest;
import com.digitalindiapayment.webservices.dmt.requestpojo.BeneficiaryAccountValidationRequest;
import com.digitalindiapayment.webservices.dmt.requestpojo.BeneficiaryValidation;
import com.digitalindiapayment.webservices.dmt.requestpojo.BeneficiaryValidationOtp;
import com.digitalindiapayment.webservices.dmt.requestpojo.DashBoardRequest;
import com.digitalindiapayment.webservices.dmt.requestpojo.ForgotPasswordRequest;
import com.digitalindiapayment.webservices.dmt.requestpojo.IfscCodeRequest;
import com.digitalindiapayment.webservices.dmt.requestpojo.Login;
import com.digitalindiapayment.webservices.dmt.requestpojo.LogoutRequest;
import com.digitalindiapayment.webservices.dmt.requestpojo.PincodeRequest;
import com.digitalindiapayment.webservices.dmt.requestpojo.ProfileRequest;
import com.digitalindiapayment.webservices.dmt.requestpojo.SearchSenderRequest;
import com.digitalindiapayment.webservices.dmt.requestpojo.ViewSenderRequest;
import com.digitalindiapayment.webservices.dmt.responsepojo.AddSenderResponse;
import com.digitalindiapayment.webservices.dmt.responsepojo.BeneficiaryAccountValidationResponse;
import com.digitalindiapayment.webservices.dmt.responsepojo.BeneficiaryValidationOtpResponse;
import com.digitalindiapayment.webservices.dmt.responsepojo.BeneficiaryValidationResponse;
import com.digitalindiapayment.webservices.dmt.responsepojo.DashBoardResponse;
import com.digitalindiapayment.webservices.dmt.responsepojo.IfscCodeResponse;
import com.digitalindiapayment.webservices.dmt.responsepojo.LoginResponse;
import com.digitalindiapayment.webservices.dmt.responsepojo.LogoutResponse;
import com.digitalindiapayment.webservices.dmt.responsepojo.PincodeResponse;
import com.digitalindiapayment.webservices.dmt.responsepojo.ProfileResponse;
import com.digitalindiapayment.webservices.dmt.responsepojo.SearchSenderResponse;
import com.digitalindiapayment.webservices.dmt.responsepojo.TransactionResponse;
import com.digitalindiapayment.webservices.dmt.responsepojo.ViewSenderResponse;
import com.digitalindiapayment.webservices.requestpojo.TransactionRequest;

import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import rx.Observable;

/**
 * Created by amolbhanushali on 2017/03/03.
 */

public interface UserManagementDmt {

  @POST(URLS.ROOTPATHDMT + "login")
  Observable<LoginResponse> dmtLogin(@Body Login login);

  @POST(URLS.ROOTPATHDMT + "test/login/{role}")
  Observable<Response<String>> userLogin(@Path("role") String role, @Body Login login);

  @POST(URLS.ROOTPATHDMT + "user/reset-session/request")
  Observable<String> resetSession(@Header("auth") String id, @Body ForgotPasswordRequest forgotPasswordRequest);

  @POST(URLS.ROOTPATHDMT + "DashboardNew")
   Observable<DashBoardResponse> dmtDashBoard(@Body DashBoardRequest dmtDashBoardRequest);

    @POST(URLS.ROOTPATHDMT + "pincodeRequest")
    Observable<PincodeResponse> pincodeRequest(@Body PincodeRequest pincodeRequest);

  @POST(URLS.ROOTPATHDMT + "Remitteraddrequest")
  Observable<AddSenderResponse> addRemitterRequest(@Body AddSenderRequest addSenderRequest);

  @POST(URLS.ROOTPATHDMT + "ProfileRequest")
  Observable<ProfileResponse> sendProfileRequest(@Body ProfileRequest profileRequest);
  @POST(URLS.ROOTPATHDMT + "logout")
  Observable<LogoutResponse> resetSession(@Body LogoutRequest logoutRequest);
  @POST(URLS.ROOTPATHDMT + "remitterviewrequest")
  Observable<ViewSenderResponse> viewsenderrequest(@Body ViewSenderRequest viewSenderRequest);

  @POST(URLS.ROOTPATHDMT + "SearchRemitter")
  Observable<SearchSenderResponse> searchSenderRequest(@Body SearchSenderRequest searchSenderRequest);

  @POST(URLS.ROOTPATHDMT + "bankifsccoderequest")
  Observable<IfscCodeResponse> sendIfscRequest(@Body IfscCodeRequest ifscCodeRequest);

  @POST(URLS.ROOTPATHDMT + "TransactionRequest")
  Observable<TransactionResponse> sendTransactionRequest(@Body TransactionRequest transactionRequest);

  @POST(URLS.ROOTPATHDMT + "Beneficiaryaccountvalidation")
  Observable<BeneficiaryAccountValidationResponse> beneficiaryAccountValidation(@Body BeneficiaryAccountValidationRequest beneficiaryAccountValidationRequest);

  @POST(URLS.ROOTPATHDMT + "Beneficiaryresendotp")
  Observable<BeneficiaryValidationOtpResponse> validateBeneficicaryOtp(@Body BeneficiaryValidationOtp beneficiaryValidationOtp);

  @POST(URLS.ROOTPATHDMT + "Beneficiaryregistrationvalidation")
  Observable<BeneficiaryValidationResponse> beneficiaryValidation(@Body BeneficiaryValidation beneficiaryValidation);

}
