package com.digitalindiapayment.webservices.dmt.responsepojo;

import com.digitalindiapayment.dmt.sender.view.ViewSenderBean;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Pinky on 24-04-2018.
 */

public class ViewSenderResponse {
    @SerializedName("response")
    @Expose
    private List<ViewSenderBean> response = null;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("status")
    @Expose
    private String status;

    public List<ViewSenderBean> getResponse() {
        return response;
    }

    public void setResponse(List<ViewSenderBean> response) {
        this.response = response;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
