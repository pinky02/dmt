package com.digitalindiapayment.webservices.dmt.requestpojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Pinky on 06-04-2018.
 */

public class DashBoardRequest {
    @SerializedName("userid")
    @Expose
    private String userid;
    @SerializedName("parent_userid")
    @Expose
    private String parentUserid;
    @SerializedName("user_type")
    @Expose
    private String userType;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getParentUserid() {
        return parentUserid;
    }

    public void setParentUserid(String parentUserid) {
        this.parentUserid = parentUserid;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }
}
