package com.digitalindiapayment.webservices.dmt.responsepojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Pinky on 06-04-2018.
 */

public class DashBoardResponse implements Parcelable {
    @SerializedName("montly_remitter_count")
    @Expose
    private Integer montlyRemitterCount;
    @SerializedName("INTERRESPVAL")
    @Expose
    private String iNTERRESPVAL;
    @SerializedName("overall_remitter_balance")
    @Expose
    private Double overallRemitterBalance;
    @SerializedName("monthly_txn_amount")
    @Expose
    private Integer monthlyTxnAmount;
    @SerializedName("montly_remitter_balance")
    @Expose
    private Double montlyRemitterBalance;
    @SerializedName("monthlyrefund_tot_count")
    @Expose
    private Integer monthlyrefundTotCount;
    @SerializedName("monthly_tot_count")
    @Expose
    private Integer monthlyTotCount;
    @SerializedName("overallrefund_tot_count")
    @Expose
    private Integer overallrefundTotCount;
    @SerializedName("overall_tot_count")
    @Expose
    private Integer overallTotCount;
    @SerializedName("overall_txn_amt")
    @Expose
    private Integer overallTxnAmt;
    @SerializedName("overall_remitter_count")
    @Expose
    private Integer overallRemitterCount;
    @SerializedName("monthlyrefund_txn_amt")
    @Expose
    private Integer monthlyrefundTxnAmt;
    @SerializedName("overallrefund_txn_amt")
    @Expose
    private Integer overallrefundTxnAmt;
    @SerializedName("INTERRESP")
    @Expose
    private Integer iNTERRESP;

    protected DashBoardResponse(Parcel in) {
        montlyRemitterCount = in.readInt();
        iNTERRESPVAL = in.readString();
        overallRemitterBalance = in.readDouble();
        monthlyTxnAmount = in.readInt();
        montlyRemitterBalance = in.readDouble();
        monthlyrefundTotCount = in.readInt();
        monthlyTotCount = in.readInt();
        montlyRemitterCount = in.readInt();
        overallrefundTotCount = in.readInt();
        overallTotCount = in.readInt();
        overallTxnAmt = in.readInt();
        overallRemitterCount = in.readInt();
        monthlyrefundTxnAmt = in.readInt();
        overallrefundTxnAmt = in.readInt();
        iNTERRESP = in.readInt();
    }
    public static final Creator<DashBoardResponse> CREATOR = new Creator<DashBoardResponse>() {
        @Override
        public DashBoardResponse createFromParcel(Parcel in) {
            return new DashBoardResponse(in);
        }

        @Override
        public DashBoardResponse[] newArray(int size) {
            return new DashBoardResponse[size];
        }
    };
    public Integer getMontlyRemitterCount() {
        return montlyRemitterCount;
    }

    public void setMontlyRemitterCount(Integer montlyRemitterCount) {
        this.montlyRemitterCount = montlyRemitterCount;
    }

    public String getINTERRESPVAL() {
        return iNTERRESPVAL;
    }

    public void setINTERRESPVAL(String iNTERRESPVAL) {
        this.iNTERRESPVAL = iNTERRESPVAL;
    }

    public Double getOverallRemitterBalance() {
        return overallRemitterBalance;
    }

    public void setOverallRemitterBalance(Double overallRemitterBalance) {
        this.overallRemitterBalance = overallRemitterBalance;
    }

    public Integer getMonthlyTxnAmount() {
        return monthlyTxnAmount;
    }

    public void setMonthlyTxnAmount(Integer monthlyTxnAmount) {
        this.monthlyTxnAmount = monthlyTxnAmount;
    }

    public Double getMontlyRemitterBalance() {
        return montlyRemitterBalance;
    }

    public void setMontlyRemitterBalance(Double montlyRemitterBalance) {
        this.montlyRemitterBalance = montlyRemitterBalance;
    }

    public Integer getMonthlyrefundTotCount() {
        return monthlyrefundTotCount;
    }

    public void setMonthlyrefundTotCount(Integer monthlyrefundTotCount) {
        this.monthlyrefundTotCount = monthlyrefundTotCount;
    }

    public Integer getMonthlyTotCount() {
        return monthlyTotCount;
    }

    public void setMonthlyTotCount(Integer monthlyTotCount) {
        this.monthlyTotCount = monthlyTotCount;
    }

    public Integer getOverallrefundTotCount() {
        return overallrefundTotCount;
    }

    public void setOverallrefundTotCount(Integer overallrefundTotCount) {
        this.overallrefundTotCount = overallrefundTotCount;
    }

    public Integer getOverallTotCount() {
        return overallTotCount;
    }

    public void setOverallTotCount(Integer overallTotCount) {
        this.overallTotCount = overallTotCount;
    }

    public Integer getOverallTxnAmt() {
        return overallTxnAmt;
    }

    public void setOverallTxnAmt(Integer overallTxnAmt) {
        this.overallTxnAmt = overallTxnAmt;
    }

    public Integer getOverallRemitterCount() {
        return overallRemitterCount;
    }

    public void setOverallRemitterCount(Integer overallRemitterCount) {
        this.overallRemitterCount = overallRemitterCount;
    }

    public Integer getMonthlyrefundTxnAmt() {
        return monthlyrefundTxnAmt;
    }

    public void setMonthlyrefundTxnAmt(Integer monthlyrefundTxnAmt) {
        this.monthlyrefundTxnAmt = monthlyrefundTxnAmt;
    }

    public Integer getOverallrefundTxnAmt() {
        return overallrefundTxnAmt;
    }

    public void setOverallrefundTxnAmt(Integer overallrefundTxnAmt) {
        this.overallrefundTxnAmt = overallrefundTxnAmt;
    }

    public Integer getINTERRESP() {
        return iNTERRESP;
    }

    public void setINTERRESP(Integer iNTERRESP) {
        this.iNTERRESP = iNTERRESP;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }
}
