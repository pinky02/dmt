package com.digitalindiapayment.webservices.dmt.responsepojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Pinky on 21-04-2018.
 */

public class AddSenderResponse {
    @SerializedName("remitterid")
    @Expose
    private Integer remitterid;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("remittercode")
    @Expose
    private Integer remittercode;
    @SerializedName("status")
    @Expose
    private String status;

    public Integer getRemitterid() {
        return remitterid;
    }

    public void setRemitterid(Integer remitterid) {
        this.remitterid = remitterid;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getRemittercode() {
        return remittercode;
    }

    public void setRemittercode(Integer remittercode) {
        this.remittercode = remittercode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
