package com.digitalindiapayment.webservices.dmt.responsepojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Pinky on 02-05-2018.
 */

public class Beneficiary {

    @SerializedName("beneficiaryemailid")
    @Expose
    private String beneficiaryemailid;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("accountnumber")
    @Expose
    private String accountnumber;
    @SerializedName("beneficiaryname")
    @Expose
    private String beneficiaryname;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("beneficiarystatus")
    @Expose
    private Integer beneficiarystatus;
    @SerializedName("mmid")
    @Expose
    private String mmid;
    @SerializedName("relationshipid")
    @Expose
    private Integer relationshipid;
    @SerializedName("branch")
    @Expose
    private String branch;
    @SerializedName("bank")
    @Expose
    private String bank;
    @SerializedName("impsstatus")
    @Expose
    private Integer impsstatus;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("ifscode")
    @Expose
    private String ifscode;
    @SerializedName("beneficiarymobilenumber")
    @Expose
    private String beneficiarymobilenumber;
    @SerializedName("beneficiaryid")
    @Expose
    private Integer beneficiaryid;

    public String getBeneficiaryemailid() {
        return beneficiaryemailid;
    }

    public void setBeneficiaryemailid(String beneficiaryemailid) {
        this.beneficiaryemailid = beneficiaryemailid;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAccountnumber() {
        return accountnumber;
    }

    public void setAccountnumber(String accountnumber) {
        this.accountnumber = accountnumber;
    }

    public String getBeneficiaryname() {
        return beneficiaryname;
    }

    public void setBeneficiaryname(String beneficiaryname) {
        this.beneficiaryname = beneficiaryname;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Integer getBeneficiarystatus() {
        return beneficiarystatus;
    }

    public void setBeneficiarystatus(Integer beneficiarystatus) {
        this.beneficiarystatus = beneficiarystatus;
    }

    public String getMmid() {
        return mmid;
    }

    public void setMmid(String mmid) {
        this.mmid = mmid;
    }

    public Integer getRelationshipid() {
        return relationshipid;
    }

    public void setRelationshipid(Integer relationshipid) {
        this.relationshipid = relationshipid;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public Integer getImpsstatus() {
        return impsstatus;
    }

    public void setImpsstatus(Integer impsstatus) {
        this.impsstatus = impsstatus;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getIfscode() {
        return ifscode;
    }

    public void setIfscode(String ifscode) {
        this.ifscode = ifscode;
    }

    public String getBeneficiarymobilenumber() {
        return beneficiarymobilenumber;
    }

    public void setBeneficiarymobilenumber(String beneficiarymobilenumber) {
        this.beneficiarymobilenumber = beneficiarymobilenumber;
    }

    public Integer getBeneficiaryid() {
        return beneficiaryid;
    }

    public void setBeneficiaryid(Integer beneficiaryid) {
        this.beneficiaryid = beneficiaryid;
    }

}