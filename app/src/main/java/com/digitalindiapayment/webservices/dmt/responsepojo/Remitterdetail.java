package com.digitalindiapayment.webservices.dmt.responsepojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Pinky on 02-05-2018.
 */

public class Remitterdetail {

    @SerializedName("NomineeAge")
    @Expose
    private String nomineeAge;
    @SerializedName("Emailid")
    @Expose
    private String emailid;
    @SerializedName("Category")
    @Expose
    private String category;
    @SerializedName("idproofissueplace")
    @Expose
    private String idproofissueplace;
    @SerializedName("stateid")
    @Expose
    private String stateid;
    @SerializedName("Gender")
    @Expose
    private String gender;
    @SerializedName("remitterid")
    @Expose
    private Integer remitterid;
    @SerializedName("FatherOrSpouseName")
    @Expose
    private String fatherOrSpouseName;
    @SerializedName("idproofexpirydate")
    @Expose
    private String idproofexpirydate;
    @SerializedName("idproofnumber")
    @Expose
    private String idproofnumber;
    @SerializedName("Education")
    @Expose
    private String education;
    @SerializedName("NameofNominee")
    @Expose
    private String nameofNominee;
    @SerializedName("MotherName")
    @Expose
    private String motherName;
    @SerializedName("remitteraddress1")
    @Expose
    private String remitteraddress1;
    @SerializedName("remitteraddress2")
    @Expose
    private String remitteraddress2;
    @SerializedName("remittername")
    @Expose
    private String remittername;
    @SerializedName("laddress")
    @Expose
    private String laddress;
    @SerializedName("pincode")
    @Expose
    private Integer pincode;
    @SerializedName("remitterMobilenumber")
    @Expose
    private String remitterMobilenumber;
    @SerializedName("NomineeDateofBirth")
    @Expose
    private String nomineeDateofBirth;
    @SerializedName("Religion")
    @Expose
    private String religion;
    @SerializedName("Title")
    @Expose
    private String title;
    @SerializedName("cityid")
    @Expose
    private String cityid;
    @SerializedName("consumedlimit")
    @Expose
    private Integer consumedlimit;
    @SerializedName("MiddleName")
    @Expose
    private String middleName;
    @SerializedName("Nationality")
    @Expose
    private String nationality;
    @SerializedName("ResidentialStatus")
    @Expose
    private String residentialStatus;
    @SerializedName("MaritalStatus")
    @Expose
    private String maritalStatus;
    @SerializedName("kycremarks")
    @Expose
    private String kycremarks;
    @SerializedName("lstate")
    @Expose
    private String lstate;
    @SerializedName("remaininglimit")
    @Expose
    private Integer remaininglimit;
    @SerializedName("lpincode")
    @Expose
    private Integer lpincode;
    @SerializedName("remitterstatus")
    @Expose
    private Integer remitterstatus;
    @SerializedName("idproofissuedate")
    @Expose
    private String idproofissuedate;
    @SerializedName("alternatenumber")
    @Expose
    private String alternatenumber;
    @SerializedName("lcity")
    @Expose
    private String lcity;
    @SerializedName("Relationship")
    @Expose
    private String relationship;
    @SerializedName("kycstatus")
    @Expose
    private Integer kycstatus;
    @SerializedName("idproof")
    @Expose
    private String idproof;
    @SerializedName("LastName")
    @Expose
    private String lastName;
    @SerializedName("DateofBirth")
    @Expose
    private String dateofBirth;
    @SerializedName("MaidenName")
    @Expose
    private String maidenName;

    public String getNomineeAge() {
        return nomineeAge;
    }

    public void setNomineeAge(String nomineeAge) {
        this.nomineeAge = nomineeAge;
    }

    public String getEmailid() {
        return emailid;
    }

    public void setEmailid(String emailid) {
        this.emailid = emailid;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getIdproofissueplace() {
        return idproofissueplace;
    }

    public void setIdproofissueplace(String idproofissueplace) {
        this.idproofissueplace = idproofissueplace;
    }

    public String getStateid() {
        return stateid;
    }

    public void setStateid(String stateid) {
        this.stateid = stateid;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Integer getRemitterid() {
        return remitterid;
    }

    public void setRemitterid(Integer remitterid) {
        this.remitterid = remitterid;
    }

    public String getFatherOrSpouseName() {
        return fatherOrSpouseName;
    }

    public void setFatherOrSpouseName(String fatherOrSpouseName) {
        this.fatherOrSpouseName = fatherOrSpouseName;
    }

    public String getIdproofexpirydate() {
        return idproofexpirydate;
    }

    public void setIdproofexpirydate(String idproofexpirydate) {
        this.idproofexpirydate = idproofexpirydate;
    }

    public String getIdproofnumber() {
        return idproofnumber;
    }

    public void setIdproofnumber(String idproofnumber) {
        this.idproofnumber = idproofnumber;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getNameofNominee() {
        return nameofNominee;
    }

    public void setNameofNominee(String nameofNominee) {
        this.nameofNominee = nameofNominee;
    }

    public String getMotherName() {
        return motherName;
    }

    public void setMotherName(String motherName) {
        this.motherName = motherName;
    }

    public String getRemitteraddress1() {
        return remitteraddress1;
    }

    public void setRemitteraddress1(String remitteraddress1) {
        this.remitteraddress1 = remitteraddress1;
    }

    public String getRemitteraddress2() {
        return remitteraddress2;
    }

    public void setRemitteraddress2(String remitteraddress2) {
        this.remitteraddress2 = remitteraddress2;
    }

    public String getRemittername() {
        return remittername;
    }

    public void setRemittername(String remittername) {
        this.remittername = remittername;
    }

    public String getLaddress() {
        return laddress;
    }

    public void setLaddress(String laddress) {
        this.laddress = laddress;
    }

    public Integer getPincode() {
        return pincode;
    }

    public void setPincode(Integer pincode) {
        this.pincode = pincode;
    }

    public String getRemitterMobilenumber() {
        return remitterMobilenumber;
    }

    public void setRemitterMobilenumber(String remitterMobilenumber) {
        this.remitterMobilenumber = remitterMobilenumber;
    }

    public String getNomineeDateofBirth() {
        return nomineeDateofBirth;
    }

    public void setNomineeDateofBirth(String nomineeDateofBirth) {
        this.nomineeDateofBirth = nomineeDateofBirth;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCityid() {
        return cityid;
    }

    public void setCityid(String cityid) {
        this.cityid = cityid;
    }

    public Integer getConsumedlimit() {
        return consumedlimit;
    }

    public void setConsumedlimit(Integer consumedlimit) {
        this.consumedlimit = consumedlimit;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getResidentialStatus() {
        return residentialStatus;
    }

    public void setResidentialStatus(String residentialStatus) {
        this.residentialStatus = residentialStatus;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getKycremarks() {
        return kycremarks;
    }

    public void setKycremarks(String kycremarks) {
        this.kycremarks = kycremarks;
    }

    public String getLstate() {
        return lstate;
    }

    public void setLstate(String lstate) {
        this.lstate = lstate;
    }

    public Integer getRemaininglimit() {
        return remaininglimit;
    }

    public void setRemaininglimit(Integer remaininglimit) {
        this.remaininglimit = remaininglimit;
    }

    public Integer getLpincode() {
        return lpincode;
    }

    public void setLpincode(Integer lpincode) {
        this.lpincode = lpincode;
    }

    public Integer getRemitterstatus() {
        return remitterstatus;
    }

    public void setRemitterstatus(Integer remitterstatus) {
        this.remitterstatus = remitterstatus;
    }

    public String getIdproofissuedate() {
        return idproofissuedate;
    }

    public void setIdproofissuedate(String idproofissuedate) {
        this.idproofissuedate = idproofissuedate;
    }

    public String getAlternatenumber() {
        return alternatenumber;
    }

    public void setAlternatenumber(String alternatenumber) {
        this.alternatenumber = alternatenumber;
    }

    public String getLcity() {
        return lcity;
    }

    public void setLcity(String lcity) {
        this.lcity = lcity;
    }

    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    public Integer getKycstatus() {
        return kycstatus;
    }

    public void setKycstatus(Integer kycstatus) {
        this.kycstatus = kycstatus;
    }

    public String getIdproof() {
        return idproof;
    }

    public void setIdproof(String idproof) {
        this.idproof = idproof;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDateofBirth() {
        return dateofBirth;
    }

    public void setDateofBirth(String dateofBirth) {
        this.dateofBirth = dateofBirth;
    }

    public String getMaidenName() {
        return maidenName;
    }

    public void setMaidenName(String maidenName) {
        this.maidenName = maidenName;
    }

}
