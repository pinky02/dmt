package com.digitalindiapayment.webservices.dmt.requestpojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Pinky on 27-06-2018.
 */

public class BeneficiaryValidationOtp {

    @SerializedName("remitterid")
    @Expose
    private String remitterid;
    @SerializedName("sessiontoken")
    @Expose
    private String sessiontoken;
    @SerializedName("beneficiaryid")
    @Expose
    private String beneficiaryid;

    public String getRemitterid() {
        return remitterid;
    }

    public void setRemitterid(String remitterid) {
        this.remitterid = remitterid;
    }

    public String getSessiontoken() {
        return sessiontoken;
    }

    public void setSessiontoken(String sessiontoken) {
        this.sessiontoken = sessiontoken;
    }

    public String getBeneficiaryid() {
        return beneficiaryid;
    }

    public void setBeneficiaryid(String beneficiaryid) {
        this.beneficiaryid = beneficiaryid;
    }
}
