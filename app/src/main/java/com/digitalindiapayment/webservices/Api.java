package com.digitalindiapayment.webservices;

import com.digitalindiapayment.webservices.apis.Banks;
import com.digitalindiapayment.webservices.apis.LandingPage;
import com.digitalindiapayment.webservices.apis.Reports;
import com.digitalindiapayment.webservices.apis.Settlement;
import com.digitalindiapayment.webservices.apis.Tickets;
import com.digitalindiapayment.webservices.apis.UserManagement;
import com.digitalindiapayment.webservices.dmt.apis.UserManagementDmt;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by DM on 12-01-2017.
 */

public class Api {

    private Api() {
    }

    ;

    private static Retrofit getRetrofit() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60,TimeUnit.SECONDS)
                .addInterceptor(interceptor)
                .build();

        client.connectTimeoutMillis();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://aeps.digitalindiapayments.com/AEPS_API/api1/v1/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(client)
                .build();
        return retrofit;
    }


    public static UserManagement userManagement() {
        return getRetrofit().create(UserManagement.class);
    }
    public static UserManagementDmt userManagementDmt() {
        return getRetrofit().create(UserManagementDmt.class);
    }

    public static Banks banksDetails() {
        return getRetrofit().create(Banks.class);
    }


    public static Reports reports() {
        return getRetrofit().create(Reports.class);
    }

    public static LandingPage landingPage() {
        return getRetrofit().create(LandingPage.class);
    }

    public static Settlement settlement(){return  getRetrofit().create(Settlement.class);}

    public static Tickets getTicket(){return  getRetrofit().create(Tickets.class);}

//    public static Update update()
//    {
//        return getRetrofit().create(Update.class);
//    }

}
