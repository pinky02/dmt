package com.digitalindiapayment.webservices;

import com.digitalindiapayment.BuildConfig;

/**
 * Created by amolbhanushali on 2017/03/03.
 */

public class URLS {

//    public static final String BASEURL = "http://43.224.136.156/AEPS_API/";
//    public static final String ROOTPATH = BASEURL + "api1/v1/";
    public static final String BASEURLDMT = "http://43.224.136.144:8080/DMTService/";
    public static final String ROOTPATHDMT = BASEURLDMT;
    public static final String BASEURL = BuildConfig.BaseUrl;
    public static final String ROOTPATH = BASEURL;// + "api1/v1/";
    public static final String TICKET_URL = BuildConfig.TICKET_URL;

}