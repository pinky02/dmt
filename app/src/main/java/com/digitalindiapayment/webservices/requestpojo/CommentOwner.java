package com.digitalindiapayment.webservices.requestpojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Pinky on 08-06-2018.
 */

public class CommentOwner {
    @SerializedName("owner_type")
    @Expose
    private Integer ownerType;
    @SerializedName("owner_name")
    @Expose
    private String ownerName;
    @SerializedName("owner_phone")
    @Expose
    private String ownerPhone;

    public Integer getOwnerType() {
        return ownerType;
    }

    public void setOwnerType(Integer ownerType) {
        this.ownerType = ownerType;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getOwnerPhone() {
        return ownerPhone;
    }

    public void setOwnerPhone(String ownerPhone) {
        this.ownerPhone = ownerPhone;
    }
}
