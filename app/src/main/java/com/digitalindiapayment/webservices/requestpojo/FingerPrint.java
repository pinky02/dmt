package com.digitalindiapayment.webservices.requestpojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 29-08-2017.
 */

class FingerPrint implements Parcelable {
    @SerializedName("mc")
    @Expose
    private String mc;
    @SerializedName("udc")
    @Expose
    private String udc;
    @SerializedName("dpId")
    @Expose
    private String dpId;
    @SerializedName("rdsId")
    @Expose
    private String rdsId;
    @SerializedName("rdsVer")
    @Expose
    private String rdsVer;
    @SerializedName("dc")
    @Expose
    private String dc;
    @SerializedName("mi")
    @Expose
    private String mi;
    @SerializedName("pid")
    @Expose
    private String pid;

    public FingerPrint() {
    }

    protected FingerPrint(Parcel in) {
        mc = in.readString();
        udc = in.readString();
        dpId = in.readString();
        rdsId = in.readString();
        rdsVer = in.readString();
        dc = in.readString();
        mi = in.readString();
        pid = in.readString();
    }

    public static final Creator<FingerPrint> CREATOR = new Creator<FingerPrint>() {
        @Override
        public FingerPrint createFromParcel(Parcel in) {
            return new FingerPrint(in);
        }

        @Override
        public FingerPrint[] newArray(int size) {
            return new FingerPrint[size];
        }
    };

    public String getMc() {
        return mc;
    }

    public void setMc(String mc) {
        this.mc = mc;
    }

    public String getUdc() {
        return udc;
    }

    public void setUdc(String udc) {
        this.udc = udc;
    }

    public String getDpId() {
        return dpId;
    }

    public void setDpId(String dpId) {
        this.dpId = dpId;
    }

    public String getRdsId() {
        return rdsId;
    }

    public void setRdsId(String rdsId) {
        this.rdsId = rdsId;
    }

    public String getRdsVer() {
        return rdsVer;
    }

    public void setRdsVer(String rdsVer) {
        this.rdsVer = rdsVer;
    }

    public String getDc() {
        return dc;
    }

    public void setDc(String dc) {
        this.dc = dc;
    }

    public String getMi() {
        return mi;
    }

    public void setMi(String mi) {
        this.mi = mi;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mc);
        dest.writeString(udc);
        dest.writeString(dpId);
        dest.writeString(rdsId);
        dest.writeString(rdsVer);
        dest.writeString(dc);
        dest.writeString(mi);
        dest.writeString(pid);
    }
}
