package com.digitalindiapayment.webservices.requestpojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Nivrutti Pawar on 2017/12/06.
 */

public class AddSettlementDetailsRequest {

    @SerializedName("beneficiary_name")
    @Expose
    private String beneficiary_name;
    @SerializedName("account_number")
    @Expose
    private String account_number;
    @SerializedName("ifsc_code")
    @Expose
    private String ifsc_code;
    @SerializedName("bank_name")
    @Expose
    private String bank_name;

    public String getAccount_number() {
        return account_number;
    }

    public String getBank_name() {
        return bank_name;
    }

    public String getBeneficiary_name() {
        return beneficiary_name;
    }

    public String getIfsc_code() {
        return ifsc_code;
    }

    public void setAccount_number(String account_number) {
        this.account_number = account_number;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    public void setBeneficiary_name(String beneficiary_name) {
        this.beneficiary_name = beneficiary_name;
    }

    public void setIfsc_code(String ifsc_code) {
        this.ifsc_code = ifsc_code;
    }
}
