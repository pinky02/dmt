package com.digitalindiapayment.webservices.requestpojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;


public class BankTransactionRequest implements Parcelable,AadhaarTransaction {

    @SerializedName("aadharNo")
    private String aadhaarNo;

    @SerializedName("bankCode")
    private String bankCode;


    @SerializedName("bankName")
    private String bankName;

    @SerializedName("amount")
    private String amount;


    @SerializedName("remark")
    private String remark;


    @SerializedName("fingerPrint")
    private String fingerPrint; 

    @SerializedName("transactionId")
    private String transactionId;



    public void setAadhaarNo(String aadhaarNo) {
        this.aadhaarNo = aadhaarNo;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }





    public void setBankName(String bankName) {
        this.bankName = bankName;
    }



    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getFingerPrint() {
        return fingerPrint;
    }

    public void setFingerPrint(String fingerPrint) {
        this.fingerPrint = fingerPrint;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.aadhaarNo);
        dest.writeString(this.bankCode);
        dest.writeString(this.bankName);
        dest.writeString(this.amount);
        dest.writeString(this.remark);
        dest.writeString(this.fingerPrint);
        dest.writeString(this.transactionId);
    }

    public BankTransactionRequest() {
    }

    protected BankTransactionRequest(Parcel in) {
        this.aadhaarNo = in.readString();
        this.bankCode = in.readString();
        this.bankName = in.readString();
        this.amount = in.readString();
        this.remark = in.readString();
        this.fingerPrint = in.readString();
        this.transactionId = in.readString();
    }

    public static final Parcelable.Creator<BankTransactionRequest> CREATOR = new Parcelable.Creator<BankTransactionRequest>() {
        @Override
        public BankTransactionRequest createFromParcel(Parcel source) {
            return new BankTransactionRequest(source);
        }

        @Override
        public BankTransactionRequest[] newArray(int size) {
            return new BankTransactionRequest[size];
        }
    };

    @Override
    public String getAadhaar() {
        return aadhaarNo;
    }

    @Override
    public String getBankName() {
        return bankName;
    }

    @Override
    public String getAmount() {
        return amount;
    }
}
