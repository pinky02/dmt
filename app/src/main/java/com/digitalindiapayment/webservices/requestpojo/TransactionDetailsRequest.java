package com.digitalindiapayment.webservices.requestpojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Amol on 04-04-2017.
 */

public class TransactionDetailsRequest {

    @SerializedName("transactionId")
    public String transactionId;


}
