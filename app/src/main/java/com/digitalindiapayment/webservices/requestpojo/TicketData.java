package com.digitalindiapayment.webservices.requestpojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Pinky on 31-05-2018.
 */

public class TicketData {
    @SerializedName("priority")
    @Expose
    private String priority;
    @SerializedName("product")
    @Expose
    private String product;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("created_by")
    @Expose
    private Integer createdBy;
    @SerializedName("issue1")
    @Expose
    private Integer issue1;
    @SerializedName("issue2")
    @Expose
    private Integer issue2;

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getIssue1() {
        return issue1;
    }

    public void setIssue1(Integer issue1) {
        this.issue1 = issue1;
    }

    public Integer getIssue2() {
        return issue2;
    }

    public void setIssue2(Integer issue2) {
        this.issue2 = issue2;
    }
}
