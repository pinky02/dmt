package com.digitalindiapayment.webservices.requestpojo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by amolbhanushali on 2017/12/09.
 */

public class CreateTicketRequest {
    @SerializedName("type")
    private ProblemType type;
    @SerializedName("subject")
    private String subject;
    @SerializedName("publicReply")
    private PublicReply publicReply;
    @SerializedName("project")
    private Project project;

    public ProblemType getType() {
        return type;
    }

    public void setType(ProblemType type) {
        this.type = type;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public PublicReply getPublicReply() {
        return publicReply;
    }

    public void setPublicReply(PublicReply publicReply) {
        this.publicReply = publicReply;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }
}
