package com.digitalindiapayment.webservices.requestpojo;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Base64;

import com.digitalindiapayment.pojo.morphoXmlBeans.PidData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;



/**
 * Created by admin on 29-08-2017.
 */

public class RDFingurePrintRequest implements Parcelable,AadhaarTransaction{
    @SerializedName("aadharNo")
    @Expose
    private String aadharNo;
    @SerializedName("bankCode")
    @Expose
    private String bankCode;
    @SerializedName("fingerPrint")
    @Expose
    private FingerPrint fingerPrint;
    @SerializedName("transactionId")
    @Expose
    private String transactionId;

    @SerializedName("bankName")
    private String bankName;

    @SerializedName("amount")
    private String amount;

    @SerializedName("remark")
    private String remark;

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }



    public void setAmount(String amount) {
        this.amount = amount;
    }





    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    protected RDFingurePrintRequest(Parcel in) {
        aadharNo = in.readString();
        bankCode = in.readString();
        fingerPrint = in.readParcelable(FingerPrint.class.getClassLoader());
        transactionId = in.readString();
        bankName = in.readString();
        amount = in.readString();
        remark = in.readString();

    }

    public RDFingurePrintRequest(PidData pidData){
        fingerPrint = new FingerPrint();
        fingerPrint.setDc(pidData._DeviceInfo.dc);
        fingerPrint.setDpId(pidData._DeviceInfo.getDpId());
        fingerPrint.setMc(pidData._DeviceInfo.getMc());
        fingerPrint.setMi(pidData._DeviceInfo.mi);

        //fingerPrint.setPid(pidData.Data.content);
        String Skey ="";
        String hmac = "";
        String data = "";
        try {
            Skey = bytesToHex(Base64.decode(pidData._Skey.content, Base64.DEFAULT));
            hmac = bytesToHex(Base64.decode(pidData._Hmac, Base64.DEFAULT));
            data = bytesToHex(Base64.decode(pidData.Data.content, Base64.DEFAULT));
        }catch (Exception e){

        }
        fingerPrint.setPid(
                "303031323536"
                        +Skey
                        +"3030323030383230313931323330"
                        +"303033303438"
                        +hmac
                        +"30303430313030303030343730303030"
                        +"30303530313030303030343730303030"
                        +"3030363035354d456665783171303363466a594d626633543568425538546e4b726f6c4659484f3764325971595147557654744f426849347051535849"
                        +data);
        fingerPrint.setRdsId(pidData._DeviceInfo.rdsId);
        fingerPrint.setRdsVer(pidData._DeviceInfo.rdsVer);
        fingerPrint.setUdc(pidData._DeviceInfo.dc);

    }
    public String bytesToHex(byte[] in) {
        final StringBuilder builder = new StringBuilder();
        for(byte b : in) {
            builder.append(String.format("%02x", b));
        }
        return builder.toString();
    }

    public static final Creator<RDFingurePrintRequest> CREATOR = new Creator<RDFingurePrintRequest>() {
        @Override
        public RDFingurePrintRequest createFromParcel(Parcel in) {
            return new RDFingurePrintRequest(in);
        }

        @Override
        public RDFingurePrintRequest[] newArray(int size) {
            return new RDFingurePrintRequest[size];
        }
    };

    public String getAadharNo() {
        return aadharNo;
    }

    public void setAadharNo(String aadharNo) {
        this.aadharNo = aadharNo;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public FingerPrint getFingerPrint() {
        return fingerPrint;
    }

    public void setFingerPrint(FingerPrint fingerPrint) {
        this.fingerPrint = fingerPrint;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(aadharNo);
        dest.writeString(bankCode);
        dest.writeParcelable(this.fingerPrint,flags);
        dest.writeString(transactionId);
        dest.writeString(bankName);
        dest.writeString(amount);
        dest.writeString(remark);
    }

    @Override
    public String getAadhaar() {
        return aadharNo;
    }

    @Override
    public String getBankName() {
        return bankName;
    }

    @Override
    public String getAmount() {
        return amount;
    }
}
