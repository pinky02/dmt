package com.digitalindiapayment.webservices.requestpojo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by amolbhanushali on 2017/12/09.
 */

public class PublicReply {
    @SerializedName("body")
    private String body;

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
