package com.digitalindiapayment.webservices.requestpojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Pinky on 31-05-2018.
 */

public class TicketRequest {
    @SerializedName("customer_data")
    @Expose
    private CustomerData customerData;
    @SerializedName("ticket_data")
    @Expose
    private TicketData ticketData;
    @SerializedName("api_key")
    @Expose
    private String apiKey;

    public CustomerData getCustomerData() {
        return customerData;
    }

    public void setCustomerData(CustomerData customerData) {
        this.customerData = customerData;
    }

    public TicketData getTicketData() {
        return ticketData;
    }

    public void setTicketData(TicketData ticketData) {
        this.ticketData = ticketData;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

}
