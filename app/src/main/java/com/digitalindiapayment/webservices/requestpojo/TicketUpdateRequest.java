package com.digitalindiapayment.webservices.requestpojo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by amolbhanushali on 2017/12/11.
 */

public class TicketUpdateRequest {
    @SerializedName("publicReply")
    private PublicReply publicReply;

    public PublicReply getPublicReply() {
        return publicReply;
    }

    public void setPublicReply(PublicReply publicReply) {
        this.publicReply = publicReply;
    }
}
