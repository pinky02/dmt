package com.digitalindiapayment.webservices.requestpojo;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Amol on 04-04-2017.
 */

public class CreditAPIRequest implements Parcelable{

    @SerializedName("bankCode")
    public String bankCode;
    @SerializedName("amount")
    public String amount;
    @SerializedName("referenceNumber")
    public String referenceNumber;
    @SerializedName("bankBranch")
    public String bankBranch;
    @SerializedName("modeOfTransfer")
    public String modeOfTransfer;

    @SerializedName("bankId")
    public String bankId;



    public CreditAPIRequest(Parcel in) {
        bankCode = in.readString();
        amount = in.readString();
        referenceNumber = in.readString();
        bankBranch = in.readString();
        modeOfTransfer = in.readString();
        bankId = in.readString();
    }

    public static final Creator<CreditAPIRequest> CREATOR = new Creator<CreditAPIRequest>() {
        @Override
        public CreditAPIRequest createFromParcel(Parcel in) {
            return new CreditAPIRequest(in);
        }

        @Override
        public CreditAPIRequest[] newArray(int size) {
            return new CreditAPIRequest[size];
        }
    };

    public CreditAPIRequest() {

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(bankCode);
        parcel.writeString(amount);
        parcel.writeString(referenceNumber);
        parcel.writeString(bankBranch);
        parcel.writeString(modeOfTransfer);
        parcel.writeString(bankId);
    }
}
