package com.digitalindiapayment.webservices.requestpojo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 03-07-2017.
 */

public class ConfirmTransactionRequest {
    @SerializedName("transactionId")
    private String transactionId;

    @SerializedName("aadharNo")
    private String aadharNo;

    /*public ConfirmTransactionRequest(String transactionId) {
        this.transactionId = transactionId;
    }*/

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getAadharNo() {
        return aadharNo;
    }

    public void setAadharNo(String aadharNo) {
        this.aadharNo = aadharNo;
    }
}
