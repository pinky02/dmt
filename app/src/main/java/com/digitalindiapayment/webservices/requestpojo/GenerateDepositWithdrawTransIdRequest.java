package com.digitalindiapayment.webservices.requestpojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 23-05-2017.
 */

public class GenerateDepositWithdrawTransIdRequest implements Parcelable {

    @SerializedName("aadharNo")
    private String aadharNo;

    @SerializedName("bankCode")
    private String bankCode;

    @SerializedName("amount")
    private String amount;

    @SerializedName("custName")
    private String custName;
    @SerializedName("custPhoneNo")
    private String custPhoneNo;

    public String getAadharNo() {
        return aadharNo;
    }

    public void setAadharNo(String aadharNo) {
        this.aadharNo = aadharNo;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getCustPhoneNo() {
        return custPhoneNo;
    }

    public void setCustPhoneNo(String custPhoneNo) {
        this.custPhoneNo = custPhoneNo;
    }

    public GenerateDepositWithdrawTransIdRequest(String aadharNo, String bankCode, String amount, String custName, String custPhoneNo) {
        this.aadharNo = aadharNo;
        this.bankCode = bankCode;
        this.amount = amount;
        this.custName = custName;
        this.custPhoneNo = custPhoneNo;
    }

    protected GenerateDepositWithdrawTransIdRequest(Parcel in) {
        this.aadharNo = in.readString();
        this.bankCode = in.readString();
        this.amount = in.readString();
        this.custName = in.readString();
        this.custPhoneNo = in.readString();
    }

    public static final Creator<GenerateDepositWithdrawTransIdRequest> CREATOR = new Creator<GenerateDepositWithdrawTransIdRequest>() {
        @Override
        public GenerateDepositWithdrawTransIdRequest createFromParcel(Parcel in) {
            return new GenerateDepositWithdrawTransIdRequest(in);
        }

        @Override
        public GenerateDepositWithdrawTransIdRequest[] newArray(int size) {
            return new GenerateDepositWithdrawTransIdRequest[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }
}
