package com.digitalindiapayment.webservices.requestpojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 22-07-2017.
 */

public class ResetSessionRequest {
    @SerializedName("phone_no")
    @Expose
    private String phoneNo;
    @SerializedName("otp")
    @Expose
    private String otp;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("mobile_auth_code")
    @Expose
    private String mobile_auth_code;




    public ResetSessionRequest(String phoneNo, String otp, String password) {
        this.phoneNo = phoneNo;
        this.otp = otp;
        this.password = password;
    }

    public String getMobile_auth_code() {
        return mobile_auth_code;
    }

    public void setMobile_auth_code(String mobile_auth_code) {
        this.mobile_auth_code = mobile_auth_code;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
