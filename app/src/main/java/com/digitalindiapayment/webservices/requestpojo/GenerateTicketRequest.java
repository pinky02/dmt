package com.digitalindiapayment.webservices.requestpojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Pinky on 01-06-2018.
 */

public class GenerateTicketRequest {
    @SerializedName("data")
    @Expose
    public List<TicketRequest> data = null;

    public List<TicketRequest> getData() {
        return data;
    }

    public void setData(List<TicketRequest> data) {
        this.data = data;
    }

}
