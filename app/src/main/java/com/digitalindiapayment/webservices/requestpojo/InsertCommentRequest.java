package com.digitalindiapayment.webservices.requestpojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Pinky on 08-06-2018.
 */

public class InsertCommentRequest {
    @SerializedName("api_key")
    @Expose
    private String apiKey;
    @SerializedName("ticket_no")
    @Expose
    private String ticketNo;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("comment_owner")
    @Expose
    private CommentOwner commentOwner;

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getTicketNo() {
        return ticketNo;
    }

    public void setTicketNo(String ticketNo) {
        this.ticketNo = ticketNo;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public CommentOwner getCommentOwner() {
        return commentOwner;
    }

    public void setCommentOwner(CommentOwner commentOwner) {
        this.commentOwner = commentOwner;
    }

}
