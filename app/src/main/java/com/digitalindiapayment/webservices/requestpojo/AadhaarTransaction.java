package com.digitalindiapayment.webservices.requestpojo;

/**
 * Created by admin on 07-11-2017.
 */

public interface AadhaarTransaction {
    public String getAadhaar();
    public String getBankName();
    public String getAmount();
    public String getBankCode();

}
