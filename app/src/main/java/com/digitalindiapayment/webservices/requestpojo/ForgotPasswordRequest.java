package com.digitalindiapayment.webservices.requestpojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj Kumar on 05-05-2017.
 */

public class ForgotPasswordRequest {
    @SerializedName("phone_no")
    @Expose
    private String phone_no;


    public ForgotPasswordRequest(String phone_no) {
        this.phone_no = phone_no;
    }

    public String getPhone_no() {
        return phone_no;
    }

    public void setPhone_no(String phone_no) {
        this.phone_no = phone_no;
    }
}
