package com.digitalindiapayment.webservices.requestpojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 22-05-2017.
 */

public class GenerateTransactionIdRequest implements Parcelable {
    @SerializedName("aadharNo")
    private String aadharNo;

    @SerializedName("bankCode")
    private String bankCode;

    @SerializedName("custName")
    private String custName;
    @SerializedName("custPhoneNo")
    private String custPhoneNo;


    public String getAadharNo() {
        return aadharNo;
    }

    public void setAadharNo(String aadharNo) {
        this.aadharNo = aadharNo;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getCustPhoneNo() {
        return custPhoneNo;
    }

    public void setCustPhoneNo(String custPhoneNo) {
        this.custPhoneNo = custPhoneNo;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.aadharNo);
        dest.writeString(this.bankCode);
    }


    public GenerateTransactionIdRequest(String aadharNo, String bankCode, String custName, String custPhoneNo) {
        this.aadharNo = aadharNo;
        this.bankCode = bankCode;
        this.custName = custName;
        this.custPhoneNo = custPhoneNo;
    }

    public GenerateTransactionIdRequest(Parcel in) {
        this.aadharNo = in.readString();
        this.bankCode = in.readString();
    }

    public static final Parcelable.Creator<GenerateTransactionIdRequest> CREATOR = new Creator<GenerateTransactionIdRequest>() {
        @Override
        public GenerateTransactionIdRequest createFromParcel(Parcel source) {
            return null;
        }

        @Override
        public GenerateTransactionIdRequest[] newArray(int size) {
            return new GenerateTransactionIdRequest[0];
        }
    };

}
