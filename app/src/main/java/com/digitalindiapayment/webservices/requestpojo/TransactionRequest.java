package com.digitalindiapayment.webservices.requestpojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Pinky on 26-06-2018.
 */

public class TransactionRequest {
    @SerializedName("sessiontoken")
    @Expose
    private String sessiontoken;
    @SerializedName("bcagent")
    @Expose
    private String bcagent;
    @SerializedName("remitterid")
    @Expose
    private String remitterid;
    @SerializedName("beneficiaryid")
    @Expose
    private String beneficiaryid;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("remarks")
    @Expose
    private String remarks;
    @SerializedName("cpid")
    @Expose
    private String cpid;
    @SerializedName("channelpartnerrefno")
    @Expose
    private String channelpartnerrefno;
    @SerializedName("flag")
    @Expose
    private String flag;
    @SerializedName("userid")
    @Expose
    private String userid;
    @SerializedName("process_type")
    @Expose
    private String processType;
    @SerializedName("req_type")
    @Expose
    private String reqType;
    @SerializedName("ifsc_code")
    @Expose
    private String ifscCode;

    public String getSessiontoken() {
        return sessiontoken;
    }

    public void setSessiontoken(String sessiontoken) {
        this.sessiontoken = sessiontoken;
    }

    public String getBcagent() {
        return bcagent;
    }

    public void setBcagent(String bcagent) {
        this.bcagent = bcagent;
    }

    public String getRemitterid() {
        return remitterid;
    }

    public void setRemitterid(String remitterid) {
        this.remitterid = remitterid;
    }

    public String getBeneficiaryid() {
        return beneficiaryid;
    }

    public void setBeneficiaryid(String beneficiaryid) {
        this.beneficiaryid = beneficiaryid;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getCpid() {
        return cpid;
    }

    public void setCpid(String cpid) {
        this.cpid = cpid;
    }

    public String getChannelpartnerrefno() {
        return channelpartnerrefno;
    }

    public void setChannelpartnerrefno(String channelpartnerrefno) {
        this.channelpartnerrefno = channelpartnerrefno;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getProcessType() {
        return processType;
    }

    public void setProcessType(String processType) {
        this.processType = processType;
    }

    public String getReqType() {
        return reqType;
    }

    public void setReqType(String reqType) {
        this.reqType = reqType;
    }

    public String getIfscCode() {
        return ifscCode;
    }

    public void setIfscCode(String ifscCode) {
        this.ifscCode = ifscCode;
    }
}
