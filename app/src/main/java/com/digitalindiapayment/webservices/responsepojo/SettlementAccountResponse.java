package com.digitalindiapayment.webservices.responsepojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 20-11-2017.
 */

public class SettlementAccountResponse {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("response")
    @Expose
    private SettlementAccount response;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public SettlementAccount getResponse() {
        return response;
    }

    public void setResponse(SettlementAccount response) {
        this.response = response;
    }
}
