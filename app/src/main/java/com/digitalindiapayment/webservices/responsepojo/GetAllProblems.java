package com.digitalindiapayment.webservices.responsepojo;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by admin on 27-11-2017.
 */

public class GetAllProblems {
    @SerializedName("status")
    private String status;
    @SerializedName("errorCode")
    private String errorCode;
    @SerializedName("response")
    private String response;
    @SerializedName("data")
    private List<TypeList> typeLists;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public List<TypeList> getTypeLists() {
        return typeLists;
    }

    public void setTypeLists(List<TypeList> typeLists) {
        this.typeLists = typeLists;
    }
}
