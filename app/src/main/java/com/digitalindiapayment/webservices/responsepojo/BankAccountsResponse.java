package com.digitalindiapayment.webservices.responsepojo;

import com.digitalindiapayment.pojo.BankDetailsBean;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by admin on 04-07-2017.
 */

public class BankAccountsResponse{
        @SerializedName("bankAccounts")
        @Expose
        private List<BankDetailsBean> bankAccounts = null;

        public List<BankDetailsBean> getBankAccounts() {
            return bankAccounts;
        }

        public void setBankAccounts(List<BankDetailsBean> bankAccounts) {
            this.bankAccounts = bankAccounts;
        }


}
