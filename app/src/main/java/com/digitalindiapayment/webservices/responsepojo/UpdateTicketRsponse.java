package com.digitalindiapayment.webservices.responsepojo;

import com.digitalindiapayment.webservices.responsepojo.ConversationList;
import com.google.gson.annotations.SerializedName;

/**
 * Created by amolbhanushali on 2017/12/11.
 */

public class UpdateTicketRsponse {
    @SerializedName("status")
    private String status;
    @SerializedName("errorCode")
    private String errorCode;
    @SerializedName("response")
    private String response;
    @SerializedName("data")
    private ConversationRes conversationList;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public ConversationRes getConversationList() {
        return conversationList;
    }

    public void setConversationList(ConversationRes conversationList) {
        this.conversationList = conversationList;
    }
}
