package com.digitalindiapayment.webservices.responsepojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 01-07-2017.
 */

public class CreditResponseBean {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("modeOfTransfer")
    @Expose
    private String modeOfTransfer;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("timestamp")
    @Expose
    private String timestamp;
    @SerializedName("bank")
    @Expose
    private String bank;
    @SerializedName("branch")
    @Expose
    private String branch;
    @SerializedName("referenceNo")
    @Expose
    private String referenceNo;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getModeOfTransfer() {
        return modeOfTransfer;
    }

    public void setModeOfTransfer(String modeOfTransfer) {
        this.modeOfTransfer = modeOfTransfer;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }
}
