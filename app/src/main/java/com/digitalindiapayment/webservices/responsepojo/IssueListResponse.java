package com.digitalindiapayment.webservices.responsepojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Pinky on 05-06-2018.
 */

public class IssueListResponse {
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("data")
    @Expose
    public List<ParentIssueList> data = null;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public List<ParentIssueList> getData() {
        return data;
    }

    public void setData(List<ParentIssueList> data) {
        this.data = data;
    }
}
