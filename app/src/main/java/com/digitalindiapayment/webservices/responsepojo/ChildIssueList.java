package com.digitalindiapayment.webservices.responsepojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Pinky on 05-06-2018.
 */

public class ChildIssueList {
    @SerializedName("issue_id")
    @Expose
    private Integer issueId;
    @SerializedName("issue_name")
    @Expose
    private String issueName;

    public ChildIssueList(int id, String issueName) {
        this.issueId = id;
        this.issueName = issueName;
    }

    public Integer getIssueId() {
        return issueId;
    }

    public void setIssueId(Integer issueId) {
        this.issueId = issueId;
    }

    public String getIssueName() {
        return issueName;
    }
    @Override
    public String toString() {
        return issueName;
    }
    public void setIssueName(String issueName) {
        this.issueName = issueName;
    }
}
