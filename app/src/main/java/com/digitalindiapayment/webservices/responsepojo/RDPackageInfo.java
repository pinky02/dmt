package com.digitalindiapayment.webservices.responsepojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 07-11-2017.
 */

public class RDPackageInfo {
    @SerializedName("packageName")
    @Expose
    private String packageName;
    @SerializedName("isNecessary")
    @Expose
    private String isNecessary;
    @SerializedName("link")
    @Expose
    private String link;

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getIsNecessary() {
        return isNecessary;
    }

    public void setIsNecessary(String isNecessary) {
        this.isNecessary = isNecessary;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
