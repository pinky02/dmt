package com.digitalindiapayment.webservices.responsepojo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by amolbhanushali on 2017/12/09.
 */

public class TicketLoginResponse {
    @SerializedName("status")
    private String status;
    @SerializedName("errorCode")
    private String errorCode;
    @SerializedName("data")
    private ResponseData data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public ResponseData getData() {
        return data;
    }

    public void setData(ResponseData data) {
        this.data = data;
    }

    public class ResponseData {
        @SerializedName("token")
        private String tiketToken;
        @SerializedName("userId")
        private String ticketAgentID;

        public String getTiketToken() {
            return tiketToken;
        }

        public void setTiketToken(String tiketToken) {
            this.tiketToken = tiketToken;
        }

        public String getTicketAgentID() {
            return ticketAgentID;
        }

        public void setTicketAgentID(String ticketAgentID) {
            this.ticketAgentID = ticketAgentID;
        }
    }
}
