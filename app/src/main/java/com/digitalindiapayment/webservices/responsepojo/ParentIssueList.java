package com.digitalindiapayment.webservices.responsepojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Pinky on 05-06-2018.
 */
 public class ParentIssueList  {
    @SerializedName("problem_id")
    @Expose
    private Integer problemId;
    @SerializedName("problem_name")
    @Expose
    private String problemName;
    @SerializedName("issue_list")
    @Expose
    public List<ChildIssueList> issueList = null;

    public ParentIssueList(int id, String problemName) {
        this.problemId = id;
        this.problemName = problemName;
    }

    public Integer getProblemId() {
        return problemId;
    }

    public void setProblemId(Integer problemId) {
        this.problemId = problemId;
    }

    public String getProblemName() {
        return problemName;
    }
    @Override
    public String toString() {
        return problemName;
    }

    public void setProblemName(String problemName) {
        this.problemName = problemName;
    }

    public List<ChildIssueList> getIssueList() {
        return issueList;
    }

    public void setIssueList(List<ChildIssueList> issueList) {
        this.issueList = issueList;
    }

}
