package com.digitalindiapayment.webservices.responsepojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 06-11-2017.
 */

public class ScannerDeviceResponse {

    @SerializedName("rdService")
    @Expose
    public RdService rdService;
    @SerializedName("managementClient")
    @Expose
    public ManagementClient managementClient;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("deviceId")
    @Expose
    private String deviceId;

    public String getManagementClientLink() {
        return managementClient.getLink();
    }

    public String getRDServiceLink() {
        return rdService.getLink();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public RdService getRdService() {
        return rdService;
    }

    public void setRdService(RdService rdService) {
        this.rdService = rdService;
    }

    public ManagementClient getManagementClient() {
        return managementClient;
    }

    public void setManagementClient(ManagementClient managementClient) {
        this.managementClient = managementClient;
    }

    public String getManagementClientPackage() {
        if (hasManagementClient()) {
            return managementClient.getPackageName();
        }
        return "";
    }

    public Boolean hasManagementClient() {
        return !managementClient.getPackageName().equals("");
    }

    public String getRDSevicePackage() {
        return rdService.getPackageName();
    }

}
