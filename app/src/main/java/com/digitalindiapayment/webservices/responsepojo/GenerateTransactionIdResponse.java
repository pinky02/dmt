package com.digitalindiapayment.webservices.responsepojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 22-05-2017.
 */

public class GenerateTransactionIdResponse implements Parcelable {

    @SerializedName("transactionId")
    private String transactionId;

    @SerializedName("timestamp")
    private String timestamp;

    @SerializedName("aadharNo")
    private String adharNo;

    @SerializedName("bank")
    private String bank;

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getAdharNo() {
        return adharNo;
    }

    public void setAdharNo(String adharNo) {
        this.adharNo = adharNo;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.transactionId);
        dest.writeString(this.timestamp);
        dest.writeString(this.adharNo);
        dest.writeString(this.bank);
    }

    public GenerateTransactionIdResponse() {
    }

    protected GenerateTransactionIdResponse(Parcel in) {
        this.transactionId = in.readString();
        this.timestamp = in.readString();
        this.adharNo = in.readString();
        this.bank = in.readString();
    }


    public static final Parcelable.Creator<GenerateTransactionIdResponse> CREATOR = new Creator<GenerateTransactionIdResponse>() {
        @Override
        public GenerateTransactionIdResponse createFromParcel(Parcel source) {
            return null;
        }

        @Override
        public GenerateTransactionIdResponse[] newArray(int size) {
            return new GenerateTransactionIdResponse[0];
        }
    };


}
