package com.digitalindiapayment.webservices.responsepojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 23-05-2017.
 */

public class GenerateDepositWithdrawTransIdResponse implements Parcelable {

    @SerializedName("transactionId")
    private String transactionId;

    @SerializedName("timestamp")
    private String timestamp;

    @SerializedName("aadharNo")
    private String adharNo;

    @SerializedName("bank")
    private String bank;

    @SerializedName("amount")
    private String amount;


    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getAdharNo() {
        return adharNo;
    }

    public void setAdharNo(String adharNo) {
        this.adharNo = adharNo;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    protected GenerateDepositWithdrawTransIdResponse(Parcel in) {
        this.transactionId = in.readString();
        this.timestamp = in.readString();
        this.adharNo = in.readString();
        this.bank = in.readString();
        this.amount = in.readString();

    }

    public static final Creator<GenerateDepositWithdrawTransIdResponse> CREATOR = new Creator<GenerateDepositWithdrawTransIdResponse>() {
        @Override
        public GenerateDepositWithdrawTransIdResponse createFromParcel(Parcel in) {
            return new GenerateDepositWithdrawTransIdResponse(in);
        }

        @Override
        public GenerateDepositWithdrawTransIdResponse[] newArray(int size) {
            return new GenerateDepositWithdrawTransIdResponse[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }
}
