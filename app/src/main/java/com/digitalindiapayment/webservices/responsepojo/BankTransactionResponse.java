package com.digitalindiapayment.webservices.responsepojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;


public class BankTransactionResponse implements Parcelable {



    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getAadhaarNo() {
        return aadhaarNo;
    }

    public void setAadhaarNo(String aadhaarNo) {
        this.aadhaarNo = aadhaarNo;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getServiceMod() {
        return serviceMod;
    }

    public void setServiceMod(String serviceMod) {
        this.serviceMod = serviceMod;
    }

    @SerializedName("transactionId")
    private String transactionId;

    @SerializedName("timestamp")
    private String timestamp;

    @SerializedName("aadharNo")
    private String aadhaarNo;

    @SerializedName("bank")
    private String bankCode;

    @SerializedName("bankName")
    private String bankName;

    @SerializedName("balance")
    private String balance;

    @SerializedName("response")
    private String response;

    @SerializedName("remark")
    private String remark;

    @SerializedName("serviceMod")
    private String serviceMod;


    @SerializedName("status")
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isCompleted(){
        return this.status != null ? this.status.equals("Complete") : false ;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.transactionId);
        dest.writeString(this.timestamp);
        dest.writeString(this.aadhaarNo);
        dest.writeString(this.bankCode);
        dest.writeString(this.bankName);
        dest.writeString(this.balance);
        dest.writeString(this.response);
        dest.writeString(this.remark);
        dest.writeString(this.serviceMod);
        dest.writeString(this.status);
    }

    public BankTransactionResponse() {
    }

    protected BankTransactionResponse(Parcel in) {
        this.transactionId = in.readString();
        this.timestamp = in.readString();
        this.aadhaarNo = in.readString();
        this.bankCode = in.readString();
        this.bankName = in.readString();
        this.balance = in.readString();
        this.response = in.readString();
        this.remark = in.readString();
        this.serviceMod = in.readString();
        this.status = in.readString();
    }

    public static final Parcelable.Creator<BankTransactionResponse> CREATOR = new Parcelable.Creator<BankTransactionResponse>() {
        @Override
        public BankTransactionResponse createFromParcel(Parcel source) {
            return new BankTransactionResponse(source);
        }

        @Override
        public BankTransactionResponse[] newArray(int size) {
            return new BankTransactionResponse[size];
        }
    };
}
