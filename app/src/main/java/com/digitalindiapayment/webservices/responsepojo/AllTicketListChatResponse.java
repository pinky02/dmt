package com.digitalindiapayment.webservices.responsepojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Pinky on 07-06-2018.
 */

public class AllTicketListChatResponse {
    @SerializedName("TicketDetail")
    @Expose
    private TicketDetail ticketDetail;

    public TicketDetail getTicketDetail() {
        return ticketDetail;
    }

    public void setTicketDetail(TicketDetail ticketDetail) {
        this.ticketDetail = ticketDetail;
    }
}
