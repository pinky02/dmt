package com.digitalindiapayment.webservices.responsepojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 01-07-2017.
 */

public class ConfirmTransactionResponse {
    @SerializedName("transactionId")
    @Expose
    private Integer transactionId;
    @SerializedName("aadharNo")
    @Expose
    private Integer adharNo;

    public Integer getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Integer transactionId) {
        this.transactionId = transactionId;
    }

    public Integer getAdharNo() {
        return adharNo;
    }

    public void setAdharNo(Integer adharNo) {
        this.adharNo = adharNo;
    }
}
