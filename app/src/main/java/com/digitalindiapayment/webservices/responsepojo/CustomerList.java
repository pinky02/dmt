package com.digitalindiapayment.webservices.responsepojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Pinky on 07-06-2018.
 */

public class CustomerList {
    @SerializedName("customer_code")
    @Expose
    private String customerCode;
    @SerializedName("TicketList")
    @Expose
    public List<TicketListData> ticketList = null;

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public List<TicketListData> getTicketList() {
        return ticketList;
    }

    public void setTicketList(List<TicketListData> ticketList) {
        this.ticketList = ticketList;
    }
}
