package com.digitalindiapayment.webservices.responsepojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Pinky on 07-06-2018.
 */

public class TicketDetail {
    @SerializedName("ticket_comment")
    @Expose
    private String ticketComment;
    @SerializedName("ticket_priority")
    @Expose
    private String ticketPriority;
    @SerializedName("ticket_product")
    @Expose
    private String ticketProduct;
    @SerializedName("issue1")
    @Expose
    private String issue1;
    @SerializedName("issue2")
    @Expose
    private String issue2;
    @SerializedName("current_status")
    @Expose
    private String currentStatus;
    @SerializedName("created_by")
    @Expose
    private String createdBy;
    @SerializedName("created_date")
    @Expose
    private String createdDate;
    @SerializedName("created_by_type")
    @Expose
    private String createdByType;
    @SerializedName("status_history")
    @Expose
    private List<StatusHistory> statusHistory = null;
    @SerializedName("CommentList")
    @Expose
    private List<CommentList> commentList = null;
    @SerializedName("count_of_comment")
    @Expose
    private Integer countOfComment;

    public String getTicketComment() {
        return ticketComment;
    }

    public void setTicketComment(String ticketComment) {
        this.ticketComment = ticketComment;
    }

    public String getTicketPriority() {
        return ticketPriority;
    }

    public void setTicketPriority(String ticketPriority) {
        this.ticketPriority = ticketPriority;
    }

    public String getTicketProduct() {
        return ticketProduct;
    }

    public void setTicketProduct(String ticketProduct) {
        this.ticketProduct = ticketProduct;
    }

    public String getIssue1() {
        return issue1;
    }

    public void setIssue1(String issue1) {
        this.issue1 = issue1;
    }

    public String getIssue2() {
        return issue2;
    }

    public void setIssue2(String issue2) {
        this.issue2 = issue2;
    }

    public String getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedByType() {
        return createdByType;
    }

    public void setCreatedByType(String createdByType) {
        this.createdByType = createdByType;
    }

    public List<StatusHistory> getStatusHistory() {
        return statusHistory;
    }

    public void setStatusHistory(List<StatusHistory> statusHistory) {
        this.statusHistory = statusHistory;
    }

    public List<CommentList> getCommentList() {
        return commentList;
    }

    public void setCommentList(List<CommentList> commentList) {
        this.commentList = commentList;
    }

    public Integer getCountOfComment() {
        return countOfComment;
    }

    public void setCountOfComment(Integer countOfComment) {
        this.countOfComment = countOfComment;
    }

}
