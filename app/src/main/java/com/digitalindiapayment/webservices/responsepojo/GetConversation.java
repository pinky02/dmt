package com.digitalindiapayment.webservices.responsepojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by amolbhanushali on 2017/12/11.
 */

public class GetConversation {
    @SerializedName("conversation")
    @Expose
    private List<ConversationList> conversation = null;

    public List<ConversationList> getConversation() {
        return conversation;
    }

    public void setConversation(List<ConversationList> conversation) {
        this.conversation = conversation;
    }
}
