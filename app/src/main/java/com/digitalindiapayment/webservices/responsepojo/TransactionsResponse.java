package com.digitalindiapayment.webservices.responsepojo;

import com.digitalindiapayment.pojo.LastFiveTransaction;
import com.digitalindiapayment.pojo.TransactionReportBean;
import com.digitalindiapayment.reports.TransactionReport.Transaction;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by admin on 01-07-2017.
 */

public class TransactionsResponse {
    @SerializedName("transactions")
    @Expose
    private List<LastFiveTransaction> transactions = null;

    public List<LastFiveTransaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<LastFiveTransaction> transactions) {
        this.transactions = transactions;
    }

}
