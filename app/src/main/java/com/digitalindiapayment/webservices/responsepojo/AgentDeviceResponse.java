package com.digitalindiapayment.webservices.responsepojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by admin on 07-11-2017.
 */

public class AgentDeviceResponse {
    @SerializedName("devices")
    @Expose
    private List<ScannerDeviceResponse> devices = null;

    public List<ScannerDeviceResponse> getDevices() {
        return devices;
    }

    public void setDevices(List<ScannerDeviceResponse> devices) {
        this.devices = devices;
    }
}
