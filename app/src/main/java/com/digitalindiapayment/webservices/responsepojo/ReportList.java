package com.digitalindiapayment.webservices.responsepojo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Nivrutti Pawar on 17-11-2017.
 */

public class ReportList {
    @SerializedName("name")
    private String name;
    @SerializedName("settlement_request")
    private String settlement_request;
    @SerializedName("beneficiary_name")
    private String beneficiary_name;
    @SerializedName("account_number")
    private String account_number;
    @SerializedName("transaction_amount")
    private String transaction_amount;
    @SerializedName("created_at")
    private String created_at;
    @SerializedName("request_id")
    private String request_id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSettlement_request() {
        return settlement_request;
    }

    public void setSettlement_request(String settlement_request) {
        this.settlement_request = settlement_request;
    }

    public String getBeneficiary_name() {
        return beneficiary_name;
    }

    public void setBeneficiary_name(String beneficiary_name) {
        this.beneficiary_name = beneficiary_name;
    }

    public String getAccount_number() {
        return account_number;
    }

    public void setAccount_number(String account_number) {
        this.account_number = account_number;
    }

    public String getTransaction_amount() {
        return transaction_amount;
    }

    public void setTransaction_amount(String transaction_amount) {
        this.transaction_amount = transaction_amount;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getRequest_id() {
        return request_id;
    }

    public void setRequest_id(String request_id) {
        this.request_id = request_id;
    }
}
