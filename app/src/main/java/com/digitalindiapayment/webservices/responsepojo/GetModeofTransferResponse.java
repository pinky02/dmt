package com.digitalindiapayment.webservices.responsepojo;

import com.digitalindiapayment.pojo.ModeofTransfer;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.List;
/**
 * Created by Amol on 04-04-2017.
 */

public class GetModeofTransferResponse {

    @SerializedName("modes")
    private List<ModeofTransfer> modes = new ArrayList<>();

    public List<ModeofTransfer> getModeofTransfer() {
        return modes;
    }

}
