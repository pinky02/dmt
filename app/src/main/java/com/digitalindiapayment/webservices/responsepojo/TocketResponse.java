package com.digitalindiapayment.webservices.responsepojo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by amolbhanushali on 2017/12/30.
 */

public class TocketResponse {
    @SerializedName("__v")
    private String v;
    @SerializedName("subject")
    private String subject;
    @SerializedName("createdBy")
    private String createdBy;
    @SerializedName("updatedBy")
    private String updatedBy;
    @SerializedName("code")
    private String code;
    @SerializedName("_id")
    private String _id;

    public String getV() {
        return v;
    }

    public void setV(String v) {
        this.v = v;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }
}
