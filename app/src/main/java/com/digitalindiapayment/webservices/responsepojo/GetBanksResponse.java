package com.digitalindiapayment.webservices.responsepojo;


import com.digitalindiapayment.pojo.BankDetailsBean;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class GetBanksResponse {

    @SerializedName("banks")
    private List<BankDetailsBean> banks = new ArrayList<BankDetailsBean>();

    public List<BankDetailsBean> getBankDetailsBeanList() {
        return banks;
    }

    public void setBankDetailsBeanList(List<BankDetailsBean> bankDetailsBeanList) {
        this.banks = bankDetailsBeanList;
    }
}
