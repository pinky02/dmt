package com.digitalindiapayment.webservices.responsepojo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by amolbhanushali on 2017/12/09.
 */

public class CreateTicketResponse {
    @SerializedName("status")
    private String status;
    @SerializedName("errorCode")
    private String errorCode;
    @SerializedName("error")
    private String error;
    @SerializedName("response")
    private TocketResponse response;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public TocketResponse getResponse() {
        return response;
    }

    public void setResponse(TocketResponse response) {
        this.response = response;
    }
}
