package com.digitalindiapayment.webservices.responsepojo;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Nivrutti Pawar on 17-11-2017.
 */

public class SettlementReportResponse {
    @SerializedName("status")
    private String status;
    @SerializedName("response")
    private List<ReportList> reportList;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<ReportList> getReportList() {
        return reportList;
    }

    public void setReportList(List<ReportList> reportList) {
        this.reportList = reportList;
    }
}
