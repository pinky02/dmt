package com.digitalindiapayment.webservices.responsepojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Pinky on 31-05-2018.
 */

public class GenerateTicketResponse {
    @SerializedName("response")
    @Expose
    private String response;
    @SerializedName("ticket_no")
    @Expose
    private String ticketNo;
    @SerializedName("msg")
    @Expose
    private Integer msg;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getTicketNo() {
        return ticketNo;
    }

    public void setTicketNo(String ticketNo) {
        this.ticketNo = ticketNo;
    }

    public Integer getMsg() {
        return msg;
    }

    public void setMsg(Integer msg) {
        this.msg = msg;
    }
}
