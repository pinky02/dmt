package com.digitalindiapayment.webservices.responsepojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 03-07-2017.
 */

public class TransactionStatusResponse {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("transactionId")
    @Expose
    private Integer transactionId;
    @SerializedName("timestamp")
    @Expose
    private String timestamp;
    @SerializedName("aadharNo")
    @Expose
    private Integer adharNo;
    @SerializedName("bank")
    @Expose
    private String bank;
    @SerializedName("balance")
    @Expose
    private Integer balance;
    @SerializedName("response")
    @Expose
    private String response;
    @SerializedName("remark")
    @Expose
    private String remark;
    @SerializedName("serviceMod")
    @Expose
    private String serviceMod;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Integer transactionId) {
        this.transactionId = transactionId;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public Integer getAdharNo() {
        return adharNo;
    }

    public void setAdharNo(Integer adharNo) {
        this.adharNo = adharNo;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getServiceMod() {
        return serviceMod;
    }

    public void setServiceMod(String serviceMod) {
        this.serviceMod = serviceMod;
    }

    public Boolean isStatusSuccess(){
        return !this.status.equals("pending");
    }
}
