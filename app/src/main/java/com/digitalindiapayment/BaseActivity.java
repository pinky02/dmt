package com.digitalindiapayment;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.digitalindiapayment.login.LoginActivity;
import com.digitalindiapayment.util.Util;
import com.digitalindiapayment.webservices.InValidToken;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.net.SocketTimeoutException;


public class BaseActivity extends AppCompatActivity {
    private ProgressDialog prgDialog;


    protected void showDialog() {
        try {
            if (prgDialog == null) {
                prgDialog = ProgressDialog.show(this, "", "Please Wait");
            } else {
                prgDialog.show();
            }
        } catch (Exception e) {

        }
    }


    protected void disMissDialog() {
        try {
            if (prgDialog != null && prgDialog.isShowing())
                prgDialog.dismiss();
        } catch (Exception e) {

        }
    }

    protected void redirectToLogin(){
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(InValidToken event) {

        Util.showShortToast(getApplicationContext(),getString(R.string.user_logout_warning_message));
        redirectToLogin();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(SocketTimeoutException e) {

        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(getResources().getString(R.string.error_connection_timeout_head));
        alertDialog.setMessage(getString(R.string.error_connection_timeout_message));
        alertDialog.setCancelable(false);
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        alertDialog.show();
        //finish();
        //UtilDmt.showShortToast(getApplicationContext(),getString(R.string.user_logout_warning_message));
        //redirectToLogin();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }


    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
