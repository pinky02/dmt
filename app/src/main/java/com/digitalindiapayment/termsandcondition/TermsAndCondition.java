// The present software is not subject to the US Export Administration Regulations (no exportation license required), May 2012
package com.digitalindiapayment.termsandcondition;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.digitalindiapayment.R;
import com.digitalindiapayment.bluetoothcontroller.BluetoothController;
import com.digitalindiapayment.connection.USBConnection;
import com.digitalindiapayment.morphoScanConnect.RDFingureScanActivity;
import com.digitalindiapayment.screenanlytics.AppFirebaseAnalytics;
import com.digitalindiapayment.screenanlytics.IAnalytics;
import com.digitalindiapayment.util.Util;
import com.esys.impressivedemo.ImpressiveclassEnum;
import com.morpho.sample.KeyFile;
import com.morpho.sample.MorphoCaptureFingerPrintActivity;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TermsAndCondition extends Activity {

    @BindView(R.id.connectRegisterDevice)
    TextView connectRegisterDevice;
    @BindView(R.id.deviceIcon)
    ImageView deviceIcon;
    String sensorName;
    IAnalytics iAnalytics;
    Bundle trackEventProp;
    private Button btn_scan;
    private CheckBox checkbox;
    private Map<String, Class> scannerSelection;
    private Map<String, Integer> scannerIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.morpho_main_activity);
        ButterKnife.bind(this);

        sensorName = Util.PREFERENCES.getDeviceName(TermsAndCondition.this);

        ButtonClick buttonClick = new ButtonClick();
        btn_scan = (Button) findViewById(R.id.btn_scan);
        checkbox = (CheckBox) findViewById(R.id.checkbox);
        btn_scan.setOnClickListener(buttonClick);


        scannerSelection = new HashMap<>();
        // If Scanner is bluetooth device then redirect to splash screen (For bluetooth connection)
        scannerSelection.put(getResources().getString(R.string.device_hemen_name), BluetoothController.class);
        if (Util.isBluetoothConnected)
            scannerSelection.put(getResources().getString(R.string.device_evolute_name), RDFingureScanActivity.class);
        else
            scannerSelection.put(getResources().getString(R.string.device_evolute_name), BluetoothController.class);
        // If device is wired connection then dirctly call their respective Activity
        //scannerSelection.put(getString(R.string.device_morpho_name),MorphoCaptureFingerPrintActivity.class);

        if (Util.PREFERENCES.isRDEnabled(this)) {
            scannerSelection.put(getString(R.string.device_morpho_name), RDFingureScanActivity.class);
            scannerSelection.put(getString(R.string.device_mantra_name), RDFingureScanActivity.class);
            scannerSelection.put(getString(R.string.device_digital_persona_name), RDFingureScanActivity.class);
            scannerSelection.put(getString(R.string.device_startek_name), RDFingureScanActivity.class);
            scannerSelection.put(getString(R.string.device_morphol0s_name),RDFingureScanActivity.class);
        } else {
            scannerSelection.put(getString(R.string.device_morpho_name), MorphoCaptureFingerPrintActivity.class);
        }

        scannerIcon = new HashMap<>();
        scannerIcon.put(getString(R.string.device_morpho_name), R.drawable.ic_fingerprint_scanner);
        scannerIcon.put(getString(R.string.device_mantra_name), R.drawable.ic_fingerprint_scanner);
        scannerIcon.put(getString(R.string.device_digital_persona_name), R.drawable.ic_fingerprint_scanner);
        scannerIcon.put(getString(R.string.device_hemen_name), R.drawable.ic_mintra);
        scannerIcon.put(getString(R.string.device_evolute_name), R.drawable.ic_evoluate);
        scannerIcon.put(getString(R.string.device_startek_name), R.drawable.ic_fingerprint_scanner);
        scannerIcon.put(getString(R.string.device_morphol0s_name),R.drawable.ic_fingerprint_scanner);


        connectRegisterDevice.setText(String.format(getString(R.string.register_device_message), sensorName));
        Drawable drawable = getResources().getDrawable(scannerIcon.get(sensorName));
        if (drawable != null) {
            deviceIcon.setImageDrawable(drawable);
        }


        //scannerSelection.put(getString(R.string.device_mantra_name),R.class);
        //scannerSelection.put(getString(R.string.device_mantra_name),MantraFingureScanActivity.class);

        //scannerSelection.put(getString(R.string.device_mantra_name),MantraScanner.class);

        iAnalytics = new AppFirebaseAnalytics(TermsAndCondition.this);
        trackEventProp = new Bundle();
    }


    @Override
    protected void onResume() {
        super.onResume();
        iAnalytics.trackScreen(getString(R.string.terms_and_condition_screen));
    }

    class ButtonClick implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btn_scan:

                    if (checkbox.isChecked()) {
                        trackEventProp.putString(getString(R.string.action), getString(R.string.click));
                        trackEventProp.putString(getString(R.string.assigned_device), sensorName);
                        trackEventProp.putString(getString(R.string.mobile), Util.PREFERENCES.getUserId(TermsAndCondition.this));

                        //Log.d("Morpho", "SensorName " + sensorName);
                        String deviceName = KeyFile.getDevise(getApplicationContext());
                        //Log.d("Morpho", "Above if loop" + deviceName);

                        Boolean isUSBWiredConnection = USBConnection.class.isAssignableFrom(scannerSelection.get(sensorName));

                        if (isUSBWiredConnection) {
                            if (deviceName == null || deviceName.equalsIgnoreCase("")) {
                                Toast.makeText(getApplicationContext(), R.string.device_not_found, Toast.LENGTH_LONG).show();
                                trackEventProp.putString(getString(R.string.message), getString(R.string.device_not_found));
                                return;
                            }
                        }

                        if (scannerSelection.get(sensorName) != null) {
                            Intent intent = new Intent(TermsAndCondition.this, scannerSelection.get(sensorName));
                            intent.putExtra("ENUM", ImpressiveclassEnum.SCAN);
                            startActivity(intent);
                            finish();
                            trackEventProp.putString(getString(R.string.message), getString(R.string.success));
                        } else {
                            Util.showLongToast(TermsAndCondition.this, getString(R.string.invalid_request));
                            trackEventProp.putString(getString(R.string.message), getString(R.string.invalid_request));
                        }

                        iAnalytics.trackCustomEvent(R.string.device_selection, trackEventProp);

                    } else {
                        Toast.makeText(getApplicationContext(), "Please check T & C", Toast.LENGTH_LONG).show();
                    }
                    break;
            }
        }
    }


}
