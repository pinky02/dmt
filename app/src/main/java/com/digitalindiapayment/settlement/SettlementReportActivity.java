package com.digitalindiapayment.settlement;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.digitalindiapayment.R;
import com.digitalindiapayment.util.Util;
import com.digitalindiapayment.webservices.responsepojo.ReportList;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class SettlementReportActivity extends AppCompatActivity implements SettlementReportContract.View {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recyclerSettlementReport)
    RecyclerView recyclerView;
    @BindView(R.id.progressdialogdip)
    RelativeLayout progressdialogdip;
    @BindView(R.id.noInternetMsg)
    TextView noInternetMsg;
    @BindView(R.id.noInternetContainer)
    LinearLayout noInternetContainer;
    @BindView(R.id.refreshButton)
    ImageView refreshButton;
    @BindView(R.id.tvNoData)
    TextView tvNoData;

    private Context mContext;

    private LinearLayoutManager mLayoutManager;
    private SettlementReportsAdapter mAdapter;
    private SettlementReportContract.UserAction userAction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settlement_report);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.settlement_reports);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mContext = this;
        mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);

        noInternetContainer.setVisibility(View.GONE);
        progressdialogdip.setVisibility(View.GONE);
        userAction = new SettlementReportPresenter(SettlementReportActivity.this, getApplicationContext());
        showLoading();
        userAction.getSettlementReport();
    }

    @Override
    public void noInternet() {
        hideLoading();
        noInternetContainer.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
    }

    @Override
    public void exception(Throwable e) {
        Util.showShortToast(getApplicationContext(), e.getMessage());
        hideLoading();
    }

    @Override
    public void fail(String message) {
        Util.showShortToast(getApplicationContext(), message);
        hideLoading();

    }

    @Override
    public void success(String message) {

    }

    @Override
    public void showLoading() {
        progressdialogdip.setVisibility(View.VISIBLE);
        noInternetContainer.setVisibility(View.GONE);
    }

    @Override
    public void hideLoading() {
        progressdialogdip.setVisibility(View.GONE);
    }


    @Override
    public void showSettlementReportList(List<ReportList> reportLists) {
        hideLoading();
        if (reportLists != null && reportLists.size() > 0) {
            tvNoData.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            mAdapter = new SettlementReportsAdapter(reportLists, mContext);
            recyclerView.setAdapter(mAdapter);
        } else {
            //report list is empty or null
            tvNoData.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.refreshButton)
    public void onViewClicked() {
        showLoading();
        userAction.getSettlementReport();
    }
}
