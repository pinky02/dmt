package com.digitalindiapayment.settlement;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.digitalindiapayment.R;
import com.digitalindiapayment.pojo.SettingOption;
import com.digitalindiapayment.settings.SettingOptionAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class SettlementActivity extends AppCompatActivity implements OnItemClick {



    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.settlmentOptions)
    RecyclerView settlmentOptions;

    private SettlementAdapter settlementOptionAdapter;
    private List<SettingOption> settlementOptionMenus;
    private SettingOption currentSelectedSettingOption;
    public final int SETTLEMENT_DETAILS = 0;
    public final int SETTLEMENT_REQUEST = 1;
    public final int SETTLEMENT_REPORT = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settlement);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.settlement);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        settlementOptionMenus = new ArrayList<SettingOption>();
        settlementOptionAdapter = new SettlementAdapter(settlementOptionMenus, getApplicationContext(), this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        settlmentOptions.setLayoutManager(mLayoutManager);
        settlmentOptions.setItemAnimator(new DefaultItemAnimator());
        settlmentOptions.setAdapter(settlementOptionAdapter);

        settlementOptions();
    }

    private void settlementOptions() {
        SettingOption settlement = new SettingOption(getString(R.string.settlement_detail), R.drawable.ic_settlement_detail);
        settlementOptionMenus.add(SETTLEMENT_DETAILS,settlement);
        settlement = new SettingOption(getResources().getString(R.string.settlement_request), R.drawable.ic_settlement_request);
        settlementOptionMenus.add(SETTLEMENT_REQUEST,settlement);
        settlement = new SettingOption(getResources().getString(R.string.settlement_rprt), R.drawable.ic_settlement_report);
        settlementOptionMenus.add(SETTLEMENT_REPORT,settlement);

        settlementOptionAdapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(int position) {

        switch (position) {
            case SETTLEMENT_DETAILS:
                Intent intent = new Intent(this, SettlementDetailsActivity.class);
                startActivity(intent);
                break;
            case SETTLEMENT_REQUEST:
                Intent settlementReqIntent=new Intent(this,SettlementRequestActivity.class);
                startActivity(settlementReqIntent);
                break;
            case SETTLEMENT_REPORT:
                Intent settlementReportIntent=new Intent(this,SettlementReportActivity.class);
                startActivity(settlementReportIntent);
                break;
        }

    }
}
