package com.digitalindiapayment.settlement;

import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.digitalindiapayment.R;
import com.digitalindiapayment.pojo.SettingOption;
import com.pnikosis.materialishprogress.ProgressWheel;

import java.util.List;

/**
 * Created by admin on 03-11-2017.
 */

public class SettlementAdapter extends RecyclerView.Adapter<SettlementAdapter.ViewHolder> {
    private List<SettingOption> settlementOptions;
    private Context context;
    private OnItemClick itemClick;

    public SettlementAdapter(List<SettingOption> settlementOptions, Context context,OnItemClick itemClick) {
        this.settlementOptions=settlementOptions;
        this.context=context;
        this.itemClick=itemClick;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=LayoutInflater.from(parent.getContext()).inflate(R.layout.row_settlement_adapter,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        SettingOption option = settlementOptions.get(position);

        // Set Icon
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            holder.settlementIcon.setBackground(context.getDrawable(option.getIcon()));
        }else {
            holder.settlementIcon.setBackground(context.getResources().getDrawable(option.getIcon()));
        }

        holder.settlementTitle.setText(option.getTitle());
        holder.navigation.setVisibility(option.getHasNavigation() ? View.VISIBLE : View.INVISIBLE );

        if(option.getHasLoader()) {
            holder.progressWheel.setVisibility(option.getProgressStatus() ? View.VISIBLE : View.INVISIBLE);
        }

        holder.relativeMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClick.onItemClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return settlementOptions.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView settlementIcon,navigation;
        private TextView settlementTitle;
        private ProgressWheel progressWheel;
        private RelativeLayout relativeMain;
        public ViewHolder(View itemView) {
            super(itemView);
            settlementIcon=(ImageView)itemView.findViewById(R.id.settingIcon);
            navigation=(ImageView)itemView.findViewById(R.id.navigation);
            settlementTitle=(TextView) itemView.findViewById(R.id.settingTitle);
            progressWheel=(ProgressWheel)itemView.findViewById(R.id.progressWheel);
            relativeMain=(RelativeLayout)itemView.findViewById(R.id.relativeMain);
        }
    }
}
