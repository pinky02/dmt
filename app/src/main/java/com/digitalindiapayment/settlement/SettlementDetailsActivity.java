package com.digitalindiapayment.settlement;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.digitalindiapayment.R;
import com.digitalindiapayment.settlement.add_settlement.AddSettlementActivity;
import com.digitalindiapayment.util.Util;
import com.digitalindiapayment.webservices.responsepojo.SettlementAccount;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class SettlementDetailsActivity extends AppCompatActivity implements SettlementDetailsContract.View {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.imglayout)
    LinearLayout imglayout;
    @BindView(R.id.tvBenificiaryName)
    TextView tvBenificiaryName;
    @BindView(R.id.tvBankName)
    TextView tvBankName;
    @BindView(R.id.tvAccNo)
    TextView tvAccNo;
    @BindView(R.id.tvIfscCoder)
    TextView tvIfscCoder;
    @BindView(R.id.tvSettlementType)
    TextView tvSettlementType;

    SettlementDetailsContract.UserAction userAction;
    @BindView(R.id.progressdialogdip)
    RelativeLayout progressdialogdip;
    @BindView(R.id.noInternetMsg)
    TextView noInternetMsg;
    @BindView(R.id.noInternetContainer)
    LinearLayout noInternetContainer;

    @BindView(R.id.linearMain)
    ScrollView linearMain;
    @BindView(R.id.refreshButton)
    ImageView refreshButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settlement_details);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.settlement_acc_details);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        noInternetContainer.setVisibility(View.GONE);
        progressdialogdip.setVisibility(View.GONE);
        userAction = new SettlementDetailsPresenter(SettlementDetailsActivity.this, getApplicationContext());

    }

    @Override
    protected void onResume() {
        super.onResume();
        showLoading();
        linearMain.setVisibility(View.GONE);
        userAction.getSettlementDetails();
    }

    @Override
    public void noInternet() {
        hideLoading();
        noInternetContainer.setVisibility(View.VISIBLE);
        linearMain.setVisibility(View.GONE);
    }

    @Override
    public void exception(Throwable e) {
        hideLoading();
        Util.showShortToast(getApplicationContext(), e.getMessage());

    }

    @Override
    public void fail(String message) {
        hideLoading();
        Util.showShortToast(getApplicationContext(), message);
    }

    @Override
    public void success(String message) {

    }

    @Override
    public void showLoading() {
        progressdialogdip.setVisibility(View.VISIBLE);
        noInternetContainer.setVisibility(View.GONE);
    }

    @Override
    public void hideLoading() {
        progressdialogdip.setVisibility(View.GONE);
    }

    @Override
    public void showSettlementDetails(SettlementAccount response) {
        hideLoading();
        linearMain.setVisibility(View.VISIBLE);
        tvBankName.setText(response.getBankName());
        tvBenificiaryName.setText(response.getBeneficiaryName());
        tvAccNo.setText(response.getAccountNumber());
        tvIfscCoder.setText(response.getIfscCode());
        tvSettlementType.setText(response.getSettlementType());

    }

    @Override
    public void addSettlement() {
        Intent intent = new Intent(this, AddSettlementActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.refreshButton)
    public void onViewClicked() {
        showLoading();
        userAction.getSettlementDetails();
    }
}
