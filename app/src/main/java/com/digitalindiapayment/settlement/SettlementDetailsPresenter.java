package com.digitalindiapayment.settlement;

import android.content.Context;

import com.digitalindiapayment.R;
import com.digitalindiapayment.util.Util;
import com.digitalindiapayment.webservices.Api;
import com.digitalindiapayment.webservices.core.ApiFail;
import com.digitalindiapayment.webservices.core.ApiSuccess;
import com.digitalindiapayment.webservices.core.HttpErrorResponse;
import com.digitalindiapayment.webservices.responsepojo.SettlementAccountResponse;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Nivrutti Pawar on 04-11-2017.
 */

public class SettlementDetailsPresenter implements SettlementDetailsContract.UserAction {
    private final SettlementDetailsContract.View view;
    private final Context context;

    public SettlementDetailsPresenter(SettlementDetailsContract.View view, Context context) {
        this.view = view;
        this.context = context;
    }

    @Override
    public void getSettlementDetails() {
        //call api
        Api.settlement().getSettlementDetails(Util.PREFERENCES.getToken(context))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ApiSuccess<SettlementAccountResponse>() {
                    @Override
                    public void call(SettlementAccountResponse settlementAccountResponse) {
                        if (settlementAccountResponse.getResponse().getIsExist().equalsIgnoreCase("true"))
                            view.showSettlementDetails(settlementAccountResponse.getResponse());
                        else if (settlementAccountResponse.getResponse().getIsExist().equalsIgnoreCase("false"))
                            view.addSettlement();

                    }
                }, new ApiFail() {
                    @Override
                    public void httpStatus(HttpErrorResponse response) {
                        view.fail(response.getError());
                    }

                    @Override
                    public void noNetworkError() {
                        view.noInternet();
                    }

                    @Override
                    public void unknownError(Throwable e) {
                        view.fail(context.getString(R.string.error));
                    }
                });
    }
}
