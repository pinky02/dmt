package com.digitalindiapayment.settlement;

import android.content.Context;

import com.digitalindiapayment.R;
import com.digitalindiapayment.util.Util;
import com.digitalindiapayment.webservices.Api;
import com.digitalindiapayment.webservices.core.ApiFail;
import com.digitalindiapayment.webservices.core.ApiSuccess;
import com.digitalindiapayment.webservices.core.HttpErrorResponse;
import com.digitalindiapayment.webservices.requestpojo.SettlementRequest;
import com.digitalindiapayment.webservices.responsepojo.SettlementStatusResponse;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Nivrutti Pawar on 04-11-2017.
 */

public class SettlementRequestPresenter implements SettlementRequestContract.UserAction {
    private final SettlementRequestContract.View view;
    private final Context context;

    public SettlementRequestPresenter(SettlementRequestContract.View view, Context context) {
        this.view = view;
        this.context = context;
    }


    @Override
    public void getSettlementRequest() {
        Api.settlement().getSettlementRequest(Util.PREFERENCES.getToken(context))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ApiSuccess<SettlementStatusResponse>() {
                    @Override
                    public void call(SettlementStatusResponse settlementStatusResponse) {
                        view.showSettlementRequest(settlementStatusResponse);
                    }
                }, new ApiFail() {
                    @Override
                    public void httpStatus(HttpErrorResponse response) {
                        view.failSettlementRequest(response.getError());
                    }

                    @Override
                    public void noNetworkError() {
                        view.noInternet();
                    }

                    @Override
                    public void unknownError(Throwable e) {
                        view.fail(context.getString(R.string.error));
                    }
                });
    }

    @Override
    public void submitSettlementRequest(SettlementRequest settlementRequest) {
        Api.settlement().sendSettlementRequest(Util.PREFERENCES.getToken(context),settlementRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ApiSuccess<String>() {
                    @Override
                    public void call(String s) {
                        view.success(context.getString(R.string.success));
                    }
                }, new ApiFail() {
                    @Override
                    public void httpStatus(HttpErrorResponse response) {
                        view.fail(response.getError());
                    }

                    @Override
                    public void noNetworkError() {
                        view.noInternet();
                    }

                    @Override
                    public void unknownError(Throwable e) {
                        view.fail(context.getString(R.string.error));
                    }
                });
    }
}
