package com.digitalindiapayment.settlement.add_settlement;

import com.digitalindiapayment.mvp.BaseView;
import com.digitalindiapayment.webservices.requestpojo.AddSettlementDetailsRequest;

/**
 * Created by Nivrutti Pawar on 2017/12/06.
 */

public class AddSettlementContract {

    public interface View extends BaseView{

    }
    public interface UserAction {

        void submitSettlementDetails(AddSettlementDetailsRequest addSettlementDetailsRequest);
    }
}
