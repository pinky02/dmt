package com.digitalindiapayment.settlement.add_settlement;

import android.content.Context;

import com.digitalindiapayment.R;
import com.digitalindiapayment.util.Util;
import com.digitalindiapayment.webservices.Api;
import com.digitalindiapayment.webservices.core.ApiFail;
import com.digitalindiapayment.webservices.core.ApiSuccess;
import com.digitalindiapayment.webservices.core.HttpErrorResponse;
import com.digitalindiapayment.webservices.requestpojo.AddSettlementDetailsRequest;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Nivrutti Pawar on 2017/12/06.
 */

public class AddSettlementPresenter implements AddSettlementContract.UserAction {
    private final AddSettlementContract.View view;
    private final Context context;

    public AddSettlementPresenter(AddSettlementContract.View view, Context context) {
        this.view = view;
        this.context = context;
    }

    public void submitSettlementDetails(AddSettlementDetailsRequest addSettlementDetailsRequest) {
        Api.settlement().addSettlmentDetail(Util.PREFERENCES.getToken(context), addSettlementDetailsRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ApiSuccess<String>() {
                    @Override
                    public void call(String s) {
                        view.success(s);
                    }
                }, new ApiFail() {
                    @Override
                    public void httpStatus(HttpErrorResponse response) {
                        view.fail(response.getError());
                    }

                    @Override
                    public void noNetworkError() {
                        view.noInternet();
                    }

                    @Override
                    public void unknownError(Throwable e) {
                        view.fail(context.getString(R.string.error));
                    }
                });
    }
}
