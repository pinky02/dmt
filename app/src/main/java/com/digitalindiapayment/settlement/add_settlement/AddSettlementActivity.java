package com.digitalindiapayment.settlement.add_settlement;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.digitalindiapayment.R;
import com.digitalindiapayment.util.Util;
import com.digitalindiapayment.webservices.requestpojo.AddSettlementDetailsRequest;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddSettlementActivity extends AppCompatActivity implements AddSettlementContract.View, View.OnClickListener {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.imglayout)
    LinearLayout imglayout;
    @BindView(R.id.edtBenificiaryName)
    EditText edtBenificiaryName;
    @BindView(R.id.edtBankName)
    EditText edtBankName;
    @BindView(R.id.edtAccNo)
    EditText edtAccNo;
    @BindView(R.id.edtIfscCode)
    EditText edtIfscCode;
    @BindView(R.id.btnAddSettlement)
    Button btnSubmit;

    @BindView(R.id.progressdialogdip)
    RelativeLayout progressdialogdip;
    @BindView(R.id.noInternetMsg)
    TextView noInternetMsg;
    @BindView(R.id.noInternetContainer)
    LinearLayout noInternetContainer;
    @BindView(R.id.scrollView)
    ScrollView scrollView;
    @BindView(R.id.relativeMsg)
    RelativeLayout relativeMsg;
    @BindView(R.id.ifscLayout)
    TextInputLayout ifscLayout;
    @BindView(R.id.accNoLayout)
    TextInputLayout accNoLayout;
    @BindView(R.id.bankLayout)
    TextInputLayout bankLayout;
    @BindView(R.id.beneficiaryLayout)
    TextInputLayout beneficiaryLayout;

    AddSettlementContract.UserAction userAction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_settlement);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.add_settlement_details);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        userAction = new AddSettlementPresenter(AddSettlementActivity.this, getApplicationContext());
        btnSubmit.setOnClickListener(this);

        noInternetContainer.setVisibility(View.GONE);
        progressdialogdip.setVisibility(View.GONE);
    }

    @Override
    public void noInternet() {
        hideLoading();
        scrollView.setVisibility(View.GONE);
        btnSubmit.setVisibility(View.GONE);
        relativeMsg.setVisibility(View.VISIBLE);
        noInternetContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void exception(Throwable e) {
        btnSubmit.setText(R.string.submit);
        btnSubmit.setEnabled(true);
        Util.showShortToast(getApplicationContext(), e.getMessage());
        hideLoading();
    }

    @Override
    public void fail(String message) {
        btnSubmit.setText(R.string.submit);
        btnSubmit.setEnabled(true);
        hideLoading();
        Util.showShortToast(getApplicationContext(), message);

    }

    @Override
    public void success(String message) {
        hideLoading();
        btnSubmit.setText(R.string.submit);
        btnSubmit.setEnabled(true);
        noInternetContainer.setVisibility(View.GONE);
        scrollView.setVisibility(View.VISIBLE);
        btnSubmit.setVisibility(View.VISIBLE);
        Util.showLongToast(AddSettlementActivity.this, message);
        finish();
    }

    @Override
    public void showLoading() {
        relativeMsg.setVisibility(View.VISIBLE);
        progressdialogdip.setVisibility(View.VISIBLE);
        noInternetContainer.setVisibility(View.GONE);
    }

    @Override
    public void hideLoading() {
        progressdialogdip.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View view) {
        //on add button click call add settlemtn api with validation.
        if (validation()) {
            hideSoftKeyboard();
            String beneficiaryName = edtBenificiaryName.getText().toString();
            String bankName = edtBankName.getText().toString();
            String ifscCode = edtIfscCode.getText().toString();
            String accNo = edtAccNo.getText().toString();
            AddSettlementDetailsRequest addRequest=new AddSettlementDetailsRequest();
            addRequest.setBeneficiary_name(beneficiaryName);
            addRequest.setBank_name(bankName);
            addRequest.setIfsc_code(ifscCode);
            addRequest.setAccount_number(accNo);
            btnSubmit.setEnabled(false);
            btnSubmit.setText(getString(R.string.submitting_process));
            userAction.submitSettlementDetails(addRequest);

        }

    }

    private boolean validation() {
        String beneficiaryName = edtBenificiaryName.getText().toString();
        String bankName = edtBankName.getText().toString();
        String ifscCode = edtIfscCode.getText().toString();
        String accNo = edtAccNo.getText().toString();
        if (beneficiaryName.isEmpty()) {
            Util.showShortToast(getApplicationContext(), "Benificiary name cannot be empty");
            edtBenificiaryName.requestFocus();
            return false;
        } else if (bankName.isEmpty()) {
            Util.showShortToast(getApplicationContext(), "Bank name cannot be empty");
            edtBankName.requestFocus();
            return false;
        } else if (ifscCode.isEmpty()) {
            Util.showShortToast(getApplicationContext(), "Ifsc code cannot be empty");
            edtIfscCode.requestFocus();
            return false;

        } else if (ifscCode.length() != 11) {
            Util.showShortToast(getApplicationContext(), "Ifsc code must be 11 digit");
            edtIfscCode.requestFocus();
            return false;
        } else if (accNo.isEmpty()) {
            Util.showShortToast(getApplicationContext(), "Account number cannot be empty");
            edtAccNo.requestFocus();
            return false;
        }else if (accNo.length()>20) {
            Util.showShortToast(getApplicationContext(), "Account number must be less than 20 digit");
            edtAccNo.requestFocus();
            return false;
        }else {
            return true;
        }
    }

    @OnClick(R.id.refreshButton)
    public void onViewClicked() {
        noInternetContainer.setVisibility(View.GONE);
        scrollView.setVisibility(View.VISIBLE);
        btnSubmit.setVisibility(View.VISIBLE);
        btnSubmit.setText(R.string.submit);
        btnSubmit.setEnabled(true);
    }


    public void hideSoftKeyboard() {
        if(getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }
}
