package com.digitalindiapayment.settlement;

import android.icu.text.DecimalFormat;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.digitalindiapayment.R;
import com.digitalindiapayment.util.Util;
import com.digitalindiapayment.webservices.requestpojo.SettlementRequest;
import com.digitalindiapayment.webservices.responsepojo.SettlementStatusResponse;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class SettlementRequestActivity extends AppCompatActivity implements SettlementRequestContract.View {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    SettlementRequestContract.UserAction userAction;
    @BindView(R.id.imglayout)
    LinearLayout imglayout;
    @BindView(R.id.tvLastDayBal)
    TextView tvLastDayBal;
    @BindView(R.id.tvAvailableBal)
    TextView tvAvailableBal;
    @BindView(R.id.edtTransactionAmount)
    EditText edtTransactionAmount;
    @BindView(R.id.tvRemainingBal)
    TextView tvRemainingBal;
    @BindView(R.id.layoutContent)
    LinearLayout layoutContent;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;

    @BindView(R.id.progressdialogdip)
    RelativeLayout progressdialogdip;
    @BindView(R.id.noInternetMsg)
    TextView noInternetMsg;
    @BindView(R.id.noInternetContainer)
    LinearLayout noInternetContainer;
    @BindView(R.id.scrollView)
    ScrollView scrollView;

    double availableBalance;
    double amount;
    double remainingBalance;
    @BindView(R.id.refreshButton)
    ImageView refreshButton;
    @BindView(R.id.linear_main)
    LinearLayout linearMain;
    @BindView(R.id.relativeMsg)
    RelativeLayout relativeMsg;
    @BindView(R.id.tvErrorMsg)
    TextView tvErrorMsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settlement_request);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.settlement_req);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        userAction = new SettlementRequestPresenter(this, getApplicationContext());
        availableBalance = 0;

        noInternetContainer.setVisibility(View.GONE);
        progressdialogdip.setVisibility(View.GONE);


        edtTransactionAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    if (!s.toString().trim().equals("")) {
                        double transactionAmt = Double.parseDouble(s.toString());
                        if (transactionAmt > 0) {
                            remainingBalance = (availableBalance - transactionAmt);
                            tvRemainingBal.setText(String.valueOf(  String.format("%.3f",remainingBalance)));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!edtTransactionAmount.getText().toString().trim().isEmpty()) {
                    //edtTransactionAmount
                    amount = Double.parseDouble(edtTransactionAmount.getText().toString());

                    if (remainingBalance < 100) {
                        Util.showLongToast(SettlementRequestActivity.this, getString(R.string.remaining_balance_check));
                        return;
                    }

                    if (amount > 0 && amount < availableBalance) {
                        btnSubmit.setEnabled(false);
                        btnSubmit.setText(R.string.processing);
                        userAction.submitSettlementRequest(new SettlementRequest(edtTransactionAmount.getText().toString()));
                    } else {
                        Util.showLongToast(SettlementRequestActivity.this, getString(R.string.enter_valid_amount));
                    }
                }else {
                    Toast.makeText(SettlementRequestActivity.this, R.string.str_txn_amt, Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        showLoading();
        scrollView.setVisibility(View.GONE);
        userAction.getSettlementRequest();
    }

    @Override
    public void noInternet() {
        hideLoading();
        scrollView.setVisibility(View.GONE);
        btnSubmit.setVisibility(View.GONE);
        relativeMsg.setVisibility(View.VISIBLE);
        noInternetContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void exception(Throwable e) {
        btnSubmit.setText(R.string.submit);
        btnSubmit.setEnabled(true);
        Util.showShortToast(getApplicationContext(), e.getMessage());
        hideLoading();
    }

    @Override
    public void fail(String message) {
        btnSubmit.setText(R.string.submit);
        btnSubmit.setEnabled(true);
        hideLoading();
        Util.showShortToast(getApplicationContext(), message);

        //finish();
    }

    @Override
    public void success(String message) {
        hideLoading();
        btnSubmit.setText(R.string.submit);
        btnSubmit.setEnabled(true);
        noInternetContainer.setVisibility(View.GONE);
        scrollView.setVisibility(View.VISIBLE);
        btnSubmit.setVisibility(View.VISIBLE);
        Util.showLongToast(SettlementRequestActivity.this, message);
        finish();
    }

    @Override
    public void showLoading() {
        relativeMsg.setVisibility(View.VISIBLE);
        progressdialogdip.setVisibility(View.VISIBLE);
        noInternetContainer.setVisibility(View.GONE);
    }

    @Override
    public void hideLoading() {
        progressdialogdip.setVisibility(View.GONE);
    }

    @Override
    public void showSettlementRequest(SettlementStatusResponse response) {
        hideLoading();
        scrollView.setVisibility(View.VISIBLE);
        tvLastDayBal.setText(response.getResponse().getClosingBalance());
        availableBalance = Double.parseDouble(response.getResponse().getAvailableBalance());
        tvAvailableBal.setText("" + availableBalance);
        tvRemainingBal.setText(response.getResponse().getAvailableBalance());
    }

    @Override
    public void failSettlementRequest(String message) {

        hideLoading();
        btnSubmit.setVisibility(View.GONE);
        tvErrorMsg.setVisibility(View.VISIBLE);
        scrollView.setVisibility(View.GONE);
        tvErrorMsg.setText(message);
    }

    @OnClick(R.id.refreshButton)
    public void onViewClicked() {
        showLoading();
        userAction.getSettlementRequest();
    }
}
