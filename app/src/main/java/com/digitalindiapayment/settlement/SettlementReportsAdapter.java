package com.digitalindiapayment.settlement;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.digitalindiapayment.R;
import com.digitalindiapayment.webservices.responsepojo.ReportList;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Nivrutti Pawar on 04-11-2017.
 */

public class SettlementReportsAdapter extends RecyclerView.Adapter<SettlementReportsAdapter.ViewHolder> {
    private List<ReportList> reportLists;
    private Context mContex;

    public SettlementReportsAdapter(List<ReportList> reportLists, Context mContex) {
        this.reportLists = reportLists;
        this.mContex = mContex;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_settlment_details, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ReportList reportList = reportLists.get(position);
        holder.tvBenificiaryName.setText(reportList.getBeneficiary_name());
        holder.tvAmount.setText(mContex.getResources().getString(R.string.rs) + " " + reportList.getTransaction_amount());
        holder.tvReqId.setText(reportList.getRequest_id());
        holder.tvAccNo.setText(reportList.getAccount_number());
        holder.tvStatus.setText(reportList.getSettlement_request());
        holder.tvdate.setText(reportList.getCreated_at());
    }

    @Override
    public int getItemCount() {
        return reportLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvBenificiaryName)
        TextView tvBenificiaryName;
        @BindView(R.id.tvAmount)
        TextView tvAmount;
        @BindView(R.id.tvReqId)
        TextView tvReqId;
        @BindView(R.id.tvAccNo)
        TextView tvAccNo;
        @BindView(R.id.tvStatus)
        TextView tvStatus;
        @BindView(R.id.tvdate)
        TextView tvdate;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
