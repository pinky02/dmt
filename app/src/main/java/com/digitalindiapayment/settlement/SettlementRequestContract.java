package com.digitalindiapayment.settlement;

import com.digitalindiapayment.mvp.BaseView;
import com.digitalindiapayment.webservices.requestpojo.SettlementRequest;
import com.digitalindiapayment.webservices.responsepojo.SettlementStatusResponse;

/**
 * Created by Nivrutti Pawar on 04-11-2017.
 */

public class SettlementRequestContract {
    public interface View extends BaseView {
        void showSettlementRequest(SettlementStatusResponse response);
        void failSettlementRequest(String message);

    }

    public interface UserAction {
        void getSettlementRequest();

        void submitSettlementRequest(SettlementRequest settlementRequest);
    }
}
