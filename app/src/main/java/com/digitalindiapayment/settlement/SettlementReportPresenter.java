package com.digitalindiapayment.settlement;

import android.content.Context;

import com.digitalindiapayment.R;
import com.digitalindiapayment.util.Util;
import com.digitalindiapayment.webservices.Api;
import com.digitalindiapayment.webservices.core.ApiFail;
import com.digitalindiapayment.webservices.core.ApiSuccess;
import com.digitalindiapayment.webservices.core.HttpErrorResponse;
import com.digitalindiapayment.webservices.responsepojo.SettlementReportResponse;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Nivrutti Pawar on 04-11-2017.
 */

public class SettlementReportPresenter implements SettlementReportContract.UserAction {
    private final SettlementReportContract.View view;
    private final Context context;

    public SettlementReportPresenter(SettlementReportContract.View view, Context context) {
        this.view = view;
        this.context = context;
    }

    @Override
    public void getSettlementReport() {
        Api.settlement().getSettlementReport(Util.PREFERENCES.getToken(context))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ApiSuccess<SettlementReportResponse>() {
                    @Override
                    public void call(SettlementReportResponse reportResponse) {
                        view.showSettlementReportList(reportResponse.getReportList());
                    }
                }, new ApiFail() {
                    @Override
                    public void httpStatus(HttpErrorResponse response) {
                        String message = response.getError();
                        view.fail(message);
                    }

                    @Override
                    public void noNetworkError() {
                        view.noInternet();
                    }

                    @Override
                    public void unknownError(Throwable e) {
                        view.fail(context.getString(R.string.error));
                    }
                });
    }
}
