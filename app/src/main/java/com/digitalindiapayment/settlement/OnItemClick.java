package com.digitalindiapayment.settlement;

/**
 * Created by admin on 03-11-2017.
 */

public interface OnItemClick {
    void onItemClick(int position);
}
