package com.digitalindiapayment.settlement;

import com.digitalindiapayment.mvp.BaseView;
import com.digitalindiapayment.webservices.responsepojo.SettlementAccount;
import com.digitalindiapayment.webservices.responsepojo.SettlementAccountResponse;

/**
 * Created by Nivrutti Pawar on 04-11-2017.
 */

public class SettlementDetailsContract {
    public interface View extends BaseView {
        void showSettlementDetails(SettlementAccount response);
        void addSettlement();
    }

    public interface UserAction {
        void getSettlementDetails();
    }
}
