package com.digitalindiapayment.settlement;

import com.digitalindiapayment.mvp.BaseView;
import com.digitalindiapayment.webservices.responsepojo.ReportList;

import java.util.List;

/**
 * Created by Nivrutti Pawar on 04-11-2017.
 */

public class SettlementReportContract {
    public interface View extends BaseView {
        void showSettlementReportList(List<ReportList> reportLists);
    }

    public interface UserAction {
        void getSettlementReport();
    }
}
