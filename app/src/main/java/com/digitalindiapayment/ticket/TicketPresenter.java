package com.digitalindiapayment.ticket;

import android.content.Context;
import com.digitalindiapayment.R;
import com.digitalindiapayment.webservices.Api;
import com.digitalindiapayment.webservices.core.ApiFail;
import com.digitalindiapayment.webservices.core.ApiSuccess;
import com.digitalindiapayment.webservices.core.HttpErrorResponse;
import com.digitalindiapayment.webservices.requestpojo.TicketRequest;
import com.digitalindiapayment.webservices.responsepojo.GenerateTicketResponse;
import com.digitalindiapayment.webservices.responsepojo.IssueListResponse;
import com.google.gson.Gson;
import java.util.List;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Pinky on 31-05-2018.
 */

public class TicketPresenter implements TicketContract.UserAction{
    Context context;
    TicketContract.View view;

    public TicketPresenter(Context context, TicketContract.View view) {
        this.context = context;
        this.view = view;
    }

    @Override
    public void getIssueList(String apiKey, String productName) {


        Api.getTicket().getIssueList(apiKey,productName)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ApiSuccess<IssueListResponse>() {

                    @Override
                    public void call(IssueListResponse issueListResponse) {
                        view.showIssueList(issueListResponse,issueListResponse.getData());

                    }
                }, new ApiFail() {
                    @Override
                    public void httpStatus(HttpErrorResponse response) {
                        String message = response.getError();
                        view.fail(message);
                    }

                    @Override
                    public void noNetworkError() {
                        view.noInternet();
                    }

                    @Override
                    public void unknownError(Throwable e) {
                        view.fail(context.getString(R.string.error));
                    }
                });
    }



    @Override
    public void createTicket(List <TicketRequest> generateTicketRequest) {

        Gson gson = new Gson();
        String json = gson.toJson(generateTicketRequest);
      //  List<GenerateTicketResponse> createTicketResponse =  new ArrayList <GenerateTicketResponse>();
        Api.getTicket().createTicketNew(json)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ApiSuccess<List<GenerateTicketResponse>>() {
                    @Override
                    public void call(List<GenerateTicketResponse> createTicketResponse) {
//                        Gson gson = new Gson();
//                        String json = gson.toJson(createTicketResponse);

                        GenerateTicketResponse response = createTicketResponse.get(0);
                        view.createTicketSuccess(response.getTicketNo().toString(),response.getResponse().toString());
                       // String success = response.getResponse().toString();

                    }
                }, new ApiFail() {
                    @Override
                    public void httpStatus(HttpErrorResponse response) {
                        String message = response.getError();
                        view.fail(message);
                    }

                    @Override
                    public void noNetworkError() {
                        view.noInternet();
                    }

                    @Override
                    public void unknownError(Throwable e) {
                        view.fail(context.getString(R.string.error));
                    }
                });
    }
}
