package com.digitalindiapayment.ticket.ticket_list;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.digitalindiapayment.R;
import com.digitalindiapayment.reports.TransactionReport.RecyclerViewOnClick;
import com.digitalindiapayment.ticket.OnItemClick;
import com.digitalindiapayment.webservices.responsepojo.TicketDetail;
import com.digitalindiapayment.webservices.responsepojo.TicketListData;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by Pinky on 07-06-2018.
 */

public class TicketListAdapter extends RecyclerView.Adapter<TicketListAdapter.TicketListViewHolder>{
    private List<TicketListData> ticketDetails ;
    int position;
    private Context context;
    TicketListViewHolder holder;
    private TicketDetail responseBean;

    private RecyclerViewOnClick recyclerViewOnClick;
    private OnItemClick onItemClick;

    public TicketListAdapter(Context context,  List<TicketListData> ticketDataList, OnItemClick onItemClick){
        this.context = context;
        this.ticketDetails = ticketDataList;
       this.onItemClick = onItemClick;
    }

    @Override
    public TicketListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View ItemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.ticketlist_row, parent, false);

        ItemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                recyclerViewOnClick.onClick(v);
            }
        });

        return new TicketListAdapter.TicketListViewHolder(ItemView);
    }
    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(final TicketListViewHolder holder, final int position) {
        this.position= position;
        this.holder = holder;
        final TicketListData transactionReportBean = ticketDetails.get(position);
        int l_nLen = ticketDetails.size();
        holder.ticket_number.setText(transactionReportBean.getTicketNo());
        holder.ticket_description.setText(transactionReportBean.getIssue1());
        holder.ticket_status.setText(transactionReportBean.getStatus());
//        if(transactionReportBean.getStatus().equals("Closed")){
//            holder.ticket_status.setText(transactionReportBean.getStatus());
//            holder.ticket_status.setTextColor(R.color.red);
//        }
//        else if(transactionReportBean.getStatus().equals("Open")){
//            holder.ticket_status.setText(transactionReportBean.getStatus());
//            holder.ticket_status.setTextColor(R.color.green);
//        }
//        else{
//            holder.ticket_status.setText(transactionReportBean.getStatus());
//            holder.ticket_status.setTextColor(R.color.grey);
//        }

        holder.ticket_list_row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClick.onRowClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return getTransactionsList() != null ? ticketDetails.size() : 0;
    }

    public List <TicketListData> getTransactionsList() {
        return ticketDetails;
    }

    public void setTransactionsList(List <TicketListData> transactions) {
        this.ticketDetails = transactions;
        notifyDataSetChanged();
    }


    public class TicketListViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.ticket_number)
        TextView ticket_number;

        @BindView(R.id.ticket_description)
        TextView ticket_description;

        @BindView(R.id.ticket_status)
        TextView ticket_status;

        @BindView(R.id.ticket_list_row)
        LinearLayout ticket_list_row;

        public TicketListViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
          //  ticket_number.setPaintFlags(ticket_number.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);
        }
    }
    }

