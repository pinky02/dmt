package com.digitalindiapayment.ticket.ticket_list;

import com.digitalindiapayment.mvp.BaseView;
import com.digitalindiapayment.webservices.responsepojo.AllTicketListResponse;
import com.digitalindiapayment.webservices.responsepojo.CustomerList;
import com.digitalindiapayment.webservices.responsepojo.IssueListResponse;
import com.digitalindiapayment.webservices.responsepojo.TicketList;
import com.digitalindiapayment.webservices.responsepojo.TicketListData;
import com.digitalindiapayment.webservices.responsepojo.TicketListResponse;

import java.util.List;

/**
 * Created by Pinky on 06-06-2018.
 */

public class TicketListContact {
    public interface View extends BaseView {
     public void showIssueList(List<CustomerList> ticketListResponse);
    }

    public interface UserAction {

        void getTicketList(String apiKey, String customeId);

    }

}
