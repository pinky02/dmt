package com.digitalindiapayment.ticket.ticket_list;

import android.content.Context;

import com.digitalindiapayment.R;
import com.digitalindiapayment.ticket.TicketContract;
import com.digitalindiapayment.webservices.Api;
import com.digitalindiapayment.webservices.core.ApiFail;
import com.digitalindiapayment.webservices.core.ApiSuccess;
import com.digitalindiapayment.webservices.core.HttpErrorResponse;
import com.digitalindiapayment.webservices.responsepojo.AllTicketListResponse;
import com.digitalindiapayment.webservices.responsepojo.CustomerList;
import com.digitalindiapayment.webservices.responsepojo.IssueListResponse;
import com.digitalindiapayment.webservices.responsepojo.TicketListResponse;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Pinky on 06-06-2018.
 */

public class TicketListPresenter implements TicketListContact.UserAction {
    private final TicketListContact.View view;
    private final Context context;

    public TicketListPresenter(final TicketListContact.View view, Context context) {
        this.view = view;
        this.context = context;
    }

    @Override
    public void getTicketList(String apiKey, String customerId) {
        view.showLoading();
        Api.getTicket().getAllTicketList(apiKey, customerId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ApiSuccess <AllTicketListResponse>() {

                    @Override
                    public void call(AllTicketListResponse ticketListResponse) {
                        view.hideLoading();
                           view.showIssueList(ticketListResponse.getCustomerList());
                    }
                }, new ApiFail() {
                    @Override
                    public void httpStatus(HttpErrorResponse response) {
                        String message = response.getError();
                        view.fail(message);
                    }

                    @Override
                    public void noNetworkError() {
                        view.noInternet();
                    }

                    @Override
                    public void unknownError(Throwable e) {
                        view.fail(context.getString(R.string.error));
                    }
                });
    }
}