package com.digitalindiapayment.ticket.ticket_list;

import android.content.Intent;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.digitalindiapayment.BaseActivity;
import com.digitalindiapayment.R;
import com.digitalindiapayment.login.UserProfile;
import com.digitalindiapayment.ticket.OnItemClick;
import com.digitalindiapayment.ticket.TicketActivity;
import com.digitalindiapayment.ticket.querydetails.QueryDetailsActivity;
import com.digitalindiapayment.util.Util;
import com.digitalindiapayment.webservices.responsepojo.CustomerList;
import com.digitalindiapayment.webservices.responsepojo.Ticket;
import com.digitalindiapayment.webservices.responsepojo.TicketListData;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TicketListActivity extends BaseActivity implements TicketListContact.View,OnItemClick {
    @BindView(R.id.multilevelinfo_header)
    LinearLayout multilevelinfo_header;
    @BindView(R.id.tvNoData)
    TextView tvNoData;
    @BindView(R.id.ticketList)
    RecyclerView ticketList;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;
    String customerId;
    private UserProfile userProfile;
    private TicketListAdapter ticketListAdapter = null;
    private List<TicketListData> ticketlistDeatil = new ArrayList<TicketListData>();
    TicketListPresenter ticketListPresenter = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticket_list);
        ButterKnife.bind(this);
        ticketListPresenter = new TicketListPresenter(this,getApplicationContext());
        if (getIntent().hasExtra(Util.KEYS.DATA))
            userProfile = getIntent().getParcelableExtra(Util.KEYS.DATA);
        Intent i = getIntent();
        customerId = Util.PREFERENCES.getAgentUserId(this);


    }
    @Override
    protected void onResume() {
        super.onResume();
        showLoading();
        if (getIntent().hasExtra(Util.KEYS.DATA))
            userProfile = getIntent().getParcelableExtra(Util.KEYS.DATA);
        Intent i = getIntent();
        customerId = Util.PREFERENCES.getAgentUserId(this);
        multilevelinfo_header.setVisibility(View.GONE);
        ticketListPresenter.getTicketList("dipl689ssslodjemkews",customerId);
    }
    @Override
    public void noInternet() {

    }

    @Override
    public void exception(Throwable e) {
        btnSubmit.setEnabled(true);
        btnSubmit.setText(getString(R.string.submit));
        Util.showShortToast(getApplicationContext(), e.getMessage());
    }

    @Override
    public void fail(String message) {
        btnSubmit.setEnabled(true);
        btnSubmit.setText(getString(R.string.submit));
        Util.showShortToast(getApplicationContext(), message);
    }

    @Override
    public void success(String message) {

    }

    @Override
    public void showLoading() {
        showDialog();
    }

    @Override
    public void hideLoading() {
        disMissDialog();
    }

    @Override
    public void showIssueList(List<CustomerList> ticketListResponse) {

        ticketlistDeatil = ticketListResponse.get(0).getTicketList();
        if(ticketlistDeatil.size()>0){
            multilevelinfo_header.setVisibility(View.VISIBLE);
            ticketListAdapter = new TicketListAdapter(this,ticketlistDeatil, this);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            ticketList.setLayoutManager(mLayoutManager);
            ticketList.setItemAnimator(new DefaultItemAnimator());
            ticketList.setAdapter(ticketListAdapter);
            this.ticketlistDeatil =  ticketlistDeatil;
            ticketListAdapter.setTransactionsList(ticketlistDeatil);
        }
        else{
            multilevelinfo_header.setVisibility(View.GONE);
            ticketList.setVisibility(View.GONE);
            tvNoData.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onRowClick(int position) {
        Intent intent = new Intent(this, QueryDetailsActivity.class);
        intent.putExtra(Util.KEYS.TICKET_CODE, ticketlistDeatil.get(position).getTicketNo());
      //  intent.putExtra(UtilDmt.KEYS.PROBLEM, typeList.get(position).getName());
        startActivity(intent);
    }


    @OnClick(R.id.btnSubmit)
        public void askQuery() {
            Intent i = new Intent(this, TicketActivity.class);
            i.putExtra(Util.KEYS.DATA, userProfile);
            startActivity(i);
        }

}

