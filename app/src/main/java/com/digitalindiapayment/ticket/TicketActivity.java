package com.digitalindiapayment.ticket;

import android.content.Intent;
import android.net.Uri;

import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.digitalindiapayment.BaseActivity;
import com.digitalindiapayment.R;
import com.digitalindiapayment.adapter.MyArrayAdapter;
import com.digitalindiapayment.help_support.ImageAdapter;
import com.digitalindiapayment.help_support.OnImageClickListener;
import com.digitalindiapayment.login.UserProfile;
import com.digitalindiapayment.pojo.BankDetailsBean;
import com.digitalindiapayment.ticket.ticket_list.TicketListActivity;
import com.digitalindiapayment.util.Util;
import com.digitalindiapayment.webservices.requestpojo.CustomerData;
import com.digitalindiapayment.webservices.requestpojo.GenerateTicketRequest;
import com.digitalindiapayment.webservices.requestpojo.TicketData;
import com.digitalindiapayment.webservices.requestpojo.TicketRequest;
import com.digitalindiapayment.webservices.responsepojo.ChildIssueList;
import com.digitalindiapayment.webservices.responsepojo.IssueListResponse;
import com.digitalindiapayment.webservices.responsepojo.ParentIssueList;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;

public class TicketActivity extends BaseActivity implements OnImageClickListener, View.OnClickListener,TicketContract.View {


    private static final int RESULT_LOAD_IMAGE = 100;
    private static final int RESULT_CAMERA_IMAGE = 101;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.spinner)
    Spinner spinner;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;
    @BindView(R.id.edtDesc)
    EditText edtDesc;
    @BindView(R.id.imgCamera)
    ImageView imgCamera;
    @BindView(R.id.noInternetImg)
    ImageView noInternetImg;
    @BindView(R.id.noInternetMsg)
    TextView noInternetMsg;
    @BindView(R.id.refreshButton)
    ImageView refreshButton;
    @BindView(R.id.noInternetContainer)
    LinearLayout noInternetContainer;
    @BindView(R.id.progressbar)
    MaterialProgressBar progressbar;
    @BindView(R.id.message)
    TextView message;
    @BindView(R.id.progressdialogdip)
    RelativeLayout progressdialogdip;
    @BindView(R.id.relativeMsg)
    RelativeLayout relativeMsg;
    @BindView(R.id.scrollView)
    ScrollView scrollView;
    @BindView(R.id.autocomplet_parentissue)
    Spinner autocompletParentissue;
    @BindView(R.id.autocomplet_childissue)
    Spinner autocomplet_childissue;


    private List<ParentIssueList> parentIssueLists = new ArrayList<ParentIssueList>();
    private List<ChildIssueList> childIssueLists = new ArrayList<ChildIssueList>();
    private MyArrayAdapter<ParentIssueList> adapterParent;
    private MyArrayAdapter<ChildIssueList> adapterChild;
    private ParentIssueList parentBean;
    private ChildIssueList childBean;
    private int PERMISSION = 500;
    private ImageAdapter mAdapter;
    private String comment;
    String selectedPrority;
    private UserProfile userProfile;
    TicketPresenter ticketPresenter = null;
    Integer problemId;
    Integer problemTypeId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticket);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.ask_query);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ticketPresenter = new TicketPresenter(getApplicationContext(),TicketActivity.this);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        if (getIntent().hasExtra(Util.KEYS.DATA))
        userProfile = getIntent().getParcelableExtra(Util.KEYS.DATA);
        ArrayList<String> priorities = new ArrayList<String>();
        priorities.add("Low");
        priorities.add("Moderate");
        priorities.add("High");
        ArrayAdapter adapter1 = new ArrayAdapter(this, android.R.layout.simple_spinner_item, priorities);
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter1);

//        adapterParent = new MyArrayAdapter<ParentIssueList>(getApplicationContext(),
//                R.layout.textviewforspinner, parentIssueLists);
//        autocompletParentissue.setAdapter(adapterParent);
        adapterParent = new MyArrayAdapter<ParentIssueList>(getApplicationContext(),
                R.layout.textviewforsticketspinner, new ArrayList<ParentIssueList>());
        autocompletParentissue.setAdapter(adapterParent);
        adapterChild = new MyArrayAdapter<ChildIssueList>(getApplicationContext(),
                R.layout.textviewforsticketspinner, new ArrayList<ChildIssueList>());
        autocomplet_childissue.setAdapter(adapterChild);
//        String apiKey = "dipl689ssslodjemkews";
//        String productName = "AEPS";
//        ticketPresenter.getIssueList(apiKey,productName);
//        autocompletParentissue.setEnabled(false);
//        autocomplet_childissue.setEnabled(false);
    }
    @Override
    public void onResume(){
        super.onResume();
        btnSubmit.setEnabled(true);
        String apiKey = "dipl689ssslodjemkews";
        String productName = "AEPS";
        ticketPresenter.getIssueList(apiKey,productName);

    }
    @Override
    public void noInternet() {

    }
    @Override
    protected void onPause() {
        super.onPause();
        hideSoftKeyboard();
    }
    @Override
    public void exception(Throwable e) {
        Util.showShortToast(getApplicationContext(), e.getMessage());
    }
    @Override
    public void fail(String message) {
        Util.showShortToast(getApplicationContext(), message);

    }

    @Override
    public void success(String message) {

    }

    @Override
    public void showLoading() {
        showDialog();
    }

    @Override
    public void hideLoading() {
        disMissDialog();
    }

    @OnClick(R.id.btnSubmit)
    public void submitTicket(){
        if (autocompletParentissue.getSelectedItemPosition() < 1) {
            Util.showShortToast(getApplicationContext(), getString(R.string.pls_select_problem));
            return;
        }
//        if (autocomplet_childissue.getSelectedItemPosition() < 1) {
//            UtilDmt.showShortToast(getApplicationContext(), getString(R.string.pls_select_problem_type));
//            return;
//        }
        String customer_id =  Util.PREFERENCES.getAgentUserId(this);
        selectedPrority = spinner.getSelectedItem().toString();
        comment = edtDesc.getText().toString();
        TicketRequest ticketRequest =  new TicketRequest();
        List<TicketRequest>ticketRequestList = new ArrayList <TicketRequest>();

        CustomerData  customerData = new CustomerData();
        TicketData ticketData = new TicketData();
        customerData.setCustomerCode(customer_id);
        ticketData.setPriority(selectedPrority);
        ticketData.setProduct("AEPS");
        ticketData.setIssue1(problemId);
        ticketData.setIssue2(problemTypeId);
        ticketData.setComment(comment);
        ticketData.setCreatedBy(1);
        ticketRequest.setCustomerData(customerData);
        ticketRequest.setTicketData(ticketData);
        ticketRequest.setApiKey("dipl689ssslodjemkews");

      //  generateTicketRequest.setData(ticketRequestList);
        ticketRequestList.add(ticketRequest);
    //   ticketPresenter.createTicket(ticketRequestList);
        ticketPresenter.createTicket(ticketRequestList);
        btnSubmit.setClickable(false);



}
    @Override
    public void onClick(View v) {

    }

    @Override
    public void onImageClick(int position) {

    }

    @Override
    public void onDeleteImage(int position) {

    }

    @Override
    public void createTicketSuccess(String ticketNumber,String message) {
        if(message.equals("success")){
            Intent intent  = new Intent(TicketActivity.this, TicketListActivity.class);
            intent.putExtra("TicketNumber",ticketNumber);
            startActivity(intent);
        }
    }

    @Override
    public void showIssueList(IssueListResponse issueListResponse,List<ParentIssueList> parentIssueList) {

        adapterParent.clear();
        parentIssueList.add(0, new ParentIssueList(0, getString(R.string.issue1)));
        adapterParent.addAll(parentIssueList);
        adapterParent.notifyDataSetChanged();
        adapterChild.clear();
        childIssueLists.add(0,new ChildIssueList(0,getString(R.string.issue2)));
        adapterChild.addAll(childIssueLists);
        adapterChild.notifyDataSetChanged();

    }

    @OnItemSelected(R.id.autocomplet_parentissue)
    public void parentIssueClicked() {
        adapterChild = new MyArrayAdapter<ChildIssueList>(getApplicationContext(),
                R.layout.textviewforspinner, new ArrayList<ChildIssueList>());


        ParentIssueList parentIssueList = adapterParent.getItem(autocompletParentissue.getSelectedItemPosition());
        problemId= parentIssueList.getProblemId();
        childIssueLists= new ArrayList <>();
        childIssueLists.add(0,new ChildIssueList(0,getString(R.string.issue2)));
        if (parentIssueList.getIssueList() != null) {
            childIssueLists.addAll(parentIssueList.getIssueList());
        }
        adapterChild.addAll( childIssueLists);
        autocomplet_childissue.setAdapter(adapterChild);
        if(childIssueLists.size()>1) {
            autocomplet_childissue.setVisibility(View.VISIBLE);
        }
        else {
            autocomplet_childissue.setVisibility(View.GONE);
        }
        adapterChild.notifyDataSetChanged();
    }
    @OnItemSelected(R.id.autocomplet_childissue)
    public void childIssueClicked() {
        ChildIssueList childIssueList = adapterChild.getItem(autocomplet_childissue.getSelectedItemPosition());
        problemTypeId = childIssueList.getIssueId();
    }

    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }
}
