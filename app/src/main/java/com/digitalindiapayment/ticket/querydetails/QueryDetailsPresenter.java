package com.digitalindiapayment.ticket.querydetails;

import android.content.Context;

import com.digitalindiapayment.R;
import com.digitalindiapayment.util.Util;
import com.digitalindiapayment.webservices.Api;
import com.digitalindiapayment.webservices.core.ApiFail;
import com.digitalindiapayment.webservices.core.ApiSuccess;
import com.digitalindiapayment.webservices.core.HttpErrorResponse;
import com.digitalindiapayment.webservices.requestpojo.InsertCommentRequest;
import com.digitalindiapayment.webservices.requestpojo.TicketUpdateRequest;
import com.digitalindiapayment.webservices.responsepojo.AllTicketListChatResponse;
import com.digitalindiapayment.webservices.responsepojo.GetTicketResponse;
import com.digitalindiapayment.webservices.responsepojo.UpdateTicketRsponse;
import com.google.gson.Gson;


import retrofit2.Response;
import retrofit2.http.HTTP;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Nivrutti Pawar on 27-11-2017.
 */

public class QueryDetailsPresenter implements QueryDetailsContract.UserAction {

    Context context;
    QueryDetailsContract.View view;

    public QueryDetailsPresenter(Context context, QueryDetailsContract.View view) {
        this.context = context;
        this.view = view;
    }

    @Override
    public void getTicket(String ticketCode) {
        String token = Util.PREFERENCES.getTicketToken(context);
        String appInstance = Util.PREFERENCES.getAppInstanceCode(context);
        Api.getTicket().getTicket(appInstance, token, ticketCode)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ApiSuccess<GetTicketResponse>() {
                    @Override
                    public void call(GetTicketResponse getTicketResponse) {
                        if (getTicketResponse.getStatus().equalsIgnoreCase(Util.KEYS.SUCCESS))
                            view.ticketSuccess(getTicketResponse);
                        else
                            view.fail(context.getString(R.string.someting_wrong));
                    }
                }, new ApiFail() {
                    @Override
                    public void httpStatus(HttpErrorResponse response) {
                        String message = response.getError();
                        view.fail(message);
                    }

                    @Override
                    public void noNetworkError() {
                        view.noInternet();
                    }

                    @Override
                    public void unknownError(Throwable e) {
                        view.fail(context.getString(R.string.error));
                    }
                });
    }

    @Override
    public void updateTicket(InsertCommentRequest insertCommentRequest) { view.showLoading();
        Gson gson = new Gson();
        String json = gson.toJson(insertCommentRequest);
        Api.getTicket().insertComment(json)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ApiSuccess<String>() {
                    @Override
                    public void call(String response) {

                    }
                }, new ApiFail() {
                    @Override
                    public void httpStatus(HttpErrorResponse response) {
                        String message = response.getError();
                        view.fail(message);
                    }

                    @Override
                    public void noNetworkError() {
                       // view.noInternet();
                        view.hideLoading();
                        view.updateTicketSuccess();
                    }

                    @Override
                    public void unknownError(Throwable e) {
                        view.fail(context.getString(R.string.error));
                    }
                });
    }

    @Override
    public void getConversation(String apiKey,String ticketCode) {
        String token = Util.PREFERENCES.getTicketToken(context);
        String appInstance = Util.PREFERENCES.getAppInstanceCode(context);
        Api.getTicket().getAllTicketListChat(apiKey, ticketCode)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ApiSuccess<AllTicketListChatResponse>() {
                    @Override
                    public void call(AllTicketListChatResponse getConversationsResponse) {
                        if (getConversationsResponse.getTicketDetail().getCountOfComment()==0){
                            view.fail(context.getString(R.string.error));
                        }
                        else
                        view.conversationSucess(getConversationsResponse.getTicketDetail(),getConversationsResponse.getTicketDetail().getCommentList());
                    }
                }, new ApiFail() {
                    @Override
                    public void httpStatus(HttpErrorResponse response) {
                        view.fail(response.getError());
                    }

                    @Override
                    public void noNetworkError() {
                        view.noInternet();
                    }

                    @Override
                    public void unknownError(Throwable e) {
                        view.fail(context.getString(R.string.error));
                    }
                });

    }


}
