package com.digitalindiapayment.ticket.querydetails;

import com.digitalindiapayment.mvp.BaseView;
import com.digitalindiapayment.webservices.requestpojo.InsertCommentRequest;
import com.digitalindiapayment.webservices.requestpojo.TicketUpdateRequest;
import com.digitalindiapayment.webservices.responsepojo.CommentList;
import com.digitalindiapayment.webservices.responsepojo.ConversationRes;
import com.digitalindiapayment.webservices.responsepojo.GetTicketResponse;
import com.digitalindiapayment.webservices.responsepojo.TicketDetail;

import java.util.List;

/**
 * Created by Nivrutti Pawar on 27-11-2017.
 */

public class QueryDetailsContract {

    public interface View extends BaseView {
        public void ticketSuccess(GetTicketResponse ticketListResponse);

        public void updateTicketSuccess();

        void conversationSucess(TicketDetail ticketDetail, List <CommentList> conversationList);


    }

    public interface UserAction {
        public void getTicket(String ticketCode);

        public void updateTicket(InsertCommentRequest insertCommentRequest);

        void getConversation(String apikey, String ticketCode);

    }

}
