package com.digitalindiapayment.ticket.querydetails;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.digitalindiapayment.R;
import com.digitalindiapayment.help_support.CommentAdapter;
import com.digitalindiapayment.help_support.HorizontalImageAdapter;
import com.digitalindiapayment.util.Util;
import com.digitalindiapayment.webservices.requestpojo.CommentOwner;
import com.digitalindiapayment.webservices.requestpojo.InsertCommentRequest;
import com.digitalindiapayment.webservices.requestpojo.PublicReply;
import com.digitalindiapayment.webservices.requestpojo.TicketUpdateRequest;
import com.digitalindiapayment.webservices.responsepojo.CommentList;
import com.digitalindiapayment.webservices.responsepojo.ConversationRes;
import com.digitalindiapayment.webservices.responsepojo.GetTicketResponse;
import com.digitalindiapayment.webservices.responsepojo.TicketDetail;
import com.digitalindiapayment.webservices.responsepojo.TicketList;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;


public class QueryDetailsActivity extends AppCompatActivity implements QueryDetailsContract.View, View.OnClickListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recyclerView)
    RecyclerView imageRecyclerView;
    @BindView(R.id.scrollView)
    ScrollView scrollView;
    @BindView(R.id.commentRecyclerview)
    RecyclerView commentRecyclerview;
    @BindView(R.id.tvProblem)
    TextView tvProblem;
    @BindView(R.id.tvDate)
    TextView tvDate;
    @BindView(R.id.tvTxnId)
    TextView tvTxnId;
    @BindView(R.id.tvStatus)
    TextView tvStatus;
    @BindView(R.id.imdSend)
    ImageView imgSend;
    @BindView(R.id.edtReply)
    EditText edtReply;
    @BindView(R.id.tvTicketId)
    TextView tvTicketId;
    @BindView(R.id.noInternetImg)
    ImageView noInternetImg;
    @BindView(R.id.noInternetMsg)
    TextView noInternetMsg;
    @BindView(R.id.refreshButton)
    ImageView refreshButton;
    @BindView(R.id.noInternetContainer)
    LinearLayout noInternetContainer;
    @BindView(R.id.progressbar)
    MaterialProgressBar progressbar;
    @BindView(R.id.message)
    TextView message;
    @BindView(R.id.progressdialogdip)
    RelativeLayout progressdialogdip;
    @BindView(R.id.relativeMsg)
    RelativeLayout relativeMsg;
    @BindView(R.id.linearSendMsg)
    LinearLayout linearSendMsg;
    @BindView(R.id.tvNoDataMsg)
    TextView tvNoDataMsg;
    @BindView(R.id.tvComment)
    TextView tvComment;
    // @BindView(R.id.linearCommnet)
    //LinearLayout linearCommnet;

    private HorizontalImageAdapter mAdapter;
    private CommentAdapter mCommentAdapter;
    private String ticketCode;
    List<CommentList> conversationList;
    private Context mContext;
    private LinearLayoutManager commentLayoutManager;
    private QueryDetailsContract.UserAction presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_query_details);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.query_details);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mContext = this;
        presenter = new QueryDetailsPresenter(getApplicationContext(), QueryDetailsActivity.this);
        //image reycclerview
        imageRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        mAdapter = new HorizontalImageAdapter();
        imageRecyclerView.setAdapter(mAdapter);
        //coomment recyclerview
        commentLayoutManager = new LinearLayoutManager(this);
        commentLayoutManager.setStackFromEnd(true);
        commentRecyclerview.setLayoutManager(commentLayoutManager);
        commentRecyclerview.setNestedScrollingEnabled(false);

        imgSend.setOnClickListener(this);
        if (getIntent().hasExtra(Util.KEYS.TICKET_CODE)) {
            ticketCode = getIntent().getStringExtra(Util.KEYS.TICKET_CODE);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        showLoading();
        presenter.getConversation("dipl689ssslodjemkews",ticketCode);
    }

    @Override
    public void noInternet() {
        hideLoading();
        tvNoDataMsg.setVisibility(View.GONE);
        linearSendMsg.setVisibility(View.GONE);
        scrollView.setVisibility(View.GONE);
        relativeMsg.setVisibility(View.VISIBLE);
        noInternetContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void exception(Throwable e) {
        hideLoading();
    }

    @Override
    public void fail(String message) {
        hideLoading();
        commentRecyclerview.setVisibility(View.GONE);
        tvNoDataMsg.setVisibility(View.VISIBLE);
        linearSendMsg.setVisibility(View.VISIBLE);
        imgSend.setEnabled(true);
    }

    @Override
    public void success(String message) {

    }

    @Override
    public void showLoading() {
        linearSendMsg.setVisibility(View.GONE);
        relativeMsg.setVisibility(View.VISIBLE);
        scrollView.setVisibility(View.GONE);
        progressdialogdip.setVisibility(View.VISIBLE);
        noInternetContainer.setVisibility(View.GONE);
    }

    @Override
    public void hideLoading() {
        progressdialogdip.setVisibility(View.GONE);

    }

    @OnClick(R.id.refreshButton)
    public void onViewClicked() {
        showLoading();
        presenter.getTicket(ticketCode);
        presenter.getConversation("dipl689ssslodjemkews",ticketCode);
    }

    @Override
    public void ticketSuccess(GetTicketResponse ticketListResponse) {
        imgSend.setEnabled(true);
        hideLoading();
        if (ticketListResponse.getData().size() > 0) {
            scrollView.setVisibility(View.VISIBLE);
            linearSendMsg.setVisibility(View.VISIBLE);
            tvNoDataMsg.setVisibility(View.GONE);
            tvComment.setVisibility(View.VISIBLE);
            TicketList ticketList = ticketListResponse.getData().get(0);
            tvProblem.setText(ticketList.getSubject());
            tvDate.setText(Util.dateFormat(ticketList.getCreatedAt()));
            tvStatus.setText(ticketList.getCurrentStatus().getName());
            tvTicketId.setText(ticketList.getCode());
        } else {

            tvComment.setVisibility(View.VISIBLE);
            tvNoDataMsg.setVisibility(View.VISIBLE);
            scrollView.setVisibility(View.GONE);
            linearSendMsg.setVisibility(View.VISIBLE);
        }

    }


    @Override
    public void updateTicketSuccess() {
        hideSoftKeyboard();
        edtReply.setText("");
        presenter.getConversation("dipl689ssslodjemkews",ticketCode);
    }


    @Override
    public void conversationSucess(TicketDetail ticketDetail,List<CommentList> conversationList) {
        if(ticketDetail.getCountOfComment()==0){
             commentRecyclerview.setVisibility(View.GONE);
            linearSendMsg.setVisibility(View.VISIBLE);
        }
        else{
            hideLoading();
            imgSend.setEnabled(true);
            if (conversationList != null && conversationList.size() > 0) {
                scrollView.setVisibility(View.VISIBLE);
                linearSendMsg.setVisibility(View.VISIBLE);
                tvNoDataMsg.setVisibility(View.GONE);
                tvComment.setVisibility(View.VISIBLE);
                commentRecyclerview.setVisibility(View.VISIBLE);
                tvProblem.setText(ticketDetail.getIssue1());
                tvDate.setText(ticketDetail.getCreatedDate());
                tvStatus.setText(ticketDetail.getCurrentStatus());
                tvTicketId.setText(ticketCode);
                this.conversationList = conversationList;
                mCommentAdapter = new CommentAdapter(getApplicationContext(), conversationList);
                commentRecyclerview.setAdapter(mCommentAdapter);
                commentRecyclerview.scrollToPosition(mCommentAdapter.getItemCount() - 1);
                }
        }
    }

    @Override
    public void onClick(View view) {
        if (!edtReply.getText().toString().isEmpty()) {
            String name = Util.PREFERENCES.getAgentName(this);
            InsertCommentRequest updateRequest = new InsertCommentRequest();
            updateRequest.setApiKey("dipl689ssslodjemkews");
            updateRequest.setComment(edtReply.getText().toString());
            updateRequest.setTicketNo(ticketCode);
            CommentOwner commentOwner = new CommentOwner();
            commentOwner.setOwnerType(3);
            commentOwner.setOwnerName(name);
            commentOwner.setOwnerPhone(Util.PREFERENCES.getMObile(this));
            updateRequest.setCommentOwner(commentOwner);
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
//                updateRequest.setComment(Html.toHtml(edtReply.getText(), Html.TO_HTML_PARAGRAPH_LINES_INDIVIDUAL));}
//            else {
//                updateRequest.setComment(Html.toHtml(edtReply.getText()));
//            }
            imgSend.setEnabled(true);
            presenter.updateTicket(updateRequest);
        } else {
            //reply cannot be empty
            Util.showShortToast(getApplicationContext(), mContext.getString(R.string.comment_err));
            edtReply.requestFocus();
        }
    }

    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }
}
