package com.digitalindiapayment.ticket;

import com.digitalindiapayment.mvp.BaseView;
import com.digitalindiapayment.webservices.requestpojo.GenerateTicketRequest;
import com.digitalindiapayment.webservices.requestpojo.TicketRequest;
import com.digitalindiapayment.webservices.responsepojo.IssueListResponse;
import com.digitalindiapayment.webservices.responsepojo.ParentIssueList;

import java.util.List;

/**
 * Created by Pinky on 31-05-2018.
 */

public class TicketContract {
    public interface View extends BaseView {

        void createTicketSuccess(String ticketNumber,String message);
        void showIssueList(IssueListResponse issueListResponse,List<ParentIssueList> parentIssueLists);

      //  void createTicketsFail();

     //   void uploadImagesSucess();
    }

    public interface UserAction {

        void getIssueList(String apiKey, String productName);
        void createTicket(List <TicketRequest> ticketRequests);

      // void uploadImages(String commentId, List<Uri> imageArray);
    }

}
