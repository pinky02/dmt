package com.digitalindiapayment.ticket;

/**
 * Created by Nivrutti Pawar on 18-11-2017.
 */

public interface OnImageClickListener {
    void onImageClick(int position);

    void onDeleteImage(int position);
}
