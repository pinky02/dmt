package com.digitalindiapayment.transaction;

import com.digitalindiapayment.mvp.BaseView;
import com.digitalindiapayment.pojo.BankDetailsBean;
import com.digitalindiapayment.webservices.requestpojo.GenerateDepositWithdrawTransIdRequest;
import com.digitalindiapayment.webservices.responsepojo.GenerateDepositWithdrawTransIdResponse;
import com.digitalindiapayment.webservices.responsepojo.GenerateTransactionIdResponse;

import java.util.List;

/**
 * Created by Amol on 12-03-2017.
 */

public class TransactionContract {
    interface View extends BaseView {
        void showBanks(List<BankDetailsBean> bankDetailsBeen);
        void processTransaction(GenerateDepositWithdrawTransIdResponse transactionIdResponse);
        void transactionFail(String message);
    }

    interface UserActions{
        void submit(String servicesPath, GenerateDepositWithdrawTransIdRequest request);
        void loadBank();
    }
}
