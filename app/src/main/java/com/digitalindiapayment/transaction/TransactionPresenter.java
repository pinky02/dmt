package com.digitalindiapayment.transaction;

import android.content.Context;

import com.digitalindiapayment.R;
import com.digitalindiapayment.util.Util;
import com.digitalindiapayment.webservices.requestpojo.GenerateDepositWithdrawTransIdRequest;
import com.digitalindiapayment.webservices.responsepojo.GenerateDepositWithdrawTransIdResponse;
import com.digitalindiapayment.webservices.responsepojo.GetBanksResponse;
import com.digitalindiapayment.webservices.Api;
import com.digitalindiapayment.webservices.core.ApiFail;
import com.digitalindiapayment.webservices.core.ApiSuccess;
import com.digitalindiapayment.webservices.core.HttpErrorResponse;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Amol on 12-03-2017.
 */

public class TransactionPresenter implements TransactionContract.UserActions {


    private final TransactionContract.View view;
    private final Context context;

    public TransactionPresenter(final TransactionContract.View view, Context context) {
        this.view = view;
        this.context = context;
    }

    @Override
    public void submit(String servicesPath, GenerateDepositWithdrawTransIdRequest request) {
            Api.banksDetails().getTransactionId(Util.PREFERENCES.getToken(context),servicesPath ,request)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new ApiSuccess<GenerateDepositWithdrawTransIdResponse>() {
                        @Override
                        public void call(GenerateDepositWithdrawTransIdResponse transactionResponse) {
                            view.processTransaction(transactionResponse);
                        }
                    }, new ApiFail() {
                        @Override
                        public void httpStatus(HttpErrorResponse response) {
                            String message = response.getError();
                            view.transactionFail(message);
                            //view.fail(message);

                        }
                        @Override
                        public void noNetworkError() {
                            view.transactionFail(context.getString(R.string.lost_connectivity));
                            //view.noInternet();
                        }

                        @Override
                        public void unknownError(Throwable e) {
                            view.transactionFail(context.getString(R.string.error));
                            // view.exception(e);
                        }

                    });

    }

    @Override
    public void loadBank() {


        Api.banksDetails().getBank(Util.PREFERENCES.getToken(context)).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new ApiSuccess<GetBanksResponse>() {
            @Override
            public void call(GetBanksResponse getBanks) {
                view.showBanks(getBanks.getBankDetailsBeanList());
            }
        }, new ApiFail() {
            @Override
            public void httpStatus(HttpErrorResponse response) {
                view.fail(response.getError());

            }

            @Override
            public void noNetworkError() {
                view.noInternet();


            }

            @Override
            public void unknownError(Throwable e) {
                view.fail(e.getMessage());

            }


        });
    }

}