package com.digitalindiapayment.transaction;

import android.view.View;
import android.widget.EditText;

/**
 * Created by admin on 06-10-2017.
 */

public class BalanceEnquiryScreen  extends TransactionScreen {
    @Override
    public void success(String message) {

    }

    @Override
    String getServicesType() {
        return "Balance Enquiry";
    }

    @Override
    String getServicesApiPath() {
        return "balanceinquiry";
    }

    @Override
    void setVisibilityRemark(EditText editText) {
        editText.setVisibility(View.GONE);
    }

    @Override
    void setVisibilityAmount(EditText amount) {
        amount.setVisibility(View.GONE);
    }
}
