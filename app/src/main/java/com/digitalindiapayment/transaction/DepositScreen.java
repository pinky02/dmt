package com.digitalindiapayment.transaction;

import android.view.View;
import android.widget.EditText;

/**
 * Created by Expert on 16-03-2017.
 */

public class DepositScreen extends TransactionScreen {
    @Override
    String getServicesType() {
        return "Deposit";
    }

    @Override
    String getServicesApiPath() {
        return "deposit";
    }

    @Override
    void setVisibilityRemark(final EditText editText) {
        editText.setVisibility(View.GONE);
    }

    @Override
    void setVisibilityAmount(EditText amount) {

    }


    @Override
    public void success(String message) {

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }
}
