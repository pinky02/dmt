package com.digitalindiapayment.transaction;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.digitalindiapayment.BaseActivity;
import com.digitalindiapayment.R;
import com.digitalindiapayment.adapter.MyArrayAdapter;
import com.digitalindiapayment.adhar.AdharContract;
import com.digitalindiapayment.adhar.AdharPresenter;
import com.digitalindiapayment.pojo.BankDetailsBean;
import com.digitalindiapayment.progressbutton.ProgressButtonComponent;
import com.digitalindiapayment.termsandcondition.TermsAndCondition;
import com.digitalindiapayment.transaction.confirmation.TransactionConfirmationScreen;
import com.digitalindiapayment.util.Util;
import com.digitalindiapayment.webservices.requestpojo.BankTransactionRequest;
import com.digitalindiapayment.webservices.requestpojo.GenerateDepositWithdrawTransIdRequest;
import com.digitalindiapayment.webservices.requestpojo.RDFingurePrintRequest;
import com.digitalindiapayment.webservices.responsepojo.GenerateDepositWithdrawTransIdResponse;
import com.morpho.sample.FingerprintData;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Amol on 12-03-2017.
 */
abstract public class TransactionScreen extends BaseActivity implements TransactionContract.View, TextWatcher, AdharContract.View {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    //    @BindView(R.id.bank_spinner)
//    Spinner bankspinner;
    @BindView(R.id.aadhaar_no)
    EditText aadhaarNo;
    @BindView(R.id.remark)
    EditText remark;
    @BindView(R.id.amount)
    EditText amount;
    @BindView(R.id.autocomplet_bank_list)
    AutoCompleteTextView autocompletBankList;
    @BindView(R.id.submit)
    ProgressButtonComponent submit;
    @BindView(R.id.aadhaarBtn)
    Button aadhaarBtn;
    @BindView(R.id.custName)
    EditText edtCustName;
    @BindView(R.id.custNamePhone)
    EditText edtCustPhoneNo;

    private List<BankDetailsBean> bankList = new ArrayList<BankDetailsBean>();
    private String transactionId;
    private MyArrayAdapter<BankDetailsBean> adapter;
    private TransactionContract.UserActions presenter;
    private Editable adharwithouthiphen;
    private BankDetailsBean bean;
    private AdharContract.UserAction aadhaarPresenter;

    abstract String getServicesType();

    abstract String getServicesApiPath();

    abstract void setVisibilityRemark(final EditText editText);

    abstract void setVisibilityAmount(final EditText amount);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction);
        ButterKnife.bind(this);
        //EventBus.getDefault().register(this);

        setSupportActionBar(toolbar);

//        aadhaarNo.addTextChangedListener(this);

        getSupportActionBar().setTitle(getServicesType());
        toolbar.setTitle(getServicesType());

        //Make the button enable and Onclick listner on that
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();

            }
        });

        presenter = new TransactionPresenter(this, getApplicationContext());


//        bankList.add(0, new BankDetailsBean(0, "Select Bank"));
        adapter = new MyArrayAdapter<BankDetailsBean>(getApplicationContext(),
                R.layout.textviewforspinner, bankList);

        autocompletBankList.setAdapter(adapter);


        setVisibilityRemark(remark);
        setVisibilityAmount(amount);

        aadhaarPresenter = new AdharPresenter(this, getApplicationContext());


        aadhaarBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aadhaarPresenter.showScanIntent();
            }
        });

        // On Btn click of scan view aadhaarPresenter.showScanIntent


    }

    @Override
    protected void onResume() {
        super.onResume();
        submit.setEnabled(true);
        showLoading();

        if (bankList == null || bankList.size() == 0) {
            presenter.loadBank();
            autocompletBankList.setEnabled(false);
        } else {
            hideLoading();
            autocompletBankList.setEnabled(true);
        }


        autocompletBankList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                bean = adapter.getItem(position);

            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        aadhaarPresenter.processAdharDetails(requestCode, resultCode, data);
    }

    @Override
    public void showLoading() {
        showDialog();
    }

    @Override
    public void hideLoading() {
        disMissDialog();
    }

    @Override
    public void noInternet() {
        Util.showShortToast(getApplicationContext(), "No internet");
        hideLoading();
        finish();


    }

    @Override
    public void exception(Throwable e) {
        Util.showShortToast(getApplicationContext(), e.getMessage());
        hideLoading();
        finish();
    }

    @Override
    public void fail(String message) {
        Util.showShortToast(getApplicationContext(), message);
        hideLoading();
        finish();
    }

    @OnClick(R.id.submit)
    public void submitData() {

        if (!Util.isNetworkAvailable(getApplicationContext())) {
            Util.showShortToast(getApplicationContext(), "No Internet");
            return;
        }

        if (aadhaarNo.getText().length() != 12) {
            Toast.makeText(getApplicationContext(), getString(R.string.invlid_aadhar), Toast.LENGTH_SHORT).show();
            return;
        }

        if (aadhaarNo.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(getApplicationContext(), getString(R.string.pls_enter_aadhar), Toast.LENGTH_SHORT).show();
            return;
        }
        if (autocompletBankList.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(getApplicationContext(), getString(R.string.pls_select_bank), Toast.LENGTH_SHORT).show();
            return;
        }
        if (bean == null || !bean.getBankName().equals(autocompletBankList.getText().toString())) {
            Toast.makeText(getApplicationContext(), getString(R.string.pls_check_bank_name), Toast.LENGTH_SHORT).show();
            return;
        }

        if (amount.isShown()) {
            if (amount.getText().length() == 0) {
                Toast.makeText(getApplicationContext(), getString(R.string.pls_enter_amount), Toast.LENGTH_SHORT).show();
                return;
            }

            if (Integer.parseInt(amount.getText().toString()) > 10000) {
                Toast.makeText(getApplicationContext(), getString(R.string.maxlimit_amount), Toast.LENGTH_SHORT).show();
                return;
            }


            try {

                int amountInt = Integer.parseInt(amount.getText().toString());
                if (amountInt < 1) {
                    Toast.makeText(getApplicationContext(), getString(R.string.invlid_amount), Toast.LENGTH_SHORT).show();
                    return;
                }
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), getString(R.string.invlid_amount), Toast.LENGTH_SHORT).show();
                return;
            }
        }

        if (edtCustPhoneNo.getText().toString().equalsIgnoreCase("") ) {
            Toast.makeText(this, R.string.cust_mob_no, Toast.LENGTH_SHORT).show();
            edtCustPhoneNo.requestFocus();
            return;
        }
        if (edtCustPhoneNo.getText().toString().length()<10){
            Toast.makeText(this, R.string.mob_no_validation, Toast.LENGTH_SHORT).show();
            edtCustPhoneNo.requestFocus();
            return;
        }

        //submit.setEnabled(false);

        setEnableUI(false);
        submit.setInProgress(true);


        GenerateDepositWithdrawTransIdRequest transIdRequest = new GenerateDepositWithdrawTransIdRequest(aadhaarNo.getText().toString(), bean.getCode() + "", amount.getText().toString(), edtCustName.getText().toString(), edtCustPhoneNo.getText().toString());

        presenter.submit(getServicesApiPath(), transIdRequest);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // EventBus.getDefault().unregister(this);
    }

    //RDService
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(RDFingurePrintRequest request) {

        //on location 4 and 8 '-' is there to remove that
//        final String adharnumber = String.valueOf(adharwithouthiphen.delete(4, 5).delete(8, 9));

//        BankDetailsBean bean = (BankDetailsBean) autocompletBankList.getAdapter();

        //BankTransactionRequest request = new BankTransactionRequest();
//        request.setAdhaarNo(adharnumber);
        request.setAadharNo(aadhaarNo.getText().toString());

        request.setBankCode(bean.getCode() + "");
        request.setBankName(bean.getBankName());
        request.setAmount(amount.getText().toString());
        request.setRemark(remark.getText().toString());
        request.setTransactionId(transactionId);
        Intent intent = new Intent(this, TransactionConfirmationScreen.class);
        intent.putExtra(Util.KEYS.DATA, request);
        intent.putExtra(Util.KEYS.PATH, getServicesApiPath());
        intent.putExtra(Util.KEYS.SERVICES, getServicesType());
        startActivity(intent);
        finish();
    }

    // Without RD
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(FingerprintData event) {

        //on location 4 and 8 '-' is there to remove that
//        final String adharnumber = String.valueOf(adharwithouthiphen.delete(4, 5).delete(8, 9));

//        BankDetailsBean bean = (BankDetailsBean) autocompletBankList.getAdapter();

        BankTransactionRequest request = new BankTransactionRequest();
//        request.setAdhaarNo(adharnumber);
        request.setAadhaarNo(aadhaarNo.getText().toString());

        request.setBankCode(bean.getCode() + "");
        request.setBankName(bean.getBankName());
        request.setFingerPrint(event.fp);
        request.setAmount(amount.getText().toString());
        request.setRemark(remark.getText().toString());
        request.setTransactionId(transactionId);
        Intent intent = new Intent(this, TransactionConfirmationScreen.class);
        intent.putExtra(Util.KEYS.DATA, request);
        intent.putExtra(Util.KEYS.PATH, getServicesApiPath());
        intent.putExtra(Util.KEYS.SERVICES, getServicesType());
        startActivity(intent);
        finish();
    }


    @Override
    public void showBanks(List<BankDetailsBean> bankDetailsBeen) {
        adapter.clear();
        bankList = new ArrayList<BankDetailsBean>();
        bankList.addAll(bankDetailsBeen);
        adapter.addAll(bankDetailsBeen);
        adapter.notifyDataSetChanged();
        hideLoading();
        autocompletBankList.setEnabled(true);


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    /**
     * after 4 character add hyphen
     * <p>
     * limit 12
     */

    @Override
    public void afterTextChanged(Editable s) {

        // Remove spacing char
        if (s.length() > 0 && (s.length() % 5) == 0) {
            final char c = s.charAt(s.length() - 1);
            if (Util.KEYS.hiphen == c) {
                s.delete(s.length() - 1, s.length());
            }
        }

        // Insert char where needed.
        if (s.length() > 0 && (s.length() % 5) == 0) {
            final char c = s.charAt(s.length() - 1);
            s.insert(s.length() - 1, String.valueOf(Util.KEYS.hiphen));
        }

        adharwithouthiphen = s;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void setAdharDetails(String uId) {
        aadhaarNo.setText(uId);
    }


    void setEnableUI(Boolean isEnable) {
        aadhaarNo.setEnabled(isEnable);
        aadhaarBtn.setEnabled(isEnable);
        autocompletBankList.setEnabled(isEnable);
        submit.setEnabled(isEnable);
        remark.setEnabled(isEnable);
        amount.setEnabled(isEnable);
    }

    @Override
    public void processTransaction(GenerateDepositWithdrawTransIdResponse transactionIdResponse) {
        setEnableUI(false);
        submit.setInProgress(false);

        if (transactionIdResponse.getTransactionId() != null) {
            transactionId = transactionIdResponse.getTransactionId();
            Intent intent = new Intent(getApplicationContext(), TermsAndCondition.class);
            startActivity(intent);
        } else {

            setEnableUI(true);
        }


    }

    @Override
    public void transactionFail(String message) {
        setEnableUI(true);
        submit.setInProgress(false);
        Util.showLongToast(getApplicationContext(), message);


    }
}
