package com.digitalindiapayment.transaction.confirmation;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.digitalindiapayment.BaseActivity;
import com.digitalindiapayment.R;
import com.digitalindiapayment.receipt.BankTransactionReceiptScreen;
import com.digitalindiapayment.receipt.BankTransactionWebView;
import com.digitalindiapayment.util.Util;
import com.digitalindiapayment.webservices.requestpojo.AadhaarTransaction;
import com.digitalindiapayment.webservices.requestpojo.BankTransactionRequest;
import com.digitalindiapayment.webservices.requestpojo.GenerateDepositWithdrawTransIdRequest;
import com.digitalindiapayment.webservices.requestpojo.RDFingurePrintRequest;
import com.digitalindiapayment.webservices.responsepojo.BankTransactionResponse;
import com.digitalindiapayment.webservices.responsepojo.GenerateDepositWithdrawTransIdResponse;
import com.digitalindiapayment.webservices.responsepojo.GenerateTransactionIdResponse;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.digitalindiapayment.R.drawable.request;

/**
 * Created by Amol on 12-03-2017.
 */
public class TransactionConfirmationScreen extends BaseActivity implements TransactionConfirmationContract.View {


    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.date)
    TextView date;

    @BindView(R.id.aadhaarNumber)
    TextView aadhaarNumber;

    @BindView(R.id.bankName)
    TextView bankName;

    @BindView(R.id.services)
    TextView services;

    @BindView(R.id.amount)
    TextView amount;
    @BindView(R.id.imageView5)
    ImageView imageView5;

    @BindView(R.id.confirm)
    Button confirm;


    private String path = "";
    private String servicesType = "";
    private AadhaarTransaction aadhaarTransaction;
    private RDFingurePrintRequest requestWithRD;
    private BankTransactionRequest requestWithoutRD;
    private TransactionConfirmationContract.UserActions presenter;
    private String remark;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_confirm);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        //Make the button enable and Onclick listner on that
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        presenter = new TransactionConfirmationPresenter(this, this);

        aadhaarTransaction = getIntent().getParcelableExtra(Util.KEYS.DATA);


        path = getIntent().getStringExtra(Util.KEYS.PATH);
        servicesType = getIntent().getStringExtra(Util.KEYS.SERVICES);
        getSupportActionBar().setTitle(servicesType);
        services.setText(servicesType);
        aadhaarNumber.setText(aadhaarTransaction.getAadhaar());
        bankName.setText(aadhaarTransaction.getBankName());

        amount.setText(aadhaarTransaction.getAmount());


        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String dataString = df.format(new Date());
        date.setText(dataString);



        //GenerateDepositWithdrawTransIdRequest transIdRequest = new GenerateDepositWithdrawTransIdRequest(aadhaarTransaction.getAadhaar(),aadhaarTransaction.getBankCode(),aadhaarTransaction.getAmount(),,custPhoneNo);

        //presenter.getTransactionId(servicesType,transIdRequest);

//        confirm.setEnabled(false);
    }


    @Override
    public void noInternet() {
        disMissDialog();
        Util.showShortToast(getApplicationContext(), "No internet");
        finish();
    }

    @Override
    public void exception(Throwable e) {
        disMissDialog();
        Util.showShortToast(getApplicationContext(), e.getMessage());
        finish();

    }

    @Override
    public void fail(String message) {
        disMissDialog();
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
        finish();
    }

    @Override
    public void success(String message) {

    }

    @Override
    public void showLoading() {
        showDialog();
    }

    @Override
    public void hideLoading() {
        disMissDialog();
    }


    @OnClick(R.id.confirm)
    public void onClick() {
        Log.v("Button Press", "");
        confirm.setEnabled(false);
        showDialog();
        if(aadhaarTransaction instanceof  RDFingurePrintRequest) {
            presenter.submit(path, (RDFingurePrintRequest)aadhaarTransaction);
        }else{
            presenter.submit(path, (BankTransactionRequest)aadhaarTransaction);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        confirm = (Button) findViewById(R.id.confirm);
        confirm.setEnabled(true);
    }

    @Override
    public void showTransacrion(BankTransactionResponse balanceResponse) {

        disMissDialog();
        Intent intent = new Intent(this, BankTransactionWebView.class);
        intent.putExtra(Util.KEYS.DATA, balanceResponse);
        startActivity(intent);
        finish();
    }

    @Override
    public void activateConfirmButton(GenerateDepositWithdrawTransIdResponse transactionIdResponse) {
        confirm.setEnabled(true);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
