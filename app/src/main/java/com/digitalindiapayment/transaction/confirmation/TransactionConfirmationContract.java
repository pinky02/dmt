package com.digitalindiapayment.transaction.confirmation;

import android.content.Context;

import com.digitalindiapayment.mvp.BaseView;
import com.digitalindiapayment.webservices.requestpojo.BankTransactionRequest;
import com.digitalindiapayment.webservices.requestpojo.GenerateDepositWithdrawTransIdRequest;
import com.digitalindiapayment.webservices.requestpojo.RDFingurePrintRequest;
import com.digitalindiapayment.webservices.responsepojo.BankTransactionResponse;
import com.digitalindiapayment.webservices.responsepojo.GenerateDepositWithdrawTransIdResponse;
import com.digitalindiapayment.webservices.responsepojo.GenerateTransactionIdResponse;

import retrofit2.Response;

/**
 * Created by Amol on 12-03-2017.
 */

public class TransactionConfirmationContract {
    interface View extends BaseView {
        void showTransacrion(BankTransactionResponse balanceResponse);
        void activateConfirmButton(GenerateDepositWithdrawTransIdResponse transactionIdResponse);
    }

    interface UserActions {
        void submit(String path, RDFingurePrintRequest request);
        void submit(String path, BankTransactionRequest request);
        void getTransactionId(String servicesType,GenerateDepositWithdrawTransIdRequest request);

    }
}
