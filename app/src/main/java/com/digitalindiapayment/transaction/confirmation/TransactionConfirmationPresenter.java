package com.digitalindiapayment.transaction.confirmation;

import android.content.Context;
import android.util.Log;

import com.digitalindiapayment.R;
import com.digitalindiapayment.util.Util;
import com.digitalindiapayment.webservices.Api;
import com.digitalindiapayment.webservices.core.ApiFail;
import com.digitalindiapayment.webservices.core.ApiSuccess;
import com.digitalindiapayment.webservices.core.HttpErrorResponse;
import com.digitalindiapayment.webservices.requestpojo.BankTransactionRequest;
import com.digitalindiapayment.webservices.requestpojo.ConfirmTransactionRequest;
import com.digitalindiapayment.webservices.requestpojo.GenerateDepositWithdrawTransIdRequest;
import com.digitalindiapayment.webservices.requestpojo.RDFingurePrintRequest;
import com.digitalindiapayment.webservices.responsepojo.BankTransactionResponse;
import com.digitalindiapayment.webservices.responsepojo.GenerateDepositWithdrawTransIdResponse;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by Amol on 12-03-2017.
 */

public class TransactionConfirmationPresenter implements TransactionConfirmationContract.UserActions {


    private static final String TAG = "TrnsacnCnfmPrestr";
    private final TransactionConfirmationContract.View view;
    private final Context context;

    public TransactionConfirmationPresenter(final TransactionConfirmationContract.View view, Context context) {
        this.view = view;
        this.context = context;
    }


    @Override
    public void submit(String path, RDFingurePrintRequest request) {
        Log.v("Button Press", "api call");

        Api.banksDetails().transaction(Util.PREFERENCES.getToken(context), path, request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ApiSuccess<BankTransactionResponse>() {
                    @Override
                    public void call(BankTransactionResponse balanceResponse) {

                        getTransactionStatus(balanceResponse.getTransactionId());


                    }
                }, new ApiFail() {
                    @Override
                    public void httpStatus(HttpErrorResponse response) {
                        String message = response.getError();
                        switch (response.getHttpStatusCode()) {
                            case HttpErrorResponse.HTTP_STATUS.STATUS_403:
                                message = context.getResources().getString(R.string.transaction_limit_reached);
                            case HttpErrorResponse.HTTP_STATUS.STATUS_400:
                                message = response.getError();
                        }
                        view.fail(message);

                    }


                    @Override
                    public void noNetworkError() {

                        view.noInternet();
                    }

                    @Override
                    public void unknownError(Throwable e) {

                        view.exception(e);
                    }


                });
    }

    @Override
    public void submit(String path, BankTransactionRequest request) {
        Api.banksDetails().transaction(Util.PREFERENCES.getToken(context), path, request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ApiSuccess<BankTransactionResponse>() {
                    @Override
                    public void call(BankTransactionResponse balanceResponse) {

                        getTransactionStatus(balanceResponse.getTransactionId());


                    }
                }, new ApiFail() {
                    @Override
                    public void httpStatus(HttpErrorResponse response) {
                        String message = response.getError();
                        switch (response.getHttpStatusCode()) {
                            case HttpErrorResponse.HTTP_STATUS.STATUS_403:
                                message = context.getResources().getString(R.string.transaction_limit_reached);
                            case HttpErrorResponse.HTTP_STATUS.STATUS_400:
                                message = response.getError();
                        }
                        view.fail(message);

                    }


                    @Override
                    public void noNetworkError() {

                        view.noInternet();
                    }

                    @Override
                    public void unknownError(Throwable e) {

                        view.exception(e);
                    }


                });
    }

    // Server Polling Implemented
    public void getTransactionStatus(String transactionId) {

        Log.d(TAG, "Transaction " + transactionId);
        ConfirmTransactionRequest transaction = new ConfirmTransactionRequest();
        transaction.setTransactionId(transactionId);
        //rx.Observable.
        Api.banksDetails().getTransactionStatus(Util.PREFERENCES.getToken(context), transaction)
                .repeatWhen(new Func1<Observable<? extends Void>, Observable<?>>() {
                    @Override
                    public rx.Observable<?> call(rx.Observable<? extends Void> observable) {
                        Log.d(TAG, "repeatWhen");
                        return observable.delay(2, TimeUnit.SECONDS);
                    }
                })
                .takeUntil(new Func1<BankTransactionResponse, Boolean>() {
                    @Override
                    public Boolean call(BankTransactionResponse transactionStatusResponse) {
                        Log.d(TAG, "takeUntil " + transactionStatusResponse.isCompleted());
                        return transactionStatusResponse.isCompleted();
                    }
                })
                .filter(new Func1<BankTransactionResponse, Boolean>() {
                    @Override
                    public Boolean call(BankTransactionResponse transactionStatusResponse) {
                        Log.d(TAG,"filter");
                        return transactionStatusResponse.isCompleted();
                    }
                })
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ApiSuccess<BankTransactionResponse>() {
                    @Override
                    public void call(BankTransactionResponse balanceResponse) {
                        Log.d(TAG,"subscribe");
                        //if (BankTransactionResponse.isCompleted()) {
                        view.showTransacrion(balanceResponse);
                        //}
                    }
                }, new ApiFail() {
                    @Override
                    public void httpStatus(HttpErrorResponse response) {
                        Log.d(TAG, "ApiFail: httpStatus");
                        view.fail(response.getError());
                    }

                    @Override
                    public void noNetworkError() {
                        Log.d(TAG, "ApiFail: No Internet");
                        view.noInternet();
                    }

                    @Override
                    public void unknownError(Throwable e) {
                        Log.d(TAG, "ApiFail: Exception" + e.toString());
                        view.exception(e);
                    }
                })
        ;


    }

    @Override
    public void getTransactionId(String servicesType, GenerateDepositWithdrawTransIdRequest request) {
        Log.v("Button Press", "api call");
/*
        //view.activateConfirmButton();
        if (servicesType.equals(context.getString(R.string.service_type_deposit))) {

            Api.banksDetails().getDepositTransactionId(UtilDmt.PREFERENCES.getToken(context), request)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new ApiSuccess<GenerateDepositWithdrawTransIdResponse>() {
                        @Override
                        public void call(GenerateDepositWithdrawTransIdResponse transactionResponse) {
                            view.activateConfirmButton(transactionResponse);
                        }
                    }, new ApiFail() {
                        @Override
                        public void httpStatus(HttpErrorResponse response) {
                            String message = response.getError();
                            switch (response.getHttpStatusCode()) {
                                case HttpErrorResponse.HTTP_STATUS.STATUS_403:
                                    message = context.getResources().getString(R.string.transaction_limit_reached);
                                case HttpErrorResponse.HTTP_STATUS.STATUS_400:
                                    message = response.getError();
                            }
                            //view.fail(message);

                        }


                        @Override
                        public void noNetworkError() {

                            //view.noInternet();
                        }

                        @Override
                        public void unknownError(Throwable e) {

                           // view.exception(e);
                        }


                    });

        } else if (servicesType.equals (context.getString(R.string.service_type_withdraw))) {
            Api.banksDetails().getWithdrawTransactionId(UtilDmt.PREFERENCES.getToken(context), request)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new ApiSuccess<GenerateDepositWithdrawTransIdResponse>() {
                        @Override
                        public void call(GenerateDepositWithdrawTransIdResponse transactionResponse) {
                            view.activateConfirmButton(transactionResponse);
                        }
                    }, new ApiFail() {
                        @Override
                        public void httpStatus(HttpErrorResponse response) {
                            String message = response.getError();
                            switch (response.getHttpStatusCode()) {
                                case HttpErrorResponse.HTTP_STATUS.STATUS_403:
                                    message = context.getResources().getString(R.string.transaction_limit_reached);
                                case HttpErrorResponse.HTTP_STATUS.STATUS_400:
                                    message = response.getError();
                            }
                           // view.fail(message);

                        }
                        @Override
                        public void noNetworkError() {

                            //view.noInternet();
                        }
                        @Override
                        public void unknownError(Throwable e) {

                            //view.exception(e);
                        }
                    });
        }*/
    }
}