package com.digitalindiapayment.adapter;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
/**
 * Created by Amol on 12-03-2017.
 */

public class MyArrayAdapter<T> extends ArrayAdapter<T> {

    private List<T> list;
    private List<T> mOriginalValues;

    public MyArrayAdapter(Context context, int resource,

                          List<T> objects) {
        super(context, resource, objects);
        list = objects;
    }


    public void addAll(List<T> objects) {
        if (objects == null || list == null) {
            return;
        }
        list.addAll(objects);
    }

    @Override
    public Filter getFilter() {

        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                clear();
                addAll((List<T>) results.values); // has the filtered values
                notifyDataSetChanged();  // notifies the data with new filtered values
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();        // Holds the results of a filtering operation in values
                List<T> FilteredArrList = new ArrayList<T>();

                if (mOriginalValues == null) {
                    mOriginalValues = new ArrayList<T>(list); // saves the original data in mOriginalValues
                }

                /********
                 *
                 *  If constraint(CharSequence that is received) is null returns the mOriginalValues(Original) values
                 *  else does the Filtering and returns FilteredArrList(Filtered)
                 *
                 ********/
                if (constraint == null || constraint.length() == 0) {

                    // set the Original result to return
                    results.count = mOriginalValues.size();
                    results.values = mOriginalValues;
                } else {
                    constraint = constraint.toString().toLowerCase(Locale.getDefault());
                    for (int i = 0; i < mOriginalValues.size(); i++) {
                        T data = mOriginalValues.get(i);
                        if (data.toString().toLowerCase(Locale.getDefault()).contains(constraint.toString())) {
                            FilteredArrList.add(data);
                        }
                    }
                    // set the Filtered result to return
                    results.count = FilteredArrList.size();
                    results.values = FilteredArrList;
                }
                return results;
            }
        };
        return filter;
    }

    public void clear() {
        list.clear();
        notifyDataSetChanged();
    }
}