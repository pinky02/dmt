package com.digitalindiapayment.changePassword;


import com.digitalindiapayment.mvp.BaseView;
import com.digitalindiapayment.pojo.ChangePassword;

/**
 * Created by DM on 20-01-2017.
 */

public class ChangePasswordContract {

    interface View extends BaseView {

        void showLogin();

        //void showInvalidMobile();

        //void showInvalidotp();

        //void showInvalidParam();

        //void showInvalidMobOTP();
        void showInvalidMessage(String message);
    }

    interface UserActions {
        void submitForgotPassword(ChangePassword changePassword);
    }
}
