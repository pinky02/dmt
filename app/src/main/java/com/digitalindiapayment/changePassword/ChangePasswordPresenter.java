package com.digitalindiapayment.changePassword;

import android.content.Context;

import com.digitalindiapayment.R;
import com.digitalindiapayment.pojo.ChangePassword;
import com.digitalindiapayment.pojo.ChangePasswordResponse;
import com.digitalindiapayment.webservices.Api;
import com.digitalindiapayment.webservices.core.ApiFail;
import com.digitalindiapayment.webservices.core.ApiSuccess;
import com.digitalindiapayment.webservices.core.HttpErrorResponse;

import retrofit2.Response;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by DM on 20-01-2017.
 */

public class ChangePasswordPresenter implements ChangePasswordContract.UserActions {
    private final ChangePasswordContract.View view;
    private final Context context;

    public ChangePasswordPresenter(ChangePasswordContract.View view, Context context) {
        this.view = view;
        this.context = context;
    }

    @Override
    public void submitForgotPassword(ChangePassword changePassword) {
        Api.userManagement().resetPassword(changePassword)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ApiSuccess<String>() {
                               @Override
                               public void call(String changePasswordResponseResponse) {
                                   //if(changePasswordResponseResponse.equals(context.getString(R.string.api_success_key))){
                                       view.showLogin();
                                   //}
                               }
                           }, new ApiFail() {
                               @Override
                               public void httpStatus(HttpErrorResponse response) {
                                   view.showInvalidMessage(response.getError());
                               }

                               @Override
                               public void noNetworkError() {
                                   view.noInternet();

                               }

                               @Override
                               public void unknownError(Throwable e) {
                                   view.fail(e.getMessage());
                               }
                           }
                );
    }
}
