package com.digitalindiapayment.changePassword;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsMessage;
import android.text.InputType;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.atritechnocrat.utility.AndroidUtils;
import com.atritechnocrat.view.ValidatorTextInputLayout;

import com.digitalindiapayment.BaseActivity;
import com.digitalindiapayment.R;
import com.digitalindiapayment.pojo.ChangePassword;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by DM on 20-01-2017.
 */

public class ChangePass extends BaseActivity implements ChangePasswordContract.View {
    private static final String TAG = "ChangePass";
    ChangePasswordContract.UserActions changePasswordPresenter;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.mobileno)
    EditText mobileno;
    @BindView(R.id.mobileNumberValidator)
    ValidatorTextInputLayout mobileNumberValidator;
    @BindView(R.id.otp)
    EditText otp;
    @BindView(R.id.otpValidator)
    ValidatorTextInputLayout otpValidator;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.passwordValidator)
    ValidatorTextInputLayout passwordValidator;
    @BindView(R.id.confirmPassword)
    EditText confirmPassword;
    @BindView(R.id.confirmPasswordValidator)
    ValidatorTextInputLayout confirmPasswordValidator;
    @BindView(R.id.submitPassword)
    Button submitPassword;
    @BindView(R.id.showPassword)
    ImageView showPassword;

    IntentFilter smsListenerFilter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.change_pass);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        changePasswordPresenter = new ChangePasswordPresenter(this, getApplicationContext());
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        String mobile = getIntent().getStringExtra(getString(R.string.mobilenumber));
        mobileno.setText("" + mobile);
        otp.requestFocus();
        mobileno.setEnabled(false);
        showPassword.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent e) {
                switch (e.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        password.setInputType(InputType.TYPE_CLASS_TEXT);
                        confirmPassword.setInputType(InputType.TYPE_CLASS_TEXT);
                        break;
                    }
                    case MotionEvent.ACTION_UP: {
                        password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                        password.setSelection(password.length());
                        confirmPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                        confirmPassword.setSelection(confirmPassword.length());
                        break;
                    }
                }

                return false;
            }
        });

        setChangePasswordOnclick();
        // Permission to read SMS
        if(ContextCompat.checkSelfPermission(getBaseContext(), "android.permission.READ_SMS") != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{"android.permission.READ_SMS"},1);
        }

        smsListenerFilter = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");

    }

    private BroadcastReceiver smsListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle data = intent.getExtras();
            Object[] pdus = (Object[]) data.get("pdus");
            for (int i = 0; i < pdus.length; i++) {
                SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) pdus[i]);
                String sender = smsMessage.getDisplayOriginatingAddress();
                Log.d(TAG, "Sender :" + sender);
                if (sender.equals(getString(R.string.smsgetway_sendername))) {
                    Log.d(TAG, "below if Sender :" + sender);
                    String messageBody = smsMessage.getMessageBody();
                    String number = messageBody.replaceAll("[^0-9]", "");
                    otp.setText(number);
                    Log.d(TAG, "messageBody :" + messageBody);
                }
            }
        }
    };


    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(smsListener, smsListenerFilter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(smsListener);
    }

    protected void setChangePasswordOnclick() {
        submitPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChangePassword changePassword = new ChangePassword();
                if (mobileNumberValidator.validate() && otpValidator.validate() && passwordValidator.validate() && confirmPasswordValidator.validate()) {
                    if (mobileno.getText().toString().length() < 10) {
                        mobileno.setError(getString(R.string.enter_10_digit_mobile));
                        return;
                    }
                    if (otp.getText().toString().length() < 4) {
                        otp.setError(getString(R.string.valid_otp));
                        return;
                    }
                    if (!password.getText().toString().equals(confirmPassword.getText().toString())) {
                        Toast.makeText(ChangePass.this, getString(R.string.password_not_match), Toast.LENGTH_LONG).show();
                    } else {
                        changePassword.setPhone_no(mobileno.getText().toString());
                        changePassword.setOtp(Integer.parseInt(otp.getText().toString()));
                        changePassword.setPassword(password.getText().toString());
                        changePassword.setPassword_confirmation(confirmPassword.getText().toString());
                        changePasswordPresenter.submitForgotPassword(changePassword);
                        submitPassword.setText(getString(R.string.submitting_process));
                        submitPassword.setEnabled(false);
                        //submitPassword.setOnClickListener(null);
                    }
                }
            }
        });
    }

    @Override
    public void showLogin() {
        Toast.makeText(getApplicationContext(), getString(R.string.change_password_success), Toast.LENGTH_LONG).show();
        finish();
    }

    @Override
    public void showInvalidMessage(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
        finish();
    }


    @Override
    public void noInternet() {
        submitPassword.setText(getString(R.string.submit));
        submitPassword.setEnabled(true);
        Toast.makeText(getApplicationContext(), getString(R.string.invalid_or_missing_parameters), Toast.LENGTH_LONG).show();
        //setChangePasswordOnclick();
    }

    @Override
    public void exception(Throwable e) {
        AndroidUtils.showLongToast(getApplicationContext(), e.getMessage());
        finish();
    }

    @Override
    public void fail(String message) {
        AndroidUtils.showLongToast(getApplicationContext(), message);
    }

    @Override
    public void success(String message) {

    }

    @Override
    public void showLoading() {
        showDialog();
    }

    @Override
    public void hideLoading() {
        disMissDialog();

    }


}
