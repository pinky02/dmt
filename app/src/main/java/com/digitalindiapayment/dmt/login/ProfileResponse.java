package com.digitalindiapayment.dmt.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ashwinnbhanushali on 11/05/17.
 */

public class ProfileResponse {
    @SerializedName("Profile")
    @Expose
    private UserProfile profile;

    public UserProfile getProfile() {
        return profile;
    }

    public void setProfile(UserProfile profile) {
        this.profile = profile;
    }
}
