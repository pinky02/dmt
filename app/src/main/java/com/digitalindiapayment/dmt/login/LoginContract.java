package com.digitalindiapayment.dmt.login;


import com.digitalindiapayment.mvp.BaseView;
import com.digitalindiapayment.webservices.dmt.requestpojo.Login;
import com.digitalindiapayment.webservices.dmt.requestpojo.LogoutRequest;
import com.digitalindiapayment.webservices.dmt.responsepojo.LoginResponse;


/**
 * Created by Vikas Pawar on 12-01-2017.
 */

public class LoginContract {

    public interface View extends BaseView {

        void showHome(String token, UserProfile userProfile);
        void onResetSessionCall();
        void onResetSessionFail();
        void onResetSessionComplete();
        void show(LoginResponse loginResponse);

    }

    public interface UserActions {
        void submitDmtLogin(Login login);
        void getUsersInfo();
        void setResetSession(LogoutRequest logoutRequest);
    }

}
