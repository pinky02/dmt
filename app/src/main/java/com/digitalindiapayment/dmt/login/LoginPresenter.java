package com.digitalindiapayment.dmt.login;

import android.content.Context;
import android.util.Log;

import com.digitalindiapayment.webservices.Api;
import com.digitalindiapayment.webservices.core.ApiFail;
import com.digitalindiapayment.webservices.core.ApiSuccess;
import com.digitalindiapayment.webservices.core.HttpErrorResponse;
import com.digitalindiapayment.webservices.dmt.requestpojo.DashBoardRequest;
import com.digitalindiapayment.webservices.dmt.requestpojo.Login;
import com.digitalindiapayment.webservices.dmt.requestpojo.LogoutRequest;
import com.digitalindiapayment.webservices.dmt.responsepojo.DashBoardResponse;
import com.digitalindiapayment.webservices.dmt.responsepojo.LoginResponse;
import com.digitalindiapayment.webservices.dmt.responsepojo.LogoutResponse;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Pinky Yadav on 10-04-2018.
 */

public class LoginPresenter implements LoginContract.UserActions {
    private static final String TAG = "LoginPresentor";
    private final LoginContract.View view;
    private final Context context;
    DashBoardRequest dmtDashBoard;

    public LoginPresenter(LoginContract.View view, Context context) {
        this.view = view;
        this.context = context;
    }


    @Override
    public void submitDmtLogin(Login loginDmt) {
        view.showLoading();
        //view.showHome(UtilDmt.PREFERENCES.getToken(context));
       // login.setMobile_auth_code(UtilDmt.PREFERENCES.getUniqueDeviceKey(context));
        Api.userManagementDmt().dmtLogin(loginDmt)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ApiSuccess<LoginResponse>() {
                    @Override
                    public void call(LoginResponse loginResponse) {
                        Log.d(TAG, "subscribe > call");
                        view.hideLoading();
                        if(loginResponse.getStatus()==0){
                           view.onResetSessionCall();
                        }
                        else if(loginResponse.getStatus()==1)
                        view.show(loginResponse);

                    }
                }, new ApiFail() {
                    @Override
                    public void httpStatus(HttpErrorResponse response) {
                        view.hideLoading();
                      //  view.onResetSessionFail();
                    }

                    @Override
                    public void noNetworkError() {
                        view.noInternet();
                    }

                    @Override
                    public void unknownError(Throwable e) {
                        view.fail(e.getMessage());
                    }
                });
       // return Api.userManagementDmt().dmtDashBoard(dmtDashBoardRequest);

    }


    @Override
    public void getUsersInfo() {

    }


    @Override
    public void setResetSession(LogoutRequest request) {
        Api.userManagementDmt().resetSession(request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ApiSuccess<LogoutResponse>() {
                    @Override
                    public void call(LogoutResponse logoutResponse) {
                        Log.d(TAG, "subscribe > call");
                        //if(addMemberResponseResponse.equals(context.getString(R.string.api_success_key))){
                        view.onResetSessionComplete();
                        //}
                    }
                }, new ApiFail() {
                    @Override
                    public void httpStatus(HttpErrorResponse response) {
                        view.onResetSessionFail();
                    }

                    @Override
                    public void noNetworkError() {
                        view.noInternet();
                    }

                    @Override
                    public void unknownError(Throwable e) {
                        view.fail(e.getMessage());
                    }
                });
    }

}

