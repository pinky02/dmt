package com.digitalindiapayment.dmt.login;

import android.content.Context;

import com.digitalindiapayment.webservices.core.ApiSuccess;

/**
 * Created by admin on 29-09-2017.
 */

public class ProfileApiSuccess extends ApiSuccess<UserProfile> {

    private LoginContract.View view;
    private Context context;

    public ProfileApiSuccess(LoginContract.View view, Context context) {
        this.view = view;
        this.context = context;
    }

    @Override
    public void call(UserProfile userProfile) {

    }
}
