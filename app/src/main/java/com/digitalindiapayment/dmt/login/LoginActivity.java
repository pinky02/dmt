package com.digitalindiapayment.dmt.login;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.digitalindiapayment.BaseActivity;
import com.digitalindiapayment.BuildConfig;
import com.digitalindiapayment.R;
import com.digitalindiapayment.dmt.util.UtilDmt;
import com.digitalindiapayment.webservices.dmt.requestpojo.Login;
import com.digitalindiapayment.webservices.dmt.requestpojo.LogoutRequest;
import com.digitalindiapayment.webservices.dmt.responsepojo.LoginResponse;
import com.digitalindiapayment.dmt.dashboard.*;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Amol Bhanushali on 03-03-2017.
 */
public class LoginActivity extends BaseActivity implements LoginContract.View {

    public static final int RESET_SESSION = 101;
    private static final String TAG = "LoginActivity";
    LoginContract.UserActions loginPresenter;
    @BindView(R.id.username)
    EditText username;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.login)
    Button loginbtn;
    @BindView(R.id.vesion)
    TextView vesion;
    Login login;
    @BindView(R.id.forget_password)
    TextView forgotPassword;
    Dialog builder;
    EditText number;
    Button submitOtp;
    TextView resetTextView;
    Button resetButton;
    @BindView(R.id.contact_us)
    TextView contactUs;
    @BindView(R.id.imgCheck)
    ImageView imgCheck;
    @BindView(R.id.tvCheckbox)
    TextView tvCheckbox;
    @BindView(R.id.progressCheck)
    ProgressBar progressCheck;

    String whichitem="";
    private boolean iscaptchaverify = false;
    Login loginDmt;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_main);
        ButterKnife.bind(this);

        //FirebaseMessaging.getInstance().subscribeToTopic(UtilDmt.KEYS.DEFAULT_NOTIFICATION_TOPIC);
        UtilDmt.PREFERENCES.setIsLogin(this, false);
      //  UtilDmt.PREFERENCES.setNotificationRegId(LoginActivity.this, FirebaseInstanceId.getInstance().getToken());
        UtilDmt.PREFERENCES.setEvoluteBluetoothMac(getApplicationContext(), "");
        UtilDmt.PREFERENCES.setEvoluteBluetoothName(getApplicationContext(), "");
        UtilDmt.isBluetoothConnected = false;
        Log.d(TAG, " Firebase InstanceId : " + UtilDmt.PREFERENCES.getNotificationRegId(getApplicationContext()));

        /*if (getIntent().hasExtra(UtilDmt.KEYS.INFORMATION)){
            from=getIntent().getStringExtra(UtilDmt.KEYS.INFORMATION);
            actionCode=getIntent().getStringExtra(UtilDmt.KEYS.ACTION_CODE);
        }*/
        username.setText(UtilDmt.PREFERENCES.getUserId(this));

        if (BuildConfig.DEBUG) {
            // do something for a debug build
            username.setText("9554777894");
            password.setText("dipl@123");
            //username.setText("9702839333");
           // password.setText("dipl@123");
           // password.setText("nokia@123");
            //username.setText("9730966971");
            //password.setText("password");
           // username.setText("9702839333");
          //  username.setText("8898348660");
            //password.setText("dipl@123");
            iscaptchaverify = true;
        }


        login = new Login();
        loginDmt = new Login();


        password.setFilters(new InputFilter[]{
                new InputFilter() {
                    public CharSequence filter(CharSequence src, int start,
                                               int end, Spanned dst, int dstart, int dend) {
                        if (src.equals("")) { // for backspace
                            return src;
                        }
                        if (src.toString().matches("[\\x00-\\x7F]+")) {
                            return src;
                        }
                        return "";
                    }
                }
        });



        loginPresenter = new LoginPresenter(this, getApplicationContext());
        String versionName = UtilDmt.getVersionName(getApplicationContext());
        //vesion.setText(getString(R.string.verion) + " " + versionName);

        //if (!BuildConfig.DEBUG) {
        UtilDmt.PREFERENCES.clearLoginSession(LoginActivity.this);
        //}
    }

    @OnClick(R.id.login)
    public void setLogin() {
        if (username.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(getApplicationContext(), getString(R.string.pls_enter_mobile_no), Toast.LENGTH_SHORT).show();

            return;
        }
        if (password.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(getApplicationContext(), getString(R.string.pls_enter_password), Toast.LENGTH_SHORT).show();
            return;
        }
        if (!iscaptchaverify) {
            Toast.makeText(this, R.string.str_verify_captcha, Toast.LENGTH_SHORT).show();
            return;
        }
        Log.d("whichitem",whichitem);
        loginDmt.setPhoneNo("9952777568");
        loginDmt.setPassword("123456");
//        loginDmt.setPhoneNo("9952777568");
//      loginDmt.setPassword("123456");
        loginDmt.setCaptcha("234");

        // Crash-Analytics


    loginPresenter.submitDmtLogin(loginDmt);


        //this method will be used when we will login all users via single method
        //loginPresenter.submitLogin(login);

    }
    @Override
    public void onResetSessionCall() {
        builder = new Dialog(LoginActivity.this);
        builder.setContentView(R.layout.layout_reset_session);
        builder.setCancelable(true);

        resetTextView = (TextView) builder.findViewById(R.id.resetTextView);
        resetButton = (Button) builder.findViewById(R.id.resetButton);

        Window window = builder.getWindow();
        window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent resetSession = new Intent(LoginActivity.this, ResetSession.class);
                //resetSession.putExtra(UtilDmt.KEYS.DATA,username.getText().toString());
                //startActivityForResult(resetSession,RESET_SESSION);
                builder.setCancelable(false);
              //  loginPresenter.resetSession(new ForgotPasswordRequest(username.getText().toString()));
                resetButton.setText(getString(R.string.processing));
                LogoutRequest logoutRequest = new LogoutRequest();
                logoutRequest.setMobilenumber(loginDmt.getPhoneNo());
                loginPresenter.setResetSession(logoutRequest);
                builder.dismiss();
            }
        });
        builder.show();
    }

    @Override
    public void onResetSessionFail() {

    }

    @Override
    public void onResetSessionComplete() {
        setLogin();
    }

    @Override
    public void showHome(final String token, UserProfile userProfile) {

        hideLoading();
        UtilDmt.PREFERENCES.setToken(this, token);
        UtilDmt.PREFERENCES.setIsLogin(this, true);
        Intent loginToHome = new Intent(getApplicationContext(), HomePageActivity.class);
        /*if (from!=null){
            loginToHome.putExtra(UtilDmt.KEYS.INFORMATION,from);
            loginToHome.putExtra(UtilDmt.KEYS.ACTION_CODE, actionCode);
        }*/
        loginToHome.putExtra(UtilDmt.KEYS.DATA, userProfile);
       // loginToHome.putExtra(MyFirebaseMessagingService.NOTIFICATION_DATA, getIntent().getParcelableExtra(MyFirebaseMessagingService.NOTIFICATION_DATA));
        UtilDmt.PREFERENCES.setLogin(this, login);
        loginToHome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(loginToHome);
        finish();
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        /*if (intent.hasExtra("information")){
            from=intent.getStringExtra("information");
            actionCode =intent.getStringExtra("actionCode");
        }*/
    }


    @Override
    public void noInternet() {
        Toast.makeText(getApplicationContext(), "No internet", Toast.LENGTH_LONG).show();
        disMissDialog();

    }

    @Override
    public void exception(Throwable e) {
        Toast.makeText(getApplicationContext(), getString(R.string.invalid_credentials), Toast.LENGTH_LONG).show();
        disMissDialog();
    }

    @Override
    public void fail(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
        disMissDialog();
    }

    @Override
    public void success(String message) {

    }

    @Override
    public void showLoading() {
        showDialog();
    }

    @Override
    public void hideLoading() {
        disMissDialog();
    }



    @Override
    public void show(LoginResponse loginResponse) {
        Intent dmtHomePage = new Intent(LoginActivity.this, HomePageActivity.class);
        //  resetSession.putExtra(getString(R.string.mobilenumber), username.getText().toString());
       // dmtHomePage.putExtra("DashboardData", loginResponse);
        UtilDmt.PREFERENCES.setUserId(getApplicationContext(), ""+loginResponse.getUserid());
        UtilDmt.PREFERENCES.setParentUserId(getApplicationContext(), ""+loginResponse.getParentid());
        UtilDmt.PREFERENCES.setUserType(getApplicationContext(), ""+loginResponse.getUserType());
        UtilDmt.PREFERENCES.setBCAgent(getApplicationContext(),""+loginResponse.getBcagent());
        UtilDmt.PREFERENCES.setSessionToken(getApplicationContext(),""+loginResponse.getSessiontoken());
        startActivity(dmtHomePage);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == RESET_SESSION) {
                loginPresenter.getUsersInfo();
            }
        }

    }



    public void showLogin() {

    }

    @Override
    protected void onResume() {
        super.onResume();
      //  iAnalytics.trackScreen(getString(R.string.title_activity_login));
    }



//    @OnClick(R.id.tvCheckbox)
//    public void verifyCaptcha() {
//        progressCheck.setVisibility(View.VISIBLE);
//        tvCheckbox.setVisibility(View.GONE);
//        SafetyNet.getClient(this)
//                .verifyWithRecaptcha(getString(R.string.site_key))
//                .addOnSuccessListener(this, new OnSuccessListener<SafetyNetApi.RecaptchaTokenResponse>() {
//                    @Override
//                    public void onSuccess(SafetyNetApi.RecaptchaTokenResponse recaptchaTokenResponse) {
//                        progressCheck.setVisibility(View.GONE);
//                        tvCheckbox.setVisibility(View.GONE);
//                        imgCheck.setVisibility(View.VISIBLE);
//                        iscaptchaverify = true;
//                        Log.e(TAG, "onSuccess");
//                    }
//                })
//                .addOnFailureListener(this, new OnFailureListener() {
//                    @Override
//                    public void onFailure(@NonNull Exception e) {
//                        progressCheck.setVisibility(View.GONE);
//                        tvCheckbox.setVisibility(View.VISIBLE);
//                        Toast.makeText(LoginActivity.this, R.string.str_verify_again, Toast.LENGTH_SHORT).show();
//                        Log.e(TAG, "onFailure: " + e.getMessage());
//                    }
//                });
//    }
}
