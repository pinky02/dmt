package com.digitalindiapayment.dmt.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.util.Log;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ListView;
import android.widget.Toast;

import com.digitalindiapayment.R;
import com.digitalindiapayment.webservices.dmt.requestpojo.Login;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.UUID;

import static com.digitalindiapayment.util.Util.KEYS.UNIQUE_DEVICE_ID;

/**
 * Created by Expert on 15-03-2017.
 */

public class UtilDmt {

    public static boolean isBluetoothConnected = false;

    static public class KEYS {
        public static final String DATA = "data";
        public static final String PATH = "path";
        public static final String SERVICES = "services";
        public static final char hiphen = '-';
        public static final String TITAL = "tital";
        public static final String NOTIFICATION_FCM_TOKEN = "notification_token";
        public static final String DEFAULT_NOTIFICATION_TOPIC = "news";
        public static final String UNIQUE_DEVICE_ID = "uniqueID";
        public static final int MORPHO_DEVICE_INFO = 130;
        public static final int FINGERPRINT_DATA_REQUEST = 135;

        public static final String IS_LOGIN = "isLogin";
        public static final String INFORMATION = "information";
        public static final String ACTION_CODE = "actionCode";

        public static final String DEVICE = "device";
        public static final String APP_INSTANCE = "appInstance";
        public static final String USER_ID = "userid";
        public static final String SENDER_CONTACT_NUMBER = "contactnum";
        public static final String BC_AGENT = "bcagent";
        public static final String SESSION_TOKEN = "sessiontoken";
        public static final String REMITTER_ID = "remitterid";


        public static final String PARENT_USER_ID = "parentid";
        public static final String USER_TYPE = "user_type";
        public static final String TICKET_TOKEN = "ticketToken";
        public static final String TICKET_AGENT_ID = "ticketAgentId";
        public static final String SUCCESS = "success";
        public static final String PROBLEM = "problem";
        public static final String PROBLEM_ID = "problemID";

        public static final String TICKET_CODE = "ticketCode";
        public static final String FNAME = "fname";
        public static final String LNAME = "lname";
        public static final String EMAIL = "email";
        public static final String MBILE = "mobile";
        public static final String EVOLUTE_BLUETOOTH_NAME = "bluetoothName";
        public static final String EVOLUTE_MAC_ADDRESS = "macAddress";

    }


    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static void showShortToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

    }

    public static String indianCurrencyRepresentation(String amount) {
        String amountD = "0.0";
        try {
            Float floatAmount = Float.parseFloat(amount);
            DecimalFormat decimalFormat = new DecimalFormat("#.##");
            float twoDigitsF = Float.valueOf(decimalFormat.format(floatAmount));
            return String.format("%.02f", floatAmount);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return amountD;


    }

    public static void showLongToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();

    }

    public static String getVersionName(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            return "0.0";
        }
    }

    public static String dateFormat(String date) {
        SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        spf.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date newDate = new Date();
        try {
            newDate = spf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        spf = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss aaa", Locale.ENGLISH);
        date = spf.format(newDate);
        return date;
    }
    public static void runLayoutAnimation(Context context, ListView listview) {
        LayoutAnimationController layoutAnimCtrlr = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_down_slide);
        listview.setLayoutAnimation(layoutAnimCtrlr);
        listview.startLayoutAnimation();
    }
    static public class PREFERENCES {
        public static final String MyPREFERENCES = "app";
        public static final String TOKEN = "token";
        public static final String TERMINALID = "terminalID";

        public static final String LOGIN = "login";
        public static final String USERTYPE = "usertype";
        public static final String DEVICETYPE = "deviceType";

        public static final String EVOLUTE_DEVICE_MAC_ADDRESS = "evoluteDeviceMacAddress";
        public static final String RD_ENABLED_STATUS = "isRDEnabled";
        public static final String IS_USER_LOGIN_INTO_APP = "isUserLoginIntoApp";


        /*
        *   SCL-1687-9868-6046
        *   SCL-4346-4562-3491
         */
        public static final String MORPHO_ACTIVATION_LINK = "SCL-1687-9868-6046";

        public static void setRDEnabled(Context context, String status) {
            SharedPreferences sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putBoolean(RD_ENABLED_STATUS, status.equals("1"));
            editor.apply();
        }

        public static Boolean isRDEnabled(Context context) {
            SharedPreferences sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            return sharedpreferences.getBoolean(RD_ENABLED_STATUS, false);
        }


        public static void removeToken(Context context) {
            SharedPreferences sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.remove(TOKEN);
            editor.apply();
        }

        /**
         * @param context
         * @param token   SET TOKEN
         */
        public static void setToken(Context context, String token) {
            removeToken(context);
            SharedPreferences sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putString(TOKEN, token);
            editor.apply();
        }


        public static String getToken(Context context) {
            SharedPreferences sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            return sharedpreferences.getString(TOKEN, "");

        }


        /**
         * This method is used to set login information - LoginId - mobile number and UserType = Role
         *
         * @param context
         * @param login
         */
        public static void setLogin(Context context, Login login) {
            SharedPreferences sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putString(LOGIN, login.getPhoneNo());
            editor.putString(USERTYPE, "agent");
            editor.apply();
        }


        /**
         * This method is used to get the login id - Mobile number
         *
         * @param context
         * @return
         */
        public static String getUserId(Context context) {
            SharedPreferences sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            return sharedpreferences.getString(LOGIN, "");
        }

        public static String getUserType(Context context) {
            SharedPreferences sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            return sharedpreferences.getString(USERTYPE, "");
        }


        public static void setTerminalID(Context context, String terminalID) {
            SharedPreferences sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putString(TERMINALID, terminalID);
            editor.apply();
        }

        public static String getTerminalID(Context context) {
            SharedPreferences sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            return sharedpreferences.getString(TERMINALID, "");
        }

        public static void setDeviceName(Context context, String deviceName) {
            SharedPreferences sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putString(DEVICETYPE, deviceName);
            editor.apply();
        }

        public static String getDeviceName(Context context) {
            SharedPreferences sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            return sharedpreferences.getString(DEVICETYPE, "");
        }

        public static void setEvoluteBluetoothDeviceMacId(Context context, String deviceName) {
            SharedPreferences sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putString(EVOLUTE_DEVICE_MAC_ADDRESS, deviceName);
            editor.apply();
        }

        public static String getEvoluteBluetoothDeviceMacId(Context context) {
            SharedPreferences sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            return sharedpreferences.getString(EVOLUTE_DEVICE_MAC_ADDRESS, "");
        }

        public static void setDefaultPrinter(Context context, String deviceName) {
            SharedPreferences sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putString(EVOLUTE_DEVICE_MAC_ADDRESS, deviceName);
            editor.apply();
        }

        public static String getDefaultPrinter(Context context) {
            SharedPreferences sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            return sharedpreferences.getString(EVOLUTE_DEVICE_MAC_ADDRESS, "");
        }

        public static void setNotificationRegId(Context context, String token) {
            SharedPreferences pref = context.getSharedPreferences(MyPREFERENCES, 0);
            SharedPreferences.Editor editor = pref.edit();
            editor.putString(KEYS.NOTIFICATION_FCM_TOKEN, token);
            editor.apply();
        }


        public static String getNotificationRegId(Context context) {
            SharedPreferences sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            return sharedpreferences.getString(KEYS.NOTIFICATION_FCM_TOKEN, "");
        }

        public static String getUniqueDeviceKey(Context context) {
            SharedPreferences sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            if (sharedpreferences.getString(UNIQUE_DEVICE_ID, "").equals("")) {
                UUID uuid = UUID.randomUUID();
                String generatedUUID = uuid.toString();
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putString(UNIQUE_DEVICE_ID, generatedUUID);
                editor.apply();
                return generatedUUID;
            } else {
                return sharedpreferences.getString(UNIQUE_DEVICE_ID, "");
            }
        }

        public static void clearLoginSession(Context context) {
            // UtilDmt.PREFERENCES.setDeviceName(context, context.getString(R.string.device_morpho_name));
            // UtilDmt.PREFERENCES.setDefaultPrinter(context, context.getString(R.string.device_evolute_printer));
            UtilDmt.PREFERENCES.setTerminalID(context, "");
            UtilDmt.PREFERENCES.setToken(context, "");
            // KeyFile.setDevise(context, "");
        }

        public static void setIsLogin(Context context, boolean isLogin) {
            SharedPreferences pref = context.getSharedPreferences(MyPREFERENCES, 0);
            SharedPreferences.Editor editor = pref.edit();
            editor.putBoolean(KEYS.IS_LOGIN, isLogin);
            editor.apply();
        }

        public static boolean isLogin(Context context) {
            SharedPreferences sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            return sharedpreferences.getBoolean(KEYS.IS_LOGIN, false);
        }

        public static String getAppInstanceCode(Context mContext) {
            SharedPreferences sharedPreferences = mContext.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            return sharedPreferences.getString(KEYS.APP_INSTANCE, "");
        }

        public static void setAppInstanceCode(Context mContext, String appInstanceCode) {
            SharedPreferences sharedPreferences = mContext.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(KEYS.APP_INSTANCE, appInstanceCode);
            editor.apply();
        }

        public static void setAgetUserId(Context context, String userId) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(KEYS.USER_ID, userId);
            editor.apply();
        }

        public static String getAgentUserId(Context mContext) {
            SharedPreferences sharedPreferences = mContext.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            return sharedPreferences.getString(KEYS.USER_ID, "");
        }

        public static void setUserId(Context context, String userId) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(KEYS.USER_ID, userId);
            editor.apply();
        }

        public static String getUserIdDmt(Context mContext) {
            SharedPreferences sharedPreferences = mContext.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            return sharedPreferences.getString(KEYS.USER_ID, "");
        }

        public static void setParentUserId(Context context, String userId) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(KEYS.PARENT_USER_ID, userId);
            editor.apply();
        }

        public static String getParentUserId(Context mContext) {
            SharedPreferences sharedPreferences = mContext.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            return sharedPreferences.getString(KEYS.PARENT_USER_ID, "");
        }

        public static void setUserType(Context context, String userId) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(KEYS.USER_TYPE, userId);
            editor.apply();
        }

        public static String getUserTypeDmt(Context mContext) {
            SharedPreferences sharedPreferences = mContext.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            return sharedPreferences.getString(KEYS.USER_TYPE, "");
        }

        public static String getBCAgent(Context mContext) {
            SharedPreferences sharedPreferences = mContext.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            return sharedPreferences.getString(KEYS.BC_AGENT, "");
        }

        public static void setBCAgent(Context context, String userId) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(KEYS.BC_AGENT, userId);
            editor.apply();
        }

        public static String getSessionToken(Context mContext) {
            SharedPreferences sharedPreferences = mContext.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            return sharedPreferences.getString(KEYS.SESSION_TOKEN, "");
        }

        public static void setSessionToken(Context context, String userId) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(KEYS.SESSION_TOKEN, userId);
            editor.apply();
        }
        public static String getRemitterId(Context mContext) {
            SharedPreferences sharedPreferences = mContext.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            return sharedPreferences.getString(KEYS.REMITTER_ID, "");
        }

        public static void setRemitterId(Context context, String remitterId) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(KEYS.REMITTER_ID, remitterId);
            editor.apply();
        }
        public static void setTicketAgetId(Context context, String ticketAgentId) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(KEYS.TICKET_AGENT_ID, ticketAgentId);
            editor.apply();
        }

        public static String getTicketId(Context mContext) {
            SharedPreferences sharedPreferences = mContext.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            return sharedPreferences.getString(KEYS.TICKET_AGENT_ID, "");
        }

        public static void setTicketToken(Context context, String ticketToken) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(KEYS.TICKET_TOKEN, ticketToken);
            editor.apply();
        }

        public static String getTicketToken(Context mContext) {
            SharedPreferences sharedPreferences = mContext.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            return sharedPreferences.getString(KEYS.TICKET_TOKEN, "");
        }

        public static void setFname(Context context, String fname) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(KEYS.FNAME, fname);
            editor.apply();
        }

        public static String getFname(Context mContext) {
            SharedPreferences sharedPreferences = mContext.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            return sharedPreferences.getString(KEYS.FNAME, "");
        }

        public static void setLname(Context context, String lname) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(KEYS.LNAME, lname);
            editor.apply();
        }

        public static String getLname(Context mContext) {
            SharedPreferences sharedPreferences = mContext.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            return sharedPreferences.getString(KEYS.LNAME, "");
        }

        public static void setEmail(Context context, String email) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(KEYS.EMAIL, email);
            editor.apply();
        }

        public static String getEmail(Context mContext) {
            SharedPreferences sharedPreferences = mContext.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            return sharedPreferences.getString(KEYS.EMAIL, "");
        }

        public static void setMobile(Context context, String mobile) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(KEYS.MBILE, mobile);
            editor.apply();
        }

        public static String getMObile(Context mContext) {
            SharedPreferences sharedPreferences = mContext.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            return sharedPreferences.getString(KEYS.MBILE, "");
        }

        public static void setEvoluteBluetoothName(Context context, String bluetoothName) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(KEYS.EVOLUTE_BLUETOOTH_NAME, bluetoothName);
            editor.apply();
        }

        public static String getEvoluteBluetoothName(Context mContext) {
            SharedPreferences sharedPreferences = mContext.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            return sharedPreferences.getString(KEYS.EVOLUTE_BLUETOOTH_NAME, "");
        }

        public static void setEvoluteBluetoothMac(Context context, String macAddress) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(KEYS.EVOLUTE_MAC_ADDRESS, macAddress);
            editor.apply();
        }

        public static String getEvoluteBluetoothMac(Context mContext) {
            SharedPreferences sharedPreferences = mContext.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            return sharedPreferences.getString(KEYS.EVOLUTE_MAC_ADDRESS, "");
        }
        public static void setSenderContactNumber(Context context, String contactNumber) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(KEYS.SENDER_CONTACT_NUMBER, contactNumber);
            editor.apply();
        }

        public static String getSenderContactNumber(Context mContext) {
            SharedPreferences sharedPreferences = mContext.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            return sharedPreferences.getString(KEYS.SENDER_CONTACT_NUMBER, "");
        }

//        public static void notificationRedirect(Activity activity, DIPLNotification diplNotification) {
//            Intent i = new Intent();
//            switch (diplNotification.getType()) {
//                case "creditrequest":
//                    i = new Intent(activity, TransactionReportActivity.class);
//                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP );
//                    activity.startActivity(i);
//                    break;
//
//                case "debitrequest":
//                    i = new Intent(activity, TransactionReportActivity.class);
//                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP );
//
//                    activity.startActivity(i);
//                    break;
//
//                case "walletreport":
//                    i = new Intent(activity, WalletReportActivity.class);
//                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP );
//                    /*i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
//                            Intent.FLAG_ACTIVITY_CLEAR_TASK);*/
//                    activity.startActivity(i);
//                    break;
//            }
//
//        }
//
//
//    }

        public static Bitmap getBitmap(String path, Context context) {

            Uri uri = Uri.fromFile(new File(path));
            InputStream in = null;
            try {
                final int IMAGE_MAX_SIZE = 1200000; // 1.2MP
                in = context.getContentResolver().openInputStream(uri);

                // Decode image size
                BitmapFactory.Options o = new BitmapFactory.Options();
                o.inJustDecodeBounds = true;
                BitmapFactory.decodeStream(in, null, o);
                in.close();


                int scale = 1;
                while ((o.outWidth * o.outHeight) * (1 / Math.pow(scale, 2)) >
                        IMAGE_MAX_SIZE) {
                    scale++;
                }
                Log.d("", "scale = " + scale + ", orig-width: " + o.outWidth + ", orig-height: " + o.outHeight);

                Bitmap b = null;
                in = context.getContentResolver().openInputStream(uri);
                if (scale > 1) {
                    scale--;
                    // scale to max possible inSampleSize that still yields an image
                    // larger than target
                    o = new BitmapFactory.Options();
                    o.inSampleSize = scale;
                    b = BitmapFactory.decodeStream(in, null, o);

                    // resize to desired dimensions
                    int height = b.getHeight();
                    int width = b.getWidth();
                    Log.d("", "1th scale operation dimenions - width: " + width + ", height: " + height);

                    double y = Math.sqrt(IMAGE_MAX_SIZE
                            / (((double) width) / height));
                    double x = (y / height) * width;

                    Bitmap scaledBitmap = Bitmap.createScaledBitmap(b, (int) x,
                            (int) y, true);
                    b.recycle();
                    b = scaledBitmap;

                    System.gc();
                } else {
                    b = BitmapFactory.decodeStream(in);
                }
                in.close();

                Log.d("", "bitmap size - width: " + b.getWidth() + ", height: " +
                        b.getHeight());
                return b;
            } catch (IOException e) {
                Log.e("", e.getMessage(), e);
                return null;
            }
        }

        static public class AADHAAR {
            public static final String AADHAAR_DATA_TAG = "PrintLetterBarcodeData",
                    AADHAR_UID_ATTR = "uid",
                    AADHAR_NAME_ATTR = "name",
                    AADHAR_GENDER_ATTR = "gender",
                    AADHAR_YOB_ATTR = "yob",
                    AADHAR_CO_ATTR = "co",
                    AADHAR_VTC_ATTR = "vtc",
                    AADHAR_PO_ATTR = "po",
                    AADHAR_DIST_ATTR = "dist",
                    AADHAR_STATE_ATTR = "state",
                    AADHAR_PC_ATTR = "pc";
        }

        public class SCREENS {
            public static final String SETTLEMENT = "settlement";
            public static final String REPORT = "report";
        }

    }
}
