package com.digitalindiapayment.dmt.dashboard;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.percent.PercentRelativeLayout;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.digitalindiapayment.BaseActivity;
import com.digitalindiapayment.R;
import com.digitalindiapayment.dmt.util.UtilDmt;
import com.digitalindiapayment.profile.ProfileActivity;
import com.digitalindiapayment.dmt.sender.add_sender.AddSenderActivity;
import com.digitalindiapayment.dmt.sender.view.ViewSenderActivity;
import com.digitalindiapayment.dmt.sender.view.receiver_details.ReceiverActivity;
import com.digitalindiapayment.webservices.dmt.requestpojo.DashBoardRequest;
import com.digitalindiapayment.webservices.dmt.responsepojo.DashBoardResponse;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomePageActivity extends BaseActivity implements HomeContract.View{
//    @BindView(R.id.toolbar)
//    Toolbar toolbar;

    @BindView(R.id.search_bar)
    EditText search_bar;
    @BindView(R.id.search)
    Button search;
    @BindView(R.id.scroll_view)
    ScrollView scroll_view;
    @BindView(R.id.layout_current_transaction)
    LinearLayout layout_current_transaction;
    @BindView(R.id.trxn_for_month)
    TextView trxn_for_month;
    @BindView(R.id.txn_amount)
    TextView txn_amount;
    @BindView(R.id.current_month_total_sender)
    TextView current_month_total_sender;
    @BindView(R.id.sender_count)
    TextView sender_count;
    @BindView(R.id.current_month_refund)
    TextView current_month_refund;
    @BindView(R.id.refund_amount)
    TextView refund_amount;
    @BindView(R.id.total_transaction_count)
    TextView total_transaction_count;
    @BindView(R.id.total_sender_count)
    TextView total_sender_count;
    @BindView(R.id.total_refund_count)
    TextView total_refund_count;
    @BindView(R.id.footer_layout)
    PercentRelativeLayout footerLayout;
    @BindView(R.id.ic_setting_light_text)
    TextView icSettingLightText;
    @BindView(R.id.ic_report_light_text)
    TextView icReportLightText;
    @BindView(R.id.ic_add_sender_text)
    TextView icSenderLightText;
    @BindView(R.id.ic_profile)
    ImageView ic_profile;
    @BindView(R.id.ic_profile_text)
    TextView ic_profile_text;


    HomePresenter homePresenter = null;
    DashBoardRequest dashBoardRequest;
    View view;
    int year;
    String month;
    String whichitem;
    AlertDialog alertDialog1;
    String[]monthName={"January","February","March", "April", "May", "June", "July",
            "August", "September", "October", "November",
            "December"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);
        ButterKnife.bind(this);
       // setSupportActionBar(toolbar);
        Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month =monthName[c.get(Calendar.MONTH)];
        String UserId =   UtilDmt.PREFERENCES.getUserIdDmt(this);
        String ParentUserId = UtilDmt.PREFERENCES.getParentUserId(this);
        String UserType = UtilDmt.PREFERENCES.getUserTypeDmt(this);
        dashBoardRequest = new DashBoardRequest();
        dashBoardRequest.setUserid(UserId);
        dashBoardRequest.setParentUserid(ParentUserId);
        dashBoardRequest.setUserType(UserType);
        homePresenter = new HomePresenter(this,getApplicationContext());
        homePresenter.geDashboardData(dashBoardRequest);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void noInternet() {

    }

    @Override
    public void exception(Throwable e) {

    }

    @Override
    public void fail(String message) {

    }

    @Override
    public void success(String message) {

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showHome(DashBoardResponse dashBoardResponse) {
        trxn_for_month.setText("TRANSACTIONS FOR"+" "+month.toUpperCase()+" "+year );
        txn_amount.setText(""+dashBoardResponse.getMonthlyTxnAmount());
        current_month_total_sender.setText("SENDERS FOR"+" "+month.toUpperCase()+" "+year );
        sender_count.setText(""+dashBoardResponse.getMontlyRemitterCount());
        current_month_refund.setText("REFUNDS FOR"+" "+month.toUpperCase()+" "+year );
        refund_amount.setText(""+dashBoardResponse.getMonthlyrefundTxnAmt());
        total_transaction_count.setText(""+dashBoardResponse.getOverallTxnAmt());
        total_sender_count.setText(""+dashBoardResponse.getOverallRemitterCount());
        total_refund_count.setText(""+dashBoardResponse.getOverallrefundTxnAmt());
    }
    @OnClick(R.id.add_sender)
    public void ic_sender_light() {
        final String[] values = {" Add","View"};
        AlertDialog.Builder builder = new AlertDialog.Builder(HomePageActivity.this);

        builder.setTitle("Select Your Choice");

        builder.setSingleChoiceItems(values, -1, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int item) {

                switch(item)
                {
                    case 0:
                        whichitem  = "Add";
                        break;
                    case 1:
                        whichitem = "View";
                        break;

                }
            }
        })
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        if(whichitem.equals("Add")){
                            Intent intent = new Intent(HomePageActivity.this, AddSenderActivity.class);
                            startActivity(intent);

                        }
                        else if(whichitem.equals("View")){
                            Intent intent = new Intent(HomePageActivity.this, ViewSenderActivity.class);
                            startActivity(intent);
                        }
                        else{

                        }
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        alertDialog1.dismiss();
                    }
                });

        alertDialog1 = builder.create();
        alertDialog1.show();
        alertDialog1.setCancelable(false);

    }
//    @OnClick(R.id.ic_report_light)
//    public void ic_report_light() {
//        final Intent intent = new Intent(HomePageActivity.this, ReportsActivity.class);
//
//        startActivity(intent);
//    }
    @OnClick(R.id.ic_profile)
    public void ic_profile() {
        final Intent intent = new Intent(HomePageActivity.this, ProfileActivity.class);
        startActivity(intent);
    }
    @OnClick(R.id.ic_setting_light)
    public void onClickSettings(View view) {
       /* Intent intent = new Intent(HomePageActivity.this, SettingsActivity.class);
        intent.putExtra(UtilDmt.KEYS.DATA, userProfile);
        startActivity(intent);*/
    }
    @OnClick(R.id.search)
    public void searchSender(){
    String searchSender = search_bar.getText().toString();
        if(searchSender.length() <10){
            UtilDmt.showShortToast(this,"Please enter 10 digit mobile number of sender");
        }
        else{
            Intent i = new Intent(HomePageActivity.this, ReceiverActivity.class);
            i.putExtra("MobileNumber", searchSender);
            startActivity(i);
        }
    }

}
