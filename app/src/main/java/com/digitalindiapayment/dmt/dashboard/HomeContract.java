package com.digitalindiapayment.dmt.dashboard;

import com.digitalindiapayment.mvp.BaseView;
import com.digitalindiapayment.webservices.dmt.requestpojo.DashBoardRequest;
import com.digitalindiapayment.webservices.dmt.responsepojo.DashBoardResponse;

/**
 * Created by Pinky on 11-04-2018.
 */

public class HomeContract {
    interface View extends BaseView {
        void showHome(DashBoardResponse dashBoardResponse);
    }

    interface UserActions {
        void geDashboardData(DashBoardRequest dashBoardRequest);
    }


}
