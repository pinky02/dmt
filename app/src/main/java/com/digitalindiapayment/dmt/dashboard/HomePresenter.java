package com.digitalindiapayment.dmt.dashboard;

import android.content.Context;

import com.digitalindiapayment.webservices.Api;
import com.digitalindiapayment.webservices.core.ApiFail;
import com.digitalindiapayment.webservices.core.ApiSuccess;
import com.digitalindiapayment.webservices.core.HttpErrorResponse;
import com.digitalindiapayment.webservices.dmt.requestpojo.DashBoardRequest;
import com.digitalindiapayment.webservices.dmt.responsepojo.DashBoardResponse;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Pinky on 11-04-2018.
 */

public class HomePresenter implements HomeContract.UserActions {

    private static final String TAG = "HomePresenter";
    private final HomeContract.View view;
    private final Context context;

    public HomePresenter(HomeContract.View view, Context context) {
        this.view = view;
        this.context = context;
    }


    @Override
    public void geDashboardData(DashBoardRequest dashBoardRequest) {view.showLoading();
        Api.userManagementDmt().dmtDashBoard(dashBoardRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ApiSuccess<DashBoardResponse>() {
                    @Override
                    public void call(DashBoardResponse dashBoardResponse) {
                        view.hideLoading();
                         view.showHome(dashBoardResponse);
                    }
                }, new ApiFail() {
                    @Override
                    public void httpStatus(HttpErrorResponse response) {
                        view.hideLoading();
                    }

                    @Override
                    public void noNetworkError() {
                        view.noInternet();
                    }

                    @Override
                    public void unknownError(Throwable e) {
                        view.fail(e.getMessage());
                    }
                });
    }
}
