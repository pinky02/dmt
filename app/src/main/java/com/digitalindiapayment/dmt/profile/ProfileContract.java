package com.digitalindiapayment.dmt.profile;

import com.digitalindiapayment.webservices.dmt.requestpojo.ProfileRequest;
import com.digitalindiapayment.webservices.dmt.responsepojo.ProfileResponse;
import com.digitalindiapayment.dmt.login.UserProfile;
import com.digitalindiapayment.mvp.BaseView;
import com.digitalindiapayment.webservices.dmt.requestpojo.DashBoardRequest;
import com.digitalindiapayment.webservices.dmt.requestpojo.ForgotPasswordRequest;
import com.digitalindiapayment.webservices.dmt.requestpojo.Login;
import com.digitalindiapayment.webservices.dmt.responsepojo.LoginResponse;

/**
 * Created by Pinky on 23-04-2018.
 */

public class ProfileContract {
    interface View extends BaseView {
        void showProfile(ProfileResponse profileResponse);
    }

    interface UserActions {
        void getUsersProfile(ProfileRequest profileRequest);
    }
}
