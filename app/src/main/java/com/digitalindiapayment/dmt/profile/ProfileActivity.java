package com.digitalindiapayment.dmt.profile;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.digitalindiapayment.BaseActivity;
import com.digitalindiapayment.R;
import com.digitalindiapayment.dmt.util.UtilDmt;
import com.digitalindiapayment.webservices.dmt.requestpojo.ProfileRequest;
import com.digitalindiapayment.webservices.dmt.responsepojo.ProfileResponse;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfileActivity extends BaseActivity implements ProfileContract.View {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.user_image)
    ImageView userImage;
    @BindView(R.id.userName)
    TextView userName;
    @BindView(R.id.userId)
    TextView userId;
    @BindView(R.id.tvName)
    TextView Name;
    @BindView(R.id.emailIdName)
    TextView emailIdName;
    @BindView(R.id.mobileNumber)
    TextView mobileNumber;
    @BindView(R.id.linearLayout)
    LinearLayout linearLayout;
    @BindView(R.id.noInternetContainer)
    LinearLayout noInternetContainer;
    @BindView(R.id.progressdialogdip)
    RelativeLayout progressdialogdip;
    @BindView(R.id.exception_handle)
    RelativeLayout exceptionHandle;
    @BindView(R.id.profile_screen)
    ScrollView profileScreen;
    @BindView(R.id.diplUserId)
    TextView diplUserId;
    @BindView(R.id.tvPanNo)
    TextView panNo;
    @BindView(R.id.tvCity)
    TextView city;
    @BindView(R.id.tvZone)
    TextView zone;
    @BindView(R.id.tvState)
    TextView state;
    ProfilePresenter profilePresenter = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_dmt);
        ButterKnife.bind(this);
        String UserId = UtilDmt.PREFERENCES.getUserIdDmt(this);
        ProfileRequest profileRequest = new ProfileRequest();
        profileRequest.setUserId(UserId);
        profilePresenter = new ProfilePresenter(this,getApplicationContext());
        profilePresenter.getUsersProfile(profileRequest);
    }

    @Override
    public void noInternet() {

    }

    @Override
    public void exception(Throwable e) {

    }

    @Override
    public void fail(String message) {

    }

    @Override
    public void success(String message) {

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showProfile(ProfileResponse profileResponse) {
    Name.setText(profileResponse.getName().toString());
    emailIdName.setText(profileResponse.getEmail().toString());
    mobileNumber.setText(profileResponse.getMobileno().toString());
    diplUserId.setText(profileResponse.getUserId().toString());
    panNo.setText(profileResponse.getPANNO().toString());
    city.setText(profileResponse.getCity().toString());
    zone.setText(profileResponse.getZone().toString());
    state.setText(profileResponse.getState().toString());
    }
}
