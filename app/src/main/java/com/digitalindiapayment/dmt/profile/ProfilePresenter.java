package com.digitalindiapayment.dmt.profile;

import android.content.Context;
import android.util.Log;

import com.digitalindiapayment.dmt.login.LoginContract;
import com.digitalindiapayment.webservices.Api;
import com.digitalindiapayment.webservices.core.ApiFail;
import com.digitalindiapayment.webservices.core.ApiSuccess;
import com.digitalindiapayment.webservices.core.HttpErrorResponse;
import com.digitalindiapayment.webservices.dmt.requestpojo.DashBoardRequest;
import com.digitalindiapayment.webservices.dmt.requestpojo.ProfileRequest;
import com.digitalindiapayment.webservices.dmt.responsepojo.LoginResponse;
import com.digitalindiapayment.webservices.dmt.responsepojo.ProfileResponse;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Pinky on 23-04-2018.
 */

public class ProfilePresenter implements ProfileContract.UserActions {
    private static final String TAG = "ProfilePresenter";
    private final ProfileContract.View view;
    private final Context context;

    public ProfilePresenter(ProfileContract.View view, Context context) {
        this.view = view;
        this.context = context;
    }

    @Override
    public void getUsersProfile(ProfileRequest profileRequest) {
        view.showLoading();
        //view.showHome(Util.PREFERENCES.getToken(context));
        // login.setMobile_auth_code(Util.PREFERENCES.getUniqueDeviceKey(context));
        Api.userManagementDmt().sendProfileRequest(profileRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ApiSuccess<ProfileResponse>() {
                    @Override
                    public void call(ProfileResponse profileResponse) {
                        Log.d(TAG, "subscribe > call");
                        view.hideLoading();
                        if(profileResponse.getStatus()==0){
                            view.fail(profileResponse.getDescription());
                        }
                        else if(profileResponse.getStatus()==1)
                            view.showProfile(profileResponse);

                    }
                }, new ApiFail() {
                    @Override
                    public void httpStatus(HttpErrorResponse response) {
                        view.hideLoading();
                        //  view.onResetSessionFail();
                    }

                    @Override
                    public void noNetworkError() {
                        view.noInternet();
                    }

                    @Override
                    public void unknownError(Throwable e) {
                        view.fail(e.getMessage());
                    }
                });
    }
}
