package com.digitalindiapayment.dmt.sender.view.receiver_details;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.digitalindiapayment.R;

import com.digitalindiapayment.dmt.sender.imps.ImpsActivity;
import com.digitalindiapayment.dmt.sender.neft.NeftActivity;
import com.digitalindiapayment.dmt.sender.validation.ReceiverAccountValidation;
import com.digitalindiapayment.dmt.sender.view.RecyclerViewOnClick;
import com.digitalindiapayment.util.Util;
import com.digitalindiapayment.webservices.dmt.responsepojo.Beneficiary;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Pinky on 20-05-2018.
 */

public class BeneficiaryAdapter extends RecyclerView.Adapter<BeneficiaryAdapter.SatementReportViewHolder> {

    private List<Beneficiary> transactions = null;
    private Context context;
    Boolean[] mExpanded;
    int position;
    private Beneficiary responseBean;
    private RecyclerViewOnClick recyclerViewOnClick;
    String m_strDetails;
    SatementReportViewHolder holder;
    String beneficiaryName,accountNo,ifsc,beneficiaryMobileNumber,beneficiaryId;
    String bcAgentId;

    public BeneficiaryAdapter(Context context,RecyclerViewOnClick recyclerViewOnClick){
        this.context = context;
        this.recyclerViewOnClick = recyclerViewOnClick;
    }

    @Override
    public SatementReportViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View ItemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.beneficiary_detail_row, parent, false);

        ItemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                recyclerViewOnClick.onClick(v);
            }
        });

        return new BeneficiaryAdapter.SatementReportViewHolder(ItemView);
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(final SatementReportViewHolder holder, final int position) {
        this.position= position;
        this.holder = holder;
        final Beneficiary transactionReportBean = transactions.get(position);
        int l_nLen = transactions.size();

        holder.beneficiaryId.setText(transactionReportBean.getBeneficiaryid().toString());
        holder.receiverName.setText(transactionReportBean.getBeneficiaryname().toString());
        holder.accountNo.setText(transactionReportBean.getAccountnumber().toString());
        holder.ifsc.setText(transactionReportBean.getIfscode().toString());
        if(transactionReportBean.getImpsstatus().toString().contains("0")){
         //   holder.registered.setBackgroundColor(0x55FF0000);
            holder.registered.setBackgroundDrawable(new ColorDrawable(Color
                    .parseColor("#4CAF50")));  //Green

            holder.validation.setText(R.string.validation);
            holder.validation.setBackgroundDrawable(new ColorDrawable(Color
                    .parseColor("#FF9800")));  //Yellow
            holder.imps.setBackgroundDrawable(new ColorDrawable(Color
                    .parseColor("#FFFFFF")));
            holder.imps.setTextColor(R.color.black);
            holder.neft.setBackgroundDrawable(new ColorDrawable(Color
                    .parseColor("#FFFFFF")));
            holder.neft.setTextColor(R.color.black);
            holder.imps.setMovementMethod(LinkMovementMethod.getInstance());
            holder.imps.setHighlightColor(Color.TRANSPARENT);
            holder.registered.setClickable(true);
            holder.imps.setClickable(false);
        }
        else{
            holder.registered.setBackgroundDrawable(new ColorDrawable(Color
                    .parseColor("#FFFFFF")));
            holder.registered.setTextColor(R.color.black);
            holder.validation.setText(R.string.done);
            holder.validation.setBackgroundDrawable(new ColorDrawable(Color
                    .parseColor("#4CAF50"))); //green
            holder.imps.setBackgroundDrawable(new ColorDrawable(Color
                    .parseColor("#4CAF50")));
            holder.neft.setBackgroundDrawable(new ColorDrawable(Color
                    .parseColor("#4CAF50")));
            holder.registered.setClickable(false);
            holder.imps.setClickable(true);
        }

//        holder.validation.setText(transactionReportBean.getKycstatus().toString());
//        holder.remainingLimit.setText(transactionReportBean.getRemaininglimit().toString());

        // holder.details.setText(m_strDetails);
        holder.validation.setTag(position);
        holder.imps.setTag(position);
        holder.neft.setTag(position);
        holder.validation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // toggle(position);
                final int tag = (int) holder.imps.getTag();
                final Beneficiary viewBeneficiaryBean = transactions.get(tag);
                  beneficiaryName = viewBeneficiaryBean.getBeneficiaryname().toString();
                  accountNo = viewBeneficiaryBean.getAccountnumber().toString();
                  ifsc = viewBeneficiaryBean.getIfscode().toString();
                beneficiaryMobileNumber = viewBeneficiaryBean.getBeneficiarymobilenumber().toString();
                beneficiaryId = viewBeneficiaryBean.getBeneficiaryid().toString();
                Intent intent = new Intent(context, ReceiverAccountValidation.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                // resetSession.putExtra(getString(R.string.mobilenumber), username.getText().toString());
                intent.putExtra("BeneficiaryName", beneficiaryName);
                intent.putExtra("accountNo", accountNo);
                intent.putExtra("ifsc", ifsc);
                intent.putExtra("BeneficiaryMobileNumber",beneficiaryMobileNumber);
                context.startActivity(intent);
            }
        });
        holder.imps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // toggle(position);
                final int tag = (int) holder.imps.getTag();
                final Beneficiary viewBeneficiaryBean = transactions.get(tag);
                beneficiaryName = viewBeneficiaryBean.getBeneficiaryname().toString();
                accountNo = viewBeneficiaryBean.getAccountnumber().toString();
                ifsc = viewBeneficiaryBean.getIfscode().toString();
                beneficiaryMobileNumber = viewBeneficiaryBean.getBeneficiarymobilenumber().toString();
                beneficiaryId = viewBeneficiaryBean.getBeneficiaryid().toString();
                Intent intent = new Intent(context, ImpsActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("BeneficiaryName", beneficiaryName);
                intent.putExtra("accountNo", accountNo);
                intent.putExtra("ifsc", ifsc);
                intent.putExtra("BeneficiaryMobileNumber",beneficiaryMobileNumber);
                intent.putExtra("beneficiaryId",beneficiaryId);
                context.startActivity(intent);
            }
        });
        holder.neft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // toggle(position);
                final int tag = (int) holder.imps.getTag();
                final Beneficiary viewBeneficiaryBean = transactions.get(tag);
                beneficiaryName = viewBeneficiaryBean.getBeneficiaryname().toString();
                accountNo = viewBeneficiaryBean.getAccountnumber().toString();
                ifsc = viewBeneficiaryBean.getIfscode().toString();
                beneficiaryMobileNumber = viewBeneficiaryBean.getBeneficiarymobilenumber().toString();
                beneficiaryId = viewBeneficiaryBean.getBeneficiaryid().toString();
                Intent intent = new Intent(context, NeftActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("BeneficiaryName", beneficiaryName);
                intent.putExtra("accountNo", accountNo);
                intent.putExtra("ifsc", ifsc);
                intent.putExtra("BeneficiaryMobileNumber",beneficiaryMobileNumber);
                intent.putExtra("beneficiaryId",beneficiaryId);
                context.startActivity(intent);
            }
        });

        holder.registered.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // toggle(position);
                final int tag = (int) holder.imps.getTag();
                final Beneficiary viewBeneficiaryBean = transactions.get(tag);
                if(viewBeneficiaryBean.getImpsstatus() == 0){
                    beneficiaryName = viewBeneficiaryBean.getBeneficiaryname().toString();
                    accountNo = viewBeneficiaryBean.getAccountnumber().toString();
                    ifsc = viewBeneficiaryBean.getIfscode().toString();
                    beneficiaryMobileNumber = viewBeneficiaryBean.getBeneficiarymobilenumber().toString();
                    beneficiaryId = viewBeneficiaryBean.getBeneficiaryid().toString();
                    Intent intent = new Intent(context, RegisterBeneficiary.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("beneficiaryId",beneficiaryId);
                    context.startActivity(intent);
                }
                else{
                    Util.showShortToast(context,"Beneficiary Already Register");
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return getTransactionsList() != null ? transactions.size() : 0;
    }

    public List <Beneficiary> getTransactionsList() {
        return transactions;
    }

    public void setTransactionsList(List <Beneficiary> transactions) {
        this.transactions = transactions;
        notifyDataSetChanged();
    }

    public class SatementReportViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txvBeneficiaryIdValue)
        TextView beneficiaryId;

        @BindView(R.id.txvReceiverNameValue)
        TextView receiverName;

        @BindView(R.id.txvAccountNumbervalue)
        TextView accountNo;

        @BindView(R.id.txvIFSCValue)
        TextView ifsc;

        @BindView(R.id.txvRegisterValue)
        TextView registered;

        @BindView(R.id.txvValidationValue)
        TextView validation;
        @BindView(R.id.txvImpsValue)
        TextView imps;
        @BindView(R.id.txvNEFTValue)
        TextView neft;
        @BindView(R.id.linearLayout)
        LinearLayout linearLayout;


        public SatementReportViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            beneficiaryId.setPaintFlags(beneficiaryId.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);
        }
    }
}
