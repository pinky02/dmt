package com.digitalindiapayment.dmt.sender.view;

import com.digitalindiapayment.mvp.BaseView;
import com.digitalindiapayment.webservices.dmt.requestpojo.DashBoardRequest;
import com.digitalindiapayment.webservices.dmt.requestpojo.Login;
import com.digitalindiapayment.webservices.dmt.requestpojo.ViewSenderRequest;
import com.digitalindiapayment.webservices.dmt.responsepojo.LoginResponse;
import com.digitalindiapayment.webservices.dmt.responsepojo.ViewSenderResponse;

import java.util.List;

/**
 * Created by Pinky on 24-04-2018.
 */

public class ViewSenderContract {
    public interface View extends BaseView {


        void show(List<ViewSenderBean> viewSenderBeans);

    }

    interface UserActions {

        void sendViewSenderRequest (ViewSenderRequest viewSenderBean);



    }

}
