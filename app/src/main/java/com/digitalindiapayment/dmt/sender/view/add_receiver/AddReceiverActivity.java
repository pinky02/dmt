package com.digitalindiapayment.dmt.sender.view.add_receiver;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.digitalindiapayment.R;

public class AddReceiverActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_receiver);
    }
}
