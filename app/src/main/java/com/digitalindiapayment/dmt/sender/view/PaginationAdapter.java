package com.digitalindiapayment.dmt.sender.view;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.digitalindiapayment.R;
import com.digitalindiapayment.dmt.sender.view.receiver_details.ReceiverActivity;



import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import android.view.View;


/**
 * Created by Pinky on 22-04-2018.
 */

public class PaginationAdapter extends RecyclerView.Adapter<PaginationAdapter.SatementReportViewHolder> {

    private List <ViewSenderBean> transactions = null;
    private Context context;
    Boolean[] mExpanded;
    int position;
    private ViewSenderBean responseBean;
    private RecyclerViewOnClick recyclerViewOnClick;
    String m_strDetails;
    SatementReportViewHolder holder;
    String senderMobileNumber;
    String bcAgentId;

//    public PaginationAdapter(Context context, RecyclerViewOnClick recyclerViewOnClick) {
//        this.context = context;
//        this.recyclerViewOnClick = recyclerViewOnClick;
//    }
    public PaginationAdapter(Context context,RecyclerViewOnClick recyclerViewOnClick){
        this.context = context;
        this.recyclerViewOnClick = recyclerViewOnClick;

    }

    @Override
    public SatementReportViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View ItemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.sender_details_row, parent, false);

        ItemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                recyclerViewOnClick.onClick(v);
            }
       });

        return new PaginationAdapter.SatementReportViewHolder(ItemView);
    }

    @Override
    public void onBindViewHolder(final SatementReportViewHolder holder, final int position) {
        this.position= position;
        this.holder = holder;
        final ViewSenderBean transactionReportBean = transactions.get(position);
        int l_nLen = transactions.size();
//        mExpanded = new Boolean[l_nLen];
//        for (int i = 0; i < l_nLen; i++) {
//            mExpanded[i] = false;
//        }

        holder.remitterId.setText(transactionReportBean.getRemitterid().toString());
        holder.senderName.setText(transactionReportBean.getRemittername().toString());
        holder.contactNo.setText(transactionReportBean.getRemittermobilenumber().toString());
        holder.city.setText(transactionReportBean.getCityname().toString());
        holder.pincode.setText(transactionReportBean.getPincode().toString());
        holder.kycStatus.setText(transactionReportBean.getKycstatus().toString());
        holder.remainingLimit.setText(transactionReportBean.getRemaininglimit().toString());

       // holder.details.setText(m_strDetails);
        holder.remitterId.setTag(position);
        holder.remitterId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // toggle(position);
                final int tag = (int) holder.remitterId.getTag();
                final ViewSenderBean viewSenderBean = transactions.get(tag);
                senderMobileNumber = viewSenderBean.getRemittermobilenumber().toString();

                //ViewSenderBean balanceResponse = new ViewSenderBean();
            //    balanceResponse.setRemittermobilenumber(lastFiveTransact.getId());
                Intent intent = new Intent(context, ReceiverActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
               // resetSession.putExtra(getString(R.string.mobilenumber), username.getText().toString());
                intent.putExtra("MobileNumber", senderMobileNumber);
                context.startActivity(intent);
            }
        });
        //holder.remainingLimit.setText(transactionReportBean.getRemaininglimit().toString());


       /* holder.transStatus.setText(transactionReportBean.getTransactionStatus());
        holder.amount_word.setText(Util.indianCurrencyRepresentation(transactionReportBean.getAmount()));*/

    }

    @Override
    public int getItemCount() {
        return getTransactionsList() != null ? transactions.size() : 0;
    }

    public List <ViewSenderBean> getTransactionsList() {
        return transactions;
    }

    public void setTransactionsList(List <ViewSenderBean> transactions) {
        this.transactions = transactions;
        notifyDataSetChanged();
    }

    public class SatementReportViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txvRemitterIdValue)
        TextView remitterId;

        @BindView(R.id.txvSenderNameValue)
        TextView senderName;

       @BindView(R.id.txvContactvalue)
        TextView contactNo;

        @BindView(R.id.txvCityValue)
        TextView city;

        @BindView(R.id.txvPincodeValue)
        TextView pincode;

        @BindView(R.id.txvKycStatusValue)
        TextView kycStatus;
        @BindView(R.id.txvRemainingValue)
        TextView remainingLimit;

        @BindView(R.id.linearLayout)
        LinearLayout linearLayout;


        public SatementReportViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            remitterId.setPaintFlags(remitterId.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);
        }
    }
    public void toggle(int position) {
      //  holder.details.setVisibility(View.VISIBLE);
        for (int i = 0; i < mExpanded.length; i++) {
            if (i != position)
                mExpanded[i] = false;
        }
        mExpanded[position] = !mExpanded[position];

        final ViewSenderBean transactionReportBean = transactions.get(position);
        m_strDetails = new String ();
        m_strDetails = "Contact No: " + transactionReportBean.getRemittermobilenumber().toString() + "\n"
                + "Pincode: " + transactionReportBean.getPincode().toString() + "\n" + "KYC Status: "
                + transactionReportBean.getKycstatus().toString() + "\n" + "Remaining Limit: "
                + transactionReportBean.getRemaininglimit().toString();

        setTransactionsList(transactions);

    }
}
