package com.digitalindiapayment.dmt.sender.add_sender;

import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.digitalindiapayment.BaseActivity;
import com.digitalindiapayment.R;
import com.digitalindiapayment.dmt.util.UtilDmt;
import com.digitalindiapayment.webservices.dmt.requestpojo.AddSenderRequest;
import com.digitalindiapayment.webservices.dmt.requestpojo.PincodeRequest;
import com.digitalindiapayment.webservices.dmt.responsepojo.PincodeResponse;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddSenderActivity extends BaseActivity implements AddSenderContract.View{
    @BindView(R.id.edtx_name)
    EditText edtx_name;
    @BindView(R.id.edtxv_mobilenum)
    EditText edtxv_mobilenum;
    @BindView(R.id.txv_address)
    TextView txv_address;
    @BindView(R.id.txv_city)
    TextView txv_city;
    @BindView(R.id.txv_state)
    TextView txv_state;
    String Value;
    EditText editText;
    AddSenderPresenter addSenderPresenter = null;
    String pincode;
    PincodeResponse pincodeRes;
    String BcAgent,SessionToken,City,State,Address;
    AddSenderRequest addSenderRequest;
    PincodeRequest pincodeRequest;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_sender);
        ButterKnife.bind(this);
        addSenderPresenter = new AddSenderPresenter(this, getApplicationContext());
        BcAgent = UtilDmt.PREFERENCES.getBCAgent(this);
        SessionToken = UtilDmt.PREFERENCES.getSessionToken(this);
        editText = (EditText) findViewById(R.id.edtxv_pincode);
        ((EditText) findViewById(R.id.edtxv_pincode)).addTextChangedListener(new TextWatcher() {
            String checkpincode = editText.getText().toString();
            int pincodelength = checkpincode.length();

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 6) {
                     pincode = editText.getText().toString();
                     pincodeRequest = new PincodeRequest();
                     pincodeRequest.setPincode(pincode);
                     addSenderPresenter.sendPincodeRequest(pincodeRequest);
                }
                else{
                    UtilDmt.showShortToast(getApplicationContext(),getString(R.string.enter_pin_code));
                }
            }
        });

    }
    @OnClick(R.id.btn_submit)
    public void sendPinCodeRequest() {
        if (edtx_name.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(getApplicationContext(), getString(R.string.pls_enter_user_name), Toast.LENGTH_SHORT).show();
            return;
        }
        if (edtxv_mobilenum.getText().toString().equalsIgnoreCase("")) {
            if (edtxv_mobilenum.getText().toString().length() < 10) {
                Toast.makeText(getApplicationContext(), getString(R.string.pls_enter_mobile_no), Toast.LENGTH_SHORT).show();
                return;
            }
        }
        addSenderRequest = new AddSenderRequest();
        addSenderRequest.setPincode(pincode);
        addSenderRequest.setLstatename(State);
        addSenderRequest.setIdproofissueplace("");
        addSenderRequest.setCityname(City);
        addSenderRequest.setRemitteraddress(Address);
        addSenderRequest.setLremitteraddress(Address);
        addSenderRequest.setStatename(State);
        addSenderRequest.setLcityname(City);
        addSenderRequest.setIdproofexpirydate("");
        addSenderRequest.setLpincode(pincode);
        addSenderRequest.setIdproofnumber("idproofnumber");
        addSenderRequest.setIdproofissuedate("");
        addSenderRequest.setAlternatenumber("9090000000");
        addSenderRequest.setRemitteraddress1(Address);
        addSenderRequest.setRemitteraddress2(Address);
        addSenderRequest.setIdproof("idproof");
        addSenderRequest.setRemittername(edtx_name.getText().toString());
        addSenderRequest.setBcagent(BcAgent);
        addSenderRequest.setSessiontoken(SessionToken);
        addSenderRequest.setRemittermobilenumber(edtxv_mobilenum.getText().toString());
        addSenderRequest.setUserId("2");
        addSenderPresenter.addSenderRequest(addSenderRequest);

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void noInternet() {

    }

    @Override
    public void exception(Throwable e) {

    }

    @Override
    public void fail(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
        disMissDialog();
    }

    @Override
    public void success(String message) {

    }

    @Override
    public void showLoading() {
        showDialog();
    }

    @Override
    public void hideLoading() {
        disMissDialog();
    }

    @Override
    public void showSuccessPincodeResponse(PincodeResponse pincodeResponse) {
        City = pincodeResponse.getCity().toString();
        State = pincodeResponse.getState().toString();
        Address = pincodeResponse.getPostalRegion().toString();
        txv_address.setText(pincodeResponse.getPostalRegion());
        txv_city.setText(pincodeResponse.getCity());
        txv_state.setText(pincodeResponse.getState());
    }

    @Override
    public void showFailurePincodeResponse() {
        txv_address.setText("NA");
        txv_city.setText("NA");
        txv_state.setText("NA");
        UtilDmt.showShortToast(this,"Entered Pincode is Not Available");
    }

    @Override
    public void showSuccessAddSenderResponse(String message) {
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View promptView = layoutInflater.inflate(R.layout.alert_dialog, null);

        final AlertDialog alertD = new AlertDialog.Builder(this).create();
        TextView txtTitle = (TextView) promptView.findViewById(R.id.title);
        txtTitle.setText("Status");
        TextView txtMessage = (TextView) promptView.findViewById(R.id.message);
        txtMessage.setText(message);
        Button btnCancel = (Button) promptView.findViewById(R.id.cancel);
        btnCancel.setVisibility(View.GONE);
        Button btnOk = (Button) promptView.findViewById(R.id.ok);

        btnOk.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onBackPressed();
                alertD.dismiss();
            }
        });

        alertD.setView(promptView);
        alertD.show();
        alertD.setCancelable(false);

    }

    @Override
    public void showFailureAddSenderResponse(String message) {

    }
}
