package com.digitalindiapayment.dmt.sender.view;

/**
 * Created by Pinky on 24-04-2018.
 */

public interface OnRecyclerViewItemClickListener {
    public void onRecyclerViewItemClicked(int position, int id);
}
