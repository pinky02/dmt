package com.digitalindiapayment.dmt.sender.view;

import android.view.View;

/**
 * Created by Mayur on 08-04-2017.
 */

public interface RecyclerViewOnClick {

    public void onClick(View view);
}
