package com.digitalindiapayment.dmt.sender.view.receiver_details;

import android.content.Context;


import com.digitalindiapayment.webservices.Api;
import com.digitalindiapayment.webservices.core.ApiFail;
import com.digitalindiapayment.webservices.core.ApiSuccess;
import com.digitalindiapayment.webservices.core.HttpErrorResponse;
import com.digitalindiapayment.webservices.dmt.requestpojo.BeneficiaryValidation;
import com.digitalindiapayment.webservices.dmt.requestpojo.BeneficiaryValidationOtp;
import com.digitalindiapayment.webservices.dmt.requestpojo.SearchSenderRequest;
import com.digitalindiapayment.webservices.dmt.responsepojo.BeneficiaryValidationOtpResponse;
import com.digitalindiapayment.webservices.dmt.responsepojo.BeneficiaryValidationResponse;
import com.digitalindiapayment.webservices.dmt.responsepojo.Remitterdetail;
import com.digitalindiapayment.webservices.dmt.responsepojo.SearchSenderResponse;


import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Pinky on 01-05-2018.
 */

public class ReceiverPresenter implements ReceiverContract.UserActions{
    private final ReceiverContract.View view;
    private final Context context;

    public ReceiverPresenter(ReceiverContract.View view, Context context) {
        this.view = view;
        this.context = context;
    }

    @Override
    public void sendSearchSenderRequest(SearchSenderRequest searchSenderRequest) {
        view.showLoading();

        Api.userManagementDmt().searchSenderRequest(searchSenderRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ApiSuccess<SearchSenderResponse>() {
                    @Override
                    public void call(SearchSenderResponse searchSenderResponse) {
                        view.hideLoading();
                        Remitterdetail remitterdetail = new Remitterdetail();
                        remitterdetail =   searchSenderResponse.getResponse().getRemitterdetail();
                       // Log.d("Hii", String.valueOf(searchSenderResponse));
                        view.show(remitterdetail,searchSenderResponse.getResponse().getBeneficiarydetail().getBeneficiary());

                    }
                }, new ApiFail() {
                    @Override
                    public void httpStatus(HttpErrorResponse response) {
                        view.hideLoading();
                    }

                    @Override
                    public void noNetworkError() {
                        view.noInternet();
                    }

                    @Override
                    public void unknownError(Throwable e) {
                        view.fail(e.getMessage());
                    }
                });
    }

    @Override
    public void sendOtpforBeneficiaryValidation(BeneficiaryValidationOtp beneficiaryValidationOtp) {
        view.showLoading();

        Api.userManagementDmt().validateBeneficicaryOtp(beneficiaryValidationOtp)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ApiSuccess<BeneficiaryValidationOtpResponse>() {
                    @Override
                    public void call(BeneficiaryValidationOtpResponse beneficiaryValidationOtpResponse) {
                        view.hideLoading();

                       // view.otpresponse();

                    }
                }, new ApiFail() {
                    @Override
                    public void httpStatus(HttpErrorResponse response) {
                        view.hideLoading();
                    }

                    @Override
                    public void noNetworkError() {
                        view.noInternet();
                    }

                    @Override
                    public void unknownError(Throwable e) {
                        view.fail(e.getMessage());
                    }
                });
    }

    @Override
    public void validateBeneficiary(BeneficiaryValidation beneficiaryValidation) {
        Api.userManagementDmt().beneficiaryValidation(beneficiaryValidation)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ApiSuccess<BeneficiaryValidationResponse>() {
                    @Override
                    public void call(BeneficiaryValidationResponse beneficiaryValidationResponse) {
                        view.hideLoading();
                        String messasge = beneficiaryValidationResponse.getDescription().toString();
                        view.validationResponse(messasge);

                    }
                }, new ApiFail() {
                    @Override
                    public void httpStatus(HttpErrorResponse response) {
                        view.hideLoading();
                    }

                    @Override
                    public void noNetworkError() {
                        view.noInternet();
                    }

                    @Override
                    public void unknownError(Throwable e) {
                        view.fail(e.getMessage());
                    }
                });
    }

//    @Override
//    public void sendSearchSenderRequest(SearchSenderRequest searchSenderRequest) {
//        view.showLoading();
//        Api.userManagementDmt().searchSenderRequest(searchSenderRequest)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new ApiSuccess<SearchSenderResponse>() {
//                    @Override
//                    public void call(SearchSenderResponse viewSenderResponse) {
//                        view.hideLoading();
//                        viewSenderResponse.getResponse();
//                        Toast.makeText(context,"Hiii",Toast.LENGTH_SHORT).show();
//                      //  view.show(viewSenderResponse);
//
//                    }
//                }, new ApiFail() {
//                    @Override
//                    public void httpStatus(HttpErrorResponse response) {
//                        view.hideLoading();
//                        //  view.onResetSessionFail();
//                    }
//
//                    @Override
//                    public void noNetworkError() {
//                        view.noInternet();
//                    }
//
//                    @Override
//                    public void unknownError(Throwable e) {
//                        view.fail(e.getMessage());
//                    }
//                });
//        // return Api.userManagementDmt().dmtDashBoard(dmtDashBoardRequest);
//
//    }
}
