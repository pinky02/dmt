package com.digitalindiapayment.dmt.sender.add_sender;

import com.digitalindiapayment.dmt.login.UserProfile;
import com.digitalindiapayment.mvp.BaseView;
import com.digitalindiapayment.webservices.dmt.requestpojo.AddSenderRequest;
import com.digitalindiapayment.webservices.dmt.requestpojo.DashBoardRequest;
import com.digitalindiapayment.webservices.dmt.requestpojo.PincodeRequest;
import com.digitalindiapayment.webservices.dmt.responsepojo.LoginResponse;
import com.digitalindiapayment.webservices.dmt.responsepojo.PincodeResponse;

/**
 * Created by Pinky on 20-04-2018.
 */

public class AddSenderContract {

    interface View extends BaseView {

        void showSuccessPincodeResponse(PincodeResponse pincodeResponse);

        void showFailurePincodeResponse();

        void showSuccessAddSenderResponse(String message);
        void showFailureAddSenderResponse(String message);



    }

    interface UserActions {

        void sendPincodeRequest(PincodeRequest pincodeRequest);
        void addSenderRequest(AddSenderRequest addSenderRequest);

    }
}
