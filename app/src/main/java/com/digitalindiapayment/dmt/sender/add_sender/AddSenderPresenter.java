package com.digitalindiapayment.dmt.sender.add_sender;

import android.content.Context;

import com.digitalindiapayment.dmt.dashboard.HomeContract;
import com.digitalindiapayment.webservices.Api;
import com.digitalindiapayment.webservices.core.ApiFail;
import com.digitalindiapayment.webservices.core.ApiSuccess;
import com.digitalindiapayment.webservices.core.HttpErrorResponse;
import com.digitalindiapayment.webservices.dmt.requestpojo.AddSenderRequest;
import com.digitalindiapayment.webservices.dmt.requestpojo.PincodeRequest;
import com.digitalindiapayment.webservices.dmt.responsepojo.AddSenderResponse;
import com.digitalindiapayment.webservices.dmt.responsepojo.DashBoardResponse;
import com.digitalindiapayment.webservices.dmt.responsepojo.PincodeResponse;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Pinky on 20-04-2018.
 */

public class AddSenderPresenter implements AddSenderContract.UserActions{
    private static final String TAG = "HomePresenter";
    private final AddSenderContract.View view;
    private final Context context;

    public AddSenderPresenter(AddSenderContract.View view, Context context) {
        this.view = view;
        this.context = context;
    }


    @Override
    public void sendPincodeRequest(PincodeRequest pincodeRequest) {
        Api.userManagementDmt().pincodeRequest(pincodeRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ApiSuccess<PincodeResponse>() {
                    @Override
                    public void call(PincodeResponse pincodeResponse) {
                       if( pincodeResponse.getStatus().equals("1")) {
                           view.hideLoading();
                           view.showSuccessPincodeResponse(pincodeResponse);
                       }
                       else{
                           view.hideLoading();
                           view.showFailurePincodeResponse();
                       }
                    }
                }, new ApiFail() {
                    @Override
                    public void httpStatus(HttpErrorResponse response) {
                        view.hideLoading();
                    }

                    @Override
                    public void noNetworkError() {
                        view.noInternet();
                    }

                    @Override
                    public void unknownError(Throwable e) {
                        view.fail(e.getMessage());
                    }
                });
    }

    @Override
    public void addSenderRequest(AddSenderRequest addSenderRequest) {view.showLoading();
        Api.userManagementDmt().addRemitterRequest(addSenderRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ApiSuccess<AddSenderResponse>() {
                    @Override
                    public void call(AddSenderResponse addSenderResponse) {
                        if(addSenderResponse.getStatus().equals("1")) {
                            view.hideLoading();
                            view.showSuccessAddSenderResponse(addSenderResponse.getMessage());
                        }
                        else{
                            view.hideLoading();
                            view.fail("Unable to Process");
                        }

                    }
                }, new ApiFail() {
                    @Override
                    public void httpStatus(HttpErrorResponse response) {
                        view.hideLoading();
                    }

                    @Override
                    public void noNetworkError() {
                        view.noInternet();
                    }

                    @Override
                    public void unknownError(Throwable e) {
                        view.fail(e.getMessage());
                    }
                });

    }
}
