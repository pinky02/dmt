package com.digitalindiapayment.dmt.sender.imps;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.digitalindiapayment.BaseActivity;
import com.digitalindiapayment.R;
import com.digitalindiapayment.dmt.util.UtilDmt;
import com.digitalindiapayment.receipt.BankTransactionWebView;
import com.digitalindiapayment.util.Util;
import com.digitalindiapayment.webservices.dmt.responsepojo.TransactionResponse;
import com.digitalindiapayment.webservices.requestpojo.TransactionRequest;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ImpsActivity extends BaseActivity implements ImpsContract.View{
    String BeneficiaryName, AccountNo, Ifsc,BeneficiaryMobileNumber,BeneficiaryId;
    @BindView(R.id.txv_name)
    TextView name;
    @BindView(R.id.txv_ifsc)
    TextView ifsc;
    @BindView(R.id.txv_accountNumber)
    TextView accountNmuber;
    @BindView(R.id.edtxv_amount)
    EditText amount;
    @BindView(R.id.btn_submit)
    Button btn_submit;

    ImpsPresenter impsPresenter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imps);
        ButterKnife.bind(this);
        impsPresenter = new ImpsPresenter(this,getApplicationContext());
        Intent i = getIntent();
        BeneficiaryName = i.getStringExtra("BeneficiaryName");
        AccountNo = i.getStringExtra("accountNo");
        Ifsc =  i.getStringExtra("ifsc");
        BeneficiaryMobileNumber= i.getStringExtra("BeneficiaryMobileNumber");
        BeneficiaryId= i.getStringExtra("beneficiaryId");
        name.setText(BeneficiaryName);
        ifsc.setText(Ifsc);
        accountNmuber.setText(AccountNo);
    }
    @Override
    public void onResume(){
        super.onResume();

    }
    @Override
    public void noInternet() {

    }

    @Override
    public void exception(Throwable e) {

    }

    @Override
    public void fail(String message) {

    }

    @Override
    public void success(String message) {

    }

    @Override
    public void showLoading() {
        showDialog();
    }

    @Override
    public void hideLoading() {
        disMissDialog();
    }

    @Override
    public void show(TransactionResponse transactionResponse) {
        disMissDialog();
        Intent intent = new Intent(this, BankTransactionWebView.class);
        intent.putExtra("DmtTransactionId", transactionResponse.getTransactionGroupId().toString());
        startActivity(intent);
        finish();
    }

    @Override
    public void transactionFail(String message) {

        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View promptView = layoutInflater.inflate(R.layout.alert_dialog, null);

        final AlertDialog alertD = new AlertDialog.Builder(this).create();
        TextView txtTitle = (TextView) promptView.findViewById(R.id.title);
        txtTitle.setText("Status");
        TextView txtMessage = (TextView) promptView.findViewById(R.id.message);
        txtMessage.setText(message);
        Button btnCancel = (Button) promptView.findViewById(R.id.cancel);
        btnCancel.setVisibility(View.GONE);
        Button btnOk = (Button) promptView.findViewById(R.id.ok);

        btnOk.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onBackPressed();
                alertD.dismiss();
            }
        });

        alertD.setView(promptView);
        alertD.show();
        alertD.setCancelable(false);

    }

    @OnClick(R.id.btn_submit)
        public void sendImpsRequest(){
        String Amount = amount.getText().toString();
        String sessionToken = UtilDmt.PREFERENCES.getSessionToken(this);
        String bcAgent = UtilDmt.PREFERENCES.getBCAgent(this);
        String remitterId = UtilDmt.PREFERENCES.getRemitterId(this);
        String beneficiaryId =  BeneficiaryId;
        String ifsc = Ifsc;
        String userId = UtilDmt.PREFERENCES.getUserIdDmt(this);
        TransactionRequest transactionRequest = new TransactionRequest();
        if (amount.getText().length() == 0) {
            Toast.makeText(getApplicationContext(), getString(R.string.pls_enter_amount), Toast.LENGTH_SHORT).show();
            return;
        }
        transactionRequest.setSessiontoken(sessionToken);
        transactionRequest.setBcagent(bcAgent);
        transactionRequest.setRemitterid(remitterId);
        transactionRequest.setBeneficiaryid(beneficiaryId);
        transactionRequest.setAmount(Amount);
        transactionRequest.setRemarks("testData");
        transactionRequest.setCpid("136");
        transactionRequest.setChannelpartnerrefno(bcAgent);
        transactionRequest.setFlag("2");
        transactionRequest.setUserid(userId);
        transactionRequest.setProcessType("1");
        transactionRequest.setReqType("W");
        transactionRequest.setIfscCode(ifsc);

        impsPresenter.sendImpsRequest(transactionRequest);

    }
}
