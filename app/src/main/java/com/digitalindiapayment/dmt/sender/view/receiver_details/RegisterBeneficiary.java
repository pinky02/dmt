package com.digitalindiapayment.dmt.sender.view.receiver_details;

import android.app.Service;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.digitalindiapayment.R;
import com.digitalindiapayment.dmt.util.UtilDmt;
import com.digitalindiapayment.util.Util;
import com.digitalindiapayment.webservices.dmt.requestpojo.BeneficiaryValidation;
import com.digitalindiapayment.webservices.dmt.requestpojo.BeneficiaryValidationOtp;
import com.digitalindiapayment.webservices.dmt.requestpojo.SearchSenderRequest;
import com.digitalindiapayment.webservices.dmt.responsepojo.Beneficiary;
import com.digitalindiapayment.webservices.dmt.responsepojo.Remitterdetail;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegisterBeneficiary extends AppCompatActivity implements ReceiverContract.View{
    @BindView(R.id.pin_first_edittext)
    EditText mPinFirstDigitEditText;

    @BindView(R.id.btnsubmit)
    Button btnsubmit;

    ReceiverPresenter receiverPresenter = null;
    private View view;
    String BeneficiaryId;
    BeneficiaryValidationOtp validationOtp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_beneficiary);
        ButterKnife.bind(this);
        Intent i = getIntent();
        BeneficiaryId = i.getStringExtra("beneficiaryId");
        receiverPresenter = new ReceiverPresenter(this,getApplicationContext());
        BeneficiaryValidationOtp beneficiaryValidation =  new BeneficiaryValidationOtp();
        String sessionToken = UtilDmt.PREFERENCES.getSessionToken(this);
        String remitterId = UtilDmt.PREFERENCES.getRemitterId(this);
        beneficiaryValidation.setSessiontoken(sessionToken);
        beneficiaryValidation.setRemitterid(remitterId);
        beneficiaryValidation.setBeneficiaryid(BeneficiaryId);
       // validationOtp = beneficiaryValidation;
        receiverPresenter.sendOtpforBeneficiaryValidation(beneficiaryValidation);
    }




    public void hideSoftKeyboard(EditText editText) {
        if (editText == null)
            return;

        InputMethodManager imm = (InputMethodManager) getSystemService(Service.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }


    @Override
    public void noInternet() {

    }

    @Override
    public void exception(Throwable e) {

    }

    @Override
    public void fail(String message) {

    }

    @Override
    public void success(String message) {

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void show(Remitterdetail remitterdetail, List<Beneficiary> beneficiaries) {

    }

    @Override
    public void validationResponse(String message) {

        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View promptView = layoutInflater.inflate(R.layout.alert_dialog, null);

        final AlertDialog alertD = new AlertDialog.Builder(this).create();
        TextView txtTitle = (TextView) promptView.findViewById(R.id.title);
        txtTitle.setText("Status");
        TextView txtMessage = (TextView) promptView.findViewById(R.id.message);
        txtMessage.setText(message);
        Button btnCancel = (Button) promptView.findViewById(R.id.cancel);
        btnCancel.setVisibility(View.GONE);
        Button btnOk = (Button) promptView.findViewById(R.id.ok);

        btnOk.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onBackPressed();
                alertD.dismiss();
            }
        });

        alertD.setView(promptView);
        alertD.show();
        alertD.setCancelable(false);

    }


    @OnClick(R.id.btnsubmit)
        public void sendOtpValidation(){
        String sessionToken = UtilDmt.PREFERENCES.getSessionToken(this);
        String remitterId = UtilDmt.PREFERENCES.getRemitterId(this);
        String verificationCode = mPinFirstDigitEditText.getText().toString();
        String beneficiaryId = BeneficiaryId;
        BeneficiaryValidation beneficiaryValidation = new BeneficiaryValidation();
        beneficiaryValidation.setSessiontoken(sessionToken);
        beneficiaryValidation.setBeneficiaryid(beneficiaryId);
        beneficiaryValidation.setRemitterid(remitterId);
        beneficiaryValidation.setVerficationcode(verificationCode);
        receiverPresenter.validateBeneficiary(beneficiaryValidation);
        }
}
