package com.digitalindiapayment.dmt.sender.imps;

import android.content.Context;


import com.digitalindiapayment.webservices.Api;
import com.digitalindiapayment.webservices.core.ApiFail;
import com.digitalindiapayment.webservices.core.ApiSuccess;
import com.digitalindiapayment.webservices.core.HttpErrorResponse;
import com.digitalindiapayment.webservices.dmt.responsepojo.TransactionResponse;
import com.digitalindiapayment.webservices.requestpojo.TransactionRequest;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Pinky on 26-06-2018.
 */

public class ImpsPresenter implements ImpsContract.UserActions{

    private final ImpsContract.View view;
    private final Context context;

    public ImpsPresenter(ImpsContract.View view, Context context) {
        this.view = view;
        this.context = context;
    }
    @Override
    public void sendImpsRequest(TransactionRequest transactionRequest) {
        view.showLoading();
        Api.userManagementDmt().sendTransactionRequest(transactionRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ApiSuccess<TransactionResponse>() {
                    @Override
                    public void call(TransactionResponse transactionResponse) {
                      if( transactionResponse.getStatus()==0) {
                          view.hideLoading();
                          view.transactionFail(transactionResponse.getDescription());
                      }
                      else{
                          view.show(transactionResponse);
                      }

                    }
                }, new ApiFail() {
                    @Override
                    public void httpStatus(HttpErrorResponse response) {
                        view.hideLoading();
                    }

                    @Override
                    public void noNetworkError() {
                        view.hideLoading();
                        view.noInternet();
                    }

                    @Override
                    public void unknownError(Throwable e) {
                        view.hideLoading();
                        view.fail(e.getMessage());
                    }
                });
    }
}
