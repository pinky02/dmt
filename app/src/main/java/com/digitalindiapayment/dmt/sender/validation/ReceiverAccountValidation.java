package com.digitalindiapayment.dmt.sender.validation;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.digitalindiapayment.BaseActivity;
import com.digitalindiapayment.R;
import com.digitalindiapayment.dmt.util.UtilDmt;
import com.digitalindiapayment.util.Util;
import com.digitalindiapayment.webservices.dmt.requestpojo.BeneficiaryAccountValidationRequest;
import com.digitalindiapayment.webservices.dmt.requestpojo.IfscCodeRequest;
import com.digitalindiapayment.webservices.dmt.responsepojo.BeneficiaryAccountValidationResponse;
import com.digitalindiapayment.webservices.dmt.responsepojo.IfscCodeResponse;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ReceiverAccountValidation extends BaseActivity implements ValidationContract.View {
    @BindView(R.id.txvBeneficiaryIdValue)
    EditText beneficiaryName;
    @BindView(R.id.account_no)
    EditText account_no;
    @BindView(R.id.txvIFSCValue)
    EditText ifsc;
    @BindView(R.id.bank_name)
    EditText bankName;
    @BindView(R.id.txvbranch)
    EditText branch;
    @BindView(R.id.submit)
    Button submit;
    String BeneficiaryName, AccountNo, Ifsc,BeneficiaryMobileNumber;
    ValidationPresenter validationPresenter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receiver_account_validation);
        ButterKnife.bind(this);
        validationPresenter = new ValidationPresenter(this, getApplicationContext());
        Intent i = getIntent();
        BeneficiaryName = i.getStringExtra("BeneficiaryName");
        AccountNo = i.getStringExtra("accountNo");
        Ifsc =  i.getStringExtra("ifsc");
        BeneficiaryMobileNumber= i.getStringExtra("BeneficiaryMobileNumber");
        beneficiaryName.setText(BeneficiaryName);
        account_no.setText(AccountNo);
        ifsc.setText(Ifsc);
        IfscCodeRequest ifscCodeRequest = new IfscCodeRequest();
        ifscCodeRequest.setIfscCode(Ifsc);
        validationPresenter.sendIfscCodeRequest(ifscCodeRequest);
        
    }

    @Override
    public void noInternet() {

    }

    @Override
    public void exception(Throwable e) {

    }

    @Override
    public void fail(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
        disMissDialog();
    }

    @Override
    public void success(String message) {

    }

    @Override
    public void showLoading() {
        showDialog();
    }

    @Override
    public void hideLoading() {
        disMissDialog();
    }
    @OnClick(R.id.submit)
    public void accountValidation(){
            String sessionToken = UtilDmt.PREFERENCES.getSessionToken(this);
            String bcAgent = UtilDmt.PREFERENCES.getBCAgent(this);
            String remitterId = UtilDmt.PREFERENCES.getRemitterId(this);
            String beneficiaryMobileNumber = BeneficiaryMobileNumber;
            String beneficiaryName = BeneficiaryName;
            String accountNumber = AccountNo;
            String ifsc = Ifsc;
            String channelPartnerInfo = UtilDmt.PREFERENCES.getBCAgent(this);
        BeneficiaryAccountValidationRequest beneficiaryAccountValidationRequest = new BeneficiaryAccountValidationRequest();
        beneficiaryAccountValidationRequest.setSessiontoken(sessionToken);
        beneficiaryAccountValidationRequest.setBcagent(bcAgent);
        beneficiaryAccountValidationRequest.setRemitterid(remitterId);
        beneficiaryAccountValidationRequest.setBeneficiaryname(beneficiaryName);
        beneficiaryAccountValidationRequest.setBeneficiarymobilenumber(beneficiaryMobileNumber);
        beneficiaryAccountValidationRequest.setAccountnumber(accountNumber);
        beneficiaryAccountValidationRequest.setIfscode(ifsc);
        beneficiaryAccountValidationRequest.setChannelpartnerrefno(channelPartnerInfo);
        beneficiaryAccountValidationRequest.setUserid("1");
        validationPresenter.beneficiaryAccountValidation(beneficiaryAccountValidationRequest);
    }
    @Override
    public void show(IfscCodeResponse ifscCodeResponse) {
        bankName.setText(ifscCodeResponse.getBankName());
        branch.setText(ifscCodeResponse.getBankBranch());
    }

    @Override
    public void showBeneficiaryAccountValidationResponse(BeneficiaryAccountValidationResponse beneficiaryAccountValidationResponse) {
        String message = beneficiaryAccountValidationResponse.getDescription().toString();
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View promptView = layoutInflater.inflate(R.layout.alert_dialog, null);

        final AlertDialog alertD = new AlertDialog.Builder(this).create();
        TextView txtTitle = (TextView) promptView.findViewById(R.id.title);
        txtTitle.setText("Status");
        TextView txtMessage = (TextView) promptView.findViewById(R.id.message);
        txtMessage.setText(message);
        Button btnCancel = (Button) promptView.findViewById(R.id.cancel);
        btnCancel.setVisibility(View.GONE);
        Button btnOk = (Button) promptView.findViewById(R.id.ok);

        btnOk.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onBackPressed();
                alertD.dismiss();
            }
        });

        alertD.setView(promptView);
        alertD.show();
        alertD.setCancelable(false);

    }
}
