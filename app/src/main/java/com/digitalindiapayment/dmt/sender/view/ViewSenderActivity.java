package com.digitalindiapayment.dmt.sender.view;


import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.view.View;
import android.widget.Toast;

import com.digitalindiapayment.BaseActivity;
import com.digitalindiapayment.R;
import com.digitalindiapayment.dmt.util.UtilDmt;
import com.digitalindiapayment.webservices.dmt.requestpojo.ViewSenderRequest;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ViewSenderActivity extends BaseActivity implements ViewSenderContract.View, RecyclerViewOnClick {

    @BindView(R.id.multilevelinfolist)
    RecyclerView recyclerviewStatement;

    private PaginationAdapter paginationAdapter = null;
    String[] dataset = {"one,two,three"};
    ViewSenderPresenter viewSenderPresenter = null;
    private List<ViewSenderBean> transactions = new ArrayList<ViewSenderBean>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_sender_master);
        ButterKnife.bind(this);

        viewSenderPresenter = new ViewSenderPresenter(this, getApplicationContext());
        String bc_agent = UtilDmt.PREFERENCES.getBCAgent(this);
        ViewSenderRequest viewSenderRequest = new ViewSenderRequest();
        viewSenderRequest.setBcAgentId(bc_agent);
        viewSenderPresenter.sendViewSenderRequest(viewSenderRequest);
    }

    @Override
    public void noInternet() {
        Toast.makeText(getApplicationContext(), "No internet", Toast.LENGTH_LONG).show();
        disMissDialog();
    }

    @Override
    public void exception(Throwable e) {

    }

    @Override
    public void fail(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
        disMissDialog();
    }

    @Override
    public void success(String message) {

    }

    @Override
    public void showLoading() {
        showDialog();
    }

    @Override
    public void hideLoading() {
        disMissDialog();
    }

    @Override
    public void show(List<ViewSenderBean> viewSenderBeanList) {

        paginationAdapter = new PaginationAdapter(getApplicationContext(), this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerviewStatement.setLayoutManager(mLayoutManager);
        recyclerviewStatement.setItemAnimator(new DefaultItemAnimator());
        recyclerviewStatement.setAdapter(paginationAdapter);
        this.transactions = viewSenderBeanList;
        paginationAdapter.setTransactionsList(viewSenderBeanList);
        }

    @Override
    public void onClick(View view) {
        int itemPosition = recyclerviewStatement.getChildLayoutPosition(view);
       ((PaginationAdapter) recyclerviewStatement.getAdapter()).toggle(itemPosition);
        Toast.makeText(this,"Clicked",Toast.LENGTH_SHORT).show();
    }

}

