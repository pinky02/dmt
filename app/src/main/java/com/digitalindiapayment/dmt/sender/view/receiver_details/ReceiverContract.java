package com.digitalindiapayment.dmt.sender.view.receiver_details;

import com.digitalindiapayment.mvp.BaseView;
import com.digitalindiapayment.dmt.sender.view.ViewSenderBean;
import com.digitalindiapayment.webservices.dmt.requestpojo.BeneficiaryValidation;
import com.digitalindiapayment.webservices.dmt.requestpojo.BeneficiaryValidationOtp;
import com.digitalindiapayment.webservices.dmt.requestpojo.SearchSenderRequest;
import com.digitalindiapayment.webservices.dmt.requestpojo.ViewSenderRequest;
import com.digitalindiapayment.webservices.dmt.responsepojo.Beneficiary;
import com.digitalindiapayment.webservices.dmt.responsepojo.Remitterdetail;

import java.util.List;

/**
 * Created by Pinky on 01-05-2018.
 */

public class ReceiverContract {
    public interface View extends BaseView {
        void show(Remitterdetail remitterdetail,List<Beneficiary> beneficiaries);

        void validationResponse(String message);

    }

    interface UserActions {
        void sendSearchSenderRequest (SearchSenderRequest searchSenderRequest);

        void sendOtpforBeneficiaryValidation(BeneficiaryValidationOtp beneficiaryValidationOtp);

        void validateBeneficiary(BeneficiaryValidation beneficiaryValidation);
    }
}
