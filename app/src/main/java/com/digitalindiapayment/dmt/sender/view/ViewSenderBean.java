package com.digitalindiapayment.dmt.sender.view;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Pinky on 22-04-2018.
 */

public class ViewSenderBean {
    @SerializedName("lstatename")
    @Expose
    private String lstatename;
    @SerializedName("idproofissueplace")
    @Expose
    private String idproofissueplace;
    @SerializedName("remitterid")
    @Expose
    private Integer remitterid;
    @SerializedName("remitter_code")
    @Expose
    private Integer remitterCode;
    @SerializedName("lcityname")
    @Expose
    private String lcityname;
    @SerializedName("idproofexpirydate")
    @Expose
    private String idproofexpirydate;
    @SerializedName("idproofnumber")
    @Expose
    private String idproofnumber;
    @SerializedName("remitteraddress1")
    @Expose
    private String remitteraddress1;
    @SerializedName("remitteraddress2")
    @Expose
    private String remitteraddress2;
    @SerializedName("remittername")
    @Expose
    private String remittername;
    @SerializedName("pincode")
    @Expose
    private String pincode;
    @SerializedName("cityname")
    @Expose
    private String cityname;
    @SerializedName("lremitteraddress")
    @Expose
    private String lremitteraddress;
    @SerializedName("statename")
    @Expose
    private String statename;
    @SerializedName("consumedlimit")
    @Expose
    private String consumedlimit;
    @SerializedName("createdon")
    @Expose
    private String createdon;
    @SerializedName("kycremarks")
    @Expose
    private String kycremarks;
    @SerializedName("remaininglimit")
    @Expose
    private String remaininglimit;
    @SerializedName("lpincode")
    @Expose
    private String lpincode;
    @SerializedName("idproofissuedate")
    @Expose
    private String idproofissuedate;
    @SerializedName("alternatenumber")
    @Expose
    private String alternatenumber;
    @SerializedName("kycstatus")
    @Expose
    private Integer kycstatus;
    @SerializedName("bc_agent_id")
    @Expose
    private String bcAgentId;
    @SerializedName("idproof")
    @Expose
    private String idproof;
    @SerializedName("remittermobilenumber")
    @Expose
    private String remittermobilenumber;

    public String getLstatename() {
        return lstatename;
    }

    public void setLstatename(String lstatename) {
        this.lstatename = lstatename;
    }

    public String getIdproofissueplace() {
        return idproofissueplace;
    }

    public void setIdproofissueplace(String idproofissueplace) {
        this.idproofissueplace = idproofissueplace;
    }

    public Integer getRemitterid() {
        return remitterid;
    }

    public void setRemitterid(Integer remitterid) {
        this.remitterid = remitterid;
    }

    public Integer getRemitterCode() {
        return remitterCode;
    }

    public void setRemitterCode(Integer remitterCode) {
        this.remitterCode = remitterCode;
    }

    public String getLcityname() {
        return lcityname;
    }

    public void setLcityname(String lcityname) {
        this.lcityname = lcityname;
    }

    public String getIdproofexpirydate() {
        return idproofexpirydate;
    }

    public void setIdproofexpirydate(String idproofexpirydate) {
        this.idproofexpirydate = idproofexpirydate;
    }

    public String getIdproofnumber() {
        return idproofnumber;
    }

    public void setIdproofnumber(String idproofnumber) {
        this.idproofnumber = idproofnumber;
    }

    public String getRemitteraddress1() {
        return remitteraddress1;
    }

    public void setRemitteraddress1(String remitteraddress1) {
        this.remitteraddress1 = remitteraddress1;
    }

    public String getRemitteraddress2() {
        return remitteraddress2;
    }

    public void setRemitteraddress2(String remitteraddress2) {
        this.remitteraddress2 = remitteraddress2;
    }

    public String getRemittername() {
        return remittername;
    }

    public void setRemittername(String remittername) {
        this.remittername = remittername;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getCityname() {
        return cityname;
    }

    public void setCityname(String cityname) {
        this.cityname = cityname;
    }

    public String getLremitteraddress() {
        return lremitteraddress;
    }

    public void setLremitteraddress(String lremitteraddress) {
        this.lremitteraddress = lremitteraddress;
    }

    public String getStatename() {
        return statename;
    }

    public void setStatename(String statename) {
        this.statename = statename;
    }

    public String getConsumedlimit() {
        return consumedlimit;
    }

    public void setConsumedlimit(String consumedlimit) {
        this.consumedlimit = consumedlimit;
    }

    public String getCreatedon() {
        return createdon;
    }

    public void setCreatedon(String createdon) {
        this.createdon = createdon;
    }

    public String getKycremarks() {
        return kycremarks;
    }

    public void setKycremarks(String kycremarks) {
        this.kycremarks = kycremarks;
    }

    public String getRemaininglimit() {
        return remaininglimit;
    }

    public void setRemaininglimit(String remaininglimit) {
        this.remaininglimit = remaininglimit;
    }

    public String getLpincode() {
        return lpincode;
    }

    public void setLpincode(String lpincode) {
        this.lpincode = lpincode;
    }

    public String getIdproofissuedate() {
        return idproofissuedate;
    }

    public void setIdproofissuedate(String idproofissuedate) {
        this.idproofissuedate = idproofissuedate;
    }

    public String getAlternatenumber() {
        return alternatenumber;
    }

    public void setAlternatenumber(String alternatenumber) {
        this.alternatenumber = alternatenumber;
    }

    public Integer getKycstatus() {
        return kycstatus;
    }

    public void setKycstatus(Integer kycstatus) {
        this.kycstatus = kycstatus;
    }

    public String getBcAgentId() {
        return bcAgentId;
    }

    public void setBcAgentId(String bcAgentId) {
        this.bcAgentId = bcAgentId;
    }

    public String getIdproof() {
        return idproof;
    }

    public void setIdproof(String idproof) {
        this.idproof = idproof;
    }

    public String getRemittermobilenumber() {
        return remittermobilenumber;
    }

    public void setRemittermobilenumber(String remittermobilenumber) {
        this.remittermobilenumber = remittermobilenumber;
    }

}
