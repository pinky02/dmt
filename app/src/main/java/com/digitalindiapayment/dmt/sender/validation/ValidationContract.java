package com.digitalindiapayment.dmt.sender.validation;

import com.digitalindiapayment.mvp.BaseView;
import com.digitalindiapayment.webservices.dmt.requestpojo.BeneficiaryAccountValidationRequest;
import com.digitalindiapayment.webservices.dmt.requestpojo.IfscCodeRequest;
import com.digitalindiapayment.webservices.dmt.requestpojo.SearchSenderRequest;
import com.digitalindiapayment.webservices.dmt.responsepojo.Beneficiary;
import com.digitalindiapayment.webservices.dmt.responsepojo.BeneficiaryAccountValidationResponse;
import com.digitalindiapayment.webservices.dmt.responsepojo.IfscCodeResponse;
import com.digitalindiapayment.webservices.dmt.responsepojo.Remitterdetail;

import java.util.List;

/**
 * Created by Pinky on 22-05-2018.
 */

public class ValidationContract {
    public interface View extends BaseView {
        void show(IfscCodeResponse ifscCodeResponse);
        void showBeneficiaryAccountValidationResponse( BeneficiaryAccountValidationResponse beneficiaryAccountValidationResponse);

    }

    interface UserActions {
        void sendIfscCodeRequest (IfscCodeRequest searchSenderRequest);
        void beneficiaryAccountValidation(BeneficiaryAccountValidationRequest beneficiaryAccountValidationRequest);
    }
}
