package com.digitalindiapayment.dmt.sender.view.receiver_details;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.digitalindiapayment.BaseActivity;
import com.digitalindiapayment.R;
import com.digitalindiapayment.dmt.sender.view.RecyclerViewOnClick;
import com.digitalindiapayment.dmt.util.UtilDmt;
import com.digitalindiapayment.util.Util;
import com.digitalindiapayment.webservices.dmt.requestpojo.SearchSenderRequest;
import com.digitalindiapayment.webservices.dmt.responsepojo.Beneficiary;
import com.digitalindiapayment.webservices.dmt.responsepojo.Remitterdetail;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReceiverActivity extends BaseActivity implements ReceiverContract.View,RecyclerViewOnClick {

    @BindView(R.id.multilevelinfolist)
    RecyclerView recyclerviewStatement;
    @BindView(R.id.txvRemitterIdValue)
    TextView RemitterId;
    @BindView(R.id.txvSenderNameValue)
    TextView SenderName;
    @BindView(R.id.mobileNumber)
    TextView RemitterMobileNumber;
    @BindView(R.id.txvCityValue)
    TextView city;
    @BindView(R.id.txvPincodeValue)
    TextView Pincode;
    @BindView(R.id.txvKycStatusValue)
    TextView kycStatus;
    @BindView(R.id.txvRemainingValue)
    TextView limit;
    @BindView(R.id.txvConsumedLimitValue)
    TextView ConsumedLimit;
    private BeneficiaryAdapter beneficiaryAdapter = null;
    private List<Beneficiary> transactions = new ArrayList<Beneficiary>();
    ReceiverPresenter receiverPresenter = null;
    String sessionToken,bcAgent,mobileNumber;
    SearchSenderRequest searchSenderRequest;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receiver);
        ButterKnife.bind(this);
        Intent i = getIntent();
        mobileNumber =  i.getStringExtra("MobileNumber");

        receiverPresenter = new ReceiverPresenter(this,getApplicationContext());

    }
    @Override
    public void onResume(){
        super.onResume();
        searchSenderRequest = new SearchSenderRequest();
        searchSenderRequest.setSessiontoken(UtilDmt.PREFERENCES.getSessionToken(this));
        String bcAgentId = UtilDmt.PREFERENCES.getBCAgent(this);
        searchSenderRequest.setBcagent(bcAgentId);
        searchSenderRequest.setMobilenumber(mobileNumber);
        receiverPresenter.sendSearchSenderRequest(searchSenderRequest);
    }

    @Override
    public void noInternet() {
        Toast.makeText(getApplicationContext(), "No internet", Toast.LENGTH_LONG).show();
        disMissDialog();
    }

    @Override
    public void exception(Throwable e) {

    }

    @Override
    public void fail(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
        disMissDialog();
    }

    @Override
    public void success(String message) {

    }

    @Override
    public void showLoading() {
        showDialog();
    }

    @Override
    public void hideLoading() {
        disMissDialog();
    }


    @Override
    public void show(Remitterdetail remitterdetail, List <Beneficiary> beneficiaries) {
        RemitterId.setText(remitterdetail.getRemitterid().toString());
        SenderName.setText(remitterdetail.getRemittername().toString());
        RemitterMobileNumber.setText(remitterdetail.getRemitterMobilenumber().toString());
        city.setText(remitterdetail.getLcity().toString());
        Pincode.setText(remitterdetail.getPincode().toString());
        kycStatus.setText(remitterdetail.getKycstatus().toString());
        limit.setText(remitterdetail.getRemaininglimit().toString());
        ConsumedLimit.setText(remitterdetail.getConsumedlimit().toString());
        String remitterId = remitterdetail.getRemitterid().toString();
        UtilDmt.PREFERENCES.setRemitterId(this,remitterId);
        //Toast.makeText(getApplicationContext(),"Hii",Toast.LENGTH_SHORT).show();
        beneficiaryAdapter = new BeneficiaryAdapter(getApplicationContext(), this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerviewStatement.setLayoutManager(mLayoutManager);
        recyclerviewStatement.setItemAnimator(new DefaultItemAnimator());
        recyclerviewStatement.setAdapter(beneficiaryAdapter);
        this.transactions = beneficiaries;
        beneficiaryAdapter.setTransactionsList(beneficiaries);
    }

    @Override
    public void validationResponse(String message) {

    }


    @Override
    public void onClick(View view) {

    }
}
