package com.digitalindiapayment.dmt.sender.view;

import android.content.Context;
import android.util.Log;

import com.digitalindiapayment.dmt.login.LoginContract;
import com.digitalindiapayment.webservices.Api;
import com.digitalindiapayment.webservices.core.ApiFail;
import com.digitalindiapayment.webservices.core.ApiSuccess;
import com.digitalindiapayment.webservices.core.HttpErrorResponse;
import com.digitalindiapayment.webservices.dmt.requestpojo.DashBoardRequest;
import com.digitalindiapayment.webservices.dmt.requestpojo.ViewSenderRequest;
import com.digitalindiapayment.webservices.dmt.responsepojo.LoginResponse;
import com.digitalindiapayment.webservices.dmt.responsepojo.ViewSenderResponse;

import java.util.ArrayList;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static android.content.ContentValues.TAG;

/**
 * Created by Pinky on 24-04-2018.
 */

public class ViewSenderPresenter implements ViewSenderContract.UserActions{
    private final ViewSenderContract.View view;
    private final Context context;

    public ViewSenderPresenter(ViewSenderContract.View view, Context context) {
        this.view = view;
        this.context = context;
    }

    @Override
    public void sendViewSenderRequest(ViewSenderRequest viewSenderRequest) {
        view.showLoading();
        //view.showHome(Util.PREFERENCES.getToken(context));
        // login.setMobile_auth_code(Util.PREFERENCES.getUniqueDeviceKey(context));
        Api.userManagementDmt().viewsenderrequest(viewSenderRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ApiSuccess<ViewSenderResponse>() {
                    @Override
                    public void call(ViewSenderResponse viewSenderResponse) {
                        Log.d(TAG, "subscribe > call");
                        view.show(viewSenderResponse.getResponse());
                        view.hideLoading();

//                        if(viewSenderResponse.getStatus()==0){
//                            view.show();
//                        }
//                        else if(loginResponse.getStatus()==1)
//                            view.show(loginResponse);

                    }
                }, new ApiFail() {
                    @Override
                    public void httpStatus(HttpErrorResponse response) {
                        view.hideLoading();
                        //  view.onResetSessionFail();
                    }

                    @Override
                    public void noNetworkError() {
                        view.noInternet();
                    }

                    @Override
                    public void unknownError(Throwable e) {
                        view.fail(e.getMessage());
                    }
                });
        // return Api.userManagementDmt().dmtDashBoard(dmtDashBoardRequest);

    }
}
