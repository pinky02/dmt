package com.digitalindiapayment.dmt.sender.imps;

import com.digitalindiapayment.mvp.BaseView;
import com.digitalindiapayment.webservices.dmt.responsepojo.IfscCodeResponse;
import com.digitalindiapayment.webservices.dmt.responsepojo.TransactionResponse;
import com.digitalindiapayment.webservices.requestpojo.TransactionRequest;

/**
 * Created by Pinky on 26-06-2018.
 */

public class ImpsContract {
    public interface View extends BaseView {
        void show(TransactionResponse transactionResponse);
        void transactionFail(String message);
//        void show(IfscCodeResponse ifscCodeResponse);
//        void showBeneficiaryAccountValidationResponse( BeneficiaryAccountValidationResponse beneficiaryAccountValidationResponse);

    }

    interface UserActions {
        void sendImpsRequest(TransactionRequest transactionRequest);
//        void sendIfscCodeRequest (IfscCodeRequest searchSenderRequest);
//        void beneficiaryAccountValidation(BeneficiaryAccountValidationRequest beneficiaryAccountValidationRequest);
    }
}
