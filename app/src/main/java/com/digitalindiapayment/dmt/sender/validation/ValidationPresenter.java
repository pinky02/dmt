package com.digitalindiapayment.dmt.sender.validation;

import android.content.Context;

import com.digitalindiapayment.webservices.Api;
import com.digitalindiapayment.webservices.core.ApiFail;
import com.digitalindiapayment.webservices.core.ApiSuccess;
import com.digitalindiapayment.webservices.core.HttpErrorResponse;
import com.digitalindiapayment.webservices.dmt.requestpojo.BeneficiaryAccountValidationRequest;
import com.digitalindiapayment.webservices.dmt.requestpojo.IfscCodeRequest;
import com.digitalindiapayment.webservices.dmt.responsepojo.BeneficiaryAccountValidationResponse;
import com.digitalindiapayment.webservices.dmt.responsepojo.IfscCodeResponse;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Pinky on 22-05-2018.
 */

public class ValidationPresenter implements ValidationContract.UserActions{
    private final ValidationContract.View view;
    private final Context context;

    public ValidationPresenter(ValidationContract.View view, Context context) {
        this.view = view;
        this.context = context;
    }
    @Override
    public void sendIfscCodeRequest(IfscCodeRequest ifscCodeRequest) {
        view.showLoading();

        Api.userManagementDmt().sendIfscRequest(ifscCodeRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ApiSuccess<IfscCodeResponse>() {
                    @Override
                    public void call(IfscCodeResponse ifscCodeResponse) {
                        view.hideLoading();
                        view.show(ifscCodeResponse);

                    }
                }, new ApiFail() {
                    @Override
                    public void httpStatus(HttpErrorResponse response) {
                        view.hideLoading();
                    }

                    @Override
                    public void noNetworkError() {
                        view.noInternet();
                    }

                    @Override
                    public void unknownError(Throwable e) {
                        view.fail(e.getMessage());
                    }
                });
    }

    @Override
    public void beneficiaryAccountValidation(BeneficiaryAccountValidationRequest beneficiaryAccountValidationRequest) {
        view.showLoading();

        Api.userManagementDmt().beneficiaryAccountValidation(beneficiaryAccountValidationRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ApiSuccess<BeneficiaryAccountValidationResponse>() {
                    @Override
                    public void call(BeneficiaryAccountValidationResponse beneficiaryAccountValidationResponse) {
                        view.hideLoading();
                        view.showBeneficiaryAccountValidationResponse(beneficiaryAccountValidationResponse);

                    }
                }, new ApiFail() {
                    @Override
                    public void httpStatus(HttpErrorResponse response) {
                        view.hideLoading();
                    }

                    @Override
                    public void noNetworkError() {
                        view.noInternet();
                    }

                    @Override
                    public void unknownError(Throwable e) {
                        view.fail(e.getMessage());
                    }
                });
    }
}
