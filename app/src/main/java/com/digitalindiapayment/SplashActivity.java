package com.digitalindiapayment;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.digitalindiapayment.fcm.MyFirebaseMessagingService;
import com.digitalindiapayment.login.LoginActivity;
import com.digitalindiapayment.pojo.DIPLNotification;
import com.digitalindiapayment.util.Util;
import com.pnikosis.materialishprogress.ProgressWheel;

import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.UUID;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;

public class SplashActivity extends BaseActivity {

    @BindView(R.id.noInternetContainer)
    LinearLayout noInternetContainer;
    @BindView(R.id.noInternetImg)
    ImageView noInternetImg;
    @BindView(R.id.noInternetMsg)
    TextView noInternetMsg;

    @BindView(R.id.activity_splash_screen)
    RelativeLayout activity_splash_screen;


    @BindView(R.id.progressbar)
    MaterialProgressBar progressbar;
    @BindView(R.id.message)
    TextView message;
    @BindView(R.id.imageviewdip)
    ImageView imageviewdip;
    @BindView(R.id.textviewhorizontalgreen)
    TextView textviewhorizontalgreen;
    @BindView(R.id.textviewhorizontalorange)
    TextView textviewhorizontalorange;
    @BindView(R.id.textviewdigitalindia)
    TextView textviewdigitalindia;
    @BindView(R.id.textviewpatment)
    TextView textviewpatment;
    @BindView(R.id.bottomImageSplash)
    ImageView bottomImageSplash;
    @BindView(R.id.refreshButton)
    ImageView refreshButton;
    @BindView(R.id.progressdialogdip)
    RelativeLayout progressdialogdip;
    @BindView(R.id.progressIndicator)
    ProgressWheel progressIndicator;
    DIPLNotification diplNotification;
    Intent loginIntent;
    private String currentVersion, latestVersion;
    private Dialog dialog;
    private String appInstanceCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_splash_screen);
        ButterKnife.bind(this);
        noInternetContainer.setVisibility(View.GONE);
        progressdialogdip.setVisibility(View.GONE);


        loginIntent = new Intent(SplashActivity.this, LoginActivity.class);
        loginIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);


        if (getIntent().hasExtra(MyFirebaseMessagingService.NOTIFICATION_DATA)) {
            diplNotification = getIntent().getParcelableExtra(MyFirebaseMessagingService.NOTIFICATION_DATA);
            loginIntent.putExtra(MyFirebaseMessagingService.NOTIFICATION_DATA, diplNotification);
        }
        Log.e("FCM token", "@@@@@@@@");
        Log.d("FCM token", Util.PREFERENCES.getNotificationRegId(SplashActivity.this));

        if (Util.PREFERENCES.isLogin(SplashActivity.this)) {
            if (diplNotification != null) {
                Util.PREFERENCES.notificationRedirect(SplashActivity.this, diplNotification);
                finish();
                return;
            }

        }
        Util.PREFERENCES.setIsLogin(SplashActivity.this, false);
        // getCurrentVersion();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(loginIntent);
                finish();
            }
        }, 2000);


    }

    @OnClick(R.id.refreshButton)
    public void onClickReferesh(View view) {
        getCurrentVersion();
    }

    private void getCurrentVersion() {

        if (!Util.isNetworkAvailable(getApplicationContext())) {
            noInternetContainer.setVisibility(View.VISIBLE);
            activity_splash_screen.setVisibility(View.GONE);
            return;
        }
        noInternetContainer.setVisibility(View.GONE);
        progressdialogdip.setVisibility(View.GONE);
        activity_splash_screen.setVisibility(View.VISIBLE);

        PackageManager pm = this.getPackageManager();
        PackageInfo pInfo = null;

        try {
            pInfo = pm.getPackageInfo(this.getPackageName(), 0);

        } catch (PackageManager.NameNotFoundException e1) {
            e1.printStackTrace();
        }
        //  this.currentVersion = pInfo.versionName;

        // new GetLatestVersion().execute();

    }

    class GetLatestVersion extends AsyncTask<String, String, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressIndicator.setVisibility(View.GONE);
            //showDialog();

        }

        @Override
        protected JSONObject doInBackground(String... strings) {

            try {
                final Document document = Jsoup.connect("https://play.google.com/store/apps/details?id=agentapp.digitalindiapayment.com.digitalindiapayment").get();
                latestVersion = document.getElementsByAttributeValue
                        ("itemprop", "softwareVersion").first().text();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {


            //disMissDialog();

            //Intent i = new Intent(SplashActivity.this, LoginActivity.class);
            //i.putExtra(MyFirebaseMessagingService.NOTIFICATION_DATA,getIntent().getParcelableExtra(MyFirebaseMessagingService.NOTIFICATION_DATA));

            progressIndicator.setVisibility(View.VISIBLE);
            //This requires
            if (latestVersion != null) {
                try {
                    String[] currentArray = currentVersion.split(Pattern.quote("."));
                    String[] latestArray = latestVersion.split(Pattern.quote("."));
                    int actualVersion = Integer.parseInt(currentArray[2]) * 3 + Integer.parseInt(currentArray[1]) * 2 + Integer.parseInt(currentArray[0]) * 1;
                    int actuallatestVersion = Integer.parseInt(latestArray[2]) * 3 + Integer.parseInt(latestArray[1]) * 2 + Integer.parseInt(latestArray[0]) * 1;
                    if (actualVersion < actuallatestVersion) {
                        if (!isFinishing()) { //This would help to prevent Error : BinderProxy@45d459c0 is not valid; is your activity running? error
                            showUpdateDialog();
                        }
                    } else {
                        startActivity(loginIntent);
                        finish();
                    }
                    super.onPostExecute(jsonObject);
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (!currentVersion.equalsIgnoreCase(latestVersion)) {
                    if (!isFinishing()) { //This would help to prevent Error : BinderProxy@45d459c0 is not valid; is your activity running? error
                        showUpdateDialog();
                    }
                } else {
                    startActivity(loginIntent);
                    finish();
                }
            } else {
                finish();
            }

            super.onPostExecute(jsonObject);
        }

        private void showUpdateDialog() {
            final AlertDialog.Builder builder = new AlertDialog.Builder(SplashActivity.this);
            builder.setTitle("A New Update is Available");
            builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse
                            ("market://details?id=agentapp.digitalindiapayment.com.digitalindiapayment")));
                    dialog.dismiss();
                    finish();
                }
            });

            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });

            builder.setCancelable(false);
            dialog = builder.show();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (Util.PREFERENCES.getAppInstanceCode(getApplicationContext()).equals("")) {
            appInstanceCode = UUID.randomUUID().toString();
            Util.PREFERENCES.setAppInstanceCode(getApplicationContext(), appInstanceCode);
        }
    }
}

