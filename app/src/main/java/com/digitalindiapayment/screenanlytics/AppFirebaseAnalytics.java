package com.digitalindiapayment.screenanlytics;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import com.google.firebase.analytics.FirebaseAnalytics;

/**
 * Created by admin on 14-10-2017.
 */

public class AppFirebaseAnalytics implements IAnalytics {

    private FirebaseAnalytics mFirebaseAnalytics;
    private Activity activity;

    public AppFirebaseAnalytics(Activity activity) {
        this.activity = activity;
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(activity);
    }

    @Override
    public void trackScreen(String screenName) {
        mFirebaseAnalytics.setCurrentScreen(activity, screenName, null);
    }

    @Override
    public void trackEvent(String category, String action) {
        Bundle params = new Bundle();
        if (action != null && !action.equals("")) {
            params.putString("Action", action);
        }
        mFirebaseAnalytics.logEvent(category, params);

    }

    @Override
    public void trackCustomEvent(String category, Bundle bundle) {
        mFirebaseAnalytics.logEvent(category, bundle);
    }

    @Override
    public void trackCustomEvent(int category, Bundle bundle) {
        mFirebaseAnalytics.logEvent(activity.getString(category), bundle);
    }
}
