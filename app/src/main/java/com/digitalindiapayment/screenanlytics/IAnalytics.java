package com.digitalindiapayment.screenanlytics;
import android.os.Bundle;

/**
 * Created by admin on 14-10-2017.
 */

public interface IAnalytics {

    public void trackScreen(String screenName);
    public void trackEvent(String category,String action);
    public void trackCustomEvent(String category, Bundle bundle);
    public void trackCustomEvent(int category, Bundle bundle);
}
