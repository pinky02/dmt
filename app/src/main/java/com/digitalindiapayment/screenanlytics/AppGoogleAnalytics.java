package com.digitalindiapayment.screenanlytics;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import com.digitalindiapayment.application.MyApplication;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

/**
 * Created by admin on 14-10-2017.
 */

public class AppGoogleAnalytics implements IAnalytics {

    private Tracker mTracker;

    public AppGoogleAnalytics(Activity activity) {
        MyApplication application = (MyApplication) activity.getApplication() ;
        mTracker = application.getDefaultTracker();
    }

    @Override
    public void trackScreen(String screenName) {
        mTracker.setScreenName(screenName);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public void trackEvent(String category, String action) {
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory(category)
                .setAction(action)
                .build());
    }

    @Override
    public void trackCustomEvent(String category, Bundle bundle) {
    }

    @Override
    public void trackCustomEvent(int category, Bundle bundle) {

    }
}
