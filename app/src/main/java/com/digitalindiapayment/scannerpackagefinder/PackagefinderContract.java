package com.digitalindiapayment.scannerpackagefinder;

import com.digitalindiapayment.webservices.responsepojo.ScannerDeviceResponse;

/**
 * Created by admin on 06-11-2017.
 */

public class PackagefinderContract {

    public interface View{
        public void onPackageApisuccess(ScannerDeviceResponse scannerDeviceResponse);
        public void onPackageApifail(String message);
    }

    public interface UserAction{
        public void fetchDetails();
        public Boolean isPackageAvailable(String devicePackage);
    }

}
