package com.digitalindiapayment.scannerpackagefinder;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.util.Log;

import com.digitalindiapayment.util.Util;
import com.digitalindiapayment.webservices.Api;
import com.digitalindiapayment.webservices.core.ApiFail;
import com.digitalindiapayment.webservices.core.ApiSuccess;
import com.digitalindiapayment.webservices.core.HttpErrorResponse;
import com.digitalindiapayment.webservices.responsepojo.AgentDeviceResponse;
import com.digitalindiapayment.webservices.responsepojo.ScannerDeviceResponse;

import java.util.List;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by admin on 06-11-2017.
 *
 * PackageFinderPresenter is Used to
 * fetch package details from server
 *
 * also finds package is installed in Andoid application
 */

public class PackageFinderPresenter implements PackagefinderContract.UserAction {

    Context context;
    PackagefinderContract.View view;

    public PackageFinderPresenter(Context context, PackagefinderContract.View view) {
        this.context = context;
        this.view = view;
    }

    @Override
    public void fetchDetails() {
        Api.userManagement().getDevices(Util.PREFERENCES.getToken(context))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ApiSuccess<AgentDeviceResponse>() {
                    @Override
                    public void call(AgentDeviceResponse scannerDeviceResponse) {
                        view.onPackageApisuccess(scannerDeviceResponse.getDevices().get(0));
                    }
                }, new ApiFail() {
                    @Override
                    public void httpStatus(HttpErrorResponse response) {
                        view.onPackageApifail(response.getError());
                    }

                    @Override
                    public void noNetworkError() {

                    }

                    @Override
                    public void unknownError(Throwable e) {

                    }
                });
    }

    @Override
    public Boolean isPackageAvailable(String devicePackage) {
        final PackageManager pm = context.getPackageManager();
        List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);
        for (ApplicationInfo packageInfo : packages) {
            if(devicePackage.equals(packageInfo.packageName)){
             return true;
            }
        }


        return false;
    }
}
