package com.digitalindiapayment.pojo;

/**
 * Created by DM on 13-01-2017.
 */

public class Login {

    private String uid, password;
    private String userType;

    private String mobile_auth_code;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getMobile_auth_code() {
        return mobile_auth_code;
    }

    public void setMobile_auth_code(String mobile_auth_code) {
        this.mobile_auth_code = mobile_auth_code;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getuid() {
        return uid;
    }

    public void setuid(String userID) {
        this.uid = userID;
    }


}
