package com.digitalindiapayment.pojo;

import com.digitalindiapayment.settings.OnSettingOptionClick;

/**
 * Created by admin on 12-07-2017.
 */

public class SettingOption implements OnSettingOptionClick {

    private String title;
    private int icon;
    private Boolean hasNavigation;
    private Boolean hasLoader;

    private Boolean isInProgress = false;

    private OnSettingOptionClick onSettingOptionClick;

    public SettingOption() {
        this.title = "";
        this.icon = 0;
        this.hasNavigation = false;
        this.hasLoader = false;
        this.isInProgress = false;
    }

    /*
    * By Default Every Setting option has Navigation
    * And Loader is disabled
    *
     */
    public SettingOption(String title, int icon) {
        this.title = title;
        this.icon = icon;
        this.hasNavigation = true;
        this.hasLoader = false;
    }

    public void addOnOptionClickListener(OnSettingOptionClick onSettingOptionClick) {
        this.onSettingOptionClick = onSettingOptionClick;
    }

    public Boolean getProgressStatus() {
        return isInProgress;
    }

    public void setProgressStatus(Boolean inProgress) {
        isInProgress = hasLoader ? inProgress : false;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public Boolean getHasNavigation() {
        return hasNavigation;
    }

    public void setHasNavigation(Boolean hasNavigation) {
        this.hasNavigation = hasNavigation;
    }

    public Boolean getHasLoader() {
        return hasLoader;
    }

    /*
       * If SettingOption has Loader then
       * It Contains only Loader Not Navigation Icon
     */
    public void setHasLoader(Boolean hasLoader) {

        this.hasNavigation = !hasLoader ;
        this.hasLoader = hasLoader;
    }

    @Override
    public void optionClick(Object data) {
        onSettingOptionClick.optionClick(data);
    }
}
