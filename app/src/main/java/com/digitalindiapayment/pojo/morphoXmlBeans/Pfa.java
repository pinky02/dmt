package com.digitalindiapayment.pojo.morphoXmlBeans;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

/**
 * Created by admin on 29-08-2017.
 */
@Root(name = "Pfa")
public class Pfa {
    @Attribute(name = "lav", required = false)
    private String lav;
    @Attribute(name = "ms", required = false)
    private String ms;
    @Attribute(name = "mv", required = false)
    private String mv;
    @Attribute(name = "lmv", required = false)
    private String lmv;
    @Attribute(name = "av", required = false)
    private String av;

    public Pfa() {
    }

    public String getLav ()
    {
        return lav;
    }

    public void setLav (String lav)
    {
        this.lav = lav;
    }

    public String getMs ()
    {
        return ms;
    }

    public void setMs (String ms)
    {
        this.ms = ms;
    }

    public String getMv ()
    {
        return mv;
    }

    public void setMv (String mv)
    {
        this.mv = mv;
    }

    public String getLmv ()
    {
        return lmv;
    }

    public void setLmv (String lmv)
    {
        this.lmv = lmv;
    }

    public String getAv ()
    {
        return av;
    }

    public void setAv (String av)
    {
        this.av = av;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [lav = "+lav+", ms = "+ms+", mv = "+mv+", lmv = "+lmv+", av = "+av+"]";
    }
}
