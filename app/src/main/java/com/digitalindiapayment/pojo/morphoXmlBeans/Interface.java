package com.digitalindiapayment.pojo.morphoXmlBeans;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

/**
 * Created by admin on 24-08-2017.
 */
@Root(name = "Interface")
public class Interface {

    @Attribute(name = "id")
    private String id;

    @Attribute(name = "path")
    private String path;

    public Interface() {
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getPath ()
    {
        return path;
    }

    public void setPath (String path)
    {
        this.path = path;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", path = "+path+"]";
    }
}
