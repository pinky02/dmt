package com.digitalindiapayment.pojo.morphoXmlBeans;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by admin on 24-08-2017.
 */
@Root(name = "RDService")
public class RDService {

    @ElementList(name = "Interface", inline = true)
    private List<com.digitalindiapayment.pojo.morphoXmlBeans.Interface> Interface;

    @Attribute(name = "status")
    private String status;

    @Attribute(name = "info")
    private String info;

    public RDService() {
    }

    public List<com.digitalindiapayment.pojo.morphoXmlBeans.Interface> getInterface() {
        return Interface;
    }

    public void setInterface(List<com.digitalindiapayment.pojo.morphoXmlBeans.Interface> anInterface) {
        Interface = anInterface;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public String getInfo ()
    {
        return info;
    }

    public void setInfo (String info)
    {
        this.info = info;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Interface = "+Interface+", status = "+status+", info = "+info+"]";
    }


    public Boolean isReady(){
        return status.equals("READY");
    }
}
