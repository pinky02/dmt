package com.digitalindiapayment.pojo.morphoXmlBeans;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

/**
 * Created by admin on 29-08-2017.
 */
@Root(name = "Pi")
public class Pi {
    @Attribute(name = "lname", required = false)
    private String lname;
    @Attribute(name = "dobt", required = false)
    private String dobt;
    @Attribute(name = "phone", required = false)
    private String phone;
    @Attribute(name = "email", required = false)
    private String email;
    @Attribute(name = "ms", required = false)
    private String ms;
    @Attribute(name = "name", required = false)
    private String name;
    @Attribute(name = "dob", required = false)
    private String dob;
    @Attribute(name = "age", required = false)
    private String age;
    @Attribute(name = "mv", required = false)
    private String mv;
    @Attribute(name = "lmv", required = false)
    private String lmv;
    @Attribute(name = "gender", required = false)
    private String gender;

    public Pi() {
    }

    public String getLname ()
    {
        return lname;
    }

    public void setLname (String lname)
    {
        this.lname = lname;
    }

    public String getDobt ()
    {
        return dobt;
    }

    public void setDobt (String dobt)
    {
        this.dobt = dobt;
    }

    public String getPhone ()
    {
        return phone;
    }

    public void setPhone (String phone)
    {
        this.phone = phone;
    }

    public String getEmail ()
    {
        return email;
    }

    public void setEmail (String email)
    {
        this.email = email;
    }

    public String getMs ()
    {
        return ms;
    }

    public void setMs (String ms)
    {
        this.ms = ms;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getDob ()
    {
        return dob;
    }

    public void setDob (String dob)
    {
        this.dob = dob;
    }

    public String getAge ()
    {
        return age;
    }

    public void setAge (String age)
    {
        this.age = age;
    }

    public String getMv ()
    {
        return mv;
    }

    public void setMv (String mv)
    {
        this.mv = mv;
    }

    public String getLmv ()
    {
        return lmv;
    }

    public void setLmv (String lmv)
    {
        this.lmv = lmv;
    }

    public String getGender ()
    {
        return gender;
    }

    public void setGender (String gender)
    {
        this.gender = gender;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [lname = "+lname+", dobt = "+dobt+", phone = "+phone+", email = "+email+", ms = "+ms+", name = "+name+", dob = "+dob+", age = "+age+", mv = "+mv+", lmv = "+lmv+", gender = "+gender+"]";
    }

}
