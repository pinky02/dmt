package com.digitalindiapayment.pojo.morphoXmlBeans;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Text;

/**
 * Created by admin on 28-06-2017.
 */
@Root(name = "Skey")
public class Skey {
    void Skey() {
    }
    @Text(required = false)
    public String content;

    @Attribute(name = "ci", required = false)
    public String _ci;
}
