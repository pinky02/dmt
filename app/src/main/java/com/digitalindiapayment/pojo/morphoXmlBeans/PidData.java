package com.digitalindiapayment.pojo.morphoXmlBeans;


import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "PidData")
public class PidData {

    public PidData() {
    }

    @Element(name = "Resp", required = false)
    public Resp _Resp;

    @Element(name = "DeviceInfo", required = false)
    public DeviceInfo _DeviceInfo;

    @Element(name = "Hmac", required = false)
    public String _Hmac;

    @Element(name = "Skey", required = false)
    public Skey _Skey;

    //Data
    @Element(name = "Data", required = false)
    public Data Data;


    public Boolean isSuccessfull(){
        if(getResponse().errCode.equals("0")){
            return true;
        }
        return false;
    }

    public Resp getResponse(){
        return _Resp;
    }
}
