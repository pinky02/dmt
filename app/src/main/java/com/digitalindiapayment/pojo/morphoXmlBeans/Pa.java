package com.digitalindiapayment.pojo.morphoXmlBeans;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

/**
 * Created by admin on 29-08-2017.
 */
@Root(name = "Pa")
public class Pa {


    @Attribute(name = "lm", required = false)
    private String lm;
    @Attribute(name = "ms", required = false)
    private String ms;
    @Attribute(name = "street", required = false)
    private String street;
    @Attribute(name = "state", required = false)
    private String state;
    @Attribute(name = "loc", required = false)
    private String loc;
    @Attribute(name = "house", required = false)
    private String house;
    @Attribute(name = "subdist", required = false)
    private String subdist;
    @Attribute(name = "country", required = false)
    private String country;
    @Attribute(name = "co", required = false)
    private String co;
    @Attribute(name = "pc", required = false)
    private String pc;
    @Attribute(name = "po", required = false)
    private String po;
    @Attribute(name = "vtc", required = false)
    private String vtc;
    @Attribute(name = "dist", required = false)
    private String dist;

    public Pa() {
    }

    public String getLm ()
    {
        return lm;
    }

    public void setLm (String lm)
    {
        this.lm = lm;
    }

    public String getMs ()
    {
        return ms;
    }

    public void setMs (String ms)
    {
        this.ms = ms;
    }

    public String getStreet ()
    {
        return street;
    }

    public void setStreet (String street)
    {
        this.street = street;
    }

    public String getState ()
    {
        return state;
    }

    public void setState (String state)
    {
        this.state = state;
    }

    public String getLoc ()
    {
        return loc;
    }

    public void setLoc (String loc)
    {
        this.loc = loc;
    }

    public String getHouse ()
    {
        return house;
    }

    public void setHouse (String house)
    {
        this.house = house;
    }

    public String getSubdist ()
    {
        return subdist;
    }

    public void setSubdist (String subdist)
    {
        this.subdist = subdist;
    }

    public String getCountry ()
    {
        return country;
    }

    public void setCountry (String country)
    {
        this.country = country;
    }

    public String getCo ()
    {
        return co;
    }

    public void setCo (String co)
    {
        this.co = co;
    }

    public String getPc ()
    {
        return pc;
    }

    public void setPc (String pc)
    {
        this.pc = pc;
    }

    public String getPo ()
    {
        return po;
    }

    public void setPo (String po)
    {
        this.po = po;
    }

    public String getVtc ()
    {
        return vtc;
    }

    public void setVtc (String vtc)
    {
        this.vtc = vtc;
    }

    public String getDist ()
    {
        return dist;
    }

    public void setDist (String dist)
    {
        this.dist = dist;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [lm = "+lm+", ms = "+ms+", street = "+street+", state = "+state+", loc = "+loc+", house = "+house+", subdist = "+subdist+", country = "+country+", co = "+co+", pc = "+pc+", po = "+po+", vtc = "+vtc+", dist = "+dist+"]";
    }
}
