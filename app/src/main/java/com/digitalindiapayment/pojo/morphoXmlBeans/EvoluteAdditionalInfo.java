package com.digitalindiapayment.pojo.morphoXmlBeans;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by amolbhanushali on 2017/12/04.
 */

@Root(name = "Additional_Info")
public class EvoluteAdditionalInfo {
    public EvoluteAdditionalInfo() {
    }

    @ElementList(name = "Info", required = false, inline = true)
    public List<Info> info;


}
