package com.digitalindiapayment.pojo.morphoXmlBeans;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by admin on 29-08-2017.
 */
@Root(name = "Demo")
public class Demo {
    @Element(name = "Pa", required = false)
    public Pa Pa;

    @Element(name = "Pfa", required = false)
    public Pfa Pfa;

    @Element(name = "Pi", required = false)
    public Pi Pi;

    @Attribute(name = "lang", required = false)
    public String lang;

    public Pa getPa ()
    {
        return Pa;
    }

    public void setPa (Pa Pa)
    {
        this.Pa = Pa;
    }

    public Pfa getPfa ()
    {
        return Pfa;
    }

    public void setPfa (Pfa Pfa)
    {
        this.Pfa = Pfa;
    }

    public Pi getPi ()
    {
        return Pi;
    }

    public void setPi (Pi Pi)
    {
        this.Pi = Pi;
    }

    public String getLang ()
    {
        return lang;
    }

    public void setLang (String lang)
    {
        this.lang = lang;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Pa = "+Pa+", Pfa = "+Pfa+", Pi = "+Pi+", lang = "+lang+"]";
    }
}
