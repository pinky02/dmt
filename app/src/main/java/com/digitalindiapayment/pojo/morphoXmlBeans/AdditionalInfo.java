package com.digitalindiapayment.pojo.morphoXmlBeans;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by admin on 27-09-2017.
 */
@Root(name = "additional_info")
public class AdditionalInfo {

    @ElementList(name = "Param", required = false, inline = true)
    public List<Param> params;

    public List<Param> getParams() {
        return params;
    }

    public void setParams(List<Param> params) {
        this.params = params;
    }
}
