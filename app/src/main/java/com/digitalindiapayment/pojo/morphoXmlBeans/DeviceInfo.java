package com.digitalindiapayment.pojo.morphoXmlBeans;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by admin on 24-08-2017.
 */
@Root(name = "DeviceInfo")
public class DeviceInfo {

    @Attribute(name = "dc",required = false)
    public String dc;

    @Attribute(name = "rdsId",required = false)
    public String rdsId;

    @Attribute(name = "dpId",required = false)
    public String dpId;

    @Attribute(name = "mc",required = false)
    public String mc;

    @Attribute(name = "rdsVer",required = false)
    public String rdsVer;

    @Attribute(name = "mi",required = false)
    public String mi;

    @Attribute(name = "srno" ,required = false)
    public String srno;

    @Element(name = "additional_info", required = false)
    public AdditionalInfo additionalInfo;

    @Element(name = "Additional_Info", required = false)
    public EvoluteAdditionalInfo evoluteAdditionalInfo;


    public DeviceInfo() {
    }

    public String getDc ()
    {
        return dc;
    }

    public void setDc (String dc)
    {
        this.dc = dc;
    }

    public String getRdsId ()
    {
        return rdsId;
    }

    public void setRdsId (String rdsId)
    {
        this.rdsId = rdsId;
    }

    public String getDpId ()
    {
        return dpId;
    }

    public void setDpId (String dpId)
    {
        this.dpId = dpId;
    }

    public String getMc ()
    {
        return mc;
    }

    public void setMc (String mc)
    {
        this.mc = mc;
    }

    public String getRdsVer ()
    {
        return rdsVer;
    }

    public void setRdsVer (String rdsVer)
    {
        this.rdsVer = rdsVer;
    }

    public String getMi ()
    {
        return mi;
    }

    public void setMi (String mi)
    {
        this.mi = mi;
    }

    public AdditionalInfo getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(AdditionalInfo additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [dc = "+dc+", rdsId = "+rdsId+", dpId = "+dpId+", mc = "+mc+", rdsVer = "+rdsVer+", mi = "+mi+"]";
    }
}
