package com.digitalindiapayment.pojo.morphoXmlBeans;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

/**
 * Created by amolbhanushali on 2017/12/04.
 */
@Root(name = "Info")
public class Info {
    public Info() {

    }

    @Attribute(name = "name", required = false)
    public String name;

    @Attribute(name = "value", required = false)
    public String value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
