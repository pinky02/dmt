package com.digitalindiapayment.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Amol on 12-03-2017.
 */

public class BankDetailsBean implements Parcelable {

    @SerializedName("bankName")
    @Expose
    private String bankName;

    @SerializedName("code")
    @Expose
    private int code;

    @SerializedName("ifscCode")
    @Expose
    private String ifscCode;

    @SerializedName("accountName")
    @Expose
    private String accountName;

    @SerializedName("accountNo")
    @Expose
    private String accountNo;

    @SerializedName("branch")
    @Expose
    private String branch;

    @SerializedName("bankId")
    @Expose
    private Integer bankId;


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }


    public void setBankId(Integer bankId) {
        this.bankId = bankId;
    }

    public String getIfscCode() {
        return ifscCode;
    }

    public void setIfscCode(String ifscCode) {
        this.ifscCode = ifscCode;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    protected BankDetailsBean(Parcel in) {

        bankName = in.readString();
        code = in.readInt();
        ifscCode = in.readString();
        accountName = in.readString();
        accountNo = in.readString();
        branch = in.readString();
        bankId = in.readInt();
    }

    public BankDetailsBean(int id, String name) {
        this.code = id;
        this.bankName = name;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public int getBankId() {
        return bankId;
    }

    public void setBankId(int bankId) {
        this.code = bankId;
    }

    @Override
    public String toString() {
        return bankName;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(bankName);
        dest.writeInt(code);
        dest.writeString(ifscCode);
        dest.writeString(accountName);
        dest.writeString(accountNo);
        dest.writeString(branch);
        dest.writeInt(bankId);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BankDetailsBean> CREATOR = new Creator<BankDetailsBean>() {
        @Override
        public BankDetailsBean createFromParcel(Parcel in) {
            return new BankDetailsBean(in);
        }

        @Override
        public BankDetailsBean[] newArray(int size) {
            return new BankDetailsBean[size];
        }
    };
}
