package com.digitalindiapayment.pojo;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Amol on 04-04-2017.
 */

public class ModeofTransfer implements Parcelable {
    @Override
    public String toString() {
        return name;
    }

    @SerializedName("name")
    public String name;

    @SerializedName("code")
    public String code;

    public ModeofTransfer(Parcel in) {
        name = in.readString();
        code = in.readString();
    }

    public ModeofTransfer(String code, String name) {
        this.code =code;
        this.name = name;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(code);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ModeofTransfer> CREATOR = new Creator<ModeofTransfer>() {
        @Override
        public ModeofTransfer createFromParcel(Parcel in) {
            return new ModeofTransfer(in);
        }

        @Override
        public ModeofTransfer[] newArray(int size) {
            return new ModeofTransfer[size];
        }
    };
}
