package com.digitalindiapayment.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Mayur on 11-04-2017.
 */

public class WalletReportBean  {
    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("serviceMod")
    @Expose
    private String serviceMod;

    @SerializedName("amount")
    @Expose
    private String amount;

    @SerializedName("timestamp")
    @Expose
    private String timestamp;

    @SerializedName("balance")
    @Expose
    private String balance;

    @SerializedName("fees")
    @Expose
    private String fees;

    @SerializedName("credit")
    @Expose
    private String credit;


    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    @SerializedName("branch")
    @Expose
    private String branch;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getServiceMod() {
        return serviceMod;
    }

    public void setServiceMod(String serviceMod) {
        this.serviceMod = serviceMod;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getFees() {
        return fees;
    }

    public void setFees(String fees) {
        this.fees = fees;
    }

    public String getCredit() {
        return credit;
    }

    public void setCredit(String credit) {
        this.credit = credit;
    }

    public String getDebit() {
        return debit;
    }

    public void setDebit(String debit) {
        this.debit = debit;
    }

    @SerializedName("debit")
    @Expose
    private String debit;




}
