
package com.digitalindiapayment.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DashboardBean {

    @SerializedName("withdraw")
    @Expose
    private Double withdraw;
    @SerializedName("withdraw_count_today")
    @Expose
    private Integer withdrawCountToday;
    @SerializedName("deposit")
    @Expose
    private Double deposit;
    @SerializedName("deposit_count_today")
    @Expose
    private Integer depositCountToday;
    @SerializedName("balance_enquiry_count_today")
    @Expose
    private Integer balanceEnquiryCountToday;

    @SerializedName("commission_today")
    @Expose
    private Double commissionToday;
    @SerializedName("balance")
    @Expose
    private Double balance;

    public void setWithdraw(Double withdraw) {
        this.withdraw = withdraw;
    }

    public Integer getWithdrawCountToday() {
        return withdrawCountToday;
    }

    public void setWithdrawCountToday(Integer withdrawCountToday) {
        this.withdrawCountToday = withdrawCountToday;
    }

    public void setDeposit(Double deposit) {
        this.deposit = deposit;
    }

    public Integer getDepositCountToday() {
        return depositCountToday;
    }

    public void setDepositCountToday(Integer depositCountToday) {
        this.depositCountToday = depositCountToday;
    }

    public Integer getBalanceEnquiryCountToday() {
        return balanceEnquiryCountToday;
    }

    public void setBalanceEnquiryCountToday(Integer balanceEnquiryCountToday) {
        this.balanceEnquiryCountToday = balanceEnquiryCountToday;
    }

    public Double getCommissionToday() {
        return commissionToday;
    }

    public void setCommissionToday(Double commissionToday) {
        this.commissionToday = commissionToday;
    }


    public double getWithdraw() {
        return withdraw;
    }

    public void setWithdraw(double withdraw) {
        this.withdraw = withdraw;
    }

    public double getDeposit() {
        return deposit;
    }

    public void setDeposit(double deposit) {
        this.deposit = deposit;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

}
