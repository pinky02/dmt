package com.digitalindiapayment.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Mayur on 11-04-2017.
 */

public class RequestReportBean extends TransactionReportBean {



    @SerializedName("timestamp")
    @Expose
    private String timestamp;

    @SerializedName("fees")
    @Expose
    private String fees;

    @SerializedName("credit")
    @Expose
    private String credit;


    @SerializedName("bank")
    @Expose
    private String bank;

    @SerializedName("branch")
    @Expose
    private String branch;

    @SerializedName("referenceNo")
    @Expose
    private String referenceNo;

    @SerializedName("modeOfTransfer")
    @Expose
    private String modeOfTransfer;

    @SerializedName("status")
    @Expose
    private String status;






    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getFees() {
        return fees;
    }

    public void setFees(String fees) {
        this.fees = fees;
    }

    public String getCredit() {
        return credit;
    }

    public void setCredit(String credit) {
        this.credit = credit;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public String getModeOfTransfer() {
        return modeOfTransfer;
    }

    public void setModeOfTransfer(String modeOfTransfer) {
        this.modeOfTransfer = modeOfTransfer;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }



}

