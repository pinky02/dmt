
package com.digitalindiapayment.pojo;


import android.os.Parcel;
import android.os.Parcelable;

public class TransactionDetailsBean implements Parcelable {


    public String transactionId;
    public String timestamp;
    public String aadhaarNo;
    public String bankName;
    public String bankCode;
    public String balance;
    public String response;
    public String remark;
    public String serviceMod;
    public String terminalId;
    public String agentId;
    public String BCName;
    public String mATNreqID;
    public String amount;




    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.transactionId);
        dest.writeString(this.timestamp);
        dest.writeString(this.aadhaarNo);
        dest.writeString(this.bankName);
        dest.writeString(this.bankCode);
        dest.writeString(this.balance);
        dest.writeString(this.response);
        dest.writeString(this.remark);
        dest.writeString(this.serviceMod);
        dest.writeString(this.terminalId);
        dest.writeString(this.agentId);
        dest.writeString(this.BCName);
        dest.writeString(this.mATNreqID);
        dest.writeString(this.amount);
    }

    public TransactionDetailsBean() {
    }

    protected TransactionDetailsBean(Parcel in) {
        this.transactionId = in.readString();
        this.timestamp = in.readString();
        this.aadhaarNo = in.readString();
        this.bankName = in.readString();
        this.bankCode = in.readString();
        this.balance = in.readString();
        this.response = in.readString();
        this.remark = in.readString();
        this.serviceMod = in.readString();
        this.terminalId = in.readString();
        this.agentId = in.readString();
        this.BCName = in.readString();
        this.mATNreqID = in.readString();
        this.amount = in.readString();
    }

    public static final Parcelable.Creator<TransactionDetailsBean> CREATOR = new Parcelable.Creator<TransactionDetailsBean>() {
        @Override
        public TransactionDetailsBean createFromParcel(Parcel source) {
            return new TransactionDetailsBean(source);
        }

        @Override
        public TransactionDetailsBean[] newArray(int size) {
            return new TransactionDetailsBean[size];
        }
    };
}