package com.digitalindiapayment.pojo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by DM on 20-01-2017.
 */

public class ChangePassword {

    @SerializedName("phone_no")
   private String phone_no ;

    @SerializedName("otp")
    private Integer otp;

    @SerializedName("password")
    private String password ;

    @SerializedName("password_confirmation")
    private String password_confirmation;

    public String getPhone_no() {
        return phone_no;
    }

    public void setPhone_no(String phone_no) {
        this.phone_no = phone_no;
    }

    public Integer getOtp() {
        return otp;
    }

    public void setOtp(Integer otp) {
        this.otp = otp;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword_confirmation() {
        return password_confirmation;
    }

    public void setPassword_confirmation(String password_confirmation) {
        this.password_confirmation = password_confirmation;
    }
}
