package com.digitalindiapayment.pojo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Amol on 05-04-2017.
 */

public class LastFiveTransaction {

    @SerializedName("id")
    private String id;

    @SerializedName("timestamp")
    private String createdAt;

    @SerializedName("transactionId")
    private String transactionId;

    @SerializedName("amount")
    private String amount;

    @SerializedName("balance")
    private String balance;

    @SerializedName("status")
    private String transactionStatus;

    @SerializedName("remarks")
    private String transactionRemarks;

    @SerializedName("agentId")
    private String agentId;

    @SerializedName("serviceMod")
    private String service;

    @SerializedName("aadharNo")
    private String aadhaarNumber;

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public String getTransactionRemarks() {
        return transactionRemarks;
    }

    public void setTransactionRemarks(String transactionRemarks) {
        this.transactionRemarks = transactionRemarks;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getAadhaarNumber() {
        return aadhaarNumber;
    }

    public void setAadhaarNumber(String aadhaarNumber) {
        this.aadhaarNumber = aadhaarNumber;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    @SerializedName("bank")
    private String bankCode;




}
