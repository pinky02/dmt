package com.digitalindiapayment.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Map;

/**
 * Created by admin on 10-11-2017.
 */

public class DIPLNotification implements Parcelable {

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("type")
    @Expose
    private String type;

    protected DIPLNotification(Parcel in) {
        title = in.readString();
        message = in.readString();
        type = in.readString();
    }

    public static final Creator<DIPLNotification> CREATOR = new Creator<DIPLNotification>() {
        @Override
        public DIPLNotification createFromParcel(Parcel in) {
            return new DIPLNotification(in);
        }

        @Override
        public DIPLNotification[] newArray(int size) {
            return new DIPLNotification[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(message);
        dest.writeString(type);
    }

    public static DIPLNotification create(Map<String,String > stringStringMap){
        Gson gson = new Gson();
        JsonElement jsonElement = gson.toJsonTree(stringStringMap);
        return gson.fromJson(jsonElement, DIPLNotification.class);
    }

}
