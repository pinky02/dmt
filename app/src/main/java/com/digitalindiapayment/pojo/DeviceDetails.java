package com.digitalindiapayment.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 01-12-2017.
 */

public class DeviceDetails {
    @SerializedName("deviceMake")
    @Expose
    private String deviceMake;
    @SerializedName("deviceModel")
    @Expose
    private String deviceModel;
    @SerializedName("deviceSerial")
    @Expose
    private String deviceSerial;

    public String getDeviceMake() {
        return deviceMake;
    }

    public void setDeviceMake(String deviceMake) {
        this.deviceMake = deviceMake;
    }

    public String getDeviceModel() {
        return deviceModel;
    }

    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }

    public String getDeviceSerial() {
        return deviceSerial;
    }

    public void setDeviceSerial(String deviceSerial) {
        this.deviceSerial = deviceSerial;
    }
}
