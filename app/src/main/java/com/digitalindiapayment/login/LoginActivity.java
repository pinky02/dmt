package com.digitalindiapayment.login;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.atritechnocrat.view.ValidatorTextInputLayout;
import com.crashlytics.android.Crashlytics;
import com.digitalindiapayment.BaseActivity;
import com.digitalindiapayment.BuildConfig;
import com.digitalindiapayment.R;
import com.digitalindiapayment.changePassword.ChangePass;
import com.digitalindiapayment.fcm.MyFirebaseMessagingService;
import com.digitalindiapayment.landingpage.EntryPointForEveryService;
import com.digitalindiapayment.landingpage.HomeActivity;
import com.digitalindiapayment.pojo.Login;
import com.digitalindiapayment.resetSession.ResetSession;
import com.digitalindiapayment.screenanlytics.AppFirebaseAnalytics;
import com.digitalindiapayment.screenanlytics.IAnalytics;
import com.digitalindiapayment.signUp.SignUpActivity;
import com.digitalindiapayment.util.Util;
import com.digitalindiapayment.webservices.requestpojo.ForgotPasswordRequest;
import com.google.android.gms.safetynet.SafetyNet;
import com.google.android.gms.safetynet.SafetyNetApi;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Amol Bhanushali on 03-03-2017.
 */
public class LoginActivity extends BaseActivity implements LoginContract.View {

    public static final int RESET_SESSION = 101;
    private static final String TAG = "LoginActivity";
    LoginContract.UserActions loginPresenter;
    @BindView(R.id.username)
    EditText username;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.login)
    Button loginbtn;
    @BindView(R.id.vesion)
    TextView vesion;
    Login login;
    @BindView(R.id.forget_password)
    TextView forgotPassword;
    Dialog builder;
    EditText number;
    Button submitOtp;
    TextView resetTextView;
    Button resetButton;
    @BindView(R.id.contact_us)
    TextView contactUs;
    IAnalytics iAnalytics;

    @BindView(R.id.imgCheck)
    ImageView imgCheck;
    @BindView(R.id.tvCheckbox)
    TextView tvCheckbox;
    @BindView(R.id.progressCheck)
    ProgressBar progressCheck;
    private boolean iscaptchaverify = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        FirebaseMessaging.getInstance().subscribeToTopic(Util.KEYS.DEFAULT_NOTIFICATION_TOPIC);
        Util.PREFERENCES.setIsLogin(this, false);
        Util.PREFERENCES.setNotificationRegId(LoginActivity.this, FirebaseInstanceId.getInstance().getToken());
        Util.PREFERENCES.setEvoluteBluetoothMac(getApplicationContext(), "");
        Util.PREFERENCES.setEvoluteBluetoothName(getApplicationContext(), "");
        Util.isBluetoothConnected = false;
        Log.d(TAG, " Firebase InstanceId : " + Util.PREFERENCES.getNotificationRegId(getApplicationContext()));

        /*if (getIntent().hasExtra(UtilDmt.KEYS.INFORMATION)){
            from=getIntent().getStringExtra(UtilDmt.KEYS.INFORMATION);
            actionCode=getIntent().getStringExtra(UtilDmt.KEYS.ACTION_CODE);
        }*/
        username.setText(Util.PREFERENCES.getUserId(this));

        if (BuildConfig.DEBUG) {
            // do something for a debug build
//            username.setText("9702839333");
//            password.setText("dipl@123");
            //username.s
            // etText("9702839333");
            password.setText("refund@123");
            //username.setText("9730966971");
            //password.setText("password");
            username.setText("9702839333");
            //password.setText("dipl@123");
            iscaptchaverify = true;
        }


        login = new Login();

        password.setFilters(new InputFilter[]{
                new InputFilter() {
                    public CharSequence filter(CharSequence src, int start,
                                               int end, Spanned dst, int dstart, int dend) {
                        if (src.equals("")) { // for backspace
                            return src;
                        }
                        if (src.toString().matches("[\\x00-\\x7F]+")) {
                            return src;
                        }
                        return "";
                    }
                }
        });


        loginPresenter = new LoginPresenter(this, getApplicationContext());
        String versionName = Util.getVersionName(getApplicationContext());
        vesion.setText(getString(R.string.verion) + " " + versionName);

        forgotPasswordSetOnClick();
        //if (!BuildConfig.DEBUG) {
        Util.PREFERENCES.clearLoginSession(LoginActivity.this);
        //}


        contactUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, SignUpActivity.class));
            }
        });

        iAnalytics = new AppFirebaseAnalytics(LoginActivity.this);
    }

    @OnClick(R.id.login)
    public void setLogin() {

        if (username.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(getApplicationContext(), getString(R.string.pls_enter_mobile_no), Toast.LENGTH_SHORT).show();

            return;
        }
        if (password.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(getApplicationContext(), getString(R.string.pls_enter_password), Toast.LENGTH_SHORT).show();
            return;
        }
        if (!iscaptchaverify) {
            Toast.makeText(this, R.string.str_verify_captcha, Toast.LENGTH_SHORT).show();
            return;
        }

        login.setuid(username.getText().toString());
        login.setPassword(password.getText().toString());

        // Crash-Analytics
        Crashlytics.setUserIdentifier(username.getText().toString());


        loginPresenter.submitAgentLogin(login);
        //this method will be used when we will login all users via single method
        //loginPresenter.submitLogin(login);

    }

    @Override
    public void showHome(final String token, UserProfile userProfile) {

        iAnalytics.trackEvent(FirebaseAnalytics.Event.LOGIN, null);

        // Crash-Analytics
        Crashlytics.setUserIdentifier(username.getText().toString() + "  " + Util.PREFERENCES.getDeviceName(getApplicationContext()));


        hideLoading();
        Util.PREFERENCES.setAgentName(this,userProfile.getName());
        Util.PREFERENCES.setToken(this, token);
        Util.PREFERENCES.setIsLogin(this, true);

        //Change for AEPS and DMT in one App
        Intent loginToHome = new Intent(getApplicationContext(), EntryPointForEveryService.class);
        loginToHome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        loginToHome.putExtra(Util.KEYS.DATA, userProfile);
        Util.PREFERENCES.setLogin(this, login);
        loginToHome.putExtra(MyFirebaseMessagingService.NOTIFICATION_DATA, getIntent().getParcelableExtra(MyFirebaseMessagingService.NOTIFICATION_DATA));
        startActivity(loginToHome);
//        Intent loginToHome = new Intent(getApplicationContext(), HomeActivity.class);
//        loginToHome.putExtra(UtilDmt.KEYS.DATA, userProfile);
//        loginToHome.putExtra(MyFirebaseMessagingService.NOTIFICATION_DATA, getIntent().getParcelableExtra(MyFirebaseMessagingService.NOTIFICATION_DATA));
//        UtilDmt.PREFERENCES.setLogin(this, login);
//        loginToHome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        startActivity(loginToHome);
        finish();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        /*if (intent.hasExtra("information")){
            from=intent.getStringExtra("information");
            actionCode =intent.getStringExtra("actionCode");
        }*/
    }

    @Override
    public void showForgotScreen() {
    }

    @Override
    public void noInternet() {
        Toast.makeText(getApplicationContext(), "No internet", Toast.LENGTH_LONG).show();
        disMissDialog();

    }

    @Override
    public void exception(Throwable e) {
        Toast.makeText(getApplicationContext(), getString(R.string.invalid_credentials), Toast.LENGTH_LONG).show();
        disMissDialog();
    }

    @Override
    public void fail(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
        disMissDialog();
    }

    @Override
    public void success(String message) {

    }

    @Override
    public void showLoading() {
        showDialog();
    }

    @Override
    public void hideLoading() {
        disMissDialog();
    }

    @Override
    public void showOTP() {
        Log.d(TAG, "showOTP");
        //Toast.makeText(getApplicationContext(), getString(R.string.otp_success), Toast.LENGTH_LONG).show();
        Intent forgotPassword = new Intent(LoginActivity.this, ChangePass.class);
        String mobileNumber = number.getText().toString();
        forgotPassword.putExtra(getString(R.string.mobilenumber), number.getText().toString());
        startActivity(forgotPassword);
        builder.dismiss();
    }

    @Override
    public void inValidOTP(String message) {
        if (message.length() > 0) {
            //number.setHint(message);
            if (number != null && submitOtp != null) {
                number.setError(message);
                submitOtp.setEnabled(true);
                submitOtp.setText(getString(R.string.send_otp));
            }
            builder.setCancelable(true);
        }
    }

    @Override
    public void onResetSessionCall() {
        builder = new Dialog(LoginActivity.this);
        builder.setContentView(R.layout.layout_reset_session);
        builder.setCancelable(true);

        resetTextView = (TextView) builder.findViewById(R.id.resetTextView);
        resetButton = (Button) builder.findViewById(R.id.resetButton);

        Window window = builder.getWindow();
        window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent resetSession = new Intent(LoginActivity.this, ResetSession.class);
                //resetSession.putExtra(UtilDmt.KEYS.DATA,username.getText().toString());
                //startActivityForResult(resetSession,RESET_SESSION);
                builder.setCancelable(false);
                loginPresenter.resetSession(new ForgotPasswordRequest(username.getText().toString()));
                resetButton.setText(getString(R.string.processing));

                //builder.dismiss();
            }
        });
        builder.show();
    }

    @Override
    public void onResetSessionComplete() {
        builder.dismiss();
        Intent resetSession = new Intent(LoginActivity.this, ResetSession.class);
        resetSession.putExtra(getString(R.string.mobilenumber), username.getText().toString());
        startActivityForResult(resetSession, RESET_SESSION);

        //startActivityForResult(resetSession,RESET_SESSION);
    }

    @Override
    public void onResetSessionFail() {
        if (builder != null)
            builder.dismiss();
        Util.showShortToast(getApplicationContext(), getString(R.string.request_not_completed));

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == RESET_SESSION) {
                loginPresenter.getUsersInfo();
            }
        }

    }

    protected void forgotPasswordSetOnClick() {
        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                builder = new Dialog(LoginActivity.this);
                builder.setContentView(R.layout.layout_forgot_password);
                ButterKnife.bind(builder);
                Window window = builder.getWindow();
                window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
                number = (EditText) builder.findViewById(R.id.number);
                submitOtp = (Button) builder.findViewById(R.id.submitOtp);
                final ValidatorTextInputLayout validatorTextInputLayout = (ValidatorTextInputLayout) builder.findViewById(R.id.otpValidator);
                try {

                    submitOtp.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ForgotPasswordRequest forgotPasswordRequest = new ForgotPasswordRequest(number.getText().toString());
                            try {
                                if (validatorTextInputLayout.validate()) {
                                    if (number.getText().toString().length() < 10) {
                                        number.setError(getString(R.string.enter_10_digit_mobile));
                                    } else {
                                        Log.d(TAG, "Api Calling");
                                        submitOtp.setEnabled(false);
                                        builder.setCancelable(false);
                                        submitOtp.setText(getString(R.string.processing));
                                        //forgotPasswordRequest.setPhone_no(number.getText().toString());
                                        loginPresenter.forgotPassword(forgotPasswordRequest);
//                                        submitOtp.setOnClickListener(null);
//                                        builder.dismiss();
                                        //showOTP();
                                    }
                                }

                            } catch (Exception e) {
                            }
                        }
                    });
                } catch (Exception e) {
                }
                builder.show();
            }
        });
    }

    public void showLogin() {

    }

    @Override
    protected void onResume() {
        super.onResume();
        iAnalytics.trackScreen(getString(R.string.title_activity_login));
    }

    @OnClick(R.id.contact_us)
    public void onViewClicked() {
        startActivity(new Intent(LoginActivity.this, SignUpActivity.class));
    }

    @OnClick(R.id.tvCheckbox)
    public void verifyCaptcha() {
        progressCheck.setVisibility(View.VISIBLE);
        tvCheckbox.setVisibility(View.GONE);
        SafetyNet.getClient(this)
                .verifyWithRecaptcha(getString(R.string.site_key))
                .addOnSuccessListener(this, new OnSuccessListener<SafetyNetApi.RecaptchaTokenResponse>() {
                    @Override
                    public void onSuccess(SafetyNetApi.RecaptchaTokenResponse recaptchaTokenResponse) {
                        progressCheck.setVisibility(View.GONE);
                        tvCheckbox.setVisibility(View.GONE);
                        imgCheck.setVisibility(View.VISIBLE);
                        iscaptchaverify = true;
                        Log.e(TAG, "onSuccess");
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        progressCheck.setVisibility(View.GONE);
                        tvCheckbox.setVisibility(View.VISIBLE);
                        Toast.makeText(LoginActivity.this, R.string.str_verify_again, Toast.LENGTH_SHORT).show();
                        Log.e(TAG, "onFailure: " + e.getMessage());
                    }
                });
    }
}
