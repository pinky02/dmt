package com.digitalindiapayment.login;

import android.content.Context;
import android.util.Log;

import com.digitalindiapayment.R;
import com.digitalindiapayment.pojo.AddMemberResponse;
import com.digitalindiapayment.pojo.Login;
import com.digitalindiapayment.pojo.LoginResponse;
import com.digitalindiapayment.util.Util;
import com.digitalindiapayment.webservices.Api;
import com.digitalindiapayment.webservices.SessionError;
import com.digitalindiapayment.webservices.core.ApiFail;
import com.digitalindiapayment.webservices.core.ApiSuccess;
import com.digitalindiapayment.webservices.core.HttpErrorResponse;
import com.digitalindiapayment.webservices.requestpojo.ForgotPasswordRequest;
import com.digitalindiapayment.webservices.requestpojo.ResetSessionRequest;
import com.google.firebase.messaging.FirebaseMessaging;

import java.io.IOException;

import me.zhanghai.android.materialprogressbar.BuildConfig;
import retrofit2.Response;
import retrofit2.adapter.rxjava.HttpException;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by Vikas Pawar on 12-01-2017.
 */

public class LoginPresenter implements LoginContract.UserActions {
    private static final String TAG = "LoginPresentor";
    private final LoginContract.View view;
    private final Context context;

    public LoginPresenter(LoginContract.View view, Context context) {
        this.view = view;
        this.context = context;
    }


    @Override
    public void submitAgentLogin(Login login) {
        login("agent", login);
    }


    @Override
    public void submitDistributorLogin(Login login) {
        login("distributor", login);
    }

    @Override
    public void resetSession(ForgotPasswordRequest request) {
        Api.userManagement().resetSession(Util.PREFERENCES.getToken(context),request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ApiSuccess<String>() {
                    @Override
                    public void call(String addMemberResponseResponse) {
                        Log.d(TAG, "subscribe > call");
                        //if(addMemberResponseResponse.equals(context.getString(R.string.api_success_key))){
                        view.onResetSessionComplete();
                        //}
                    }
                }, new ApiFail() {
                    @Override
                    public void httpStatus(HttpErrorResponse response) {
                        view.onResetSessionFail();
                    }

                    @Override
                    public void noNetworkError() {
                        view.noInternet();
                    }

                    @Override
                    public void unknownError(Throwable e) {
                        view.fail(e.getMessage());
                    }
                });
    }

    @Override
    public void getUsersInfo() {
        view.showLoading();
        Api.userManagement().profile( Util.PREFERENCES.getToken(context),Util.PREFERENCES.getNotificationRegId(context))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProfileApiSuccess(view,context), new ApiFail() {
                    @Override
                    public void httpStatus(HttpErrorResponse response) {
                        view.inValidOTP(response.getError());
                    }

                    @Override
                    public void noNetworkError() {
                        view.noInternet();
                    }

                    @Override
                    public void unknownError(Throwable e) {
                        view.fail(e.getMessage());
                    }
                });

    }

    @Override
    public void submitSuperDistributorLogin(Login login) {
        login("superdistributor", login);
    }


    public void login(String role, Login login) {

        view.showLoading();
        //view.showHome(UtilDmt.PREFERENCES.getToken(context));

        login.setMobile_auth_code(Util.PREFERENCES.getUniqueDeviceKey(context));

        Api.userManagement().userLogin(role, login)
                .subscribeOn(Schedulers.io())
                .flatMap(new Func1<Response<String>, rx.Observable<UserProfile>>() {
                    @Override
                    public rx.Observable<UserProfile> call(Response<String> loginResponse) {
                        Log.d(TAG, "call1 " + loginResponse.body());
                        if (loginResponse.isSuccessful()) {
                            String token = loginResponse.headers().get("auth");
                            Util.PREFERENCES.setToken(context, token);
                        } else {
                            String message = "";
                            if (loginResponse.code() == 409) {
                                //UtilDmt.PREFERENCES.setToken(context, loginResponse.headers().get("auth"));
                                throw new SessionError(loginResponse);
                            }else{
                                throw new RuntimeException(message);
                            }


                        }
                        Log.d(TAG, "call1 " + "below throw calls");

                        return Api.userManagement().profile(loginResponse.headers().get("auth"),Util.PREFERENCES.getNotificationRegId(context));
                    }
                })
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new ProfileApiSuccess(view,context), new ApiFail() {
            @Override
            public void httpStatus(HttpErrorResponse response) {

                view.hideLoading();
                if (response.getHttpStatusCode() == 409) {
                    view.onResetSessionCall();
                    return;
                }
                view.fail(response.getError());
                Log.d(TAG, "httpStatus");


            }

            @Override
            public void noNetworkError() {
                Log.d(TAG, "noNetworkError");
                view.noInternet();
                view.hideLoading();
            }

            @Override
            public void unknownError(Throwable e) {
                Log.d(TAG, "unknownError" + e.getMessage());
                //view.onResetSessionCall();
                view.exception(e);
            }
        });

    }

    @Override
    public void forgotPassword(ForgotPasswordRequest forgotPasswordRequest) {
        //String token = "";//Aarogya.getToken(context);
        Api.userManagement().forgotPassword(forgotPasswordRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ApiSuccess<String>() {
                    @Override
                    public void call(String addMemberResponseResponse) {
                        Log.d(TAG, "subscribe > call");
                        //if(addMemberResponseResponse.equals(context.getString(R.string.api_success_key))){
                        view.showOTP();
                        //}
                    }
                }, new ApiFail() {
                    @Override
                    public void httpStatus(HttpErrorResponse response) {
                        view.inValidOTP(response.getError());
                    }

                    @Override
                    public void noNetworkError() {
                        view.noInternet();
                    }

                    @Override
                    public void unknownError(Throwable e) {
                        view.fail(e.getMessage());
                    }
                });
    }



}

