package com.digitalindiapayment.login;

import android.content.Context;

import com.digitalindiapayment.R;
import com.digitalindiapayment.util.Util;
import com.digitalindiapayment.webservices.core.ApiSuccess;

/**
 * Created by admin on 29-09-2017.
 */

public class ProfileApiSuccess extends ApiSuccess<UserProfile> {

    private LoginContract.View view;
    private Context context;

    public ProfileApiSuccess(LoginContract.View view, Context context) {
        this.view = view;
        this.context = context;
    }

    @Override
    public void call(UserProfile userProfile) {
        String[] scannerDevice = context.getResources().getStringArray(R.array.scannerDevice);
        // scannerDevice
        // Set Device for fingure print On Api call
        //if (UtilDmt.PREFERENCES.getDeviceName(context).equals("")) {
        Util.PREFERENCES.setAgetUserId(context,userProfile.getId());
        Util.PREFERENCES.setFname(context,userProfile.getName());
        Util.PREFERENCES.setEmail(context,userProfile.getEmail());
        Util.PREFERENCES.setMobile(context,userProfile.getMobileNumber());
        Util.PREFERENCES.setDeviceName(context, scannerDevice[userProfile.getDeviceId()]);
        //}
        if (Util.PREFERENCES.getDefaultPrinter(context).equals("")) {
            Util.PREFERENCES.setDefaultPrinter(context, context.getResources().getString(R.string.device_evolute_printer));
        }
        Util.PREFERENCES.setRDEnabled(context, userProfile.getDeviceService());
        Util.PREFERENCES.setTerminalID(context, userProfile.getTerminalId());
        view.showHome(Util.PREFERENCES.getToken(context), userProfile);
    }
}
