package com.digitalindiapayment.login;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserProfile implements Parcelable{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("userName")
    @Expose
    private String userName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("mobileNumber")
    @Expose
    private String mobileNumber;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("balance")
    @Expose
    private String balance;
    @SerializedName("joiningDate")
    @Expose
    private String joiningDate;
    @SerializedName("dipId")
    @Expose
    private String dipId;
    @SerializedName("terminalId")
    @Expose
    private String terminalId;
    @SerializedName("udc")
    @Expose
    private String udc;
    @SerializedName("device_id")
    @Expose
    private String deviceId;

    @SerializedName("device_service")
    @Expose
    private String deviceService;


    protected UserProfile(Parcel in) {
        id = in.readString();
        name = in.readString();
        userName = in.readString();
        email = in.readString();
        mobileNumber = in.readString();
        image = in.readString();
        status = in.readString();
        balance = in.readString();
        joiningDate = in.readString();
        dipId = in.readString();
        terminalId = in.readString();
        udc = in.readString();
        deviceId = in.readString();
        deviceService = in.readString();
    }

    public static final Creator<UserProfile> CREATOR = new Creator<UserProfile>() {
        @Override
        public UserProfile createFromParcel(Parcel in) {
            return new UserProfile(in);
        }

        @Override
        public UserProfile[] newArray(int size) {
            return new UserProfile[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getStatus() {
        return Integer.parseInt(status);
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getJoiningDate() {
        return joiningDate;
    }

    public void setJoiningDate(String joiningDate) {
        this.joiningDate = joiningDate;
    }

    public String getDipId() {
        return dipId;
    }

    public void setDipId(String dipId) {
        this.dipId = dipId;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getUdc() {
        return udc;
    }

    public void setUdc(String udc) {
        this.udc = udc;
    }

    public int getDeviceId() {
        return deviceId != null ? Integer.parseInt(deviceId) : 1 ;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceService() {
        return deviceService;
    }

    public void setDeviceService(String deviceService) {
        this.deviceService = deviceService;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(userName);
        dest.writeString(email);
        dest.writeString(mobileNumber);
        dest.writeString(image);
        dest.writeString(status);
        dest.writeString(balance);
        dest.writeString(joiningDate);
        dest.writeString(dipId);
        dest.writeString(terminalId);
        dest.writeString(udc);
        dest.writeString(deviceId);
        dest.writeString(deviceService);
    }
}



