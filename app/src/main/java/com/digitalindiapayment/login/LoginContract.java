package com.digitalindiapayment.login;


import com.digitalindiapayment.mvp.BaseView;
import com.digitalindiapayment.pojo.Login;
import com.digitalindiapayment.resetSession.ResetSession;
import com.digitalindiapayment.webservices.requestpojo.ForgotPasswordRequest;
import com.digitalindiapayment.webservices.requestpojo.ResetSessionRequest;

/**
 * Created by Vikas Pawar on 12-01-2017.
 */

public class LoginContract {

    interface View extends BaseView {

        void showHome(String token,UserProfile userProfile);
        void showForgotScreen();
        void showOTP();
        void showLogin();
        void inValidOTP(String message);
        void onResetSessionCall();
        void onResetSessionFail();
        void onResetSessionComplete();

    }

    interface UserActions {

        //void submitLogin (Login login);
        void submitAgentLogin(Login login);
        void forgotPassword(ForgotPasswordRequest forgotPasswordRequest);
        void submitSuperDistributorLogin(Login login);
        void submitDistributorLogin(Login login);
        void resetSession(ForgotPasswordRequest request);
        void getUsersInfo();


    }


}
