package com.digitalindiapayment.mvp;

/**
 * Created by ashwinnbhanushali on 04/11/16.
 */

public interface BaseView {
    void noInternet();

    void exception(Throwable e);

    void fail(String message);

    void success(String message);

    void showLoading();

    void hideLoading();

    //void invalidToken();
}
