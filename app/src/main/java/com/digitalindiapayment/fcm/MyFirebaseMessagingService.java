/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.digitalindiapayment.fcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.digitalindiapayment.R;
import com.digitalindiapayment.SplashActivity;
import com.digitalindiapayment.fcm.notification.NotificationUtils;
import com.digitalindiapayment.landingpage.HomeActivity;
import com.digitalindiapayment.pojo.DIPLNotification;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import java.util.Date;
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    public static final String NOTIFICATION_MESSAGE = "message";
    public static final String NOTIFICATION_TYPE = "type";
    public static final String NOTIFICATION_TITLE = "title";
    public static final String NOTIFICATION_DATA = "notification_data";
    private static final String TAG = "MyFirebaseMsgService";
    private NotificationUtils notificationUtils;

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());

        Intent intent = new Intent(this, SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP );

        intent.putExtra(NOTIFICATION_DATA, DIPLNotification.create(remoteMessage.getData()));


        if (remoteMessage.getData().size() > 0) {

            if (remoteMessage.getNotification() != null) {

            }

        } else {

        }
        notificationUtils.showNotificationMessage(remoteMessage.getData().get(NOTIFICATION_TITLE), remoteMessage.getData().get(NOTIFICATION_MESSAGE), (new Date()).toString(), intent);
    }

}
