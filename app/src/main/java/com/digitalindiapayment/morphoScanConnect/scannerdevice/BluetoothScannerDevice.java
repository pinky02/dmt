package com.digitalindiapayment.morphoScanConnect.scannerdevice;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by amolbhanushali on 2017/12/04.
 */

public class BluetoothScannerDevice implements Device ,Parcelable{
    private String name;
    private String mac;

    protected BluetoothScannerDevice(Parcel in) {
        name = in.readString();
        mac = in.readString();
    }

    public static final Creator<BluetoothScannerDevice> CREATOR = new Creator<BluetoothScannerDevice>() {
        @Override
        public BluetoothScannerDevice createFromParcel(Parcel in) {
            return new BluetoothScannerDevice(in);
        }

        @Override
        public BluetoothScannerDevice[] newArray(int size) {
            return new BluetoothScannerDevice[size];
        }
    };

    public BluetoothScannerDevice() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeString(mac);
    }
}
