package com.digitalindiapayment.morphoScanConnect.availableMorphoes;

import android.content.Context;
import android.util.Log;

import com.digitalindiapayment.morphoScanConnect.scannerdevice.Device;
import com.digitalindiapayment.pojo.morphoXmlBeans.CustOpts;
import com.digitalindiapayment.pojo.morphoXmlBeans.Demo;
import com.digitalindiapayment.pojo.morphoXmlBeans.Opts;
import com.digitalindiapayment.pojo.morphoXmlBeans.Param;
import com.digitalindiapayment.pojo.morphoXmlBeans.PidOptions;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.StringWriter;
import java.util.ArrayList;

/**
 * Created by Nivrutti Pawar on 2018/01/09.
 */

public class StartekScanner extends RDActions implements PidOption {
    private Context context;

    public StartekScanner(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    public String getPidOption(Device device) {


        try {
            ///////////
            int fingerCount = 1;
            // FMR:0 FIR:1
            int fingerType = 0;
            // For XML : 0 Protobuf:1
            int fingerFormat = 0;
            String pidVer = "2.0";
            String timeOut = "10000";
            String posh = "UNKNOWN";
            Opts opts = new Opts();
            opts.fCount = String.valueOf(fingerCount);
            opts.fType = String.valueOf(fingerType);
            opts.iCount = "0";
            opts.iType = "0";
            opts.pCount = "0";
            opts.pType = "0";
            opts.format = String.valueOf(fingerFormat);
            opts.pidVer = pidVer;
            opts.timeout = timeOut;
//            opts.otp = "";
            opts.posh = posh;
            opts.wadh = "";
            opts.env = "P";
            Demo demo = new Demo();
            CustOpts custOpts = new CustOpts();
            PidOptions pidOptions = new PidOptions();
            pidOptions.ver = pidVer;
            pidOptions.Opts = opts;
            pidOptions.Demo = demo;
            pidOptions.CustOpts = custOpts;
            custOpts.params = new ArrayList<>();
            custOpts.params.add(new Param("", ""));
            Serializer serializer = new Persister();
            StringWriter writer = new StringWriter();
            serializer.write(pidOptions, writer);
            return writer.toString();
        } catch (Exception e) {
            Log.e("Error", e.toString());
        }

        return null;
    }

    @Override
    String getPackageName() {
        return "com.acpl.registersdk";
    }

    @Override
    String getIntentCaptureActionName() {
        return "in.gov.uidai.rdservice.fp.CAPTURE";
    }

    @Override
    String getIntentDeviceInfoActionName() {
        return "in.gov.uidai.rdservice.fp.INFO";
    }

}
