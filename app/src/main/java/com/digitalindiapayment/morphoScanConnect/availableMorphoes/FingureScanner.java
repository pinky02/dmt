package com.digitalindiapayment.morphoScanConnect.availableMorphoes;


import com.digitalindiapayment.pojo.morphoXmlBeans.PidOptions;
import com.morpho.sample.FingerprintData;

/**
 * Created by admin on 06-07-2017.
 */

public interface FingureScanner {
    public void initializeDevice();
    public String getDeviseTerminalId();
    public void stop();
    public void register();
}
