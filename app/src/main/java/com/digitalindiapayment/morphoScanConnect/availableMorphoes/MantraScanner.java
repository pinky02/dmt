package com.digitalindiapayment.morphoScanConnect.availableMorphoes;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.widget.Toast;

import com.digitalindiapayment.morphoScanConnect.scannerdevice.Device;
import com.digitalindiapayment.pojo.morphoXmlBeans.CustOpts;
import com.digitalindiapayment.pojo.morphoXmlBeans.Demo;
import com.digitalindiapayment.pojo.morphoXmlBeans.Opts;
import com.digitalindiapayment.pojo.morphoXmlBeans.Param;
import com.digitalindiapayment.pojo.morphoXmlBeans.PidOptions;
import com.digitalindiapayment.util.Util;
import com.mantra.mfs100.FingerData;
import com.mantra.mfs100.MFS100;
import com.mantra.mfs100.MFS100Event;
import com.morpho.sample.FingerprintData;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.StringWriter;
import java.util.ArrayList;

import rx.Observable;

import static com.digitalindiapayment.util.Util.KEYS.FINGERPRINT_DATA_REQUEST;

/**
 * Created by admin on 06-07-2017.
 */

public class MantraScanner extends RDActions implements FingureScanner,MFS100Event,ManagementClient,PidOption{

    private static final String TAG = "MantraScanner";
    Context context;

    int timeout = 10000;
    MFS100 mfs100 = null;

    @Override
    String getPackageName() {
        return "com.mantra.rdservice";
    }

    @Override
    String getIntentCaptureActionName() {
        return "in.gov.uidai.rdservice.fp.CAPTURE";
    }

    @Override
    String getIntentDeviceInfoActionName() {
        return "in.gov.uidai.rdservice.fp.INFO";
    }

    public MantraScanner(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    public void initializeDevice() {
        mfs100 = new MFS100(this);
        mfs100.SetApplicationContext(context);
    }

    @Override
    public String getDeviseTerminalId() {
        try {
            int ret = mfs100.Init();
            if (ret != 0) {
                //SetTextOnUIThread(mfs100.GetErrorMsg(ret));
            } else {
                //SetTextOnUIThread("Init success");
                String info = "Serial: " + mfs100.GetDeviceInfo().SerialNo()
                        + " Make: " + mfs100.GetDeviceInfo().Make()
                        + " Model: " + mfs100.GetDeviceInfo().Model()
                        + "\nCertificate: " + mfs100.GetCertification();
                //SetLogOnUIThread(info);
                Log.d(TAG," info : "+info);
                return  mfs100.GetDeviceInfo().SerialNo();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            //SetTextOnUIThread("Init failed, unhandled exception");
        }
        return "";
    }

    @Override
    public void stop() {
        if (mfs100 != null) {
            mfs100.Dispose();
        }
    }

    @Override
    public void register() {
    }

    //@Override
    /*public void scanFingure(String pidOptions) {
        Intent fpScanIntent = new Intent();
        fpScanIntent.setAction("in.gov.uidai.rdservice.fp.CAPTURE");
        fpScanIntent.putExtra("PID_OPTIONS", pidOptions);
        fpScanIntent.setPackage("com.mantra.rdservice");
        ((Activity) context).startActivityForResult(fpScanIntent, FINGERPRINT_DATA_REQUEST);
    }*/

    @Override
    public void OnDeviceAttached(int i, int i1, boolean b) {

    }

    @Override
    public void OnDeviceDetached() {

    }

    @Override
    public void OnHostCheckFailed(String s) {

    }

    @Override
    public String getManagementClientPackage() {
        return "com.mantra.clientmanagement";
    }

    @Override
    public String getPidOption(Device  device) {
        try {
            ///////////
            int fingerCount = 1;
            // FMR:0 FIR:1
            int fingerType = 0;
            // For XML : 0 Protobuf:1
            int fingerFormat = 0;
            String pidVer = "2.0";
            String timeOut = "10000";
            String posh = "UNKNOWN";
            Opts opts = new Opts();
            opts.fCount = String.valueOf(fingerCount);
            opts.fType = String.valueOf(fingerType);
            opts.iCount = "0";
            opts.iType = "0";
            opts.pCount = "0";
            opts.pType = "0";
            opts.format = String.valueOf(fingerFormat);
            opts.pidVer = pidVer;
            opts.timeout = timeOut;
//            opts.otp = "";
            opts.posh = posh;
            opts.wadh = "";
            String env = "P";
            opts.env = env;
            Demo demo = new Demo();
            CustOpts custOpts = new CustOpts();
            PidOptions pidOptions = new PidOptions();
            pidOptions.ver = pidVer;
            pidOptions.Opts = opts;
            pidOptions.Demo = demo;
            pidOptions.CustOpts = custOpts;
            custOpts.params = new ArrayList<>();
            custOpts.params.add(new Param("", ""));
            Serializer serializer = new Persister();
            StringWriter writer = new StringWriter();
            serializer.write(pidOptions, writer);
            return writer.toString();
        } catch (Exception e) {
            Log.e("Error", e.toString());
        }

        return null;
    }
}
