package com.digitalindiapayment.morphoScanConnect.availableMorphoes;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.digitalindiapayment.R;
import com.digitalindiapayment.termsandcondition.TermsAndCondition;
import com.digitalindiapayment.util.Util;

/**
 * Created by admin on 09-10-2017.
 */

public abstract class RDActions {

    public static final String PID_OPTIONS = "PID_OPTIONS";
    Context context;
    private String TAG = "RDActions";

    public RDActions(Context context) {
        this.context = context;
    }

    abstract String getPackageName();

    abstract String getIntentCaptureActionName();

    abstract String getIntentDeviceInfoActionName();

    public Boolean scanFinger(String pidOption) {
        Intent fpScanIntent = new Intent(getIntentCaptureActionName());
        fpScanIntent.addCategory(Intent.CATEGORY_DEFAULT);
        if (!getPackageName().equals(""))
            fpScanIntent.setPackage(getPackageName());
        // in.setFlags(0x10000000);
        fpScanIntent.putExtra(PID_OPTIONS, pidOption);
        try {
            ((Activity) context).startActivityForResult(fpScanIntent, Util.KEYS.FINGERPRINT_DATA_REQUEST);
        } catch (Exception e) {
            e.printStackTrace();
            Util.showLongToast(context, String.format(context.getString(R.string.rd_service_not_installed),Util.PREFERENCES.getDeviceName(context) ));
            return false;
        }
        return true;
    }

    public Boolean getDeviceInfo() {

        Log.d(TAG, getIntentDeviceInfoActionName() + " package " + getPackageName());

        Intent fpScanIntent = new Intent(getIntentDeviceInfoActionName(),null);
        //fpScanIntent.addCategory(Intent.CATEGORY_DEFAULT);
        if (!getPackageName().equals(""))
            fpScanIntent.setPackage(getPackageName());
        // in.setFlags(0x10000000);
        try {
            ((Activity) context).startActivityForResult(fpScanIntent, Util.KEYS.MORPHO_DEVICE_INFO);
        } catch (Exception e) {
            e.printStackTrace();
            Util.showLongToast(context, String.format(context.getString(R.string.rd_service_not_installed),Util.PREFERENCES.getDeviceName(context) ));
            return false;
        }
        return true;

    }

}
