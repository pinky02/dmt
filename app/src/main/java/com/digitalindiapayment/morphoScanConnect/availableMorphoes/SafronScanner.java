package com.digitalindiapayment.morphoScanConnect.availableMorphoes;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.util.Log;
import com.digitalindiapayment.R;
import com.digitalindiapayment.morphoScanConnect.scannerdevice.Device;
import com.digitalindiapayment.pojo.DeviceDetails;
import com.digitalindiapayment.pojo.morphoXmlBeans.CustOpts;
import com.digitalindiapayment.pojo.morphoXmlBeans.Demo;
import com.digitalindiapayment.pojo.morphoXmlBeans.Opts;
import com.digitalindiapayment.pojo.morphoXmlBeans.Param;
import com.digitalindiapayment.pojo.morphoXmlBeans.PidOptions;
import com.digitalindiapayment.util.Util;
import com.morpho.morphosample.info.MorphoInfo;
import com.morpho.morphosample.info.ProcessInfo;
import com.morpho.morphosmart.sdk.DescriptorID;
import com.morpho.morphosmart.sdk.ErrorCodes;
import com.morpho.morphosmart.sdk.MorphoDevice;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by admin on 06-07-2017.
 */

public class SafronScanner extends RDActions implements FingureScanner,DeviceDescriptor,ManagementClient,PidOption {

    private static final String TAG = "SafronScanner";
    MorphoDevice morphoDevice;
    Context context;
    DeviceDetails deviceDetails;

    public SafronScanner(Context context) {
        super(context);
        this.context = context;
        deviceDetails = new DeviceDetails();
    }

    @Override
    String getPackageName() {
        return "com.morpho.registerdeviceservice";
    }

    @Override
    String getIntentCaptureActionName() {
        return "in.gov.uidai.rdservice.fp.CAPTURE";
    }

    @Override
    String getIntentDeviceInfoActionName() {
        return "in.gov.uidai.rdservice.fp.INFO";
    }

    @Override
    public void initializeDevice() {
        Log.d(TAG, "Initialize Device");
        morphoDevice = new MorphoDevice();

    }

    @Override
    public String getDeviseTerminalId() {
        Log.d(TAG, "getDeviceTerminalId");
        String sensorName = "";
        String terminalId = "";
        Integer nbUsbDevice = new Integer(0);
        int ret = morphoDevice.initUsbDevicesNameEnum(nbUsbDevice);
        if (ret == ErrorCodes.MORPHO_OK) {
            if (nbUsbDevice > 0) {
                sensorName = morphoDevice.getUsbDeviceName(0);
            }
        }
        int ret1 = morphoDevice.openUsbDevice(sensorName, 0);
        if (ret1 != 0) {
        } else {
            ProcessInfo.getInstance().setMSOSerialNumber(sensorName);
            String productDescriptor = morphoDevice.getProductDescriptor();
            java.util.StringTokenizer tokenizer = new java.util.StringTokenizer(productDescriptor, "\n");
            if (tokenizer.hasMoreTokens()) {
                String l_s_current = tokenizer.nextToken();
                if (l_s_current.contains("FINGER VP") || l_s_current.contains("FVP")) {
                    MorphoInfo.m_b_fvp = true;
                }
            }
            deviceDetails.setDeviceMake("Morpho");
            deviceDetails.setDeviceModel(morphoDevice.getStringDescriptorBin(DescriptorID.BINDESC_PRODUCT_NAME));
            deviceDetails.setDeviceSerial(sensorName);
            morphoDevice.closeDevice();
            terminalId = sensorName.split("-")[1];

        }
        Log.d(TAG, "sensoreName " + sensorName);
        return sensorName;
    }

    @Override
    public void stop() {
        Log.d(TAG, "stop");
    }

    @Override
    public void register() {
    }
    private List<ApplicationInfo> findpackage(String Action)
    {
        Intent intent = new Intent(Action);
        List<ResolveInfo> resInfos = context.getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        List<ApplicationInfo> appInfos = new ArrayList<ApplicationInfo>(0);
        for(ResolveInfo resolveInfo : resInfos) {
            Log.d("Package found", "activityInfo.packageName : " +resolveInfo.activityInfo.packageName + "\n");
            try {
                appInfos.add(context.getPackageManager().getApplicationInfo(resolveInfo.activityInfo.packageName, PackageManager.GET_META_DATA));
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        }
        return  appInfos;
    }

    @Override
    public DeviceDetails getDeviceDetails() {
        return deviceDetails;
    }


    @Override
    public String getManagementClientPackage() {
        return "uidai.gov.in.managementclient";
    }

    @Override
    public String getPidOption(Device device) {
        try {
            ///////////
            int fingerCount = 1;
            // FMR:0 FIR:1
            int fingerType = 0;
            // For XML : 0 Protobuf:1
            int fingerFormat = 0;
            String pidVer = "2.0";
            String timeOut = "10000";
            String posh = "UNKNOWN";
            Opts opts = new Opts();
            opts.fCount = String.valueOf(fingerCount);
            opts.fType = String.valueOf(fingerType);
            opts.iCount = "0";
            opts.iType = "0";
            opts.pCount = "0";
            opts.pType = "0";
            opts.format = String.valueOf(fingerFormat);
            opts.pidVer = pidVer;
            opts.timeout = timeOut;
//            opts.otp = "";
            opts.posh = posh;
            opts.wadh = "";
            opts.env = "P";
            Demo demo = new Demo();
            CustOpts custOpts = new CustOpts();
            PidOptions pidOptions = new PidOptions();
            pidOptions.ver = pidVer;
            pidOptions.Opts = opts;
            pidOptions.Demo = demo;
            pidOptions.CustOpts = custOpts;
            custOpts.params = new ArrayList<>();
            custOpts.params.add(new Param("", ""));
            Serializer serializer = new Persister();
            StringWriter writer = new StringWriter();
            serializer.write(pidOptions, writer);
            return writer.toString();
        } catch (Exception e) {
            Log.e("Error", e.toString());
        }

        return null;
    }
}
