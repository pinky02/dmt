package com.digitalindiapayment.morphoScanConnect.availableMorphoes;

import com.digitalindiapayment.morphoScanConnect.scannerdevice.Device;

/**
 * Created by Nivrutti Pawar on 2017/12/04.
 */

public interface PidOption {
    public String getPidOption(Device device);
}
