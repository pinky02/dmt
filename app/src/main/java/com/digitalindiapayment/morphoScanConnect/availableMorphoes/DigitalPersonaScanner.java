package com.digitalindiapayment.morphoScanConnect.availableMorphoes;

import android.content.Context;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.digitalindiapayment.morphoScanConnect.scannerdevice.Device;
import com.digitalindiapayment.pojo.morphoXmlBeans.CustOpts;
import com.digitalindiapayment.pojo.morphoXmlBeans.Demo;
import com.digitalindiapayment.pojo.morphoXmlBeans.Opts;
import com.digitalindiapayment.pojo.morphoXmlBeans.Param;
import com.digitalindiapayment.pojo.morphoXmlBeans.PidOptions;
import com.digitalindiapayment.pojo.morphoXmlBeans.RDService;
import com.digitalpersona.uareu.Reader;
import com.digitalpersona.uareu.Reader.Priority;
import com.digitalpersona.uareu.ReaderCollection;
import com.digitalpersona.uareu.UareUException;
import com.digitalpersona.uareu.UareUGlobal;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;


/**
 * Created by admin on 09-10-2017.
 */

public class DigitalPersonaScanner extends RDActions implements FingureScanner,PidOption {

    private static final String TAG = "DigitalPersonaScanner";
    private Reader m_reader = null;

    // Globals
    ReaderCollection readers;

    DigitalPersonaReader digitalPersonaReader ;

    String mDeviceName = "";

    private Context context;

    public DigitalPersonaScanner(Context context) {
        super(context);
        this.context = context;
        digitalPersonaReader = new DigitalPersonaReader();
    }

    @Override
    public void initializeDevice() {
        try {
            //m_reader = getReaders(context).get(0);
            readers = digitalPersonaReader.getReaders(context);

            int nSize = readers.size();

            Log.d(TAG,""+nSize);

            mDeviceName = (nSize == 0 ? "" : readers.get(0).GetDescription().name);
            Log.d(TAG,""+readers.get(0).GetDescription().serial_number);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getDeviseTerminalId() {
        return readers.get(0).GetDescription().serial_number;
    }

    @Override
    public void stop() {

    }

    @Override
    public void register() {

    }


    @Override
    String getPackageName() {
        return "com.integra.register.device";
    }

    @Override
    String getIntentCaptureActionName() {
        return "in.gov.uidai.rdservice.fp.CAPTURE";
    }

    @Override
    String getIntentDeviceInfoActionName() {
        return "in.gov.uidai.rdservice.fp.INFO";
    }

    @Override
    public String getPidOption(Device device) {
        try {
            ///////////
            int fingerCount = 1;
            // FMR:0 FIR:1
            int fingerType = 0;
            // For XML : 0 Protobuf:1
            int fingerFormat = 0;
            String pidVer = "2.0";
            String timeOut = "10000";
            String posh = "UNKNOWN";
            Opts opts = new Opts();
            opts.fCount = String.valueOf(fingerCount);
            opts.fType = String.valueOf(fingerType);
            opts.iCount = "0";
            opts.iType = "0";
            opts.pCount = "0";
            opts.pType = "0";
            opts.format = String.valueOf(fingerFormat);
            opts.pidVer = pidVer;
            opts.timeout = timeOut;
//            opts.otp = "";
            opts.posh = posh;
            opts.wadh = "";
            String env = "P";
            opts.env = env;
            Demo demo = new Demo();
            CustOpts custOpts = new CustOpts();
            PidOptions pidOptions = new PidOptions();
            pidOptions.ver = pidVer;
            pidOptions.Opts = opts;
            pidOptions.Demo = demo;
            pidOptions.CustOpts = custOpts;
            custOpts.params = new ArrayList<>();
            custOpts.params.add(new Param("", ""));
            Serializer serializer = new Persister();
            StringWriter writer = new StringWriter();
            serializer.write(pidOptions, writer);
            return writer.toString();
        } catch (Exception e) {
            Log.e("Error", e.toString());
        }

        return null;
    }


    public class DigitalPersonaReader{

        public Reader getReader(String name, Context applContext) throws UareUException
        {
            getReaders(applContext);

            for (int nCount = 0; nCount < readers.size(); nCount++)
            {
                if (readers.get(nCount).GetDescription().name.equals(name))
                {
                    return readers.get(nCount);
                }
            }
            return null;
        }

        public ReaderCollection getReaders(Context applContext) throws UareUException
        {
            readers = UareUGlobal.GetReaderCollection(applContext);
            readers.GetReaders();
            return readers;
        }

    }

}
