package com.digitalindiapayment.morphoScanConnect.availableMorphoes;

import com.digitalindiapayment.pojo.DeviceDetails;

/**
 * Created by admin on 01-12-2017.
 */

public interface DeviceDescriptor {
    public DeviceDetails getDeviceDetails();
}
