package com.digitalindiapayment.morphoScanConnect.availableMorphoes;

import android.content.Context;
import android.util.Log;

import com.digitalindiapayment.morphoScanConnect.scannerdevice.BluetoothScannerDevice;
import com.digitalindiapayment.morphoScanConnect.scannerdevice.Device;
import com.digitalindiapayment.pojo.morphoXmlBeans.CustOpts;
import com.digitalindiapayment.pojo.morphoXmlBeans.Demo;
import com.digitalindiapayment.pojo.morphoXmlBeans.Opts;
import com.digitalindiapayment.pojo.morphoXmlBeans.Param;
import com.digitalindiapayment.pojo.morphoXmlBeans.PidOptions;
import com.digitalindiapayment.util.Util;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.StringWriter;
import java.util.ArrayList;

/**
 * Created by Nivrutti Pawar on 2017/12/04.
 */

public class EvoluteScanner extends RDActions implements PidOption {

    public EvoluteScanner(Context context) {
        super(context);
    }

    @Override
    String getPackageName() {
        return "com.evolute.rdservice";
    }

    @Override
    String getIntentCaptureActionName() {
        return "in.gov.uidai.rdservice.fp.CAPTURE";
    }

    @Override
    String getIntentDeviceInfoActionName() {
        return "in.gov.uidai.rdservice.fp.INFO";
    }

    @Override
    public String getPidOption(Device device) {
        BluetoothScannerDevice bluetoothScannerDevice = (BluetoothScannerDevice) device;
        String type;
        if (Util.isBluetoothConnected)
            type="Y";
        else
            type="N";
        try {
            ///////////
            int fingerCount = 1;
            // FMR:0 FIR:1
            int fingerType = 0;
            // For XML : 0 Protobuf:1
            int fingerFormat = 0;
            String pidVer = "1.0";
            String timeOut = "10000";
            String posh = "UNKNOWN";
            Opts opts = new Opts();
            opts.fCount = String.valueOf(fingerCount);
            opts.fType = String.valueOf(fingerType);
            opts.iCount = "1";
            opts.iType = "0";
            opts.pCount = "1";
            opts.pType = "0";
            opts.format = String.valueOf(fingerFormat);
            opts.pidVer = "2.0";
            opts.timeout = timeOut;
            opts.otp = "";
            opts.posh = posh;
            opts.wadh = "";
            String env = "P";
            opts.env = env;
            Demo demo = new Demo();
            CustOpts custOpts = new CustOpts();
            PidOptions pidOptions = new PidOptions();
            pidOptions.ver = pidVer;
            pidOptions.Opts = opts;
            pidOptions.Demo = demo;
            pidOptions.CustOpts = custOpts;
            custOpts.params = new ArrayList<>();
            custOpts.params.add(new Param(bluetoothScannerDevice.getName(), bluetoothScannerDevice.getMac()));
            custOpts.params.add(new Param("Connection", type));


            Serializer serializer = new Persister();
            StringWriter writer = new StringWriter();
            serializer.write(pidOptions, writer);
            return writer.toString();
        } catch (Exception e) {
            Log.e("Error", e.toString());
        }

        return null;
    }
}
