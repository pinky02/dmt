package com.digitalindiapayment.morphoScanConnect;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.digitalindiapayment.R;
import com.digitalindiapayment.application.MyApplication;
import com.digitalindiapayment.connection.USBConnection;
import com.digitalindiapayment.morphoScanConnect.availableMorphoes.DigitalPersonaScanner;
import com.digitalindiapayment.morphoScanConnect.availableMorphoes.EvoluteScanner;
import com.digitalindiapayment.morphoScanConnect.availableMorphoes.MantraScanner;
import com.digitalindiapayment.morphoScanConnect.availableMorphoes.MorphoL0SoftScanner;
import com.digitalindiapayment.morphoScanConnect.availableMorphoes.PidOption;
import com.digitalindiapayment.morphoScanConnect.availableMorphoes.RDActions;
import com.digitalindiapayment.morphoScanConnect.availableMorphoes.SafronScanner;
import com.digitalindiapayment.morphoScanConnect.availableMorphoes.StartekScanner;
import com.digitalindiapayment.pojo.morphoXmlBeans.PidData;
import com.digitalindiapayment.util.Util;
import com.digitalindiapayment.webservices.requestpojo.RDFingurePrintRequest;
import com.google.gson.Gson;
import com.morpho.sample.KeyFile;

import org.greenrobot.eventbus.EventBus;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;


public class RDFingureScanActivity extends AppCompatActivity implements USBConnection {

    private static final String TAG = "RDFingureScanActivity";

    @BindView(R.id.capturefinger)
    TextView capturefinger;
    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.vertical_progressbar)
    ProgressBar verticalProgressbar;
    @BindView(R.id.textViewMessage)
    TextView textViewMessage;
    @BindView(R.id.btn_startstop)
    Button btnStartstop;
    @BindView(R.id.btn_rebootsoft)
    Button btnRebootsoft;
    @BindView(R.id.btn_closeandquit)
    Button btnCloseandquit;
    @BindView(R.id.fingerprint_linearLayout2)
    LinearLayout fingerprintLinearLayout2;
    @BindView(R.id.imgFingerprint)
    ImageView imgFingerprint;
    @BindView(R.id.putYourfinger)
    TextView putYourfinger;
    @BindView(R.id.scan)
    Button scan;
    Map<String, Object> fingureScannerHashMap;
    RDActions fingureScanner;
    String sensorName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mantra_fingure_scan);
        ButterKnife.bind(this);
        Log.d(TAG, "onCreate");
        sensorName = Util.PREFERENCES.getDeviceName(RDFingureScanActivity.this);
        fingureScannerHashMap = new HashMap<>();
        fingureScannerHashMap.put(getString(R.string.device_morpho_name), (new SafronScanner(RDFingureScanActivity.this)));
        fingureScannerHashMap.put(getString(R.string.device_mantra_name), new MantraScanner(RDFingureScanActivity.this));
        fingureScannerHashMap.put(getString(R.string.device_digital_persona_name), new DigitalPersonaScanner(RDFingureScanActivity.this));
        fingureScannerHashMap.put(getString(R.string.device_evolute_name), new EvoluteScanner(RDFingureScanActivity.this));
        fingureScannerHashMap.put(getString(R.string.device_startek_name), new StartekScanner(RDFingureScanActivity.this));
        fingureScannerHashMap.put(getString(R.string.device_morphol0s_name),new MorphoL0SoftScanner(RDFingureScanActivity.this));
        fingureScanner = (RDActions) fingureScannerHashMap.get(Util.PREFERENCES.getDeviceName(RDFingureScanActivity.this));

        scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!KeyFile.getDevise(getApplication()).equals("")) {
                    if (fingureScannerHashMap.get(sensorName) instanceof PidOption) {
                        String pidOptions = ((PidOption) fingureScannerHashMap.get(sensorName)).getPidOption(MyApplication.connectedDevice);
                        Log.d(TAG, "ScannerData " + pidOptions);
                        if (!pidOptions.equals("")) {
                            scan.setEnabled(false);
                            fingureScanner.scanFinger(pidOptions);
                        }
                    }

                } else {
                    Util.showLongToast(RDFingureScanActivity.this, getString(R.string.please_reconnect_the_device));
                }
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        scan.setEnabled(true);
        if (requestCode == Util.KEYS.FINGERPRINT_DATA_REQUEST) {
            if (resultCode == RESULT_OK) {
                Bundle b = data.getExtras();
                if (b != null) {     // in this variable you will get Pid data
                    String pidData = b.getString("PID_DATA");
                    PidData info = null;
                    Serializer serializer = new Persister();
                    try {
                        info = serializer.read(PidData.class, pidData);
                        Gson gson = new Gson();
                        String jsonInString = gson.toJson(info);
                        Log.d(TAG, " : " + jsonInString);
                        //Log.d(TAG, " Content : " + info.Data.content);
                        //Log.d(TAG, " info._Resp.errInfo : " + info._Resp.errInfo);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    // you will get value in this variable when your finger print device not connected
                    String dnc = b.getString("DNC", "");
                    // you will get value in this variable when your finger print device not registered.
                    String dnr = b.getString("DNR", "");
                    Log.d(TAG, "PidData: " + pidData);
                    Log.d(TAG, "dnc: " + dnc);
                    Log.d(TAG, "dnr: " + dnr);
                    //if (info._Resp.errInfo.length() > 1) {
                    //    Toast.makeText(RDFingureScanActivity.this, info._Resp.errInfo, Toast.LENGTH_LONG).show();
                    //}
                    //Log.d(TAG, " info._Resp.errInfo : " + info._Resp.errInfo);
                    if (info != null && info.isSuccessfull()) {
                        EventBus.getDefault().post(new RDFingurePrintRequest(info));
                        finish();
                    } else {

                        if (info != null) {
                            Log.d(TAG, " Error " + info.getResponse().getErrorInfo());
                            Toast.makeText(RDFingureScanActivity.this, info.getResponse().getErrorInfo(), Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(RDFingureScanActivity.this, "scan again", Toast.LENGTH_LONG).show();
                        }


                    }


                }
            }
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    protected void onStop() {
        Log.d(TAG, "onStop");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy");
        super.onDestroy();
    }
}
