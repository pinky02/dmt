package com.digitalindiapayment.reports;

import android.content.Intent;
import android.support.percent.PercentRelativeLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.digitalindiapayment.R;
import com.digitalindiapayment.creditrequest.CreditRequestReport.CreditRequestReportActivity;
import com.digitalindiapayment.reports.TransactionReport.TransactionReportActivity;
import com.digitalindiapayment.reports.WalletReport.WalletReportActivity;
import com.digitalindiapayment.util.Util;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ReportsActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.transaction_report)
    PercentRelativeLayout transaction_report;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.reports);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        //Make the button enable and Onclick listner on that
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @OnClick(R.id.transaction_report)
    public void transaction_report() {

        Intent intent = new Intent(ReportsActivity.this, TransactionReportActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.request_report)
    public void request_report() {

//        Util.showShortToast(getApplicationContext(),"We are working on it");

        Intent intent = new Intent(ReportsActivity.this, CreditRequestReportActivity.class);
        startActivity(intent);


    }
    @OnClick(R.id.wallet_report)
    public void wallet_report() {

//        Util.showShortToast(getApplicationContext(),"We are working on it");

        Intent intent = new Intent(ReportsActivity.this, WalletReportActivity.class);
        startActivity(intent);


    }






    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
