
package com.digitalindiapayment.reports.TransactionReport;

import java.util.List;

import com.digitalindiapayment.pojo.TransactionReportBean;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Transaction {

    @SerializedName("transactions")
    @Expose
    private List<TransactionReportBean> transactions = null;

    public List<TransactionReportBean> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<TransactionReportBean> transactions) {
        this.transactions = transactions;
    }

}
