package com.digitalindiapayment.reports.TransactionReport;


import android.content.Context;
import android.util.Log;

import com.digitalindiapayment.util.Util;
import com.digitalindiapayment.webservices.Api;
import com.digitalindiapayment.webservices.core.ApiFail;
import com.digitalindiapayment.webservices.core.ApiSuccess;
import com.digitalindiapayment.webservices.core.HttpErrorResponse;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class TransactionReportPresenter implements TransactionReportContract.UserAction {

    private final TransactionReportContract.View view;
    private final Context context;

    public TransactionReportPresenter(TransactionReportContract.View view, Context context) {
        this.view = view;
        this.context = context;
    }


    @Override
    public void generateTransactionReport() {


        Api.reports().getTransactionReport(Util.PREFERENCES.getToken(context))
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ApiSuccess<Transaction>() {
                    @Override
                    public void call(Transaction transaction) {
                        view.showTransaction(transaction.getTransactions());
                    }
                }, new ApiFail() {
                    @Override
                    public void httpStatus(HttpErrorResponse response) {
                        view.fail(response.getError());

                    }

                    @Override
                    public void noNetworkError() {
                        view.noInternet();
                    }

                    @Override
                    public void unknownError(Throwable e) {
                        view.fail(e.getMessage());

                    }
                });

    }
}