package com.digitalindiapayment.reports.TransactionReport;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.digitalindiapayment.R;
import com.digitalindiapayment.pojo.TransactionReportBean;
import com.digitalindiapayment.util.Util;
import com.digitalindiapayment.webservices.responsepojo.BankTransactionResponse;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TransactionReportAdapter extends RecyclerView.Adapter<TransactionReportAdapter.TransactionReportViewHolder> {


    private List<TransactionReportBean> transactions = null;
    private Context context;
    private BankTransactionResponse responseBean;
    private RecyclerViewOnClick recyclerViewOnClick;

    public TransactionReportAdapter(Context context, RecyclerViewOnClick recyclerViewOnClick) {
        this.context = context;
        this.recyclerViewOnClick = recyclerViewOnClick;
    }


    @Override
    public TransactionReportViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View ItemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycle_tranction_report, parent, false);
        ItemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                recyclerViewOnClick.onClick(v);
            }
        });

        return new TransactionReportViewHolder(ItemView);
    }

    @Override
    public void onBindViewHolder(TransactionReportViewHolder holder, int position) {
        final TransactionReportBean transactionReportBean = transactions.get(position);
        holder.date.setText(transactionReportBean.getCreatedAt());
        holder.adhaar_no.setText(transactionReportBean.getAadhaarNumber());
        holder.amount_word.setText(transactionReportBean.getAmount());
        holder.trans_id.setText(transactionReportBean.getId());
        holder.services.setText(transactionReportBean.getService());
        holder.transStatus.setText(transactionReportBean.getTransactionStatus());
        holder.amount_word.setText(Util.indianCurrencyRepresentation(transactionReportBean.getAmount()));

    }

    @Override
    public int getItemCount() {
        return getTransactionsList() != null ? transactions.size() : 0;
    }

    public List<TransactionReportBean> getTransactionsList() {
        return transactions;
    }

    public void setTransactionsList(List<TransactionReportBean> transactions) {
        this.transactions = transactions;
        notifyDataSetChanged();
    }

    public class TransactionReportViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.date)
        TextView date;

        @BindView(R.id.trans_id)
        TextView trans_id;

        @BindView(R.id.services)
        TextView services;

        @BindView(R.id.adhaar_no)
        TextView adhaar_no;

        @BindView(R.id.amount_word)
        TextView amount_word;

        @BindView(R.id.trans_status)
        TextView transStatus;

        public TransactionReportViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
