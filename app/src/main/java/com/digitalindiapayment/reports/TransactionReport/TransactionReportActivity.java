package com.digitalindiapayment.reports.TransactionReport;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.digitalindiapayment.R;
import com.digitalindiapayment.pojo.TransactionReportBean;
import com.digitalindiapayment.receipt.BankTransactionWebView;
import com.digitalindiapayment.util.Util;
import com.digitalindiapayment.webservices.responsepojo.BankTransactionResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class TransactionReportActivity extends AppCompatActivity implements TransactionReportContract.View, RecyclerViewOnClick {


    @BindView(R.id.progressdialogdip)
    RelativeLayout progressdialogdip;
    @BindView(R.id.refreshButton)
    ImageView refreshButton;
    /*@BindView(R.id.refreshButton)
    ImageView refreshButton;*/

    private List<TransactionReportBean> transactions = new ArrayList<TransactionReportBean>();

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.recyclerviewtransaction)
    RecyclerView recyclerviewtransaction;
    @BindView(R.id.noInternetMsg)
    TextView noInternetMsg;
    @BindView(R.id.noInternetContainer)
    LinearLayout noInternetContainer;

    private TransactionReportAdapter transactionReportAdapter = null;
    private TransactionReportPresenter transactionReportPresenter = null;

    private final int TRANSACTIONREPORT_FLAG = 200;
    private String ACTIVITY_CALLED_FROM;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.transcation_report);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        noInternetContainer.setVisibility(View.GONE);
        progressdialogdip.setVisibility(View.GONE);
        //enable the back button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        ACTIVITY_CALLED_FROM = getString(R.string.activity_called_from);
        //get data from server
        transactionReportPresenter = new TransactionReportPresenter(this, getApplicationContext());

        //setting the adapter
        transactionReportAdapter = new TransactionReportAdapter(getApplicationContext(), this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerviewtransaction.setLayoutManager(mLayoutManager);
        recyclerviewtransaction.setItemAnimator(new DefaultItemAnimator());

        recyclerviewtransaction.setAdapter(transactionReportAdapter);

    }


    @Override
    protected void onResume() {
        super.onResume();

        showLoading();
        transactionReportPresenter.generateTransactionReport();


    }

    @Override
    public void noInternet() {
        hideLoading();
        noInternetContainer.setVisibility(View.VISIBLE);
        recyclerviewtransaction.setVisibility(View.GONE);
    }

    @Override
    public void exception(Throwable e) {
        Util.showShortToast(getApplicationContext(), e.getMessage());
        hideLoading();
    }

    @Override
    public void fail(String message) {
        Util.showShortToast(getApplicationContext(), message);
        hideLoading();


    }

    @Override
    public void success(String message) {
    }

    @Override
    public void showLoading() {
        progressdialogdip.setVisibility(View.VISIBLE);
        noInternetContainer.setVisibility(View.GONE);
    }

    @Override
    public void hideLoading() {
        progressdialogdip.setVisibility(View.GONE);
    }


    @Override
    public void showTransaction(List<TransactionReportBean> transactionReportBeen) {
        hideLoading();
        this.transactions = transactionReportBeen;
        transactionReportAdapter.setTransactionsList(transactionReportBeen);
        recyclerviewtransaction.setVisibility(View.VISIBLE);
        noInternetContainer.setVisibility(View.GONE);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onClick(View view) {
        int childItemId = recyclerviewtransaction.getChildLayoutPosition(view);
        TransactionReportBean transactionReportBean = transactions.get(childItemId);

        BankTransactionResponse bankTransactionResponse = new BankTransactionResponse();
        bankTransactionResponse.setTransactionId(transactionReportBean.getId());

        Intent intent = new Intent(this, BankTransactionWebView.class);
        intent.putExtra(Util.KEYS.DATA, bankTransactionResponse);
        intent.putExtra(ACTIVITY_CALLED_FROM, getString(R.string.reports));
        //startActivity(intent);
        startActivityForResult(intent, TRANSACTIONREPORT_FLAG);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == TRANSACTIONREPORT_FLAG && resultCode == RESULT_OK) {
            transactionReportPresenter.generateTransactionReport();
        }
    }


    @OnClick(R.id.refreshButton)
    public void onViewClicked() {
        showLoading();
        transactionReportPresenter.generateTransactionReport();
    }
}
