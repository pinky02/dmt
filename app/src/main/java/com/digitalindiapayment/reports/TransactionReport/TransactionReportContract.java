package com.digitalindiapayment.reports.TransactionReport;


import com.digitalindiapayment.mvp.BaseView;
import com.digitalindiapayment.pojo.TransactionReportBean;

import java.util.List;

public class TransactionReportContract {

    public interface View extends BaseView {
        void showTransaction(List<TransactionReportBean> transactionReportBeen);
    }

    public interface UserAction {
        void generateTransactionReport();
    }

}
