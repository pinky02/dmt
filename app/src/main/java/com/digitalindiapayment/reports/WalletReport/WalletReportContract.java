package com.digitalindiapayment.reports.WalletReport;

import com.digitalindiapayment.mvp.BaseView;
import com.digitalindiapayment.pojo.RequestReportBean;
import com.digitalindiapayment.pojo.WalletReportBean;

import java.util.List;

/**
 * Created by Mayur on 11-04-2017.
 */

public class WalletReportContract {


    public interface View extends BaseView {
        void showTransaction(List<WalletReportBean> RequestReportBean);
    }

    public interface UserAction {
        void generateWalletReport();

    }

}

