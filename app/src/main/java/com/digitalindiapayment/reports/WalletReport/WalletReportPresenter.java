package com.digitalindiapayment.reports.WalletReport;

import android.content.Context;
import android.util.Log;

import com.digitalindiapayment.util.Util;
import com.digitalindiapayment.webservices.Api;
import com.digitalindiapayment.webservices.core.ApiFail;
import com.digitalindiapayment.webservices.core.ApiSuccess;
import com.digitalindiapayment.webservices.core.HttpErrorResponse;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Mayur on 11-04-2017.
 */

public class WalletReportPresenter implements WalletReportContract.UserAction {

    private final WalletReportContract.View view;
    private final Context context;

    public WalletReportPresenter(WalletReportContract.View view, Context context) {
        this.view = view;
        this.context = context;
    }

    @Override
    public void generateWalletReport() {
        {
            view.showLoading();

            Log.i("Msg1", "Feteching Wallet Report");

            Api.reports().getWalletReport(Util.PREFERENCES.getToken(context))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new ApiSuccess<WalletReport>() {
                        @Override
                        public void call(WalletReport report) {
                            view.showTransaction(report.getReport());

                        }
                    }, new ApiFail() {
                        @Override
                        public void httpStatus(HttpErrorResponse response) {
                            view.fail(response.getError());

                        }

                        @Override
                        public void noNetworkError() {
                            view.noInternet();
                        }

                        @Override
                        public void unknownError(Throwable e) {
                            view.fail(e.getMessage());

                        }
                    });

            Log.i("Msg1", "Feteching Credit Request Report Completed");
        }


    }}
