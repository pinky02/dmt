package com.digitalindiapayment.reports.WalletReport;

import com.digitalindiapayment.pojo.RequestReportBean;
import com.digitalindiapayment.pojo.WalletReportBean;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Mayur on 11-04-2017.
 */

public class WalletReport {

    @SerializedName("transaction")

    private List<WalletReportBean> report = null;

    public List<WalletReportBean> getReport() {
        return report;


    }
    public void setReport (List<WalletReportBean> report) {
        this.report = report;
    }
}
