package com.digitalindiapayment.reports.WalletReport;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.digitalindiapayment.R;
import com.digitalindiapayment.pojo.RequestReportBean;
import com.digitalindiapayment.pojo.WalletReportBean;
import com.digitalindiapayment.webservices.responsepojo.BankTransactionResponse;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Mayur on 11-04-2017.
 */

public class WalletReportAdapter extends RecyclerView.Adapter<WalletReportAdapter.WalletReportViewHolder> {


    private List<WalletReportBean> reports = null;
    private Context context;
    private BankTransactionResponse responseBean;

    public WalletReportAdapter(List<WalletReportBean> reports, Context applicationContext) {
        this.reports = reports;
        this.context = applicationContext;
    }


    @Override
    public WalletReportViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycle_wallet_report, parent, false);
        return new WalletReportViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(WalletReportViewHolder holder, int position) {
        final WalletReportBean walletReportBean = reports.get(position);
        holder.amountWord.setText(walletReportBean.getAmount());
        holder.fees.setText(walletReportBean.getFees());
        holder.credit.setText(walletReportBean.getCredit());
        holder.debit.setText(walletReportBean.getDebit());
        holder.transId.setText(walletReportBean.getId());
        holder.date.setText(walletReportBean.getTimestamp());
        holder.modeOfTransfer.setText(walletReportBean.getServiceMod());
        holder.branch.setText(walletReportBean.getBranch());
    }
    @Override
    public int getItemCount() {
        return reports.size();
    }

    public class WalletReportViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.amount_word)
        TextView amountWord;
        @BindView(R.id.branch)
        TextView branch;
        @BindView(R.id.fees)
        TextView fees;
        @BindView(R.id.mode_of_transfer)
        TextView modeOfTransfer;
        @BindView(R.id.credit)
        TextView credit;
        @BindView(R.id.debit)
        TextView debit;
        @BindView(R.id.trans_id)
        TextView transId;
        @BindView(R.id.date)
        TextView date;

        public WalletReportViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
    }
        public List<WalletReportBean> getReportList() {
            return reports;
        }
    }
    public void setReportList(List<WalletReportBean> reports) {
        this.reports = reports;
        notifyDataSetChanged();
}}
