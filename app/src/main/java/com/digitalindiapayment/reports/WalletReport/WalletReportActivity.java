package com.digitalindiapayment.reports.WalletReport;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.digitalindiapayment.R;
import com.digitalindiapayment.pojo.WalletReportBean;
import com.digitalindiapayment.receipt.BankTransactionWebView;
import com.digitalindiapayment.reports.TransactionReport.RecyclerViewOnClick;
import com.digitalindiapayment.util.Util;
import com.digitalindiapayment.webservices.responsepojo.BankTransactionResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;

/**
 * Created by Mayur on 11-04-2017.
 */

public class WalletReportActivity extends AppCompatActivity implements WalletReportContract.View, RecyclerViewOnClick {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.noInternetImg)
    ImageView noInternetImg;
    @BindView(R.id.noInternetMsg)
    TextView noInternetMsg;
    @BindView(R.id.noInternetContainer)
    LinearLayout noInternetContainer;
    @BindView(R.id.progressdialogdip)
    RelativeLayout progressdialogdip;
    @BindView(R.id.message)
    TextView message;
    @BindView(R.id.recyclerviewrequeswalletreport)
    RecyclerView recyclerviewrequeswalletreport;


    private List<WalletReportBean> reports = new ArrayList<WalletReportBean>();
    private WalletReportAdapter walletReportAdapter = null;
    private WalletReportPresenter walletReportPresenter = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wallet_report);
        ButterKnife.bind(this);


        noInternetContainer.setVisibility(View.GONE);
        progressdialogdip.setVisibility(View.GONE);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //get data from server
        walletReportPresenter = new WalletReportPresenter(this, getApplicationContext());
        walletReportPresenter.generateWalletReport();

        //setting the adapter
        walletReportAdapter = new WalletReportAdapter(reports, getApplicationContext());
        RecyclerView.LayoutManager mlayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerviewrequeswalletreport.setLayoutManager(mlayoutManager);
        recyclerviewrequeswalletreport.setItemAnimator(new DefaultItemAnimator());
        recyclerviewrequeswalletreport.setAdapter(walletReportAdapter);
    }


    @Override
    public void noInternet() {
        noInternetContainer.setVisibility(View.VISIBLE);
        progressdialogdip.setVisibility(View.GONE);
    }

    @Override
    public void exception(Throwable e) {
        hideLoading();
    }

    @Override
    public void fail(String message) {
        hideLoading();
    }

    @Override
    public void success(String message) {
        hideLoading();
    }

    @Override
    public void showLoading() {
        noInternetContainer.setVisibility(View.GONE);
        progressdialogdip.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {

        progressdialogdip.setVisibility(View.GONE);
    }

    @Override
    public void showTransaction(List<WalletReportBean> walletReportBeen) {
        hideLoading();
        this.reports = walletReportBeen;
        walletReportAdapter.setReportList(walletReportBeen);


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onClick(View view) {
        int childItemId = recyclerviewrequeswalletreport.getChildLayoutPosition(view);
        WalletReportBean walletReportBean = reports.get(childItemId);

        BankTransactionResponse bankTransactionResponse = new BankTransactionResponse();
        bankTransactionResponse.setTransactionId(walletReportBean.getId());

        Intent intent = new Intent(this, BankTransactionWebView.class);
        intent.putExtra(Util.KEYS.DATA, bankTransactionResponse);
        startActivity(intent);
    }
}
