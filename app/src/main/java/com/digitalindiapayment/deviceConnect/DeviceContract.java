package com.digitalindiapayment.deviceConnect;

import com.digitalindiapayment.pojo.DeviceDetails;

/**
 * Created by admin on 25-05-2017.
 */

public class DeviceContract {
    public interface View {
        void showDeviceValidity(Boolean result,String message);
        void showLoader();
    }

    public interface UserAction {
        void checkDeviceTerminalId(String terminalId);
        void registerDevice(DeviceDetails deviceDetails);
    }
}
