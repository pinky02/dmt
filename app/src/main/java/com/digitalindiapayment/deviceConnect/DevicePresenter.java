package com.digitalindiapayment.deviceConnect;

import android.content.Context;
import android.util.Log;

import com.digitalindiapayment.R;
import com.digitalindiapayment.pojo.DeviceDetails;
import com.digitalindiapayment.util.Util;
import com.digitalindiapayment.webservices.Api;
import com.digitalindiapayment.webservices.core.ApiFail;
import com.digitalindiapayment.webservices.core.ApiSuccess;
import com.digitalindiapayment.webservices.core.HttpErrorResponse;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by admin on 25-05-2017.
 */

public class DevicePresenter implements DeviceContract.UserAction {

    private DeviceContract.View view;
    private Context context;

    public DevicePresenter(DeviceContract.View view, Context context) {
        this.view = view;
        this.context = context;
    }

    @Override
    public void checkDeviceTerminalId(String terminalId) {
        // Get UserId from shared preference and TerminalId
        // On Api hit

        rx.Observable<String> terminalIdValidator;
        // If RD is Enabled for User & device is not mantra
        if (Util.PREFERENCES.isRDEnabled(context) && !Util.PREFERENCES.getDeviceName(context).equalsIgnoreCase(context.getString(R.string.device_mantra_name))) {
            terminalIdValidator = Api.banksDetails().validateRDTerminalID(Util.PREFERENCES.getToken(context), terminalId);
            Log.e("DeviceTerminalID","other RD Device API Call = "+terminalId);

        } else {
            // If RD is not enable for user & device is mantra (RD Mantra also)
            terminalIdValidator = Api.banksDetails().validateTerminalID(Util.PREFERENCES.getToken(context), terminalId);
            Log.e("DeviceTerminalID","other public & mantra RD Device API call = "+terminalId);

        }
        view.showLoader();
        terminalIdValidator
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ApiSuccess<String>() {
                    @Override
                    public void call(String stringStringMap) {
                        Log.d("DevicePresenter", stringStringMap.toString());
                        view.showDeviceValidity(true, stringStringMap);

                    }
                }, new ApiFail() {
                    @Override
                    public void httpStatus(HttpErrorResponse response) {
                        view.showDeviceValidity(false, response.getError());
                    }

                    @Override
                    public void noNetworkError() {
                        view.showDeviceValidity(false, "");
                    }

                    @Override
                    public void unknownError(Throwable e) {
                        view.showDeviceValidity(false, e.getMessage());
                    }
                });
    }

    @Override
    public void registerDevice(DeviceDetails deviceDetails) {
        Api.banksDetails().registerDevice(Util.PREFERENCES.getToken(context), deviceDetails)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ApiSuccess<String>() {
                    @Override
                    public void call(String s) {
                    }
                }, new ApiFail() {
                    @Override
                    public void httpStatus(HttpErrorResponse response) {
                    }

                    @Override
                    public void noNetworkError() {
                    }

                    @Override
                    public void unknownError(Throwable e) {
                    }
                });
    }
}
