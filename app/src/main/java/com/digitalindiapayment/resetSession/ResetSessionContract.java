package com.digitalindiapayment.resetSession;

import com.digitalindiapayment.webservices.requestpojo.ResetSessionRequest;

/**
 * Created by admin on 21-07-2017.
 */

public class ResetSessionContract {

    public interface View{
        public void onResetSessionCompleted(String message);
        public void onResetSessionFailed(String message);
    }
    public interface UserAction{
        public void submitClick(ResetSessionRequest request);
        //public void generateResetSessionOtp();
    }
}
