package com.digitalindiapayment.resetSession;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsMessage;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.atritechnocrat.view.ValidatorTextInputLayout;
import com.digitalindiapayment.R;
import com.digitalindiapayment.login.LoginActivity;
import com.digitalindiapayment.util.Util;
import com.digitalindiapayment.webservices.requestpojo.ResetSessionRequest;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ResetSession extends AppCompatActivity implements ResetSessionContract.View {

    private static final String TAG = "ResetSession";
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.mobileno)
    EditText mobileno;
    @BindView(R.id.mobileNumberValidator)
    ValidatorTextInputLayout mobileNumberValidator;
    @BindView(R.id.otp)
    EditText otp;
    @BindView(R.id.otpValidator)
    ValidatorTextInputLayout otpValidator;
    @BindView(R.id.submitPassword)
    Button submitPassword;
    IntentFilter smsListenerFilter;
    @BindView(R.id.password)
    EditText password;


    private ResetSessionContract.UserAction userAction;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_session);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.reset_session);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        String mobile = getIntent().getStringExtra(getString(R.string.mobilenumber));
        mobileno.setText("" + mobile);
        otp.requestFocus();
        mobileno.setEnabled(false);
        userAction = new ResetSessionPresenter(getApplicationContext(), ResetSession.this);

        password.setFilters(new InputFilter[]{
                new InputFilter() {
                    public CharSequence filter(CharSequence src, int start,
                                               int end, Spanned dst, int dstart, int dend) {
                        if (src.equals("")) { // for backspace
                            return src;
                        }
                        if (src.toString().matches("[\\x00-\\x7F]+")) {
                            return src;
                        }
                        return "";
                    }
                }
        });



        submitPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (otp.getText().toString().length() < 4) {
                    otp.setError(getString(R.string.valid_otp));
                    return;
                }
                if (password.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(getApplicationContext(), getString(R.string.pls_enter_password), Toast.LENGTH_SHORT).show();
                    return;
                }
                submitPassword.setEnabled(false);
                submitPassword.setText(getString(R.string.processing));
                userAction.submitClick(new ResetSessionRequest(mobileno.getText().toString(), otp.getText().toString(), password.getText().toString()));
            }
        });

        if (ContextCompat.checkSelfPermission(getBaseContext(), "android.permission.READ_SMS") != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{"android.permission.READ_SMS"}, 1);
        }

        smsListenerFilter = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");


        //userAction.generateResetSessionOtp();

    }

    private BroadcastReceiver smsListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle data = intent.getExtras();
            Object[] pdus = (Object[]) data.get("pdus");
            for (int i = 0; i < pdus.length; i++) {
                SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) pdus[i]);
                String sender = smsMessage.getDisplayOriginatingAddress();
                Log.d(TAG, "Sender :" + sender);
                if (sender.equals(getString(R.string.smsgetway_sendername))) {
                    Log.d(TAG, "below if Sender :" + sender);
                    String messageBody = smsMessage.getMessageBody();
                    String number = messageBody.replaceAll("[^0-9]", "");
                    otp.setText(number);
                    Log.d(TAG, "messageBody :" + messageBody);
                }
            }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(smsListener, smsListenerFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(smsListener);
    }


    @Override
    public void onResetSessionCompleted(String message) {
        //UtilDmt.showShortToast(getApplicationContext(),getString(R.string.you_can_login_into_account));
        //Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        //startActivity(intent);
        setResult(Activity.RESULT_OK);
        finish();
    }

    @Override
    public void onResetSessionFailed(String message) {
        submitPassword.setEnabled(true);
        submitPassword.setText(getString(R.string.submit));
        Util.showShortToast(getApplicationContext(),message);
    }
}
