package com.digitalindiapayment.resetSession;

import android.content.Context;
import android.util.Log;

import com.digitalindiapayment.R;
import com.digitalindiapayment.util.Util;
import com.digitalindiapayment.webservices.Api;
import com.digitalindiapayment.webservices.core.ApiFail;
import com.digitalindiapayment.webservices.core.ApiSuccess;
import com.digitalindiapayment.webservices.core.HttpErrorResponse;
import com.digitalindiapayment.webservices.requestpojo.ResetSessionRequest;


import java.io.IOException;

import retrofit2.Response;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by admin on 21-07-2017.
 */

public class ResetSessionPresenter implements ResetSessionContract.UserAction
{

    private static final String TAG = "ResetSessionPresenter";
    private Context context;
    private ResetSessionContract.View view;

    public ResetSessionPresenter(Context context, ResetSessionContract.View view) {
        this.context = context;
        this.view = view;
    }


    @Override
    public void submitClick(ResetSessionRequest request) {

        request.setMobile_auth_code(Util.PREFERENCES.getUniqueDeviceKey(context));

        Api.userManagement().validateResetSessionOtp(Util.PREFERENCES.getToken(context),request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ApiSuccess<Response<String>>() {
                    @Override
                    public void call(Response<String> resetSessionResponse) {
                        Log.d(TAG, "subscribe > call");

                        if(resetSessionResponse.isSuccessful()) {
                            Util.PREFERENCES.setToken(context, resetSessionResponse.headers().get("auth"));
                            view.onResetSessionCompleted("");
                        }else{
                            try {
                                view.onResetSessionFailed(resetSessionResponse.errorBody().string().toString());
                            } catch (IOException e) {
                                view.onResetSessionFailed(context.getString(R.string.lost_connectivity));
                                e.printStackTrace();
                            }
                        }
                        //if(addMemberResponseResponse.equals(context.getString(R.string.api_success_key))){

                        //}
                    }
                }, new ApiFail() {
                    @Override
                    public void httpStatus(HttpErrorResponse response) {
                        view.onResetSessionFailed(response.getError());
                    }

                    @Override
                    public void noNetworkError() {
                        view.onResetSessionFailed(context.getString(R.string.lost_connectivity));
                    }

                    @Override
                    public void unknownError(Throwable e) {
                        view.onResetSessionFailed(e.getMessage());
                    }
                });
    }
}
