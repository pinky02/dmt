package com.digitalindiapayment.bluetoothcontroller;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import com.digitalindiapayment.R;
import com.digitalindiapayment.application.MyApplication;
import com.digitalindiapayment.connection.BluetoothConnection;
import com.digitalindiapayment.morphoScanConnect.RDFingureScanActivity;
import com.digitalindiapayment.pojo.TransactionDetailsBean;
import com.digitalindiapayment.scannerConnect.hemen.HemenScanner;
import com.digitalindiapayment.scannerConnect.mantra.MantraScanner;
import com.digitalindiapayment.util.Util;
import com.esys.impressivedemo.BluetoothDevicelist;
import com.digitalindiapayment.scannerConnect.evolute.EvoluteScanner;
import com.esys.impressivedemo.ImpressSetup;
import com.esys.impressivedemo.ImpressiveclassEnum;
import com.esys.impressivedemo.PrintScreen;
import com.morpho.sample.FingerprintData;
import org.greenrobot.eventbus.EventBus;
import java.util.HashMap;
import java.util.Map;

public class BluetoothController extends Activity implements BluetoothConnection {


    private static final int REQUEST_ENABLE_BT = 1;

    public final static int DEVICE_SCAN_FLAG = 101;
    public final static int DEVICE_PRINT_FLAG = 102;


    private ImpressiveclassEnum myEnum;
    TransactionDetailsBean bean;
    private Map<String ,Class> scannerSelector ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scan_print_activity_splash);
        myEnum = (ImpressiveclassEnum) getIntent().getSerializableExtra("ENUM");

        bean = getIntent().getParcelableExtra("DATA");


        if (!isLibActive()) {
            Toast.makeText(this, getString(R.string.actMain_msg_does_not_support_uuid_service), Toast.LENGTH_LONG).show();
            finish();
            return;
        }



        scannerSelector = new HashMap<>();
        scannerSelector.put(getResources().getString(R.string.device_hemen_name), HemenScanner.class);
        scannerSelector.put(getResources().getString(R.string.device_mantra_name), MantraScanner.class);
        if (Util.PREFERENCES.isRDEnabled(getApplicationContext())) {
            scannerSelector.put(getString(R.string.device_evolute_name), RDFingureScanActivity.class);
        } else {
            scannerSelector.put(getResources().getString(R.string.device_evolute_name), EvoluteScanner.class);
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                CheckBlueToothState();
            }
        }, 3500);


    }


    private void checkPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) ==
                        PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                        PackageManager.PERMISSION_GRANTED) {
            startApp();

        } else {
            ActivityCompat.requestPermissions(this, new String[]{
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    123);
        }
    }

    @Override
    public void onRequestPermissionsResult(final int requestCode, @NonNull final String[] permissions, @NonNull final int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 123) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                startApp();

            } else {
                // User refused to grant permission. You can add AlertDialog here
                Toast.makeText(this, "You didn't give permission to access device location", Toast.LENGTH_LONG).show();
                finish();
            }
        }
    }


    public void startApp() {

        // If device is morpho
        if (!((MyApplication) getApplicationContext()).isBluetoothDeviceConnected()) {

            Intent i = new Intent(BluetoothController.this, BluetoothDevicelist.class);
            i.putExtra("ENUM", myEnum);
            i.putExtra("DATA", getIntent().getParcelableExtra("DATA"));

            if (myEnum == ImpressiveclassEnum.PRINT) {
                startActivityForResult(i, DEVICE_PRINT_FLAG);
            } else {
                // For scan
                startActivity(i);
                finish();
            }
            // close this activity
            //
        } else {

            //if (UtilDmt.PREFERENCES.getDeviceName(getApplicationContext()).equals(getResources().getString(R.string.device_evolute_name))) {
            Intent all_intent = null;
            if (myEnum == ImpressiveclassEnum.PRINT) {
                all_intent = new Intent(getApplicationContext(), PrintScreen.class);
                //all_intent = new Intent(getApplicationContext(), Act_Printer.class);
                all_intent.putExtra("DATA", getIntent().getParcelableExtra("DATA"));
                startActivityForResult(all_intent, DEVICE_PRINT_FLAG);

            } else if (myEnum == ImpressiveclassEnum.SCAN) {



                String scannerName =Util.PREFERENCES.getDeviceName(getApplicationContext());

                Util.showShortToast(getApplicationContext(),"Scanner Selected "+scannerName);
                    all_intent = new Intent(getApplicationContext(), scannerSelector.get(scannerName));
                    //all_intent = new Intent(getApplicationContext(), Act_FPS.class);
                    startActivityForResult(all_intent, DEVICE_SCAN_FLAG);

            }
            // }

        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        if (requestCode == REQUEST_ENABLE_BT) {
            checkPermission();
        } else if (requestCode == DEVICE_PRINT_FLAG) {
            //if (resultCode == RESULT_OK) {
            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
            //}
        } else if (requestCode == DEVICE_SCAN_FLAG) {
            finish();
            if (resultCode == RESULT_OK) {
                FingerprintData fingerprintData = new FingerprintData();
                fingerprintData.fp = data.getStringExtra("DATA");
                fingerprintData.fpv = data.getStringExtra("DATA");
                EventBus.getDefault().post(fingerprintData);


            }
        }

    }

    private void CheckBlueToothState() {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter == null) {
            Toast.makeText(this, "Bluetooth NOT support", Toast.LENGTH_LONG).show();
            finish();

        } else {
            if (bluetoothAdapter.isEnabled()) {
                checkPermission();
            } else {

                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }
        }
    }


    private boolean isLibActive() {

        ImpressSetup.onCreate(this);

        if (Build.VERSION.SDK_INT >= 15 && ImpressSetup.isActivate()) {
            return true;
        } else {

            return false;
        }

    }

}
