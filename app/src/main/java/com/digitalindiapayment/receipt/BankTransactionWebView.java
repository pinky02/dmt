package com.digitalindiapayment.receipt;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.Toast;

import com.digitalindiapayment.BaseActivity;
import com.digitalindiapayment.BuildConfig;
import com.digitalindiapayment.R;
import com.digitalindiapayment.bluetoothcontroller.BluetoothController;
import com.digitalindiapayment.pojo.TransactionDetailsBean;
import com.digitalindiapayment.printerConnect.PrinterConnectContract;
import com.digitalindiapayment.printerConnect.PrinterConnectPresenter;
import com.digitalindiapayment.printerConnect.ngxprinter.NGXPrintActivity;
import com.digitalindiapayment.util.Util;
import com.digitalindiapayment.webservices.Api;
import com.digitalindiapayment.webservices.core.ApiFail;
import com.digitalindiapayment.webservices.core.ApiSuccess;
import com.digitalindiapayment.webservices.core.HttpErrorResponse;
import com.digitalindiapayment.webservices.requestpojo.TransactionDetailsRequest;
import com.digitalindiapayment.webservices.responsepojo.BankTransactionResponse;
import com.esys.impressivedemo.ImpressiveclassEnum;
import com.morpho.sample.KeyFile;
import com.rdservice.helper.Printer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/**
 * Activity to handle printer and receipt
 */
public class BankTransactionWebView extends BaseActivity implements PrinterConnectContract.View {

    public static final int BLUETOOTH_CONNECTION_FLAG = 201;
    @BindView(R.id.toolbar)
    Toolbar toolbartip;
    @BindView(R.id.webview)
    WebView webView;
    @BindView(R.id.print)
    Button print;
    String acitivty;
    private BankTransactionResponse responseBean;
    private String ACTIVITY_CALLED_FROM;
    private PrinterConnectContract.UserAction printerPresenter;
    private TransactionDetailsBean balanceResponseBean;
    private ProgressDialog dlgPg;
    private String bluetoothName;
    private String bluetoothMac;
    String DmtTransactionId;
    String receipt;
//        if (bluetoothName.isEmpty() || bluetoothMac.isEmpty()) {
//            Intent intent = new Intent(this, BluetoothDevicelist.class);
//            startActivity(intent);
//        }


    @SuppressWarnings("deprecation")
    public static void clearCookies(Context context) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            CookieManager.getInstance().removeAllCookies(null);
            CookieManager.getInstance().flush();
        } else {
            CookieSyncManager cookieSyncMngr = CookieSyncManager.createInstance(context);
            cookieSyncMngr.startSync();
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.removeAllCookie();
            cookieManager.removeSessionCookie();
            cookieSyncMngr.stopSync();
            cookieSyncMngr.sync();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_bank_transaction_webview);
        ButterKnife.bind(this);
        clearCookies(this);

        setSupportActionBar(toolbartip);
        //Make the button enable and Onclick listner on that
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Intent i = getIntent();
        DmtTransactionId = i.getStringExtra("DmtTransactionId");
        ACTIVITY_CALLED_FROM = getString(R.string.activity_called_from);
        acitivty = getIntent().getStringExtra(ACTIVITY_CALLED_FROM);

        toolbartip.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }

        });
        responseBean = getIntent().getParcelableExtra(Util.KEYS.DATA);
        String transactionId = responseBean.getTransactionId();

        if (transactionId == null || transactionId.length() == 0) {

            Util.showLongToast(getApplicationContext(), getResources().getString(R.string.transaction_id_empty));
            finish();
            return;
        }
        //String receipt = BuildConfig.receipt + "?id=" + transactionId;

        if(DmtTransactionId.equals("")){
             receipt = BuildConfig.receipt + transactionId + "/actions/receipt";
        }
        else{
             receipt = BuildConfig.Dmtreceipt + DmtTransactionId + "/actions/receipt";
        }


        Log.d("WebView", receipt);


        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.setVerticalScrollBarEnabled(true);
        webView.setHorizontalScrollBarEnabled(true);
        webView.setWebViewClient(new WebViewClient());
        webView.loadUrl(receipt, getCustomHeaders());

        webView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return true;
            }
        });
        webView.setLongClickable(false);
        webView.setHapticFeedbackEnabled(false);

        printerPresenter = new PrinterConnectPresenter(BankTransactionWebView.this, getApplicationContext());

        bluetoothName = Util.PREFERENCES.getEvoluteBluetoothName(getApplicationContext());
        bluetoothMac = Util.PREFERENCES.getEvoluteBluetoothMac(getApplicationContext());
        // HashMap to load printer sdk
        /*Map<String,Class> printerSelection = new HashMap<>();
        printerSelection.put();
*/
    }

    @Override
    public void onBackPressed() {
        if (acitivty != null && acitivty.equalsIgnoreCase(getString(R.string.reports))) {
            setResult(RESULT_OK);
        }

        finish();
    }

    @OnClick(R.id.print)
    public void onclick() {
        createWebPagePrint(webView);
    }

    private Map<String, String> getCustomHeaders() {
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("auth", Util.PREFERENCES.getToken(getApplicationContext()));
        headers.put("AGENT", getString(R.string.agent_value));

        return headers;
    }

    public void createWebPagePrint(WebView webView) {
        /*if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT)
            return;
        PrintManager printManager = (PrintManager) getSystemService(Context.PRINT_SERVICE);
        PrintDocumentAdapter printAdapter = webView.createPrintDocumentAdapter();
        String jobName = "test";
        PrintAttributes.Builder builder = new PrintAttributes.Builder();
        builder.setMediaSize(PrintAttributes.MediaSize.ISO_A5);
        PrintJob printJob = printManager.print(jobName, printAdapter, builder.build());

        if (printJob.isCompleted()) {
            // Toast.makeText(getApplicationContext(), R.string.print_complete, Toast.LENGTH_LONG).show();
            UtilDmt.showLongToast(getApplicationContext(), "print_complete");
        } else if (printJob.isFailed()) {
            UtilDmt.showLongToast(getApplicationContext(), "not print_complete");
        }
        // Save the job object for later status checking
        */

        TransactionDetailsRequest request = new TransactionDetailsRequest();
        request.transactionId = responseBean.getTransactionId();
        showDialog();
        Api.reports().getTransactionDetails(Util.PREFERENCES.getToken(BankTransactionWebView.this), request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ApiSuccess<TransactionDetailsBean>() {
                    @Override
                    public void call(TransactionDetailsBean balanceResponse) {
                        disMissDialog();
                        balanceResponseBean = balanceResponse;

                        if (balanceResponse.remark == null) {
                            balanceResponseBean.remark = "Failed";
                        }

                        redirectionToPrint();

                        // Check added
                        //, BLUETOOTH_CONNECTION_FLAG);

                        /*

                        */

                        //Intent intent = new Intent(BankTransactionWebView.this, SplashScreen.class);
                        //intent.putExtra("ENUM", ImpressiveclassEnum.PRINT);
                        //intent.putExtra("DATA", balanceResponse);
                        //startActivity(intent);

                    }
                }, new ApiFail() {
                    @Override
                    public void httpStatus(HttpErrorResponse response) {
                        disMissDialog();
                        Toast.makeText(BankTransactionWebView.this, response.getError(), Toast.LENGTH_LONG).show();

                    }

                    @Override
                    public void noNetworkError() {
                        disMissDialog();
                        Toast.makeText(BankTransactionWebView.this, "Please check internet", Toast.LENGTH_LONG).show();

                    }

                    @Override
                    public void unknownError(Throwable e) {
                        disMissDialog();
                        Toast.makeText(BankTransactionWebView.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }


                });
    }


    @Override
    public void onPrintCompletion(String message) {
        Util.showShortToast(BankTransactionWebView.this, message);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2) {
            if (resultCode == Activity.RESULT_OK) {
            }
        } else if (requestCode == 110 && resultCode == Activity.RESULT_OK) {

            if (data.hasExtra("BluetoothName") && data.hasExtra("MAC")) {
                bluetoothName = data.getStringExtra("BluetoothName");
                bluetoothMac = data.getStringExtra("MAC");
                printRecept(balanceResponseBean);
            }


        } else if (resultCode == Activity.RESULT_OK) {
            if (requestCode == BLUETOOTH_CONNECTION_FLAG) {
                redirectionToPrint();
            }
        }

    }

    public void redirectionToPrint() {

        String selectedPrinterName = Util.PREFERENCES.getDefaultPrinter(BankTransactionWebView.this);

        if (selectedPrinterName.equals(getResources().getString(R.string.device_ngx_printer))) {
            Intent intent = new Intent(BankTransactionWebView.this, NGXPrintActivity.class);
            intent.putExtra("ENUM", ImpressiveclassEnum.PRINT);
            intent.putExtra("DATA", balanceResponseBean);
            startActivity(intent);
        } else if (KeyFile.getDevise(getApplicationContext()).equalsIgnoreCase(getString(R.string.device_evolute_name))) {
            printerPresenter.printReceipt(balanceResponseBean, selectedPrinterName);
        } else {
            if (selectedPrinterName.equals(getApplicationContext().getString(R.string.device_evolute_printer))
                    && Util.PREFERENCES.getDeviceName(getApplicationContext()).equalsIgnoreCase(getApplicationContext().getString(R.string.device_evolute_name))) {
                // printRecept(balanceResponseBean);
                //printerPresenter.printReceipt(balanceResponseBean,selectedPrinterName);
                showDialog(balanceResponseBean);
            } else if (printerPresenter.isBluetoothPrinterConnected()) {
                printerPresenter.printReceipt(balanceResponseBean, selectedPrinterName);
            } else {
                Intent intent = new Intent(BankTransactionWebView.this, BluetoothController.class);
                intent.putExtra("ENUM", ImpressiveclassEnum.PRINT);
                intent.putExtra("DATA", balanceResponseBean);
                startActivityForResult(intent, BLUETOOTH_CONNECTION_FLAG);
            }

        }


    }


    private void printRecept(TransactionDetailsBean bean) {
        Printer ptr;
        String printValue;
        byte bFontstyleSmall = 0x03;
        byte bFontstyleBig = 0x02;
        byte bFontstyleSmallLine = 0x07;
        /// new EvoluteRDPrint.EnterTextTask().execute(0);
        String TAG = "PrintRecept";
        bluetoothName = Util.PREFERENCES.getEvoluteBluetoothName(getApplicationContext());
        bluetoothMac = Util.PREFERENCES.getEvoluteBluetoothMac(getApplicationContext());
        if (bluetoothName.isEmpty() || bluetoothMac.isEmpty()) {
            Intent intent = new Intent(this, BluetoothController.class);
            intent.putExtra("ENUM", ImpressiveclassEnum.PRINT);

            startActivityForResult(intent, 110);
        } else {

            try {
                ptr = new com.rdservice.helper.Printer(Util.isBluetoothConnected);
                if (ptr != null) {

                    String line0 = "        Digital India Payments ltd        ";
                    String line1 = "  Transaction Receipt  ";
                    String line2 = "Customer Copy - " + bean.serviceMod;
                    String line3 = "Date & Time    : " + bean.timestamp;
                    String line4 = "Terminal ID    : " + bean.terminalId;
                    String line5 = "Agent ID       : " + bean.agentId;
                    String line6 = "BC Name        : " + bean.BCName;
                    String line7 = "mATN req ID    : " + bean.mATNreqID;
                    String line8 = "Txn ID         : " + bean.transactionId;
                    String line9 = "UIDAI Auth Code: " + bean.aadhaarNo;
                    String line10 = "Txn Status     : " + bean.remark;
                    String line11 = "Txn Amt        : " + bean.amount;
                    String line12 = "A/c Bal        : " + bean.balance;
                    String line13 = "   Note: Pls do not pay any charges/fee   " +
                            "               for this txn               ";
                    String newLine = "\n";

                    ptr.iFlushBuf();


                    ptr.iPrinterAddData(bFontstyleBig, newLine);

                    ptr.iPrinterAddData(bFontstyleSmallLine, line0);
                    printValue = ptr.sStartPrinting("dipl", bluetoothName, bluetoothMac);
                    ptr.iFlushBuf();


                    // int iRetVal = ptr.iBmpPrint(getApplicationContext(), R.raw.logo);
                    ptr.sGetBmpPackets(this.getApplicationContext(), R.raw.logo, "dipl", bluetoothName, bluetoothMac);
                    ptr.iFlushBuf();


                    ptr.iPrinterAddData(bFontstyleBig, line1);

                    //ptr.iPrinterAddData(bFontstyleSmall, newLine);
                    ptr.iPrinterAddData(bFontstyleSmall, line2);
                    //ptr.iPrinterAddData(bFontstyleSmall, newLine);
                    ptr.iPrinterAddData(bFontstyleSmall, line3);
                    //ptr.iPrinterAddData(bFontstyleSmall, newLine);
                    ptr.iPrinterAddData(bFontstyleSmall, line4);
                    //ptr.iPrinterAddData(bFontstyleSmall, newLine);
                    ptr.iPrinterAddData(bFontstyleSmall, line5);
                    //ptr.iPrinterAddData(bFontstyleSmall, newLine);
                    ptr.iPrinterAddData(bFontstyleSmall, line6);
                    //ptr.iPrinterAddData(bFontstyleSmall, newLine);
                    ptr.iPrinterAddData(bFontstyleSmall, line7);
                    //ptr.iPrinterAddData(bFontstyleSmall, newLine);
                    ptr.iPrinterAddData(bFontstyleSmall, line8);
                    //ptr.iPrinterAddData(bFontstyleSmall, newLine);
                    ptr.iPrinterAddData(bFontstyleSmall, line9);
                    //ptr.iPrinterAddData(bFontstyleSmall, newLine);
                    ptr.iPrinterAddData(bFontstyleSmall, line10);
                    //ptr.iPrinterAddData(bFontstyleSmall, newLine);
                    ptr.iPrinterAddData(bFontstyleSmall, line11);
                    //ptr.iPrinterAddData(bFontstyleSmall, newLine);
                    ptr.iPrinterAddData(bFontstyleSmall, line12);
                    ptr.iPrinterAddData(bFontstyleSmall, line13);
                    ptr.iPrinterAddData(bFontstyleSmall, newLine + newLine + newLine);
                    ptr.iPrinterAddData(bFontstyleSmall, newLine + newLine);


                    printValue = ptr.sStartPrinting("Dipl", bluetoothName, bluetoothMac);
                    if (printValue == null) {
                        ptr.iGetReturnCode();
                    } else {
                        try {
                            Log.e(TAG, " inXml Length is :" + printValue.length());
                            Log.e(TAG, "++++ xml Data = " + printValue);
                            Log.e(TAG, "**** Sending data via intent");
                            Intent act = new Intent(Printer.URI);
                            act.putExtra(Printer.DataTAG, printValue);
                            PackageManager packageManager = getApplicationContext().getPackageManager();
                            List activities = packageManager.queryIntentActivities(act, PackageManager.MATCH_DEFAULT_ONLY);
                            final boolean isIntentSafe = activities.size() > 0;
                            Log.d(TAG, "Boolean check for activities = " + isIntentSafe);
                            if (!isIntentSafe) {
                                Toast.makeText(getApplicationContext(), "No RD Service Available", Toast.LENGTH_SHORT).show();
                            }
                            Log.d(TAG, "No of activities = " + activities.size());
                            Log.e(TAG, "**** After sending parcelable class via intent");
                            startActivityForResult(act, 2);
                        } catch (Exception e) {
                            Log.e(TAG, "Error while connecting to RDService");
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }

    protected void showDialog(final TransactionDetailsBean bean) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(getApplicationContext().getString(R.string.print_receipt));
        alertDialogBuilder.setMessage(getApplicationContext().getString(R.string.print_confirm_message)).setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        printRecept(bean);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();

    }


}
