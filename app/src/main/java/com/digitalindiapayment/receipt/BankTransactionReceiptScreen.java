package com.digitalindiapayment.receipt;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.digitalindiapayment.BaseActivity;
import com.digitalindiapayment.R;
import com.digitalindiapayment.landingpage.HomeActivity;
import com.digitalindiapayment.util.Util;
import com.digitalindiapayment.webservices.responsepojo.BankTransactionResponse;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Amol on 12-03-2017.
 */
public class BankTransactionReceiptScreen extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.transactionId)
    TextView transactionId;
    @BindView(R.id.aadhaarNo)
    TextView aadhaarNo;
    @BindView(R.id.response)
    TextView response;
    @BindView(R.id.remark)
    TextView remark;
    @BindView(R.id.balance)
    TextView balance;
    @BindView(R.id.services)
    TextView services;
    @BindView(R.id.servicesHeader)
    TextView servicesHeader;
    @BindView(R.id.date)
    TextView date;

    @BindView(R.id.bankname)
    TextView bankname;


    private BankTransactionResponse responseBean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank_transaction_receipt);
        ButterKnife.bind(this);
        responseBean = getIntent().getParcelableExtra(Util.KEYS.DATA);
        transactionId.setText(responseBean.getTransactionId());
        aadhaarNo.setText(responseBean.getAadhaarNo());
        response.setText(responseBean.getResponse());
        remark.setText(responseBean.getRemark());
        transactionId.setText(responseBean.getTransactionId());
        balance.setText(responseBean.getBalance());
        services.setText(responseBean.getServiceMod());
        servicesHeader.setText(responseBean.getServiceMod());
        date.setText(responseBean.getTimestamp());
        bankname.setText(responseBean.getBankName());

        setSupportActionBar(toolbar);
        //Make the button enable and Onclick listner on that
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }


    @OnClick(R.id.ok)
    public void onClick() {
        finish();
        Intent newIntent = new Intent(this, HomeActivity.class);
        newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(newIntent);

    }

    @Override
    public void onBackPressed() {
        finish();
        Intent newIntent = new Intent(this, HomeActivity.class);
        newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(newIntent);

    }
}
