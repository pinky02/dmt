package com.digitalindiapayment.landingpage;

import com.digitalindiapayment.pojo.DashboardBean;
import com.digitalindiapayment.webservices.responsepojo.TransactionsResponse;

/**
 * Created by admin on 01-07-2017.
 */

public class DashboardAndTransactionBean {
    private DashboardBean dashboardBean;
    private TransactionsResponse transactionsResponse;

    public DashboardBean getDashboardBean() {
        return dashboardBean;
    }

    public void setDashboardBean(DashboardBean dashboardBean) {
        this.dashboardBean = dashboardBean;
    }

    public TransactionsResponse getTransactionsResponse() {
        return transactionsResponse;
    }

    public void setTransactionsResponse(TransactionsResponse transactionsResponse) {
        this.transactionsResponse = transactionsResponse;
    }
}
