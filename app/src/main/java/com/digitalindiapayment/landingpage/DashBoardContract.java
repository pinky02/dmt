package com.digitalindiapayment.landingpage;


import com.digitalindiapayment.login.UserProfile;
import com.digitalindiapayment.mvp.BaseView;
import com.digitalindiapayment.pojo.DashboardBean;
import com.digitalindiapayment.pojo.LastFiveTransaction;
import com.digitalindiapayment.webservices.requestpojo.DeviceInfoRequest;
import com.digitalindiapayment.webservices.responsepojo.TicketLoginResponse;

import java.util.List;

public interface DashBoardContract {

    public interface View extends BaseView {
        void showDashBoard(DashboardBean dashboardBean);
        void showLastFiveTransactions(List<LastFiveTransaction> last5Transaction);
        void onTicketLoginSucess(TicketLoginResponse.ResponseData response);
        void onTicketLoginFailed(String msg);
    }

    public interface UserAction {
        void getDashBoardDetail();
        void  sendDeviceLocationInfo(DeviceInfoRequest deviceInfoRequest);
        void ticketLogin();

    }
}
