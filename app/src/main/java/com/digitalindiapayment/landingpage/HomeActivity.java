package com.digitalindiapayment.landingpage;

import android.Manifest;
import android.app.ActionBar;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.percent.PercentRelativeLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.TouchDelegate;
import android.view.View;
import android.view.Window;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.digitalindiapayment.BuildConfig;
import com.digitalindiapayment.R;
import com.digitalindiapayment.application.MyApplication;
import com.digitalindiapayment.creditrequest.CreditRequestScreen;
import com.digitalindiapayment.fcm.MyFirebaseMessagingService;
import com.digitalindiapayment.help_support.helpactivity.HelpActivity;
import com.digitalindiapayment.login.UserProfile;
import com.digitalindiapayment.pojo.DIPLNotification;
import com.digitalindiapayment.pojo.DashboardBean;
import com.digitalindiapayment.pojo.LastFiveTransaction;
import com.digitalindiapayment.reports.ReportsActivity;
import com.digitalindiapayment.reports.TransactionReport.TransactionReportActivity;
import com.digitalindiapayment.scannerpackagefinder.PackageFinderPresenter;
import com.digitalindiapayment.scannerpackagefinder.PackagefinderContract;
import com.digitalindiapayment.settings.SettingsActivity;
import com.digitalindiapayment.settlement.SettlementActivity;
import com.digitalindiapayment.ticket.TicketActivity;
import com.digitalindiapayment.ticket.ticket_list.TicketListActivity;
import com.digitalindiapayment.transaction.BalanceEnquiryScreen;
import com.digitalindiapayment.transaction.DepositScreen;
import com.digitalindiapayment.transaction.WithdrawScreen;
import com.digitalindiapayment.util.Util;
import com.digitalindiapayment.webservices.requestpojo.DeviceInfoRequest;
import com.digitalindiapayment.webservices.responsepojo.ScannerDeviceResponse;
import com.digitalindiapayment.webservices.responsepojo.TicketLoginResponse;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;

//import com.digitalindiapayment.balanceenquiry.BalanceEnquiryScreen;


public class HomeActivity extends AppCompatActivity implements DashBoardContract.View, PackagefinderContract.View, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = "HomeActivity";
    public static final int MANAGEMENT_CLIENT_REQUEST = 202;
    public static final int RD_CLIENT_REQUEST = 202;
    private GoogleApiClient mGoogleApiClient;


    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.balanceqnquiry)
    LinearLayout ic_balance_inquiry;

    @BindView(R.id.withdraw)
    LinearLayout ic_withdraw;

    @BindView(R.id.deposite)
    LinearLayout ic_deposit;

    @BindView(R.id.walletrequest)
    LinearLayout walletrequest;

    @BindView(R.id.pay)
    LinearLayout pay;

    @BindView(R.id.ic_home_light)
    ImageView ic_home_light;

    @BindView(R.id.ic_report_light)
    ImageView ic_report_light;

    @BindView(R.id.ic_support)
    ImageView ic_support;

    @BindView(R.id.ic_setting_light)
    ImageView ic_setting_light;

    @BindView(R.id.textviewrs)
    TextView textviewrs;

    @BindView(R.id.textviewtodaysdeposite)
    TextView textviewtodaysdeposite;

    @BindView(R.id.textviewtodayswithdraw)
    TextView textviewtodayswithdraw;
    @BindView(R.id.recyclerviewtransaction)
    RecyclerView recyclerviewtransaction;
    @BindView(R.id.noInternetImg)
    ImageView noInternetImg;
    @BindView(R.id.noInternetMsg)
    TextView noInternetMsg;
    @BindView(R.id.refreshButton)
    ImageView refreshButton;
    @BindView(R.id.noInternetContainer)
    LinearLayout noInternetContainer;
    @BindView(R.id.progressbar)
    MaterialProgressBar progressbar;
    @BindView(R.id.message)
    TextView message;
    @BindView(R.id.progressdialogdip)
    RelativeLayout progressdialogdip;
    @BindView(R.id.ic_balance_inquiry)
    ImageView icBalanceInquiry;
    @BindView(R.id.ic_balance_text)
    TextView icBalanceText;
    @BindView(R.id.ic_withdraw)
    ImageView icWithdraw;
    @BindView(R.id.ic_withdraw_text)
    TextView icWithdrawText;
    @BindView(R.id.ic_deposit)
    ImageView icDeposit;
    @BindView(R.id.ic_deposit_text)
    TextView icDepositText;
    @BindView(R.id.ic_wallet_request)
    ImageView icWalletRequest;
    @BindView(R.id.ic_wallet_request_text)
    TextView icWalletRequestText;
    @BindView(R.id.ic_pay_request)
    ImageView icPayRequest;
    @BindView(R.id.ic_pay_request_text)
    TextView icPayRequestText;
    @BindView(R.id.header)
    HorizontalScrollView header;
    @BindView(R.id.textviewmybalance)
    TextView textviewmybalance;
    @BindView(R.id.linearamount)
    LinearLayout linearamount;
    @BindView(R.id.ic_down_withdraw)
    ImageView icDownWithdraw;
    @BindView(R.id.textviewtodaybalancetext)
    TextView textviewtodaybalancetext;
    @BindView(R.id.relativeLayout2)
    RelativeLayout relativeLayout2;
    @BindView(R.id.ic_up_credit)
    ImageView icUpCredit;
    @BindView(R.id.layoutdepositewithdraw)
    PercentRelativeLayout layoutdepositewithdraw;
    @BindView(R.id.textview_last_trans)
    TextView textviewLastTrans;
    @BindView(R.id.ic_home_light_text)
    TextView icHomeLightText;
    @BindView(R.id.ic_report_light_text)
    TextView icReportLightText;
    @BindView(R.id.tvSupport)
    TextView tvSupport;
    @BindView(R.id.ic_setting_light_text)
    TextView icSettingLightText;
    @BindView(R.id.footer_layout)
    PercentRelativeLayout footerLayout;
    @BindView(R.id.home_main)
    PercentRelativeLayout home_main;
    @BindView(R.id.exception_handle)
    RelativeLayout exceptionHandle;
    @BindView(R.id.textviewtodaysbalance_enq_count)
    TextView textviewtodaysbalanceEnqCount;
    @BindView(R.id.textviewtodayscommission)
    TextView textviewtodayscommission;
    @BindView(R.id.ic_settlment)
    ImageView settlement;
    @BindView(R.id.ic_settlement_text)
    TextView ic_settlement_text;
    @BindView(R.id.ic_up_balance_enq_count)
    ImageView icUpBalanceEnqCount;

    // installRequireDialog Dialog

    TextView managementClientTV;
    TextView managementClientInstall;
    TextView rdService;
    TextView rdServiceInstall;
    RelativeLayout managementClientParent;
    RelativeLayout rdServiceParent;
    TextView popuptitle;


    private DashBoardPresenter dashBoardPresenter = null;
    private List<LastFiveTransaction> transactionReportBeanList;
    private LastFiveTransactionAdapter lastFiveTransactionAdapter;

    private UserProfile userProfile;
    private String from;
    private String actionCode;

    PackagefinderContract.UserAction packageFinder;

    Dialog installRequireDialog;
    ScannerDeviceResponse scannerDeviceResponse;
    DIPLNotification diplNotification;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        Util.PREFERENCES.setIsLogin(this, true);

        Log.e("fcm", Util.PREFERENCES.getNotificationRegId(this));


        userProfile = getIntent().getParcelableExtra(Util.KEYS.DATA);


        noInternetContainer.setVisibility(View.GONE);
        progressdialogdip.setVisibility(View.VISIBLE);
        exceptionHandle.setVisibility(View.VISIBLE);
        home_main.setVisibility(View.INVISIBLE);

        ic_home_light.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_home_dark));

        //getting balance, withdraw and deposite amount
        dashBoardPresenter = new DashBoardPresenter(this, getApplicationContext());
        recyclerviewtransaction.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerviewtransaction.setLayoutManager(mLayoutManager);
        recyclerviewtransaction.setItemAnimator(new DefaultItemAnimator());
        transactionReportBeanList = new ArrayList<>();
        lastFiveTransactionAdapter = new LastFiveTransactionAdapter(getApplicationContext(), transactionReportBeanList);
        recyclerviewtransaction.setAdapter(lastFiveTransactionAdapter);


        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        mGoogleApiClient.connect();

        final View parent = (View) ic_report_light.getParent();
        parent.post(new Runnable() {
            // Post in the parent's message queue to make sure the parent
            // lays out its children before we call getHitRect()
            public void run() {
                final Rect r = new Rect();
                ic_report_light.getHitRect(r);
                r.top -= 4;
                r.bottom += 4;
                parent.setTouchDelegate(new TouchDelegate(r, ic_report_light));
            }
        });


        // If RD is not Ennble than don't show package finder popup
        if (Util.PREFERENCES.isRDEnabled(this)) {
            packageFinder = new PackageFinderPresenter(getApplicationContext(), HomeActivity.this);
            packageFinder.fetchDetails();
        }
        if (getIntent().hasExtra(MyFirebaseMessagingService.NOTIFICATION_DATA)) {
            diplNotification = getIntent().getParcelableExtra(MyFirebaseMessagingService.NOTIFICATION_DATA);
            if (diplNotification != null) {
                Log.e(TAG,"notification type "+diplNotification.getType());
                Util.PREFERENCES.notificationRedirect(HomeActivity.this, diplNotification);
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
    }

    private void handleNotificationClick(String actionCode) {
        Log.e(TAG,"notification action "+actionCode);
        if (actionCode.equalsIgnoreCase("settlement")) {
            //call perticulat controller or activity
            Intent intent = new Intent(HomeActivity.this, SettlementActivity.class);
            startActivity(intent);
        } else if (actionCode.equalsIgnoreCase("creditrequest") || actionCode.equalsIgnoreCase("debitrequest")) {
            Intent intent = new Intent(HomeActivity.this, TransactionReportActivity.class);
            startActivity(intent);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent.hasExtra(Util.KEYS.INFORMATION)) {
            from = intent.getStringExtra(Util.KEYS.INFORMATION);
            actionCode = intent.getStringExtra(Util.KEYS.ACTION_CODE);
            Log.e(TAG,"notification action "+actionCode);
            handleNotificationClick(actionCode);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        showLoading();
        dashBoardPresenter.getDashBoardDetail();
    }

    @OnClick(R.id.balanceqnquiry)
    public void ic_balance_inquiry() {
        final Intent intent = new Intent(HomeActivity.this, BalanceEnquiryScreen.class);
        startActivity(intent);

    }

    @OnClick(R.id.withdraw)
    public void ic_withdraw() {
        final Intent intent = new Intent(HomeActivity.this, WithdrawScreen.class);
        startActivity(intent);
    }

    @OnClick(R.id.deposite)
    public void ic_deposit() {
        final Intent intent = new Intent(HomeActivity.this, DepositScreen.class);
        startActivity(intent);
    }


    @OnClick(R.id.pay)
    public void pay() {
        /*final Intent intent = new Intent(HomeActivity.this, PaymentScreen.class);
        startActivity(intent);*/

        Util.showShortToast(this, "We are working on it");
    }

    @OnClick(R.id.walletrequest)
    public void walletrequest() {
        final Intent intent = new Intent(HomeActivity.this, CreditRequestScreen.class);
        startActivity(intent);
    }

    @OnClick(R.id.ic_report_light)
    public void ic_report_light() {
        final Intent intent = new Intent(HomeActivity.this, ReportsActivity.class);

        //ic_home_light.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_home_light));
        //ic_report_light.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_report_dark));
        //ic_profile_light.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_profile_light));
        //ic_setting_light.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_setting_light));

        startActivity(intent);
    }

    @OnClick(R.id.ic_setting_light)
    public void onClickSettings(View view) {
        Intent intent = new Intent(HomeActivity.this, SettingsActivity.class);
        intent.putExtra(Util.KEYS.DATA, userProfile);
        startActivity(intent);
    }


    @OnClick(R.id.refreshButton)
    public void onClickReferesh(View view) {
        showLoading();
        dashBoardPresenter.getDashBoardDetail();
    }

    @Override
    public void noInternet() {
        noInternetContainer.setVisibility(View.VISIBLE);
        home_main.setVisibility(View.GONE);
        hideLoading();

        exceptionHandle.setVisibility(View.VISIBLE);
    }

    @Override
    public void exception(Throwable e) {
        Log.d(TAG, "exception " + e.toString());

        Util.showLongToast(this, e.getMessage());
        hideLoading();
    }

    @Override
    public void fail(String message) {
        Log.d(TAG, "fail " + message);
        hideLoading();
        Util.showLongToast(this, message);
    }

    @Override
    public void success(String message) {

    }

    @Override
    public void showLoading() {
        progressdialogdip.setVisibility(View.VISIBLE);
        noInternetContainer.setVisibility(View.GONE);
    }

    @Override
    public void hideLoading() {
        progressdialogdip.setVisibility(View.GONE);

    }

    @Override
    public void showDashBoard(DashboardBean dashboardBean) {
        home_main.setVisibility(View.VISIBLE);
        exceptionHandle.setVisibility(View.GONE);

        textviewrs.setText(getResources().getString(R.string.Rs) + " " + dashboardBean.getBalance());
        textviewtodaysdeposite.setText("" + dashboardBean.getDeposit());
        textviewtodayswithdraw.setText("" + dashboardBean.getWithdraw());
        textviewtodaysbalanceEnqCount.setText("" + dashboardBean.getBalanceEnquiryCountToday());
        textviewtodayscommission.setText(" ( " + dashboardBean.getCommissionToday() + " ) ");

    }

    @Override
    public void showLastFiveTransactions(List<LastFiveTransaction> lastFiveTransaction) {
        lastFiveTransactionAdapter.clear();
        lastFiveTransactionAdapter.addAll(lastFiveTransaction);
        lastFiveTransactionAdapter.notifyDataSetChanged();
    }

    @Override
    public void onTicketLoginSucess(TicketLoginResponse.ResponseData response) {
        ic_support.setEnabled(true);
        tvSupport.setEnabled(true);
        Util.PREFERENCES.setTicketAgetId(getApplicationContext(), response.getTicketAgentID());
        Util.PREFERENCES.setTicketToken(getApplicationContext(), response.getTiketToken());
        Intent intent = new Intent(HomeActivity.this, HelpActivity.class);
        startActivity(intent);
    }

    @Override
    public void onTicketLoginFailed(String msg) {
        ic_support.setEnabled(true);
        tvSupport.setEnabled(true);
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        Util.PREFERENCES.setIsLogin(this, false);

        try {


            /*try {
                 BluetoothComm.mosOut = null;
                 BluetoothComm.misIn = null;
            } catch (NullPointerException e) {
            }*/
            System.gc();
            ((MyApplication) getApplicationContext()).closeConn();

        } catch (Exception E) {

        }
    }

    @OnClick({R.id.ic_support, R.id.tvSupport})
    public void onProfileClicked(View view) {

//        dashBoardPresenter.ticketLogin();
//        ic_support.setEnabled(false);
//        tvSupport.setEnabled(false);
//        Intent intent = new Intent(HomeActivity.this, HelpActivity.class);
//       intent.putExtra(UtilDmt.KEYS.DATA, userProfile);
//        startActivity(intent);
        /*switch (view.getId()) {
            case R.id.ic_profile_light:
                break;
            case R.id.ic_profile_light_text:
                break;
        }*/
        Intent intent = new Intent(HomeActivity.this, TicketListActivity.class);
        intent.putExtra(Util.KEYS.DATA, userProfile);
        startActivity(intent);
       // intent.putExtra(UtilDmt.KEYS.DATA, userProfile);
    }


    @OnClick(R.id.ic_settlment)
    public void onSettlement() {
        Intent intent = new Intent(HomeActivity.this, SettlementActivity.class);
        startActivity(intent);
    }

    @Override
    public void onPackageApisuccess(ScannerDeviceResponse scannerDeviceResponse) {
        this.scannerDeviceResponse = scannerDeviceResponse;
        showPackageDownloadUI();
    }

    @Override
    public void onPackageApifail(String message) {

    }

    public void showPackageDownloadUI() {
        if (installRequireDialog != null) {
            installRequireDialog.dismiss();
            installRequireDialog = null;
        }
        // Check wether Management clint and RD Service is installed on Android Application
        Boolean isManagementClientInstallRequire = !packageFinder.isPackageAvailable(scannerDeviceResponse.getManagementClientPackage()) && scannerDeviceResponse.hasManagementClient();
        Boolean isRDServiceInstallRequire = !packageFinder.isPackageAvailable(scannerDeviceResponse.getRDSevicePackage());

        if (installRequireDialog == null && (isManagementClientInstallRequire || isRDServiceInstallRequire)) {
            installRequireDialog = new Dialog(HomeActivity.this);
            installRequireDialog.setContentView(R.layout.layout_required_links);
            installRequireDialog.setCancelable(false);
            Window window = installRequireDialog.getWindow();
            window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

            managementClientTV = (TextView) installRequireDialog.findViewById(R.id.managementClientTV);
            managementClientInstall = (TextView) installRequireDialog.findViewById(R.id.managementClientInstall);
            rdService = (TextView) installRequireDialog.findViewById(R.id.rdService);
            rdServiceInstall = (TextView) installRequireDialog.findViewById(R.id.rdServiceInstall);
            managementClientParent = (RelativeLayout) installRequireDialog.findViewById(R.id.managementClientParent);
            rdServiceParent = (RelativeLayout) installRequireDialog.findViewById(R.id.rdServiceParent);
            popuptitle = (TextView) installRequireDialog.findViewById(R.id.popuptitle);

            popuptitle.setText(scannerDeviceResponse.getName() + " " + getString(R.string.service_require));
            if (!isManagementClientInstallRequire) {
                managementClientParent.setVisibility(View.GONE);
            }
            if (!isRDServiceInstallRequire) {
                rdServiceParent.setVisibility(View.GONE);
            }

            managementClientInstall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(scannerDeviceResponse.getManagementClientLink()));
                    startActivityForResult(i, MANAGEMENT_CLIENT_REQUEST);
                }
            });
            rdServiceInstall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(scannerDeviceResponse.getRDServiceLink()));
                    startActivityForResult(i, RD_CLIENT_REQUEST);
                }
            });

            installRequireDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        finish();
                        dialog.dismiss();
                    }
                    return false;
                }
            });
            installRequireDialog.show();

            if (BuildConfig.DEBUG) {
                installRequireDialog.setCancelable(true);
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (installRequireDialog != null) {
            installRequireDialog.dismiss();
            installRequireDialog = null;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MANAGEMENT_CLIENT_REQUEST || requestCode == RD_CLIENT_REQUEST) {
            showPackageDownloadUI();
        }
    }

    Double lat, lon;
    String city, state;

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.e("deviceInfo", "onConnected");

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        List<Address> addresses = null;
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());


        if (mLastLocation != null) {
            NumberFormat formatter = new DecimalFormat("#0.000000");
            lat = mLastLocation.getLatitude();
            lon = mLastLocation.getLongitude();
            lat = Double.valueOf(formatter.format(lat));
            lon = Double.valueOf(formatter.format(lon));
        }

        try {
            Log.e("deviceInfo","latitude- "+lat);
            Log.e("deviceInfo","longitude- "+lon);

            addresses = geocoder.getFromLocation(lat, lon, 1);
            city = addresses.get(0).getLocality();
            state = addresses.get(0).getAdminArea();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Log.e("deviceInfo", "version code " + Build.VERSION.SDK_INT);
        Log.e("deviceInfo", "manufacturer " + Build.MANUFACTURER);
        Log.e("deviceInfo", "model  " + Build.MODEL);
        Log.e("deviceInfo", "model  " + Build.ID);
        Log.e("deviceInfo", "ID  " + Build.SERIAL);
        Log.e("deviceInfo", "RELEASE  " + Build.VERSION.RELEASE);

        Log.e("deviceInfo", "lat  " + lat);
        Log.e("deviceInfo", "long  " + lon);
        Log.e("deviceInfo", "city  " + city);
        Log.e("deviceInfo", "state  " + state);

        int versionCode = Build.VERSION.SDK_INT;
        String manufacture = Build.MANUFACTURER;
        String model = Build.MODEL;
        String modelId = Build.ID;
        String release = Build.VERSION.RELEASE;
        DeviceInfoRequest request = new DeviceInfoRequest();
        request.setVersionCode("" + versionCode);
        request.setManufacturerName(manufacture);
        request.setModelNumber(model);
        request.setModelId(modelId);
        request.setBuildVersion(release);
        request.setLatitude("" + lat);
        request.setLongitude("" + lon);
        request.setCity(city);
        request.setState(state);
        if (dashBoardPresenter != null) {
            dashBoardPresenter.sendDeviceLocationInfo(request);
        }

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


}
