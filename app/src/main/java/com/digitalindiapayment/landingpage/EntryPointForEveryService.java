package com.digitalindiapayment.landingpage;

import android.app.ActionBar;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.digitalindiapayment.R;
import com.digitalindiapayment.dmt.dashboard.HomePageActivity;
import com.digitalindiapayment.dmt.login.LoginActivity;
import com.digitalindiapayment.dmt.login.LoginContract;
import com.digitalindiapayment.dmt.login.LoginPresenter;
import com.digitalindiapayment.dmt.util.UtilDmt;
import com.digitalindiapayment.fcm.MyFirebaseMessagingService;
import com.digitalindiapayment.login.UserProfile;
import com.digitalindiapayment.pojo.Login;
import com.digitalindiapayment.util.Util;

import com.digitalindiapayment.webservices.dmt.requestpojo.LogoutRequest;
import com.digitalindiapayment.webservices.dmt.responsepojo.LoginResponse;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EntryPointForEveryService extends AppCompatActivity implements LoginContract.View {
    @BindView(R.id.gridview)
    GridView gridview;

    private UserProfile userProfile;
    com.digitalindiapayment.pojo.Login login;
    com.digitalindiapayment.webservices.dmt.requestpojo.Login loginDmt;
    LoginContract.UserActions loginPresenter;
    TextView txtMessage;
    Dialog builder;
    TextView resetTextView;
    Button resetButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entry_point_for_every_service);
        ButterKnife.bind(this);

        login = new Login();
        loginDmt = new  com.digitalindiapayment.webservices.dmt.requestpojo.Login();
        loginPresenter = new LoginPresenter(this, getApplicationContext());
        gridview.setAdapter(new ImageAdapter(this));
        userProfile = getIntent().getParcelableExtra(Util.KEYS.DATA);
        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView <?> parent,
                                    View v, int position, long id) {

                switch (position) {
                    case 0:
                            Intent loginToAEPS = new Intent(getApplicationContext(), HomeActivity.class);
                            loginToAEPS.putExtra(Util.KEYS.DATA, userProfile);
                            loginToAEPS.putExtra(MyFirebaseMessagingService.NOTIFICATION_DATA, getIntent().getParcelableExtra(MyFirebaseMessagingService.NOTIFICATION_DATA));
                            Util.PREFERENCES.setLogin(getApplicationContext(), login);
                            startActivity(loginToAEPS);
                        break;

                    case 1:
//                        Intent loginToDmt = new Intent(getApplicationContext(), LoginActivity.class);
//                        startActivity(loginToDmt);
                        loginToDMT();
                        break;
                    default:
                        return;
                }
            }
        });
    }
    private void loginToDMT() {
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View promptView = layoutInflater.inflate(R.layout.alert_dialog_common, null);

        final AlertDialog alertD = new AlertDialog.Builder(this).create();
        TextView txtTitle = (TextView) promptView.findViewById(R.id.title);
        txtTitle.setText("Please Enter Password for way to DMT");
        txtMessage = (TextView) promptView.findViewById(R.id.message);
        txtMessage.setText("");
        Button btnCancel = (Button) promptView.findViewById(R.id.cancel);

        Button btnOk = (Button) promptView.findViewById(R.id.ok);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                alertD.dismiss();
            }
        });

        btnOk.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
              String password =   txtMessage.getText().toString();
                if (txtMessage.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(getApplicationContext(), getString(R.string.pls_enter_password), Toast.LENGTH_SHORT).show();
                    return;
                }
              else{
                  //Intent loginToDmt =  new Intent(EntryPointForEveryService.this, HomePageActivity.class);
                //  loginToDmt.putExtra("")
                 String UserId =    Util.PREFERENCES.getMObile(getApplicationContext());
                    loginDmt.setPhoneNo(UserId);
                    loginDmt.setPassword(txtMessage.getText().toString());
//        loginDmt.setPhoneNo("9952777568");
//      loginDmt.setPassword("123456");
                    loginDmt.setCaptcha("234");

                    // Crash-Analytics


                    loginPresenter.submitDmtLogin(loginDmt);

                }

                alertD.dismiss();
            }
        });
        alertD.setView(promptView);
        alertD.show();
        alertD.setCancelable(false);
    }

    @Override
    public void noInternet() {

    }

    @Override
    public void exception(Throwable e) {

    }

    @Override
    public void fail(String message) {

    }

    @Override
    public void success(String message) {

    }

    @Override
    public void showLoading() {
       // showDialog();
    }



    @Override
    public void hideLoading() {

    }

    @Override
    public void showHome(String token, com.digitalindiapayment.dmt.login.UserProfile userProfile) {
        hideLoading();
        UtilDmt.PREFERENCES.setToken(this, token);
        UtilDmt.PREFERENCES.setIsLogin(this, true);
        Intent loginToHome = new Intent(EntryPointForEveryService.this, HomePageActivity.class);
        loginToHome.putExtra(UtilDmt.KEYS.DATA, userProfile);
        UtilDmt.PREFERENCES.setLogin(this, loginDmt);
        startActivity(loginToHome);
        finish();
    }

    @Override
    public void onResetSessionCall() {
        builder = new Dialog(EntryPointForEveryService.this);
        builder.setContentView(R.layout.layout_reset_session_dmt);
        builder.setCancelable(true);

        resetTextView = (TextView) builder.findViewById(R.id.resetTextView);
        resetButton = (Button) builder.findViewById(R.id.resetButton);

        Window window = builder.getWindow();
        window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent resetSession = new Intent(LoginActivity.this, ResetSession.class);
                //resetSession.putExtra(UtilDmt.KEYS.DATA,username.getText().toString());
                //startActivityForResult(resetSession,RESET_SESSION);
                builder.setCancelable(false);
                //  loginPresenter.resetSession(new ForgotPasswordRequest(username.getText().toString()));
                resetButton.setText(getString(R.string.processing));
                LogoutRequest logoutRequest = new LogoutRequest();
                logoutRequest.setMobilenumber(loginDmt.getPhoneNo());
                loginPresenter.setResetSession(logoutRequest);
                builder.dismiss();
            }
        });
        builder.show();
    }

    @Override
    public void onResetSessionFail() {

    }
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }
    @Override
    public void onResetSessionComplete() {
        loginToDMT();
    }

    @Override
    public void show(LoginResponse loginResponse) {
        Intent dmtHomePage = new Intent(EntryPointForEveryService.this, com.digitalindiapayment.dmt.dashboard.HomePageActivity.class);
        UtilDmt.PREFERENCES.setUserId(getApplicationContext(), ""+loginResponse.getUserid());
        UtilDmt.PREFERENCES.setParentUserId(getApplicationContext(), ""+loginResponse.getParentid());
        UtilDmt.PREFERENCES.setUserType(getApplicationContext(), ""+loginResponse.getUserType());
        UtilDmt.PREFERENCES.setBCAgent(getApplicationContext(),""+loginResponse.getBcagent());
        UtilDmt.PREFERENCES.setSessionToken(getApplicationContext(),""+loginResponse.getSessiontoken());
        dmtHomePage.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(dmtHomePage);
        finish();
    }

    public class ImageAdapter extends BaseAdapter {
        private Context mContext;

        // Constructor
        public ImageAdapter(Context c) {
            mContext = c;
        }

        public int getCount() {
            return mThumbIds.length;
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
//            View grid;
//
//            ImageView imageView;
//
//
//            if (convertView == null) {
//
//                imageView = new ImageView(mContext);
//                imageView.setLayoutParams(new GridView.LayoutParams(200, 200));
//                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
//                imageView.setPadding(10, 10, 10, 10);
//                //imageView.setImageResource(mThumbIds[position]);
//            }
//            else
//            {
//                imageView = (ImageView) convertView;
//            }
//
//            imageView.setImageResource(mThumbIds[position]);
//            return imageView;

            View grid;
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {

                grid = new View(mContext);
                grid = inflater.inflate(R.layout.custom_gridview, null);
                ImageView imageView = (ImageView)grid.findViewById(R.id.grid_image);
                imageView.setImageResource(mThumbIds[position]);
            } else {
                grid = (View) convertView;
            }

            return grid;
        }




        // Keep all Images in array
        public Integer[] mThumbIds = {

                R.drawable.ic_aeps,
                R.drawable.ic_dmt,


//                R.drawable.sample_2, R.drawable.sample_3,
//                R.drawable.sample_4, R.drawable.sample_5,
//                R.drawable.sample_6, R.drawable.sample_7,
//                R.drawable.sample_0, R.drawable.sample_1,
//                R.drawable.sample_2, R.drawable.sample_3,
//                R.drawable.sample_4, R.drawable.sample_5,
//                R.drawable.sample_6, R.drawable.sample_7
        };
    }

}
