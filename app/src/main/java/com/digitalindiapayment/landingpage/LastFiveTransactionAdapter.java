package com.digitalindiapayment.landingpage;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.digitalindiapayment.R;
import com.digitalindiapayment.pojo.LastFiveTransaction;
import com.digitalindiapayment.receipt.BankTransactionWebView;
import com.digitalindiapayment.util.Util;
import com.digitalindiapayment.webservices.responsepojo.BankTransactionResponse;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Amol on 04-04-2017.
 */

public class LastFiveTransactionAdapter extends RecyclerView.Adapter<LastFiveTransactionAdapter.LastFiveTransactionViewHolder> {


    @BindView(R.id.transcationAmount)
    TextView transcationAmount;

    private List<LastFiveTransaction> list;
    private Context context;

    public LastFiveTransactionAdapter(Context context, List<LastFiveTransaction> list) {
        this.list = list;
        this.context = context;
    }

    @Override
    public LastFiveTransactionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View iteView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycleview, parent, false);
        return new LastFiveTransactionViewHolder(iteView);
    }

    @Override
    public void onBindViewHolder(final LastFiveTransactionViewHolder holder, final int position) {
        final LastFiveTransaction lastFiveTransaction = list.get(position);
        holder.aadhaarNumber.setText(lastFiveTransaction.getAadhaarNumber());
        holder.walletTranscationType.setText(lastFiveTransaction.getService());
//        holder.transcationAmount.setText(last5Transaction.getAmount());
        holder.transactionTime.setText(lastFiveTransaction.getCreatedAt());
        holder.lastFiveTransStatus.setText(lastFiveTransaction.getTransactionStatus());
        holder.transcationAmount.setText(Util.indianCurrencyRepresentation(lastFiveTransaction.getAmount()));
        holder.relativeLayout.setTag(position);
        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final int tag = (int) holder.relativeLayout.getTag();
                final LastFiveTransaction lastFiveTransact = list.get(tag);
                BankTransactionResponse balanceResponse = new BankTransactionResponse();
                balanceResponse.setTransactionId(lastFiveTransact.getId());
                Intent intent = new Intent(context, BankTransactionWebView.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(Util.KEYS.DATA, balanceResponse);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class LastFiveTransactionViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.aadhaarNumber)
        TextView aadhaarNumber;
        @BindView(R.id.transcationAmount)
        TextView transcationAmount;
        @BindView(R.id.trnscation)
        LinearLayout trnscation;
        @BindView(R.id.walletTranscationType)
        TextView walletTranscationType;
        @BindView(R.id.transactionTime)
        TextView transactionTime;
        @BindView(R.id.linearLayout)
        LinearLayout linearLayout;
        @BindView(R.id.relativeLayout)
        RelativeLayout relativeLayout;
        @BindView(R.id.lastFiveTransStatus)
        TextView lastFiveTransStatus;


        public LastFiveTransactionViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, itemView);
        }


    }


    public void addAll(List<LastFiveTransaction> objects) {
        list.addAll(objects);
    }

    public void clear() {
        list.clear();
        notifyDataSetChanged();
    }
}
