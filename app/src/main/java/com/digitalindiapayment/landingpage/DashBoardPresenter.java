package com.digitalindiapayment.landingpage;


import android.content.Context;
import android.util.Log;

import com.digitalindiapayment.R;
import com.digitalindiapayment.pojo.DashboardBean;
import com.digitalindiapayment.util.Util;
import com.digitalindiapayment.webservices.Api;
import com.digitalindiapayment.webservices.core.ApiFail;
import com.digitalindiapayment.webservices.core.ApiSuccess;
import com.digitalindiapayment.webservices.core.HttpErrorResponse;
import com.digitalindiapayment.webservices.requestpojo.DeviceInfoRequest;
import com.digitalindiapayment.webservices.requestpojo.TicketLoginRequest;
import com.digitalindiapayment.webservices.responsepojo.TicketLoginResponse;
import com.digitalindiapayment.webservices.responsepojo.TransactionsResponse;

import retrofit2.Response;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func2;
import rx.schedulers.Schedulers;


public class DashBoardPresenter implements DashBoardContract.UserAction {

    private static final String TAG = "DashBoard";

    private final DashBoardContract.View view;
    private final Context context;

    public DashBoardPresenter(DashBoardContract.View view, Context context) {
        this.view = view;
        this.context = context;
    }


    @Override
    public void getDashBoardDetail() {
        Log.i("Msg1", "Feteching Dashboad data");

        //DashboardBean TransactionDashboardBean ;
        //Api.landingPage().dashboardData(UtilDmt.PREFERENCES.getToken(context)).zipWith()
        Observable.zip(Api.landingPage().dashboardData(Util.PREFERENCES.getToken(context)),
                Api.landingPage().getTransctions(Util.PREFERENCES.getToken(context)),
                new Func2<DashboardBean, TransactionsResponse, DashboardAndTransactionBean>() {
                    @Override
                    public DashboardAndTransactionBean call(DashboardBean dashboardBeanResponse, TransactionsResponse transactionsResponse) {
                        DashboardAndTransactionBean bean = new DashboardAndTransactionBean();
                        bean.setDashboardBean(dashboardBeanResponse);
                        bean.setTransactionsResponse(transactionsResponse);

                        return bean;
                    }
                }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ApiSuccess<DashboardAndTransactionBean>() {
                    @Override
                    public void call(DashboardAndTransactionBean dashboardAndTransactionBean) {
                        view.showDashBoard(dashboardAndTransactionBean.getDashboardBean());
                        view.showLastFiveTransactions(dashboardAndTransactionBean.getTransactionsResponse().getTransactions());
                    }
                }, new ApiFail() {
                    @Override
                    public void httpStatus(HttpErrorResponse response) {

                    }

                    @Override
                    public void noNetworkError() {
                        view.noInternet();
                    }

                    @Override
                    public void unknownError(Throwable e) {
                        view.exception(e);
                    }
                });


    }

    @Override
    public void sendDeviceLocationInfo(DeviceInfoRequest deviceInfoRequest) {
        Api.userManagement().deviceLocationInfo(Util.PREFERENCES.getToken(context), deviceInfoRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ApiSuccess<Response<String>>() {
                    @Override
                    public void call(Response<String> stringResponse) {

                    }
                }, new ApiFail() {
                    @Override
                    public void httpStatus(HttpErrorResponse response) {

                    }

                    @Override
                    public void noNetworkError() {

                    }

                    @Override
                    public void unknownError(Throwable e) {

                    }
                });
    }

    @Override
    public void ticketLogin() {
        String appInstance = Util.PREFERENCES.getAppInstanceCode(context);
        String agentId = Util.PREFERENCES.getAgentUserId(context);
        String fName = Util.PREFERENCES.getFname(context);
        String email = Util.PREFERENCES.getEmail(context);
        String mobile = Util.PREFERENCES.getMObile(context);
        TicketLoginRequest request = new TicketLoginRequest();
        request.setAgentId(agentId);
        request.setFirstName(fName);
        request.setLastName("");
        request.setEmail(email);
        request.setMobile(mobile);
        Api.getTicket().ticketLogin(appInstance, request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ApiSuccess<TicketLoginResponse>() {
                    @Override
                    public void call(TicketLoginResponse response) {
                        if (response.getStatus().equalsIgnoreCase(Util.KEYS.SUCCESS))
                            view.onTicketLoginSucess(response.getData());
                        else view.onTicketLoginFailed(context.getString(R.string.ticke_fail_msg));
                    }
                }, new ApiFail() {
                    @Override
                    public void httpStatus(HttpErrorResponse response) {
                        view.onTicketLoginFailed(context.getString(R.string.ticke_fail_msg));
                    }

                    @Override
                    public void noNetworkError() {
                        view.noInternet();
                    }

                    @Override
                    public void unknownError(Throwable e) {
                        view.onTicketLoginFailed(e.getMessage());
                    }
                });
    }
}
