package com.morpho.sample;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Expert on 11-03-2017.
 */

public class KeyFile {

    public static final String FINGERS = "fingers";
    public static boolean isRebootSoft = false;
    public static final String MyPREFERENCES = "MyPrefs";
    public static final String DEVISE = "deviseNumber";

    public static void setDevise(Context context, String deviseNumber) {
        SharedPreferences sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(DEVISE, deviseNumber);
        editor.commit();
    }


    public static String getDevise(Context context) {
        SharedPreferences sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        return sharedpreferences.getString(DEVISE, "");
    }
}