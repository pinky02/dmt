// The present software is not subject to the US Export Administration Regulations (no exportation license required), May 2012
package com.morpho.sample;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.Surface;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.digitalindiapayment.R;
import com.digitalindiapayment.connection.USBConnection;
import com.morpho.morphosample.info.CaptureInfo;
import com.morpho.morphosample.info.MorphoInfo;
import com.morpho.morphosample.info.ProcessInfo;
import com.morpho.morphosample.info.subtype.CaptureType;
import com.morpho.morphosmart.sdk.CallbackMessage;
import com.morpho.morphosmart.sdk.Coder;
import com.morpho.morphosmart.sdk.CompressionAlgorithm;
import com.morpho.morphosmart.sdk.DetectionMode;
import com.morpho.morphosmart.sdk.EnrollmentType;
import com.morpho.morphosmart.sdk.ErrorCodes;
import com.morpho.morphosmart.sdk.LatentDetection;
import com.morpho.morphosmart.sdk.MorphoDevice;
import com.morpho.morphosmart.sdk.MorphoImage;
import com.morpho.morphosmart.sdk.MorphoWakeUpMode;
import com.morpho.morphosmart.sdk.Template;
import com.morpho.morphosmart.sdk.TemplateFVP;
import com.morpho.morphosmart.sdk.TemplateFVPType;
import com.morpho.morphosmart.sdk.TemplateList;
import com.morpho.morphosmart.sdk.TemplateType;

import org.greenrobot.eventbus.EventBus;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Observable;
import java.util.Observer;

public class MorphoCaptureFingerPrintActivity extends Activity implements USBConnection {

    private String strMessage = new String();
    private TextView tv;
    private Handler mHandler = new Handler();
    private ImageView iv;
    private ProgressBar progressBar;
    private Button btn_startstop, btn_rebootsoft, btn_closeandquit;
    private MorphoDevice morphoDevice = new MorphoDevice();
    private static boolean isRebootSoft = false;

    private RebootObserver rebootObserver = new RebootObserver();
    private FingerPrintObserver fingerPrintObserver = new FingerPrintObserver();
    private ArrayList<String> fpBase64Output;
    private ArrayList<String> fpvBase64Output;
    private int fingers = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.morpho_capture_fingerprint_activity);

        fpBase64Output = new ArrayList<>();
        fpvBase64Output = new ArrayList<>();
        fingers = getIntent().getIntExtra(KeyFile.FINGERS, 1);

        if (ProcessInfo.getInstance().getMorphoDevice() == null) {
            //String sensorName = ProcessInfo.getInstance().getMSOSerialNumber();
            String sensorName = KeyFile.getDevise(getApplicationContext());

            int ret = morphoDevice.openUsbDevice(sensorName, 0);
            if (ret == ErrorCodes.MORPHO_OK) {
            } else {
                Toast.makeText(getApplicationContext(), convertToInternationalMessage(ret, -101), Toast.LENGTH_LONG).show();

                finish();
                return;
            }
        }
        ProcessInfo.getInstance().setMorphoDevice(morphoDevice);

        tv = (TextView) findViewById(R.id.textViewMessage);
        iv = (ImageView) findViewById(R.id.imageView);
        progressBar = (ProgressBar) findViewById(R.id.vertical_progressbar);

        btn_startstop = (Button) findViewById(R.id.btn_startstop);
        btn_rebootsoft = (Button) findViewById(R.id.btn_rebootsoft);
        btn_closeandquit = (Button) findViewById(R.id.btn_closeandquit);

        ButtonClick buttonClick = new ButtonClick();
        btn_startstop.setOnClickListener(buttonClick);
        btn_rebootsoft.setOnClickListener(buttonClick);
        btn_closeandquit.setOnClickListener(buttonClick);
        CaptureInfo.getInstance();
        CaptureInfo.getInstance().setTemplateType(TemplateType.MORPHO_PK_ISO_FMR);
        CaptureInfo.getInstance().setTemplateFVPType(TemplateFVPType.MORPHO_NO_PK_FVP);
        CaptureType et = CaptureType.Verif;
        CaptureInfo.getInstance().setCaptureType(et);
        CaptureInfo.getInstance().setLatentDetect(false);
        //CaptureInfo.getInstance().setFingerNumber(1);
        CaptureInfo.getInstance().setFingerNumber(fingers);

        onStartstop();
        morphoDevice.resumeConnection(30, rebootObserver);


    }


    private void updateSensorMessage(String sensorMessage) {
        try {
            tv.setText(sensorMessage);
        } catch (Exception e) {
            e.getMessage();
        }
    }


    private void updateSensorProgressBar(int level) {
        try {

            final float[] roundedCorners = new float[]{5, 5, 5, 5, 5, 5, 5, 5};
            ShapeDrawable pgDrawable = new ShapeDrawable(new RoundRectShape(roundedCorners, null, null));

            int color = Color.GREEN;

            if (level <= 25) {
                color = Color.RED;
            } else if (level <= 50) {
                color = Color.YELLOW;
            }
            pgDrawable.getPaint().setColor(color);
            ClipDrawable progress = new ClipDrawable(pgDrawable, Gravity.LEFT, ClipDrawable.HORIZONTAL);
            progressBar.setProgressDrawable(progress);
            progressBar.setBackgroundDrawable(getResources().getDrawable(android.R.drawable.progress_horizontal));
            progressBar.setProgress(level);
        } catch (Exception e) {
            e.getMessage();
        }
    }


    protected void alert(String msg) {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(R.string.app_name);
        alertDialog.setMessage(msg);
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialog.show();
    }

    private void updateImage(Bitmap bitmap) {
        try {
            iv.setImageBitmap(bitmap);
        } catch (Exception e) {
            e.getMessage();
        }
    }


    private void onStartstop() {
        if (ProcessInfo.getInstance().isStarted()) {
            stop();
        } else {

            ProcessInfo.getInstance().setMorphoInfo(CaptureInfo.getInstance());

            try {
                ProcessInfo.getInstance().setCommandBioStart(true);
                btn_startstop.setText("Stop");
                enableOptionBoutton(false);
                ProcessInfo.getInstance().setStarted(true);
                lockScreenOrientation();
            } catch (Exception e) {
            }
            morphoDeviceCapture(fingerPrintObserver);

        }
    }


    private void onRebootsoft() {
        enableDisableBoutton(false);
        isRebootSoft = true;
        int ret = morphoDevice.rebootSoft(30, rebootObserver);

        if (ret != 0) {
            alert(getString(R.string.msg_rebootfailure));
        }
    }

    private void onCloseandquit() {

        try {
            morphoDevice.closeDevice();
        } catch (Exception e) {
            Log.e("ERROR", e.toString());
        } finally {
            ProcessInfo.getInstance().setMorphoDevice(null);

        }
    }

    class ButtonClick implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btn_startstop:
                    onStartstop();
                    break;
                case R.id.btn_rebootsoft:
                    onRebootsoft();
                    break;
                case R.id.btn_closeandquit:
                    onCloseandquit();
                    finish();
                    break;


            }
        }
    }

    public void morphoDeviceCapture(final Observer observer) {
        Thread commandThread = (new Thread(new Runnable() {
            @Override
            public void run() {
                final TemplateList templateList = new TemplateList();
                MorphoInfo morphoInfo = ProcessInfo.getInstance().getMorphoInfo();
                ProcessInfo processInfo = ProcessInfo.getInstance();
                int timeout;
                int acquisitionThreshold = 0;
                int advancedSecurityLevelsRequired;
                TemplateType templateType;
                TemplateFVPType templateFVPType;
                int maxSizeTemplate = 255;
                EnrollmentType enrollType;
                LatentDetection latentDetection;
                Coder coderChoice;
                int detectModeChoice;

                boolean exportFVP = false, exportFP = false;
                timeout = processInfo.getTimeout();

                if (processInfo.isFingerprintQualityThreshold()) {
                    acquisitionThreshold = processInfo.getFingerprintQualityThresholdvalue();
                }

                templateType = ((CaptureInfo) morphoInfo).getTemplateType();
                templateFVPType = ((CaptureInfo) morphoInfo).getTemplateFVPType();

                if (templateType != TemplateType.MORPHO_NO_PK_FP) {
                    exportFP = true;
                    if (templateType == TemplateType.MORPHO_PK_MAT || templateType == TemplateType.MORPHO_PK_MAT_NORM || templateType == TemplateType.MORPHO_PK_PKLITE) {
                        maxSizeTemplate = 1;
                    } else {
                        maxSizeTemplate = 255;
                    }
                } else {
                    if (MorphoInfo.m_b_fvp == false) {
                        templateType = TemplateType.MORPHO_PK_COMP;
                    }
                    maxSizeTemplate = 255;
                }

                if (templateFVPType != TemplateFVPType.MORPHO_NO_PK_FVP) {
                    exportFVP = true;
                }

                if (MorphoInfo.m_b_fvp) {
                    if (((CaptureInfo) morphoInfo).getCaptureType() != CaptureType.Verif) {
                        templateFVPType = TemplateFVPType.MORPHO_PK_FVP;
                    } else {
                        templateFVPType = TemplateFVPType.MORPHO_PK_FVP_MATCH;
                    }
                } else {
                    templateFVPType = TemplateFVPType.MORPHO_NO_PK_FVP;
                }

                if (((CaptureInfo) morphoInfo).getCaptureType() == CaptureType.Enroll) {
                    enrollType = EnrollmentType.THREE_ACQUISITIONS;
                } else {
                    enrollType = EnrollmentType.ONE_ACQUISITIONS;
                }

                if (((CaptureInfo) morphoInfo).isLatentDetect()) {
                    latentDetection = LatentDetection.LATENT_DETECT_ENABLE;
                } else {
                    latentDetection = LatentDetection.LATENT_DETECT_DISABLE;
                }

                coderChoice = processInfo.getCoder();

                detectModeChoice = DetectionMode.MORPHO_ENROLL_DETECT_MODE.getValue();

                if (processInfo.isForceFingerPlacementOnTop()) {
                    detectModeChoice |= DetectionMode.MORPHO_FORCE_FINGER_ON_TOP_DETECT_MODE.getValue();
                }

                if (processInfo.isWakeUpWithLedOff()) {
                    detectModeChoice |= MorphoWakeUpMode.MORPHO_WAKEUP_LED_OFF.getCode();
                }

                advancedSecurityLevelsRequired = 0;
                if (((CaptureInfo) morphoInfo).getCaptureType() != CaptureType.Verif) {

                    if (processInfo.isAdvancedSecLevCompReq()) {
                        advancedSecurityLevelsRequired = 1;
                    } else {
                        advancedSecurityLevelsRequired = 0;
                    }
                } else {
                    advancedSecurityLevelsRequired = 0xFF;
                    if (processInfo.isAdvancedSecLevCompReq()) {
                        advancedSecurityLevelsRequired = 1;
                    }
                }

                int callbackCmd = ProcessInfo.getInstance().getCallbackCmd();

                int nbFinger = ((CaptureInfo) morphoInfo).getFingerNumber();
                final String idUser = ((CaptureInfo) morphoInfo).getIDNumber();

                int ret = morphoDevice.setStrategyAcquisitionMode(ProcessInfo.getInstance().getStrategyAcquisitionMode());

                if (ret == 0) {
                    ret = morphoDevice.capture(timeout, acquisitionThreshold, advancedSecurityLevelsRequired,
                            nbFinger, templateType, templateFVPType, maxSizeTemplate, enrollType,
                            latentDetection, coderChoice, detectModeChoice, CompressionAlgorithm.MORPHO_NO_COMPRESS, 0, templateList, callbackCmd, observer);
                }

                ProcessInfo.getInstance().setCommandBioStart(false);

                getAndWriteFFDLogs();

                String message = "";
                try {
                    if (ret == ErrorCodes.MORPHO_OK) {
                        int NbTemplateFVP = templateList.getNbFVPTemplate();
                        int NbTemplate = templateList.getNbTemplate();
                        if (MorphoInfo.m_b_fvp) {
                            if (NbTemplateFVP > 0) {
                                TemplateFVP t = templateList.getFVPTemplate(0);
                                message += "Advanced Security Levels Compatibility: " + (t.getAdvancedSecurityLevelsCompatibility() == true ? "Yes" : "NO") + "\n";
                                for (int i = 0; i < NbTemplateFVP; i++) {
                                    t = templateList.getFVPTemplate(i);
                                    message += "Finger #" + (i + 1) + " - Quality Score: " + t.getTemplateQuality() + "\n";
                                }
                            }
                        } else {
                            if (NbTemplate > 0) {
                                for (int i = 0; i < NbTemplateFVP; i++) {
                                    Template t = templateList.getTemplate(i);
                                    message += "Finger #" + (i + 1) + " - Quality Score: " + t.getTemplateQuality() + "\n";
                                }
                            }
                        }

                        if (exportFVP) {
                            for (int i = 0; i < NbTemplateFVP; i++) {
                                TemplateFVP t = templateList.getFVPTemplate(i);
                                //  FileOutputStream fos = new FileOutputStream("sdcard/TemplateFVP_" + idUser + "_f" + (i + 1) + templateFVPType.getExtension());
                                byte[] data = t.getData();
                                String base64 = Base64.encodeToString(data, Base64.DEFAULT);
                                fpvBase64Output.add(base64);
                                //  fos.write(data);
                                //  fos.close();
                                //  message += "Finger #" + (i + 1) + " - FVP Template successfully exported in file [sdcard/TemplateFVP_" + idUser + "_f" + (i + 1) + templateFVPType.getExtension( + "]\n";
                            }
                        }

                        if (exportFP) {

                            for (int i = 0; i < NbTemplate; i++) {
                                Template t = templateList.getTemplate(i);
                                //  FileOutputStream fos = new FileOutputStream("sdcard/TemplateFP_" + idUser + "_f" + (i + 1) + templateType.getExtension());
                                byte[] data = t.getData();
                                String base64 = Base64.encodeToString(data, Base64.DEFAULT);
                                fpBase64Output.add(base64);
                                //  fos.write(data);
                                ///  fos.close();
                                //  message += "Finger #" + (i + 1) + " - FP Template successfully exported in file [sdcard/TemplateFP_" + idUser + "_f" + (i + 1) + templateType.getExtension() + "]\n";
                            }
                        }
                    }

                } catch (Exception e) {
                    Log.i("CAPTURE", e.getMessage());
                }

                final String alertMessage = message;
                final int internalError = morphoDevice.getInternalError();
                final int retvalue = ret;
                mHandler.post(new Runnable() {
                    @Override
                    public synchronized void run() {
                        stop();
                        alert(retvalue, internalError, "Capture", alertMessage);

                    }
                });

            }
        }));

        commandThread.start();
    }


    protected void alert(int codeError, int internalError, String title, String message) {
    /*    AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(title);
        String msg;
        if (codeError == 0) {
            msg = getString(R.string.OP_SUCCESS);
        } else {
            String errorInternationalization = convertToInternationalMessage(codeError, internalError);
            msg = getString(R.string.OP_FAILED) + "\n" + errorInternationalization;
        }
        msg += ((message.equalsIgnoreCase("")) ? "" : "\n" + message);
        alertDialog.setMessage(msg);
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialog.show();
        */

        if (codeError == 0) {
            FingerprintData fingerprintData = new FingerprintData();
            fingerprintData.fp = fpBase64Output.get(0);
           // fingerprintData.fpv = fpvBase64Output.get(0);
            fingerprintData.fpv = fpBase64Output.get(0);
            EventBus.getDefault().post(fingerprintData);
            finish();
        } else {
            AlertDialog alertDialog = new AlertDialog.Builder(this).create();
            alertDialog.setTitle(getString(R.string.capturefingerprint));
            alertDialog.setCancelable(false);
            String errorInternationalization = convertToInternationalMessage(codeError, internalError);
            //String msg = getString(R.string.OP_FAILED) + "\n" + errorInternationalization;

            String msg = getString(R.string.actMain_msg_device_connect_fail) + "\n" + errorInternationalization;

            msg += ((message.equalsIgnoreCase("")) ? "" : "\n" + message);
            alertDialog.setMessage(msg);
            alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    finish();
                }
            });
            alertDialog.show();
        }
    }


    @SuppressLint("DefaultLocale")
    public String convertToInternationalMessage(int iCodeError, int iInternalError) {
        switch (iCodeError) {
            case ErrorCodes.MORPHO_OK:
                return getString(R.string.MORPHO_OK);
            case ErrorCodes.MORPHOERR_INTERNAL:
                return getString(R.string.MORPHOERR_INTERNAL);
            case ErrorCodes.MORPHOERR_PROTOCOLE:
                return getString(R.string.MORPHOERR_DEVICE_DISCONNECT);
                //return getString(R.string.MORPHOERR_PROTOCOLE);
            case ErrorCodes.MORPHOERR_CONNECT:
                return getString(R.string.MORPHOERR_CONNECT);
            case ErrorCodes.MORPHOERR_CLOSE_COM:
                return getString(R.string.MORPHOERR_CLOSE_COM);
            case ErrorCodes.MORPHOERR_BADPARAMETER:
                return getString(R.string.MORPHOERR_BADPARAMETER);
            case ErrorCodes.MORPHOERR_MEMORY_PC:
                return getString(R.string.MORPHOERR_MEMORY_PC);
            case ErrorCodes.MORPHOERR_MEMORY_DEVICE:
                return getString(R.string.MORPHOERR_MEMORY_DEVICE);
            case ErrorCodes.MORPHOERR_NO_HIT:
                return getString(R.string.MORPHOERR_NO_HIT);
            case ErrorCodes.MORPHOERR_STATUS:
                return getString(R.string.MORPHOERR_STATUS);
            case ErrorCodes.MORPHOERR_DB_FULL:
                return getString(R.string.MORPHOERR_DB_FULL);
            case ErrorCodes.MORPHOERR_DB_EMPTY:
                return getString(R.string.MORPHOERR_DB_EMPTY);
            case ErrorCodes.MORPHOERR_ALREADY_ENROLLED:
                return getString(R.string.MORPHOERR_ALREADY_ENROLLED);
            case ErrorCodes.MORPHOERR_BASE_NOT_FOUND:
                return getString(R.string.MORPHOERR_BASE_NOT_FOUND);
            case ErrorCodes.MORPHOERR_BASE_ALREADY_EXISTS:
                return getString(R.string.MORPHOERR_BASE_ALREADY_EXISTS);
            case ErrorCodes.MORPHOERR_NO_ASSOCIATED_DB:
                return getString(R.string.MORPHOERR_NO_ASSOCIATED_DB);
            case ErrorCodes.MORPHOERR_NO_ASSOCIATED_DEVICE:
                return getString(R.string.MORPHOERR_NO_ASSOCIATED_DEVICE);
            case ErrorCodes.MORPHOERR_INVALID_TEMPLATE:
                return getString(R.string.MORPHOERR_INVALID_TEMPLATE);
            case ErrorCodes.MORPHOERR_NOT_IMPLEMENTED:
                return getString(R.string.MORPHOERR_NOT_IMPLEMENTED);
            case ErrorCodes.MORPHOERR_TIMEOUT:
                return getString(R.string.MORPHOERR_TIMEOUT);
            case ErrorCodes.MORPHOERR_NO_REGISTERED_TEMPLATE:
                return getString(R.string.MORPHOERR_NO_REGISTERED_TEMPLATE);
            case ErrorCodes.MORPHOERR_FIELD_NOT_FOUND:
                return getString(R.string.MORPHOERR_FIELD_NOT_FOUND);
            case ErrorCodes.MORPHOERR_CORRUPTED_CLASS:
                return getString(R.string.MORPHOERR_CORRUPTED_CLASS);
            case ErrorCodes.MORPHOERR_TO_MANY_TEMPLATE:
                return getString(R.string.MORPHOERR_TO_MANY_TEMPLATE);
            case ErrorCodes.MORPHOERR_TO_MANY_FIELD:
                return getString(R.string.MORPHOERR_TO_MANY_FIELD);
            case ErrorCodes.MORPHOERR_MIXED_TEMPLATE:
                return getString(R.string.MORPHOERR_MIXED_TEMPLATE);
            case ErrorCodes.MORPHOERR_CMDE_ABORTED:
                return getString(R.string.MORPHOERR_CMDE_ABORTED);
            case ErrorCodes.MORPHOERR_INVALID_PK_FORMAT:
                return getString(R.string.MORPHOERR_INVALID_PK_FORMAT);
            case ErrorCodes.MORPHOERR_SAME_FINGER:
                return getString(R.string.MORPHOERR_SAME_FINGER);
            case ErrorCodes.MORPHOERR_OUT_OF_FIELD:
                return getString(R.string.MORPHOERR_OUT_OF_FIELD);
            case ErrorCodes.MORPHOERR_INVALID_USER_ID:
                return getString(R.string.MORPHOERR_INVALID_USER_ID);
            case ErrorCodes.MORPHOERR_INVALID_USER_DATA:
                return getString(R.string.MORPHOERR_INVALID_USER_DATA);
            case ErrorCodes.MORPHOERR_FIELD_INVALID:
                return getString(R.string.MORPHOERR_FIELD_INVALID);
            case ErrorCodes.MORPHOERR_USER_NOT_FOUND:
                return getString(R.string.MORPHOERR_USER_NOT_FOUND);
            case ErrorCodes.MORPHOERR_COM_NOT_OPEN:
                return getString(R.string.MORPHOERR_COM_NOT_OPEN);
            case ErrorCodes.MORPHOERR_ELT_ALREADY_PRESENT:
                return getString(R.string.MORPHOERR_ELT_ALREADY_PRESENT);
            case ErrorCodes.MORPHOERR_NOCALLTO_DBQUERRYFIRST:
                return getString(R.string.MORPHOERR_NOCALLTO_DBQUERRYFIRST);
            case ErrorCodes.MORPHOERR_USER:
                return getString(R.string.MORPHOERR_USER);
            case ErrorCodes.MORPHOERR_BAD_COMPRESSION:
                return getString(R.string.MORPHOERR_BAD_COMPRESSION);
            case ErrorCodes.MORPHOERR_SECU:
                return getString(R.string.MORPHOERR_SECU);
            case ErrorCodes.MORPHOERR_CERTIF_UNKNOW:
                return getString(R.string.MORPHOERR_CERTIF_UNKNOW);
            case ErrorCodes.MORPHOERR_INVALID_CLASS:
                return getString(R.string.MORPHOERR_INVALID_CLASS);
            case ErrorCodes.MORPHOERR_USB_DEVICE_NAME_UNKNOWN:
                return getString(R.string.MORPHOERR_USB_DEVICE_NAME_UNKNOWN);
            case ErrorCodes.MORPHOERR_CERTIF_INVALID:
                return getString(R.string.MORPHOERR_CERTIF_INVALID);
            case ErrorCodes.MORPHOERR_SIGNER_ID:
                return getString(R.string.MORPHOERR_SIGNER_ID);
            case ErrorCodes.MORPHOERR_SIGNER_ID_INVALID:
                return getString(R.string.MORPHOERR_SIGNER_ID_INVALID);
            case ErrorCodes.MORPHOERR_FFD:
                return getString(R.string.MORPHOERR_FFD);
            case ErrorCodes.MORPHOERR_MOIST_FINGER:
                return getString(R.string.MORPHOERR_MOIST_FINGER);
            case ErrorCodes.MORPHOERR_NO_SERVER:
                return getString(R.string.MORPHOERR_NO_SERVER);
            case ErrorCodes.MORPHOERR_OTP_NOT_INITIALIZED:
                return getString(R.string.MORPHOERR_OTP_NOT_INITIALIZED);
            case ErrorCodes.MORPHOERR_OTP_PIN_NEEDED:
                return getString(R.string.MORPHOERR_OTP_PIN_NEEDED);
            case ErrorCodes.MORPHOERR_OTP_REENROLL_NOT_ALLOWED:
                return getString(R.string.MORPHOERR_OTP_REENROLL_NOT_ALLOWED);
            case ErrorCodes.MORPHOERR_OTP_ENROLL_FAILED:
                return getString(R.string.MORPHOERR_OTP_ENROLL_FAILED);
            case ErrorCodes.MORPHOERR_OTP_IDENT_FAILED:
                return getString(R.string.MORPHOERR_OTP_IDENT_FAILED);
            case ErrorCodes.MORPHOERR_NO_MORE_OTP:
                return getString(R.string.MORPHOERR_NO_MORE_OTP);
            case ErrorCodes.MORPHOERR_OTP_NO_HIT:
                return getString(R.string.MORPHOERR_OTP_NO_HIT);
            case ErrorCodes.MORPHOERR_OTP_ENROLL_NEEDED:
                return getString(R.string.MORPHOERR_OTP_ENROLL_NEEDED);
            case ErrorCodes.MORPHOERR_DEVICE_LOCKED:
                return getString(R.string.MORPHOERR_DEVICE_LOCKED);
            case ErrorCodes.MORPHOERR_DEVICE_NOT_LOCK:
                return getString(R.string.MORPHOERR_DEVICE_NOT_LOCK);
            case ErrorCodes.MORPHOERR_OTP_LOCK_GEN_OTP:
                return getString(R.string.MORPHOERR_OTP_LOCK_GEN_OTP);
            case ErrorCodes.MORPHOERR_OTP_LOCK_SET_PARAM:
                return getString(R.string.MORPHOERR_OTP_LOCK_SET_PARAM);
            case ErrorCodes.MORPHOERR_OTP_LOCK_ENROLL:
                return getString(R.string.MORPHOERR_OTP_LOCK_ENROLL);
            case ErrorCodes.MORPHOERR_FVP_MINUTIAE_SECURITY_MISMATCH:
                return getString(R.string.MORPHOERR_FVP_MINUTIAE_SECURITY_MISMATCH);
            case ErrorCodes.MORPHOERR_FVP_FINGER_MISPLACED_OR_WITHDRAWN:
                return getString(R.string.MORPHOERR_FVP_FINGER_MISPLACED_OR_WITHDRAWN);
            case ErrorCodes.MORPHOERR_LICENSE_MISSING:
                return getString(R.string.MORPHOERR_LICENSE_MISSING);
            case ErrorCodes.MORPHOERR_CANT_GRAN_PERMISSION_USB:
                return getString(R.string.MORPHOERR_CANT_GRAN_PERMISSION_USB);
            default:
                return String.format("Unknown error %d, Internal Error = %d", iCodeError, iInternalError);
        }
    }


    public void getAndWriteFFDLogs() {
        String ffdLogs = morphoDevice.getFFDLogs();

        if (ffdLogs != null) {
            String serialNbr = ProcessInfo.getInstance().getMSOSerialNumber();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
            String currentDateandTime = sdf.format(new Date());
            String saveFile = "sdcard/" + serialNbr + "_" + currentDateandTime + "_Audit.log";

            try {
                FileWriter fstream = new FileWriter(saveFile, true);
                BufferedWriter out = new BufferedWriter(fstream);
                out.write(ffdLogs);
                out.close();
            } catch (IOException e) {
                Log.e("getAndWriteFFDLogs", e.getMessage());
            }
        }
    }


    private void unlockScreenOrientation() {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
    }

    private void lockScreenOrientation() {
        final int orientation = getResources().getConfiguration().orientation;
        final int rotation = getWindowManager().getDefaultDisplay().getOrientation();

        if (rotation == Surface.ROTATION_0 || rotation == Surface.ROTATION_90) {
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            } else if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            }
        } else if (rotation == Surface.ROTATION_180 || rotation == Surface.ROTATION_270) {
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT);
            } else if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE);
            }
        }
    }

    private void stop() {
        unlockScreenOrientation();
        btn_startstop.setText("Start");
        enableOptionBoutton(true);
        ProcessInfo.getInstance().setStarted(false);
        if (ProcessInfo.getInstance().isCommandBioStart()) {
            ProcessInfo.getInstance().getMorphoDevice().cancelLiveAcquisition();
        }
    }


    private void enableDisableBoutton(boolean enabled) {

        btn_startstop.setEnabled(enabled);
        btn_rebootsoft.setEnabled(enabled);
        btn_closeandquit.setEnabled(enabled);

    }

    private void enableOptionBoutton(boolean enabled) {

        btn_rebootsoft.setEnabled(enabled);
        btn_closeandquit.setEnabled(enabled);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        onCloseandquit();
    }

    @Override
    protected void onResume() {
        super.onResume();


    }

    @Override
    protected void onPause() {
        if (morphoDevice != null && ProcessInfo.getInstance().isStarted()) {
            morphoDevice.cancelLiveAcquisition();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        super.onPause();
    }


    class RebootObserver implements Observer {
        @Override
        public synchronized void update(Observable observable, Object data) {
            Boolean isOpenOK = (Boolean) data;

            KeyFile.isRebootSoft = false;

            if (isOpenOK == true) {
                mHandler.post(new Runnable() {
                    @Override
                    public synchronized void run() {
                        enableDisableBoutton(true);
                    }
                });
            } else {
                mHandler.post(new Runnable() {
                    @Override
                    public synchronized void run() {
                        Button btn = (Button) findViewById(R.id.btn_closeandquit);
                        btn.setEnabled(true);
                        alert(ErrorCodes.MORPHOERR_RESUME_CONNEXION, 0, "Resume Connection", "");
                    }
                });
            }
        }
    }


    class FingerPrintObserver implements Observer {
        @Override
        public synchronized void update(Observable o, Object arg) {
            try {
                // convert the object to a callback back message.
                CallbackMessage message = (CallbackMessage) arg;

                int type = message.getMessageType();

                switch (type) {

                    case 1:
                        // message is a command.
                        Integer command = (Integer) message.getMessage();

                        // Analyze the command.
                        switch (command) {
                            case 0:
                                strMessage = "move-no-finger";
                                break;
                            case 1:
                                strMessage = "move-finger-up";
                                break;
                            case 2:
                                strMessage = "move-finger-down";
                                break;
                            case 3:
                                strMessage = "move-finger-left";
                                break;
                            case 4:
                                strMessage = "move-finger-right";
                                break;
                            case 5:
                                strMessage = "press-harder";
                                break;
                            case 6:
                                strMessage = "move-latent";
                                break;
                            case 7:
                                strMessage = "remove-finger";
                                break;
                            case 8:
                                strMessage = "finger-ok";
                                break;
                        }

                        mHandler.post(new Runnable() {
                            @Override
                            public synchronized void run() {
                                updateSensorMessage(strMessage);
                            }
                        });

                        break;
                    case 2:
                        // message is a low resolution image, display it.
                        byte[] image = (byte[]) message.getMessage();

                        MorphoImage morphoImage = MorphoImage.getMorphoImageFromLive(image);
                        int imageRowNumber = morphoImage.getMorphoImageHeader().getNbRow();
                        int imageColumnNumber = morphoImage.getMorphoImageHeader().getNbColumn();
                        final Bitmap imageBmp = Bitmap.createBitmap(imageColumnNumber, imageRowNumber, Bitmap.Config.ALPHA_8);

                        imageBmp.copyPixelsFromBuffer(ByteBuffer.wrap(morphoImage.getImage(), 0, morphoImage.getImage().length));
                        mHandler.post(new Runnable() {
                            @Override
                            public synchronized void run() {
                                updateImage(imageBmp);
                            }
                        });
                        break;
                    case 3:
                        // message is the coded image quality.
                        final Integer quality = (Integer) message.getMessage();
                        mHandler.post(new Runnable() {
                            @Override
                            public synchronized void run() {
                                updateSensorProgressBar(quality);
                            }
                        });
                        break;
                    //case 4:
                    //byte[] enrollcmd = (byte[]) message.getMessage();
                }
            } catch (Exception e) {
                alert(e.getMessage());
            }
        }

    }


}
